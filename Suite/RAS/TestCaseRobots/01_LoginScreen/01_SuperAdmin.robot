*** Settings ***
Documentation     Test Cases to execute all steps

Library         Screenshot
Library           ../../TestCaseMethods/LoginScreenTCs.py

*** Keywords ***
Failed Case Handle
    take screenshot
    close_application

Multiple Setup Methods
    launch application

Multiple Teardown Methods
    close application

*** Settings ***
Suite Setup     run keyword     Multiple Setup Methods
Suite Teardown  run keyword     Multiple Teardown Methods
Test Teardown   run keyword if test failed      Failed Case Handle

*** Test Cases ***
Login Screen Appears
    login screen appears

Login to RAS with valid email and valid password
    login with valid email valid password

logout from RAS
    logout from ras

Email & Password fields have client side verification
    client side validation for email and password fields

Error message appears for invalid credentials
    error message for login with valid email invalid password

