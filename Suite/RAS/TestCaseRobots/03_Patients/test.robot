*** Settings ***
Documentation     Test Cases to execute all steps
...
Library         Screenshot
Library           ../../Test_Methods/TestFunc.py
...

*** Keywords ***
Failed Case Handle
    take screenshot
    close_application

*** Settings ***
Default Tags  End to End Test: Structure Modeling
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
Provided precondition
    Launch SC Application
    close_application

