*** Settings ***
Documentation     Test Cases to execute all steps in Product Hierarchy
...
Library         Screenshot
Library           ../../Testcase_Methods/TestFunc.py
...

*** Keywords ***
Failed Case Handle
    take screenshot
#    close_application

*** Settings ***
Default Tags  End to End Test: Structure Modeling
Test Teardown  run keyword if test failed  Failed Case Handle
...
*** Variables ***
${ONE}      1
${TWO}      2
${THREE}    3
${FOUR}     4
${FIVE}     5
${SIX}      6
${SEVEN}    7
${EIGHT}    8
${NINE}     9
${ZERO}     0
...
*** Test Cases ***
#Test Log
#    testtest
Launch Calculator Application
    launch_application

Verify UI
    test_ui

#Test Addition Through Mouse
#    add     ${ONE}  ${TWO}
#
#Test Addition Trhough Keyboard
#    test_through_keyboard
#
#Closing Application
#    close_application
