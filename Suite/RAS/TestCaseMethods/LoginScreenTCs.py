import os
import sys

lib_path2 = os.path.abspath(os.path.join('..', '..', 'Libraries', 'library_mmt'))
sys.path.insert(0, lib_path2)
import config as conf
from app_util import launch_app, close_app

lib_path = os.path.abspath(os.path.join('..', '..', 'Libraries', 'library_common'))
sys.path.insert(0, lib_path)
from mouse_util import mouse_move_to, mouse_left_click_on, mouse_double_click_on
from screen_object_util import appears
from Logger import Logger as log_ext
from pyautogui import press, typewrite
from time import sleep
from logger_extended import add_log, add_missed_image

lib_path1 = os.path.abspath(os.path.join('..', '..', 'Libraries', 'RASGUI'))
sys.path.insert(0, lib_path1)
from LoginScreen import LoginScreen as login_class


class LoginScreenTCs(object):

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    log = log_ext().get_logger('RAS_Automation')
    # The logs will be found in the TCP_TEST.robot.log file

    login = login_class()
    process_check = ""

    def __init__(self):
        self._expression = ''

    def launch_application(self):
        self.log.info('Start:\t Checking Module- launch_application')
        self.process_check = launch_app()
        self.log.info('Application launched properly')

    def close_application(self):
        self.log.info('Start:\t Checking Module- close_application')
        close_app(process_check=self.process_check)
        self.log.info('Application closed properly')

    def add(self, var1, var2):
        mouse_left_click_on(var1)
        mouse_left_click_on("add")
        mouse_left_click_on(var2)
        mouse_left_click_on("equal")
        if appears("ans3", 5):
            self.log.info('Addition was checked')
        else:
            raise AssertionError("Addition was not correct")

    def login_screen_appears(self):
        """
        @desc - method to check if login screen appears after application is launched successfully.
        :return:
        """
        self.login.login_screen_appears()

    def login_with_valid_email_valid_password(self):
        """
        @desc - Method to check if user can login with valid email & valid password
        :return:
        """
        self.log.info('METHOD: login_with_valid_email_valid_password')
        self.login.valid_email_valid_password(to_log_out="1")

    def logout_from_ras(self):
        """
        @desc - Log out from RAS
        :return:
        """
        self.login.logout_from_ras(to_log_in="1")

    def client_side_validation_for_email_and_password_fields(self):
        """
        @desc - method to check client side validation
        :return:
        """
        self.login.email_and_password_fields_client_side_validation()

    def error_message_for_login_with_valid_email_invalid_password(self):
        """
        @desc - method to check if invalid login credentials message appears
        :return:
        """
        self.login.login_with_valid_email_invalid_password()
