import os
import sys
from time import sleep

lib_path = os.path.abspath(os.path.join('..', '..', 'Libraries', 'library_common'))
sys.path.insert(0, lib_path)
from mouse_util import mouse_left_click_on, mouse_double_click_on, mouse_move_to
import keyboard_util
from screen_object_util import appears
from initialize import value as val
from Logger import Logger as logger_ext
import pyautogui

lib_path1 = os.path.abspath(os.path.join('..', '..', 'Libraries', 'library_mmt'))
sys.path.insert(0, lib_path1)
import config as conf


class LoginScreen:
    log = logger_ext().get_logger('RAS_Automation')

    def __int__(self):
        print('LoginScreen class initialized')

    def login_screen_appears(self):
        """
        @desc - To check if login screen appears
        :return:
        """
        self.log.info('METHOD: login_screen_appears')
        if appears(conf.login_screen_appears_or_not_image):
            print("Image located: " + conf.login_screen_appears_or_not_image)
        else:
            raise AssertionError('Image not located: ' + conf.login_screen_appears_or_not_image)

    def valid_email_valid_password(self, to_log_out=None):
        """
        @desc - Logs into the RAS application with valid email and valid password
        :return:
        """
        self.log.info('METHOD: valid_email_valid_password')
        pyautogui.press('alt')
        if appears(conf.cursor_on_password_field_image, time_wait=10):
            print("Image located: " + conf.cursor_on_password_field_image)
            mouse_left_click_on(screen_obj=conf.cursor_on_password_field_image)
            pyautogui.hotkey('shift', 'tab', interval=0.2)
            pyautogui.hotkey('ctrl', 'a', interval=0.2)
            pyautogui.typewrite(conf.super_admin)
            pyautogui.press('tab')
            pyautogui.typewrite(conf.password)
            pyautogui.press('enter')
        else:
            print('Email field empty')
            pyautogui.typewrite(conf.super_admin)
            pyautogui.press('tab')
            pyautogui.typewrite(conf.password)
            pyautogui.press('enter')
        if appears(conf.login_verification_image, time_wait=25):
            print("Successfully logged in")
            if to_log_out is None:
                print('Now maximizing window')
                pyautogui.hotkey('win', 'up')
        else:
            raise AssertionError('User could not login')

    def logout_from_ras(self, to_log_in=None):
        """
        @desc - method to logout from RAS
        :return:
        """
        self.log.info('METHOD: logout_from_ras')
        sleep(40)
        if appears(conf.user_icon_image):
            print("Image located: " + conf.user_icon_image)
            mouse_left_click_on(screen_obj=conf.user_icon_image)
            if appears(conf.logout_icon_image):
                print('Image located: ' + conf.logout_icon_image)
                mouse_left_click_on(screen_obj=conf.logout_icon_image)
                if appears(conf.login_screen_appears_or_not_image):
                    print('Log out was successful')
                    if to_log_in is None:
                        pyautogui.typewrite(conf.password)
                        pyautogui.press('enter')
                        if appears(conf.login_verification_image):
                            print('login successful')
                            sleep(40)
                            pyautogui.hotkey('win', 'down')
                else:
                    raise AssertionError('Log out was unsuccessful')

    def email_and_password_fields_client_side_validation(self):
        """
        @desc - method to check if the email & password fields has client side verification
        :return:
        """
        self.log.info('METHOD: email_field_client_side_verification')
        pyautogui.press('alt')
        if appears(conf.cursor_on_password_field_image, time_wait=10):
            print("Image located: " + conf.cursor_on_password_field_image)
            mouse_left_click_on(screen_obj=conf.cursor_on_password_field_image)
            mouse_move_to(screen_obj=conf.login_screen_appears_or_not_image)
            pyautogui.hotkey('shift', 'tab', interval=0.2)
            pyautogui.hotkey('ctrl', 'a', interval=0.2)
            pyautogui.typewrite('testing')
            pyautogui.press('tab')
            pyautogui.typewrite('sd')
            pyautogui.press('tab')
            if appears(conf.email_field_client_side_validation_image) and \
                    appears(conf.password_field_client_side_validation_image):
                print('Client side validation for email & password fields is working properly')
            else:
                raise AssertionError('Client side validation for email & password fields is NOT working')
        else:
            print('Image not located: ' + conf.cursor_on_password_field_image)

    def login_with_valid_email_invalid_password(self):
        """
        @desc - method to login with invalid credentials
        :return:
        """
        self.log.info('METHOD: login_with_valid_email_invalid_password')
        pyautogui.press('tab')
        pyautogui.hotkey('ctrl', 'a')
        pyautogui.typewrite(conf.super_admin)
        pyautogui.press('tab')
        pyautogui.typewrite('yellow123')
        pyautogui.press('enter')
        if appears(conf.error_message_for_invalid_credentials_image, time_wait=4):
            print('Error message appears')
        else:
            raise AssertionError('Error message did not appear')
