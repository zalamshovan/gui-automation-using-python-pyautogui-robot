import os
import sys

lib_path = os.path.abspath(os.path.join('..', '..', 'Libraries', 'library_common'))
sys.path.insert(0, lib_path)
import mouse_util
import keyboard_util
import screen_object_util
from Logger import Logger as logger_ext
import pyautogui


class SettingsScreen:
    log = logger_ext().get_logger('RAS_Automation')

    def __int__(self):
        print("SettingsScreen class initialized")
