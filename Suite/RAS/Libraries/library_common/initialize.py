import ctypes
import sys
import tkinter.messagebox

value = {
    "Image directory": None
}

# While running automation, application will check the screen resolution of current m/c
# Set current configuration accordingly so that it can compare test images from particular directory.
user32 = ctypes.windll.user32

screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

if screensize == (1364, 768):
    value["Image directory"] = "../../images/windows/1364x768/"

elif screensize == (1280, 800):
    value["Image directory"] = "../../images/windows/1280x800/"

elif screensize == (1600, 1200):
    value["Image directory"] = "../../images/windows/1600x1200/"

elif screensize == (1600, 900):
    value["Image directory"] = "../../images/windows/1600x900/"

elif screensize == (1920, 1080):
    value["Image directory"] = "../../images/windows/1920x1080/"

else:
    strMsg = "RAS Automation does not have support for screen size " + str(screensize)
    tkinter.messagebox.showinfo("RAS Automation Warning: ", strMsg)
    sys.exit(1)
