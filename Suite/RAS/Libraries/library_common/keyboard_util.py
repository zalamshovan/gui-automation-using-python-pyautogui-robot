"""
This script contains the commonly used keyboard functionalities
"""
from pyautogui import typewrite, hotkey, press
from screen_object_util import exists


def write_text(text, interval=.2):
    """
    @desc - For writing line of text
    :param - text: string of characters
    :param - interval: Interval (in seconds) between key press
    """
    typewrite(text, interval)
    press('enter')


def save(presses=1):
    """
    @desc - For saving something by pressing ctrl+s
    :param - presses: Number of presses
    """
    i = 0
    while i < presses:
        hotkey('ctrl', 's')
        i = i+1


def quit_by_keyboard(presses=1):
    """
    @desc - For quiting something by pressing alt+f4
    :param - presses: Number of presses
    """
    i = 0
    while i < presses:
        hotkey('alt', 'f4')
        i = i+1


def down_key_press_until_appears(screen_obj):
    """
    @desc - Press down key until a definite screen object appears on the screen
    :param screen_obj: name of the image (without extension) to locate
    :return:
    """
    while exists(screen_obj) is False:
        press('down')


def up_key_press_until_appears(screen_obj):
    """
    @desc - Press up key until a definite screen object appears on the screen
    :param screen_obj: name of the image (without extension) to locate
    :return:
    """
    while exists(screen_obj) is False:
        press('up')


def clear_text_field():
    """
    @desc - Clears the text field
    :return:
    """
    hotkey('ctrl', 'a')
    press('backspace')
