"""
This script contains the commonly used mouse functionalities
"""

from pyautogui import locateCenterOnScreen, locateOnScreen, locateAllOnScreen, center, moveTo, click, dragTo, scroll, \
    dragRel, MINIMUM_DURATION, doubleClick
from screen_object_util import exists, appears, midpoint, location
from time import sleep
from initialize import value as val
from Logger import Logger as logger_extended
from datetime import datetime
from logger_extended import add_log, add_missed_image


timestamp = "{:%Y_%m_%d}".format(datetime.now())

logger = logger_extended().get_logger('Mismatched Images ' + timestamp)


def mouse_move_to(screen_obj=None, region=None):
    """
    @desc - For moving mouse cursor to a desired position
    :param- screen_obj - name of the image (without extension) to locate
    :param- region - name of the image (without extension) of a region to locate
    :rtype : object
    """
    sleep(1)  # To allow the screen displaying properly
    try:
        str_path_screen_obj = val["Image directory"] + screen_obj + ".png"
        if region is None:
            loc_screen_obj = locateCenterOnScreen(str_path_screen_obj, grayscale=True)
        else:
            loc_reg_obj = location(region)
            loc_screen_obj = locateCenterOnScreen(str_path_screen_obj, region=loc_reg_obj, grayscale=True)

    except TypeError:
        raise AssertionError("Item %s could not be located. Type mismatched" % screen_obj)

    except IOError:
        err_message2 = screen_obj + ".png"
        AssertionError(err_message2)
        logger.error("NOT FOUND:\t " + err_message2)
        raise AssertionError("No such file named %s.png presents in the test_image directory" % screen_obj)

    if loc_screen_obj is None:
        add_missed_image(screen_obj)
        raise AssertionError("%s is not located on the screen" % screen_obj)
    else:
        moveTo(loc_screen_obj)


def mouse_left_click_on(screen_obj=None, region=None, constraint=None):
    """
    @desc - Left click mouse on an object recognized from image in parameter
    :param- screen_obj - name of the image (without extension) to locate
    :param- region - name of the image (without extension) of a region to locate
    :param- constraint - to enforce time constraint
    """
    if constraint is not None:
        if region is not None:
            if appears(region, 10, constraint=1):
                mouse_move_to(screen_obj, region)
                click(button="left")
        else:
            if appears(screen_obj, 10, constraint=1):
                mouse_move_to(screen_obj)
                click(button="left")
    else:
        if region is not None:
            if appears(region, 10):
                mouse_move_to(screen_obj, region)
                click(button="left")
        else:
            if appears(screen_obj, 10):
                mouse_move_to(screen_obj)
                click(button="left")


def mouse_double_click_on(screen_obj=None, region=None, constraint=None):
    """
    @desc - Mouse double click on an object recognized from image in parameter
    :param- screen_obj - name of the image (without extension) to locate
    :param- region - name of the image (without extension) of a region to locate
    :param- constraint - to enforce time constraint
    """
    if constraint is not None:
        if region is not None:
            if appears(region, 10, constraint=1):
                mouse_move_to(screen_obj, region)
                doubleClick(button="left")
        else:
            if appears(screen_obj, 10, constraint=1):
                mouse_move_to(screen_obj)
                doubleClick(button="left")
    else:
        if region is not None:
            if appears(region, 10):
                mouse_move_to(screen_obj, region)
                doubleClick(button="left")
        else:
            if appears(screen_obj, 10):
                mouse_move_to(screen_obj)
                doubleClick(button="left")


def mouse_right_click_on(screen_obj=None, region=None, constraint=None):
    """
    @desc - Mouse right click on an object recognized from image in parameter
    :param- screen_obj - name of the image (without extension) to locate
    :param- region - name of the image (without extension) of a region to locate
    :param- constraint - to enforce time constraint
    """
    if constraint is not None:
        if region is not None:
            if appears(region, 10, constraint=1):
                mouse_move_to(screen_obj, region)
                click(button="right")
        else:
            if appears(screen_obj, 10, constraint=1):
                mouse_move_to(screen_obj)
                click(button="right")
    else:
        if region is not None:
            if appears(region, 10):
                mouse_move_to(screen_obj, region)
                click(button="right")
        else:
            if appears(screen_obj, 10):
                mouse_move_to(screen_obj)
                click(button="right")


def mouse_click_appears(screen_obj=None):
    """
    @desc - Keep clicking an object as long as it appears on the screen
    :param- screen_obj - name of the image (without extension) to locate
    """
    while appears(screen_obj, 2):
        mouse_left_click_on(screen_obj)


def drag_drop(screen_obj_drag, screen_obj_drop):
    """
    @desc - To drag and drop
    :param- screen_obj - name of the image (without extension) to locate
    """
    mouse_move_to(screen_obj_drag)
    dragTo(midpoint(screen_obj_drop), 2, button='left')


def v_scroll_until_appears(screen_obj, key=None):
    """
    @desc - Vertically scroll mouse wheel until a definite screen object appears on the screen.
    :param- screen_obj - name of the image (without extension) to locate
    :param- key - Value of key can be set as "Up" or "Down" to control the rotation of the wheel.
    """
    if key == "Up":
        while exists(screen_obj) is False:
            scroll(1)
    if key == "Down":
        while exists(screen_obj) is False:
            scroll(-1)


def drag_until_appears(screen_obj, key=None):
    """
    @desc - Vertically and horizontally drag mouse until a definite screen object appears on the screen.
    :param- screen_obj - name of the image (without extension) to locate
    :param- key - Value of key can be set as "Up", "Down" , "Left", "Right" to control the direction of the mouse.
    """
    if key == "Up":
        while exists(screen_obj) is False:
            dragRel(None, 10, MINIMUM_DURATION, button='left')

    if key == "Down":
        while exists(screen_obj) is False:
            dragRel(None, -10, MINIMUM_DURATION, button='left')

    if key == "Left":
        while exists(screen_obj) is False:
            dragRel(10, None, MINIMUM_DURATION, button='left')

    if key == "Right":
        while exists(screen_obj) is False:
            dragRel(-10, None, MINIMUM_DURATION, button='left')


def scroll_until_appears(screen_obj):
    """
    @desc - Zooming by scrolling until screen object appears
    :param- screen_obj - name of the image (without extension)
    """
    while exists(screen_obj) is False:
        scroll(1)


def go_down_until_appears(screen_obj, slide_bar_down):
    """
    @desc - Scroll down until an object is found
    :param- screen_obj - name of the image (without extension)
    """
    while exists(screen_obj) is False:
        mouse_left_click_on(slide_bar_down)