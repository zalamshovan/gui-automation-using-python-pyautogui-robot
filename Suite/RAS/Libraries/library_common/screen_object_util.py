from pyautogui import locateOnScreen, center
from time import sleep
from initialize import value as val
from Logger import Logger as LoggerExtended
from datetime import datetime
from logger_extended import add_log,add_missed_image


timestamp = "{:%Y_%m_%d}".format(datetime.now())

logger = LoggerExtended().get_logger('Mismatched Images ' + timestamp)


def location(screen_obj):
    """
    @desc - returns the location of particular screen object
    :param- screen_obj - name of the image (without extension) to locate
    :rtype : object
    """
    sleep(.5)  # To allow the screen displaying properly
    try:
        str_path_logger_extended = val["Image directory"] + screen_obj + ".png"
        loc_logger_extended = locateOnScreen(str_path_logger_extended, grayscale=True)

    except TypeError:
        raise AssertionError("Item %s could not be located. Type mismatched" % screen_obj)

    except IOError:
        err_message2 = screen_obj + ".png"
        AssertionError(err_message2)
        logger.error("NOT FOUND:\t " + err_message2)
        raise AssertionError("No such file named %s.png presents in the image directory" % screen_obj)

    if loc_logger_extended is None:
        # print("%s is not located on the screen" % screen_obj)
        return None
    else:
        return loc_logger_extended  # return the location of the object on the current screen


def midpoint(screen_obj):
    """
    @desc - returns midpoint of the screen object passed via the parameter
    :param- screen_obj - name of the image (without extension) to locate
    :rtype : center point of the image
    """
    return center(location(screen_obj))


def appears(screen_obj, time_wait=1, constraint=None):
    """
    @desc - Check the screen_obj appears or not
    :param- screen_obj - name of the image (without extension) to check
    :rtype : true/false
    """
    for x in range(0, time_wait):
        if location(screen_obj) is not None:
            sleep(2)
            return True
    # if screen_obj could not located in time, return False

    if constraint is not None:
        err_message2 = screen_obj + ".png"
        logger.error("NOT MATCHED:\t " + err_message2)
        raise AssertionError("%s.png was not matched" % screen_obj)
    else:
        add_missed_image(screen_obj)
        return False


def exists(screen_obj):
    """
    @desc - Check if a screen_obj is on screen or not
    :param- screen_obj - name of the image (without extension) to check
    :rtype : true/false
    """
    if location(screen_obj) is None:
        return False
    else:
        return True


def wait_for_vanish(screen_obj):
    """
    @desc - Wait till the screen_obj is not vanished
    :param- screen_obj - name of the image (without extension) to check
    """
    if location(screen_obj) is None:
        return False
    else:
        while exists(screen_obj):
            sleep(1)
        return True


def wait_till_appear(screen_obj):
    """
    @desc - Wait till the screen_obj is appeared
    :param- screen_obj - name of the image (without extension) to check
    """
    while 1:
        if appears(screen_obj):
            break
