import os
import sys
import subprocess
from pyautogui import moveTo, click, hotkey
from time import sleep

lib_path = os.path.abspath(os.path.join('..', '..', 'libraries', 'library_common'))
sys.path.insert(0, lib_path)
from screen_object_util import exists, appears, wait_for_vanish
from mouse_util import mouse_left_click_on, mouse_double_click_on, mouse_move_to

lib_path1 = os.path.abspath(os.path.join('..', '..', 'libraries', 'library_mmt'))
sys.path.insert(0, lib_path1)
import config


def launch_app():
    process = subprocess.Popen(config.RAS_PATH)
    # sleep(2)
    # moveTo(0, 800)
    if appears(config.loading_image, 5):
        print("Application has launched properly")
        # mouse_move_to("equal")
        sleep(5)
        return process
    else:
        raise AssertionError("Calculator has not launched properly")


def close_app(process_check):
    # To Close the application
    sleep(2)
    hotkey('alt', 'f4')
    sleep(2)
    if appears(config.yes_to_exit_application_image):
        print('Image located: ' + config.yes_to_exit_application_image)
        mouse_left_click_on(screen_obj=config.yes_to_exit_application_image)
        if process_check is None:
            raise AssertionError('Application is still running')
        else:
            print('Application closed properly')



