# Set the absolute path of your RAS executable


RAS_PATH = "C:/Program Files (x86)/Rendr Labs/Rendr Acquisition (Staging x86)/Rendr Acquisition.exe"

super_admin = "rendrsuper@yahoo.com"
support = "rendrsupp.enosis@yahoo.com"
production = "rendrprouser@yahoo.com"
facility_admin = "rendr_facilityadm@yahoo.com"
review_doctor = "rendrreviewdoctor@yahoo.com"
lead_tech = "rendrleadtech@yahoo.com"
field_tech = "rendrfieldtech@yahoo.com"
office_personnel = "rendrofficepersonnel@yahoo.com"

password = "Enosis123"

email_image_name = "email"
password_image_name = "password"
home_screen_image_name = "homeScreen"
cursor_on_password_field_image = "CursorOnPasswordField"
login_screen_appears_or_not_image = "login_screen_appears_or_not"
login_verification_image = "login_verification"
loading_image = "loading"
to_maximize_window_image = "to_maximize_window"
email_field_client_side_validation_image = "email_field_client_side_verification"
password_field_client_side_validation_image = "password_field_client_side_verification"
user_icon_image = "user_icon"
logout_icon_image = "logout2"
to_minimize_window_image = "to_minimize_window"
error_message_for_invalid_credentials_image = "error_message_for_invalid_credentials"
yes_to_exit_application_image = "yes_to_exit_application"
