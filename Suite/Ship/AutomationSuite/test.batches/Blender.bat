:This batch file read general_xml file which is stored at test.batch directory.
:Then it copy the xml path from the text file and create a combined xml using the other xmls generator when end to end automation is finished

ECHO OFF

SET targetFolderLocation=test.batches\

SET mypath1=%cd%
SET mypath2=%cd%

FOR %%a IN ("%mypath1%") DO SET "p1_dir=%%~dpa"
mkdir %p1_dir%%targetFolderLocation%

SETLOCAL EnableDelayedExpansion
FOR /F "usebackq delims=" %%a IN (`"findstr /n ^^ %p1_dir%%targetFolderLocation%generalXML.txt"`) DO (
    SET "var=%%a"
    SET "var=!var:*:=!"	
	FOR %%a IN (!var!) DO SET generalXMLs=!var!
)
SET location=test.cases\TC00_Run_All\xml\

FOR %%a IN ("%mypath2%") DO SET "p_dir=%%~dpa"
FOR %%a IN (%p_dir:~0,-1%) DO SET "p2_dir=%%~dpa"

cd %p_dir%%location%

FOR /f "delims=" %%a IN ('wmic OS Get localdatetime  ^| find "."') DO SET dt=%%a
SET YYYY=%dt:~0,4%
SET MM=%dt:~4,2%
SET DD=%dt:~6,2%
SET HH=%dt:~8,2%
SET Min=%dt:~10,2%
SET Sec=%dt:~12,2%

SET stamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%

rebot --outputdir C:/AutomationLogs/Combined/%stamp% --timestampoutputs -l Complete_Log.html -r Complete_Report.html%generalXMLs%