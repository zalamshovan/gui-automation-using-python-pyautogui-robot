@ECHO OFF

:Find and set config.py file directory
Set targetLocation=src.python.test\
Set mypath=%cd%
for %%a in ("%mypath%") do set "p_dir=%%~dpa"

:Find the setup file path for SSI new release from config file
SETLOCAL EnableDelayedExpansion
FOR /F "usebackq delims=" %%a in (`"findstr /n ^^ %p_dir%%targetLocation%config.py"`) do (
    set "var=%%a"
    set "var=!var:*:=!"	
	FOR %%a IN (!var!) DO IF /i "%%a"=="Path_Installer" Set targetPath=!var:~16!
)
ECHO %targetPath%
start /d %targetPath% for %%i in (*Setup.exe) do %%i
TIMEOUT 2
TASKKILL /F /IM cmd.exe
