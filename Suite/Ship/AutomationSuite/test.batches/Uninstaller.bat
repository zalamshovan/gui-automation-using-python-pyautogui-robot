echo off

REM C:\Users\VM\Desktop\SC_Releases\Latest_installer

set "directory="

for /d %%D in (C:\Users\VM\Desktop\SC_Releases\Latest_installer\*) do set "directory=%%~fD"

set "directory=%directory%\msi\"

echo File = %directory%

set "ShipConstructor="
set "ShipCAM="
set "NCPyros="
set "EnterprisePlatformPublisherLT="

set "elem[1]="
set "elem[2]="

set /a counter=0
setlocal ENABLEDELAYEDEXPANSION

if exist "%directory%*ShipConstructor*.<" (
	for %%a in ("%directory%*ShipConstructor*.<") do (
		set /a counter+=1
		call set elem[!counter!]=%%a
	)
)ELSE (
	ECHO "ShipConstructor.msi file not exist"
)

echo First is %elem[1]%
echo Second is %elem[2]%



if exist "%directory%*ShipCAM*.<" (
    for %%a in ("%directory%*ShipCAM*.<") do set "ShipCAM=%%a"
) ELSE (
   ECHO "ShipCAM.msi not exist"
)

if exist "%directory%*NCPyros*.<" (
   for %%a in ("%directory%*NCPyros*.<") do set "NCPyros=%%a"
) ELSE (
   ECHO "NCPyros.msi not exist"
)

if exist "%directory%*EnterprisePlatformPublisherLT*.<" (
   for %%a in ("%directory%*EnterprisePlatformPublisherLT*.<") do set "EnterprisePlatformPublisherLT=%%a"
) ELSE (
   ECHO "EnterprisePlatformPublisherLT.msi not exist"
)

echo name1 = %ShipConstructor%
echo name2 = %ShipCAM%
echo name3 = %NCPyros%
echo name4 = %EnterprisePlatformPublisherLT%


if DEFINED elem[1] (
   msiexec /x %elem[1]% /qb /norestart MSIRESTARTMANAGERCONTROL=Disable
) else (
   ECHO "ShipConstructor could not be Uninstalled"
)

if DEFINED elem[2] (
   msiexec /x %elem[2]% /qb /norestart MSIRESTARTMANAGERCONTROL=Disable
)

if DEFINED ShipCAM (
   msiexec /x %ShipCAM% /qb /norestart MSIRESTARTMANAGERCONTROL=Disable
) else (
   ECHO "ShipCAM could not be Uninstalled"
)

if DEFINED NCPyros (
   msiexec /x %NCPyros% /qb /norestart MSIRESTARTMANAGERCONTROL=Disable
) else (
   ECHO "NCPyros could not be Uninstalled"
)

if DEFINED EnterprisePlatformPublisherLT (
   msiexec /x %EnterprisePlatformPublisherLT% /qb /norestart MSIRESTARTMANAGERCONTROL=Disable
) else (
   ECHO "EnterprisePlatformPublisherLT could not be Uninstalled"
)


endlocal