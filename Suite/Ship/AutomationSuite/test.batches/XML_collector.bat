:This batch file read xml file name and test case report path from txt file which is stored at test.batch directory.
:Then it copy the xml file from recent report directory and transfer the xml file to test.cases\TC00_Run_All\xml\ directory.
ECHO OFF

SET targetFolderLocation=test.batches\

SET targetLocation=test.cases\TC00_Run_All\xml\

SET mypath1=%cd%
SET mypath2=%cd%

FOR %%a IN ("%mypath1%") DO SET "p1_dir=%%~dpa"

MKDIR %p1_dir%%targetFolderLocation%

SETLOCAL EnableDelayedExpansion
FOR /F "usebackq delims=" %%a IN (`"findstr /n ^^ %p1_dir%%targetFolderLocation%testCaseRun.txt"`) DO (
    SET "var=%%a"
    SET "var=!var:*:=!"	
	FOR %%a IN (!var!) DO SET testCase=!var!
)

FOR /F "usebackq delims=" %%a IN (`"findstr /n ^^ %p1_dir%%targetFolderLocation%testCaseXML.txt"`) DO (
    SET "var=%%a"
    SET "var=!var:*:=!"	
	FOR %%a IN (!var!) DO SET testcaseXML=!var!
)

FOR %%a IN ("%mypath2%") DO SET "p_dir=%%~dpa"
FOR %%a IN (%p_dir:~0,-1%) DO SET "p2_dir=%%~dpa"

MKDIR %p_dir%%targetLocation%

FOR /F "tokens=*" %%A IN ('DIR "%testCase%\"  /B /AD-H /T:C /O:D') DO SET recentTC=%%A
FOR /F "delims=|" %%I IN ('DIR "%testCase%\%recentTC%\*.xml" /B /O:D') DO SET xmlTC=%%I
COPY "%testCase%\%recentTC%\%xmlTC%" "%p_dir%%targetLocation%"
REN "%p_dir%%targetLocation%%xmlTC%" "%testcaseXML%"
