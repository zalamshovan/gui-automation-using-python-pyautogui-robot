: this bat file helps to execute the SC setup file to uninstall

echo off

SET Location=SCRelease\Latest_installer
SET SetupLocation=SCRelease\Latest_installer\Latest_Setup
set mypath=%cd%

for %%a in ("%mypath%") do set "p_dir=%%~dpa"
for %%a in (%p_dir:~0,-1%) do set "p2_dir=%%~dpa"

IF EXIST %p2_dir%%SetupLocation% (
    start /d %p2_dir%%SetupLocation% Setup.exe
) ELSE (
   start /d %p2_dir%%Location% Setup.exe
)
