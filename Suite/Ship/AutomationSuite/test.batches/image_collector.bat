:This batch file read test case report path from txt file which is stored at test.batch directory.
:Then from the desired path this batch file collects failed case screenshots and Installation application screenshots for combined report.

ECHO OFF

SET targetFolderLocation=test.batches\
SET mypath1=%cd%
FOR %%a IN ("%mypath1%") DO SET "p1_dir=%%~dpa"

SETLOCAL EnableDelayedExpansion
FOR /F "usebackq delims=" %%a IN (`"findstr /n ^^ %p1_dir%%targetFolderLocation%testCaseRun.txt"`) DO (
    SET "var=%%a"
    SET "var=!var:*:=!"	
	FOR %%a IN (!var!) DO SET imageFolders=!var!
)

SET combined=C:\AutomationLogs\Combined

FOR /F "tokens=*" %%Z IN ('DIR "%combined%\"  /B /AD-H /T:C /O:D') DO SET recentCombined=%%Z


FOR /F "tokens=*" %%A IN ('DIR "%imageFolders%\"  /B /AD-H /T:C /O:D') DO SET recentimageFolders=%%A
COPY "%imageFolders%\%recentimageFolders%\*.jpg" "%combined%\%recentCombined%"
COPY "%imageFolders%\%recentimageFolders%\*.png" "%combined%\%recentCombined%"