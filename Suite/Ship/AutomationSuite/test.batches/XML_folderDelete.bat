echo off

SET folder=test.cases\TC00_Run_All\xml

SET mypath=%cd%


for %%a in ("%mypath%") do set "p_dir=%%~dpa"
for %%a in (%p_dir:~0,-1%) do set "p2_dir=%%~dpa"


IF EXIST %p_dir%%folder% (
    echo XML Folder exists and deleted
    rmdir %p_dir%%folder% /s /q
) ELSE (
    echo XML Folder does not exist
)