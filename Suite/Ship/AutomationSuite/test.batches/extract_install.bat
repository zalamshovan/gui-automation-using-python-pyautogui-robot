: This bat file extracts SC installer

echo off

SET setupzipped=SCRelease
SET winrar=C:\Progra~1\WinRAR\rar
SET targetLocation=SCRelease\Latest_installer
SET _7zip=C:\Progra~1\7-Zip\7z.exe
SET "folderName="

set mypath=%cd%


for %%a in ("%mypath%") do set "p_dir=%%~dpa"
for %%a in (%p_dir:~0,-1%) do set "p2_dir=%%~dpa"


:Deleting Target location

IF EXIST %p2_dir%%targetLocation% (
    echo Target location found and deleted
    rmdir %p2_dir%%targetLocation% /s /q
) ELSE (
    echo Target location does not exist
)

:unzipping setup archive
FOR /F "delims=|" %%I IN ('DIR "%p2_dir%%setupzipped%\*.exe" /B /O:D') DO SET latest=%%I

echo %latest%

:%winrar% x "%p2_dir%%setupzipped%\%latest%" *.* "%p2_dir%%targetLocation%\"
%_7zip% x "%p2_dir%%setupzipped%\%latest%" -o"%p2_dir%%targetLocation%\"

if exist "%p2_dir%%targetLocation%\*Setup*.exe" (
	echo "Setup file is found in the target location"
	ren "%p2_dir%%targetLocation%"\*Setup*.exe Setup.exe
	start /d %p2_dir%%targetLocation% Setup.exe
)

FOR /F "delims=" %%i IN ('dir "%p2_dir%%targetLocation%" /b /ad-h /t:c /od') DO SET folderName=%%i
echo %folderName%

if exist "%p2_dir%%targetLocation%\%folderName%\*Setup*.exe" (
	echo "Setup file is not found inside another folder"
	ren %p2_dir%%targetLocation%\"%folderName%" Latest_Setup
	ren %p2_dir%%targetLocation%\Latest_Setup\*Setup*.exe Setup.exe
	start /d %p2_dir%%targetLocation%\Latest_Setup Setup.exe
)
