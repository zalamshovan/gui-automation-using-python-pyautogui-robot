@ECHO OFF

:Find and set ssiFolder.txt file directory
Set targetLocation=test.batches\
Set mypath=%cd%
for %%a in ("%mypath%") do set "p_dir=%%~dpa"

:Find the NCPyros launher file path for SSI new release from ssiFolder.txt file
SETLOCAL EnableDelayedExpansion
FOR /F "usebackq delims=" %%a in (`"findstr /n ^^ %p_dir%%targetLocation%ssiFolder.txt"`) do (
    set "var=%%a"
    set "var=!var:*:=!"	
	FOR %%a IN (!var!) DO Set targetPath=!var!
)
start /d "%targetPath%NCPyros" /max NCPyros.exe