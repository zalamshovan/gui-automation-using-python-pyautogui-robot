ECHO OFF
:wmicVersion pathToBinary [variableToSaveTo]
SETLOCAL
SET mypath=%cd%
SET targetLocation=SCRelease\Latest_installer\Latest_Setup
IF EXIST %mypath%\versionNumber.txt (
	DEL %mypath%\versionNumber.txt
)

FOR %%a IN ("%mypath%") DO SET "p_dir=%%~dpa"
FOR %%a IN (%p_dir:~0,-1%) DO SET "p2_dir=%%~dpa"

SET "item=%p2_dir%%targetLocation%\Setup.exe"
SET "item=%item:\=\\%"


FOR /f "usebackq delims=" %%a IN (`"WMIC DATAFILE WHERE name='%item%' get Version /format:Textvaluelist"`) DO (
    FOR /f "delims=" %%# IN ("%%a") DO SET "%%#"
)

IF "%~2" neq "" (
    ENDLOCAL & (
        ECHO %version% >> versionNumber.txt
        SET %~2=%version%
    )
) ELSE (
    ECHO %version% >> versionNumber.txt
)
