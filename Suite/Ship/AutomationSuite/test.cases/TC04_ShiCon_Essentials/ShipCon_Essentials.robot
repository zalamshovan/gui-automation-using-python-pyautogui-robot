*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP4_SC_Essentials.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     essentials
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: SC_Essentials
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Launching of application
	launch application

Verify Project registration and server choice
	register project

Verifying Navigator Windows
    navigator_window

Verify Customizing Component List
    customizing_component_list

Verify Project Tools
    the_project

Verify Working with Navigator
    working_with_navigator

Verify Opening Drawings
    opening_drawings_new

Verify Database Only Drawings
    database_only_drawing

Verify Showing Out of Date
    show_out_of_date_new

Verifying Properties Palette
    properties_palette

Verify Layer option
    layers_option

Verify List Option
    list

Verify Grips
    grips

Verify Visual Styles
    visual_styles

Verifying Product Hierarchy Tools
    product_hierarchy_tools

Verifying Copy Product Hierarchy
    copy_product_hierarchy

Verify Exporting Product Hierarchy to Another SC Project
    export_to_another_shipCon_project

Verify Importing Product Hierarchy From Another SC Project
    import_from_another_shipCon_project

Verifying Create Product Hierarchy Drawing
    create_product_hierarchy_drawing

Verifying Rename Product Hierarchy Drawing
    rename_product_hierarchy_drawing

Verifying Delete Product Hierarchy Drawing
    delete_product_hierarchy_drawing

Verifying Hide Show Zoom
    hide_show_find_zoom
    find_part
    zoom_to_part

Verifying Copy Names, Unload, Reload
    copy_names_unload_reload

Verifying Assign to Assemblies and Properties
    assign_to_assemblies
    properties

Verifying Assign a User-defined Attribute to a Product Hierarchy and Setting Up Assembly Levels
    assign_a_uda_to_a_product_hierarchy
    setting_up_assembly_levels

Verifying Adding an Assembly
    adding_assembly

Verifying Renaming an Assembly
    renaming_an_assembly

Verifying Editing an Assembly
    editing_assembly

Verify Changing Level of an Assembly
    changing_level_of_an_assembly

Verify Copying Assemblies
    copy_an_assembly

Verify Deleting Assemblies
    deleting_assemblies

Verify Setting Up Product HIerarcy for Training
    setting_up_product_hierarchy_for_training

Verifying Exploded Assembly View
    exploded_assembly_view

Verifying Naming Convention
    naming_convention

Verifyng Production Drawings
    production_drawings

Verifying Create Nest Test and Browse Product Hierarchy
    create_nest_test
    browse_product_hierarchy

Restarting Application
    restart_Applicaion

Verify 3D Viewpoint
    viewpoint_3d

Verify Activating UCS and Other UCS Related Operations
    activating_ucs
    flip_UCS_X
    flip_UCS_Y
    swap_UCS

Verify Hiding Objects
    hide_object

Verify Unhiding Objects
    unhide_object
    unhide_all_objects

Verify Clipping Current View
    clipping_current_view

Verify Converting 3D to 2D
    test_3d_to_2d

Verify Orthographic Projection
    orthographic_projection

Verify Removing Vertices Below Tolerance
    removing_vertices_below_tolerance

Verify Converting Ellipse to Polyline
    converting_ellipse_to_polyline

Verify Layers
    layers

Verify Toolpath
    toolpath

Verify Fillet
    fillet

Verify Mirroring about Centerline
    mirror_about_centerline

Verify Quality Matrix
    reload_drawing
    quality_matrix

Verify Property Labels
    property_labels_new

Verify Partviews
    partviews

Verify Model Link Manager
    mlink_manager

Verify Loading Partviews by Selected Extents
    load_partviews_by_selected_extents

Verify Viewing Partview in Product Hierarchy
    viewing_partview_in_product_hierarchy

Verify Checking Interference with Partviews
    checking_interferences_with_partviews

Verify Creatinh Interference Report
    creating_interference_report

Closing Application
	close application

[Teardown]   testTeardown