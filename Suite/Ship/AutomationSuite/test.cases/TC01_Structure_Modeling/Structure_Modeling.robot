*** Settings ***
Documentation     Test Cases to execute all steps in Product Hierarchy
...
Library         Screenshot
Library           ../../src.python.test/TCP1_Structure_Modeling.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     structure
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: Structure Modeling
Test Teardown  run keyword if test failed  Failed Case Handle
...
*** Test Cases ***
[Setup]     testSetup

Deploying New Project
    deploy_new_project

Verify Launching of application
	launch_application

Verify Project registration and server choice
	register_project

Verify Naming for Convention Parts
	browse_naming_convention

Verify Creating new nest test
	create nest test

Verify browsing product hierarchy
	browse_product_hierarchy

Verify Renaming Parts and Autonumber
	check keep autonumber

Verify Activation of Automatic Beveling
	activate automatic beveling

Verify Planar Group Creation from Hull Drawing
	create planar group from hull drawing

Verify Creation Of Planar Group From Other Type
	create planar group from other type

Verify Editing Planar Group Setting
    editing planar group setting

Verify Understanding Planar Groups
    understanding planar groups

Verify Move Planar Group
	move planar group

Performing Exercise R001
    exercise rm one

Restarting Application
    restart_Applicaion

Performing Exercise R002
    exercise_r002

Performing Exercise R003
    exercise_r003

Performing Exercise R004
    exercise_r004

Performing Exercise R005
    exercise_r005

Performing Exercise R006
    exercise_r006

Performing Exercise R007
    exercise_r007

Performing Exercise R008
    exercise_r008

Restarting Application After Exercises
    restart_Applicaion

Verify Planar Group Plane
	mark_group_intersection

Verify User Defined Construction Line
	draw straight line

Verify Offset Construction Line
	offset line

Verify Mirror
	mirror_line

Verify Identicals
	copy line

Verify Breaking Line Relationships
	stretch line

Verify Trimming, breaking and polyline edit
	trim line

Verify Editing Offset Construction Lines
	edit offset

Verify Swapping Construction Lines
	swap construction line

Verify Replacing Construction Lines
	replace construction line

Verifying Managing Relation
    manage relation

Restarting Application Second Time
   restart_Applicaion

Verify Creating Plate Parts
	creating plate parts

Verify Creating New Plate
    new plate creation

Verify Part Information
    part information

Verify Editing Plate Parts
    editing plate parts

Verify Opening Frames for Splitting Plate Parts
    open frame for splitting plate parts

Verify Loading Frames
    loading frames lngbhd

Verify Splitting Plate Parts
    splitting plate parts

Verify Boundary Diagnostics
    boundary_diagnostics

Verify Fixing Invalid Parts
    fixing invalid part

Verify Deleting Circle
    deleting circle

Verify Structure Display Options
    structure display

verify Construction Line And Part Relationships
    part relation

Restarting Application Third Time
   restart_Applicaion

Verify Creating Flange
    create flanges

Verify Modifying Flange
    modify flanges

Verify Adding Green Material
    add green material

Verify Modifying Green Material
    modify green material

Verify Adding Contour Construction Lines and Cutouts
    add contour construction lines

Verify Adding User Construction Lines
    add_user_construction_lines

Verify Adding Marking Lines
    add_mark_lines

Verify Adding Dynamic Marking Blocks
    add dynamic marking blocks

Verify Managing Datum Lines
    manage datum lines

Verify Orientation Icons
    orientation icons

Verify Editing Piecemarks
    editing piecemarks

Verify Corner Treatments
    adding corner treatments

Verify Moving Construction Lines
    moving construction lines

Verify Removing Corner Treatments
    remove corner treatments

Verify Defining Bevel Standards
    bevel standards

Verify Adding and Removing Bevel Standards
    adding_bevel_standards

Verify Removing Bevel Standards
    removing bevel standards

Viewing and Verifying Bevel Solids
    bevel_solids

Verify Managing the Weld Shrinkage Icon
    weld shrinkage

Verify Showing Bevel Angles on Plate Parts
    bevel angles

Restarting Application Fourth Time
   restart_Applicaion

Verify Creating Stiffeners and Loose Stiffeners on Plate Parts
    create_stiffeners

Verify Creating Loose Stiffeners on Plate Parts
    loose stiffeners to plate

Verify Editing Stifferers
    editing stiffeners

Creating Face Plates
    creating face plates

Modifying Face Plates
    modifying face plates

Verify Trimming Profiles
    trimming_profile

Verify Adding Cutouts
    adding cutouts in profiles

Verify Editing Added Cutouts
    editing cutouts

Verify Inserting Welding Seam Reliefs
    inserting welding seam reliefs

Verify Profile Tools & Utilities
    extracting lines from siffners

Restarting Application Fifth Time
    restart_Applicaion

Verify Creating Curved Plates
    create curved plates

Verify Editing Surface Properties
    editing surface properties

Editing Surface Geometric Details
    editing marklines

Verify Replacing Outer Toolpath
    replacing outer toolpath

Adding and Removing Objects to and from a Curved Plate
    adding object to curved plate

Thinning Production Information
    thinning production

Verify Extracting Production Information
    extracting production information

Restarting Application Sixth Time
    restart_Applicaion

Verify Creating Planks
    creating plank

Verify Editing Planks
    editing plank

Verify Editing Planks Collections
    editing plank collection

Verify Splitting Plank Collections
    splitting plank collections

Verify Deleting Plank
    deleting_plank

Verify Deleting Plank Collections
    deleting plank collection

Restarting Application Seventh Time
   restart_Applicaion

Verify Creating Corrugated Plate
    creating_corrugated

Verify Creating Corrugated Plate From Plate Stock
    creating corrugated plate from plate stock

Verify Drawing Polyline
    drawing polyline

Verifying Vertex Stretch
    stretch_vertex

Verify Creating new Corrugated
    new_corrugated

Verify Modifying Plate
    modifying corrugated plate

Verify Detailing Corrugated Plate
    adding objects to corrugated plate

Verify Corrugated Plate Tools
    extracting production information2

Verify Extracting Cross Section
    extracting the cross section

Restarting Application Eighth Time
   restart_Applicaion

Verify Creating Custom Plate Parts
    creating custom plate parts

Verify Converting Plate Part
    convert plate part

Verify Creating Twisted Stiffener
    creating twisted stiffener

Verify Editing Twisted Stiffener
    editing_twisted

Verify Cleaning Geometry
    cleaning_geometry

Restarting Application Ninth Time
   restart_Applicaion

Verify Inserting Standard Parts
    inserting standard part

Verify Inserting Standard Parts on Stiffener
    inserting standard part on stiffener

Verify Modifying Standards
    modify standard

Verify Converting To Structure Part
    convert standard to structure part

Verify Inserting Standard Assembly
    inserting_std_assembly

Verify Editing Assemblies
    editing_standard_assembly

Verify Anchoring Assemblies
    anchoring_standard_assemblies

Restarting Application Tenth Time
   restart_Applicaion

Verify Drawing Option
    managing structure drawing option
    copying moving mirroring

Verify Copying Moving and Mirroring Parts
    mirroring
    moving copying part

Verify Editing Parts with Identicals
    editing parts with identicals

Verify Replacing Parts to Other Planar Group
    replacing parts to other planar group

Verify Transferring Parts to Other Planar Groups
    transferring parts to other planar groups

Restarting Application Eleventh Time
   restart_Applicaion

Verify Automatic Cutouts
    automatic cutouts

Verify Making Identical Part Names the Same
    identical part names the same

Verify Checking Planar Groups
    checking planar group

Verify Showing Unused Parts
    showing unused

Verify Parts Information
    part_info

Verify Extracting Components
    extract_components

Verify Adding Manual Cutout
    add manual cutout

Closing Application
	close application
#
#Verify Structure Modeling
#    verifyingStructureModeling

[Teardown]   testTeardown