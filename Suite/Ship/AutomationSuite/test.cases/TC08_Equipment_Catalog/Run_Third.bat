for /f "delims=" %%a in ('wmic OS Get localdatetime  ^| find "."') do set dt=%%a
set YYYY=%dt:~0,4%
set MM=%dt:~4,2%
set DD=%dt:~6,2%
set HH=%dt:~8,2%
set Min=%dt:~10,2%
set Sec=%dt:~12,2%

set stamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%

pybot --outputdir C:/AutomationLogs/07_Equipment_Catalog/%stamp% --timestampoutputs --exitonfailure -o C_Third.xml -l C_Third_log.html -r C_Third_Report.html .\Equipment_Catalog.robot