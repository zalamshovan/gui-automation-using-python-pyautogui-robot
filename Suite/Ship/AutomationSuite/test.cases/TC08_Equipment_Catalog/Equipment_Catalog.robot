*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP8_Equipment_Catalog.py
...

*** Keywords ***
Failed Case Handle
    take screenshot     equip
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: Equipment Catalog
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Launching of application
	launch application

Verify Project registration and server choice
	register_project

Verifying Library Interface
    library_interface

Verify Creating Equipment Library
    creating_equipment_library

Verify Editing Equipment Stock
    editing_equipment_stock

Verify Creating Editing Standard Drawing
    creating_editing_standard_drawing

Verify Creating Equipment Model Drawing
    creating_equipment_model_drawing

Verify Equipment Model Drawing Option
    equipment_model_drawing_option

Verify Inserting Equipment into Model
    inserting_equipment_into_model

Verify Modifying Equipment
    modifying_equipment

Verify Equipment Accessory User Defined Attributes
    equipment_accessory_uda

Verify Reporting values of Accessory User Defined Attributes
    reporting_values_of_accessory_uda

Verify Managing PEL
    managing_pel

Verify Creating a New PEL Part
    creating_new_pel_part

Verify Associating Equipment to PEL Items
    associating_equipment_to_pel

Verify Disassociating Equipment from PEL Items
    diassociating_equipment_from_pel

Verify Creating PEL Items from Inserted Equipment
    creating_PEL_Items_from_Inserted_Equipment

Verify Inserting Equipment from PEL
    inserting_Equipment_from_PEL

Restart Application Before Starting Exercises
	restart_Applicaion

Verify Exercise R001
    exercise_r001

Verify Exercise R002
    exercise_r002

Verify Exercise R003
    exercise_r003

Verify Exercise R004
    exercise_r004

Closing Application
    close application

#Verify Equipment Catalog
#    verifyingEquipmentCatalog

[Teardown]   testTeardown