*** Settings ***
Documentation     Test Cases to execute all catalogs (single execution)
...
Library         Screenshot
Library         ../../src.python.test/AutoRunner.py
...


*** Test Cases ***
#Install Application
#    installApplication

Uninstalling New Installation
    uninstallApplication

Install Application
    installApplication

Structure Modeling First Run
    tc01_structure_modeling_first

HVAC Catalog First Run
    tc02_HVAC_catalog_first

Pipe Modeling First Run
    tc03_Pipe_Modeling_first

SC_Essentials First Run
    tc04_sc_essentials_first

Pipe Catalog First Run
    tc05_pipe_catalog_first

Production Documentation First Run
    tc06_prod_docu_first

Equipment Catalotg First Run
    tc07_equipment_first

Creating Combined Report
    create_combined_report_1stTimerun
