*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP5_Pipe_Catalog.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     pipecatalog
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: Pipe Catalog
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Launching of application
	launch application

Verify Project registration and server choice
	register project

Verifying Pipe Stock Catalog – Prerequisites
    materials_library
    manufacturers_library

Verifying Accessory Packages Library
    creating_new_accessory_type

Verify Creating UDA
    creating_uda

Verify Naming Conventions
    settings_of_autonumber

Verify Exercise PC R001
    exercise_pc_r001

Verify Filtering by Units
    filtering_by_units

Verify Creating Nominal Size
    creating_nominal_size

Verify Creating an International Standard
    creating_an_international_standard

Verify Crealting Size Definition
    creating_a_size_definition

Verify Exercise PC R002
    exercise_pc_r002

Verify Creating New Icon
    creating_new_icon

Verify Creating Treatment Type
    creating_new_treatment_type

Verify Treatment Properties
    properties

Verify Creating End Treatment
    creating_end_treatment

Verify Exercise PC R003
    exercise_pc_r003

Verify Creating Pipe Spec
    creating_new_spec

Verify Assigning Stocks to Spec
    assigning_stocks_to_spec

Verify Creating New Pipe Catalog
    creating_new_catalog

Verify Assigning Stockts to Catalog
    assigning_stocks_to_catalog

Verify Filtering by Any Column
    filtering_by_any_column

Verify Creating New Pipe Elements
    creating_new_pipe

Verify Creating New Branch Elements
    creating_a_new_branch

Verify Creating New Cap Elements
    creating_a_cap

Verify Creating New Cross Elements
    creating_a_cross

Verify Creating New Elbow Elements
    creating_an_elbow

Verify Creating New Reducer Elements
    creating_a_reducer

Verify Creating New Adapter Elements
    creating_an_adapter

Verify Generic Ends
    generic_ends

Verify Creating Different Types of Valves
    creating_different_types_of_valves

Verify Assigning Icons to Valve Types
    assigning_icons_to_valve_types

Verify Creating Handles and 2D Geometry for Handles
    creating_handles_and_2D_geometry_for_handles

Verify Exercise PC R004
    exercise_pc_r004

Verify Pipe Stock Catalog and Connections
    pipe_stock_catalog_and_connection

Verify Creating Accessory Packages
    creating_accessory_packages

Verify Assigning Accessoty Packages to Connections
    assigning_accessory_packages_to_connections

Verify Assigning UDA to Accessories
    assigning_uda_to_accessories

Verify Exercise PC R005
    exercise_pc_r005

Verify Pipe Benders Catalog
    parameters_of_bending_machines

Verify Exercise PC R006
    exercise_pc_r006

Closing Application
	close application

[Teardown]   testTeardown