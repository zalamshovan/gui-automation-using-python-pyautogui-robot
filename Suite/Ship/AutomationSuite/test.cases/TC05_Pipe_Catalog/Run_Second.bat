for /f "delims=" %%a in ('wmic OS Get localdatetime  ^| find "."') do set dt=%%a
set YYYY=%dt:~0,4%
set MM=%dt:~4,2%
set DD=%dt:~6,2%
set HH=%dt:~8,2%
set Min=%dt:~10,2%
set Sec=%dt:~12,2%

set stamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%

pybot --outputdir C:/AutomationLogs/05_PipeCatalog/%stamp% --timestampoutputs --exitonfailure -o B_Second.xml -l B_Second_log.html -r B_Second_Report.html .\Pipe_Catalog.robot