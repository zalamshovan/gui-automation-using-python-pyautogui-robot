*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP6_Production_Documentation.py
...

*** Keywords ***
Failed Case Handle
    take screenshot     prodocumentation
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: Product Documentation
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Launching of application
	launch_application

Verify Project registration and server choice
	register_project

Verifying Output Drawing Creation
    creation_wizard

Verify Exercise PDE R001
    exercise_pde_r001

Verify Creating BOM Definition
    creating_bom_definition

Verify Collector Options
    collector_options

Verify Inserting a BOM Table
    inserting_a_bom_table

Verify Exercise PDE R002
    exercise_pde_r002

Verify Labeling manager
    labeling

Verify Automatic and Manual Labeling
    automatic_and_manual_labeling

Verify Auto Label All
    opt1_auto_label_all

Verify Label Viewports
    opt2_label_viewports

Verify Viewport Options
    opt3_viewport_options

Verify Auto Label Parts
    opt4_auto_label_parts

Verify Manual Label
    opt5_manual_label

Verifying Leader Distribution Lines
    leader_distribution_line

Verify Redistributing Leaders
    redistributing_leaders

Verify Inserting New Leaders
    inseting_new_leaders

Verify Property Label
    property_labels_new

Verify Keywords
    keywords

Verify Check Viewport Options
    check_viewport_options

Verify Setting Global Dimension to Point
    set_global_dimension_to_point

Verify Setting Quality Matrix
    set_quality_matrix

Verify Exercise PDE R004
    exercise_pde_r004

Verify Checking BOM Revisions
    check_BOM_revisions

Verify Checking Keywords for BOM Revisions
    check_keywords_for_revision

Verify Viewing Revisions
    viewing_revisions

Verify Deleting Revisions
    deleting_revisions

Verify Exercise PDE R005
    exercise_pde_r005

Closing Application
	close application

[Teardown]   testTeardown