*** Settings ***
Documentation     Test Cases to execute all steps in Pipe Modeling
...
Library         Screenshot
Library           ../../src.python.test/TCP3_Pipe_Modeling.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     pipemodeling
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: Pipe Modeling
Test Teardown  run keyword if test failed  Failed Case Handle
...
*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Launching of application
	launch application

Verify Project registration and server choice
	register project

Verify Pipe Model Drawing
    creating_pipe_model_drawing

Verify Pipe Ribbon
    pipe_ribbon

Verify Model Drawing Interface
    model_drawing_interface

Verify System Manager
    system_management

Verify Placing Straight Pipe Elements
    inserting_straight_pipe_element

Verify Exercise R002
    exercise_r002
    copyingExerciseR002

Verify Inserting Bent Pipe Elements
    inserting_bent_pipe_elements

Verify Mitered Bent Pipes
    mitered_bent_pipes

Restarting Application
    restart_Applicaion

Verify Routing Along Polyline
    routing_along_polyline

Verify Offtes Routing
    offset_routing

Verify Exercise R003
    exercise_r003
    copyingExerciseR003

Verify Exercise A001
    exercise_a001

Verify Creating Elbow Elements
    creating_elbow

Verify Intersection Mode
    intersection_mode

Restarting Application Second Time
    restart_Applicaion

Verify Exercises R004
    exercise_R004
    copyingExerciseR004

Verify Creating Valves
    creating_valves

Verify Exercises R005
    exercise_r005
    copyingExerciseR005

Verify Creating Cross and Lateral Elements
    inserting_cross

Verify Exercises R006
    exercise_r006_old
    exercise_r006_old_added_part
    copyingExerciseR006

Verify Creating Saddles
    creating_saddle

Verify Creating Saddle to Existing Pipe
    saddle_to_existing

Verify Adding Saddle on Elbow
    add_saddle_on_elbow

Verify Exercises R007
    exercise_r007_old
    copyingExerciseR007

Verify Applying Finishes
    managing_insulation

Verify Exercises R008
    exercise_r008_new
    copyingExerciseR008

Verify Auto Part Routing
    routing_with_autopart_on

Verify Exercises R009
    exercise_r009_new
    copyingExerciseR009

Restarting Application Third Time
    restart_Applicaion

Verify UCS Intersection Command
    using_pipe_ucs_intersection_command

Verify Exercises R010
    exercise_r010_new
    copyingExerciseR010

Verify Stock Constraints
    stock_constraints

Verify Bender Constraint
    bender_constraint

Verify Switching Transform Mode
    switching_transform_mode

Verify Extracting Center Line
    extract_center_line

Verify Exercises R011
    exercise_r011_new
    copyingExerciseR011

Verify Anchoring and Locking Pipe
    anchoring_locking

Verify Connecting and Disconnecting Pipe
    connecting_disconnecting

Verify Breaking at Point
    breaking_at_point

Verify Merging Pipe to Bent Pipe
    merging_pipe_to_bent_pipe

Verify Adding bends to Route Around Obstacles
    adding_bends_to_route_around_obstacles

Verify Exercise R012
    exercise_r012_new
    copyingExerciseR013

Restarting Application Fourth Time
    restart_Applicaion

Verify Finding and Replacing Stocks
    finding_and_replacing_stock

Verify Exercise R013
    exercise_r013_new
    copyingExerciseR014

Verify Spooling a System
    spooling_system

Restaring Application Fifth Time
    restart_Applicaion

Verify Spool Manager
    spool_manager

Restaring Application For Exercise
    restart_Applicaion

Verify Exercise R014
    exercise_r014_new

Restaring Application Sixth Time
    restart_Applicaion

Verify Pipe Local Interferences
	checking_local_interferences

Verify Transferring Parts
    transferring_parts

Verify Connecting Parts From Different Drawings
    connecting_parts_from_different_drawings

Verify Exercise R015
    exercise_r015_new

Combining Exercises in one Drawing
    combiningExercises
    movingR010

Closing Application
	close application

#Verify Pipe Modeling Catalog
#    verifyingPipeModeling

[Teardown]   testTeardown