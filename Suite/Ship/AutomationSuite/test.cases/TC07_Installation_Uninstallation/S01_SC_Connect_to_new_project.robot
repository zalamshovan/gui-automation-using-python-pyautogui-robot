*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP7_Installation_Guide.py
...

Default Tags  Smoke Test: Installation Guide - Connectiing to First Project
Test Teardown     run keyword if test failed    take screenshot

*** Test Cases ***
[Setup]  testSetup

Verify Connectiing to First Project
    connecting_to_your_first_project

[Teardown]   testTeardown