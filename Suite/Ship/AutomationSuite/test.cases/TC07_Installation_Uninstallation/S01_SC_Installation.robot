*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP7_Installation_Guide.py
*** Keywords ***
Failed Case Handle
    take screenshot     Install
    closeThisApplication

*** Settings ***
Default Tags  End to End Test: Installation Automation
Test Teardown   run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Verify Installing ShipConstructor
    SC_Install

Verify Appearance of ReleaseNotes
    RunApplication
    releaseNote

Verifying Version No in ReleaseNotes
    releaseVersion

Verify SCAbout Version
    SCAboutVersion

Verify Report verification
    SCReportVerification

Verify Update Database
    verifyUpdatedDatabase

Verify NCPyros Version
    AboutNCPyros

Verify Inverse Bend Version
    AboutInverseBend

Verify Lines Fairing Version
    AboutLinesFairing

Verify Loft Space Version
    AboutLoftSpace

Verify Pinjigs Version
    AboutPinjigs

Verify Plate Expand Version
    AboutPlateExpand

Verify Print Offsets Version
    AboutPrintOffsets

Verify Shell Expand Version
    AboutShellExpand

Verify Stringer Cutouts Version
    AboutStringerCutouts

Verifying Availability of TechNotes
    checkTechNotes

Verify Release HTML file for Missing Component
    verifyMissingConponent

Verify Release HTML file for Missing Tech Summary Details
    verifyMissingTechSummary_Details

Verify Release HTML file for No Missing Summary
    verifyNoMissingSummary

Verify Release HTML file for Missing Tech Notes
    verifyMissingTechNotes

[Teardown]   testTeardown