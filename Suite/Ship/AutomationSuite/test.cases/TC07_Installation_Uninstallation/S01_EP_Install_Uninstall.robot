*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP7_Installation_Guide.py
*** Keywords ***
Failed Case Handle
    take screenshot     EP_Install

*** Settings ***
Default Tags  Installation Automation
Test Teardown  run keyword if test failed  Failed Case Handle
...

*** Test Cases ***
[Setup]  testSetup

Verify Installation of EnterprisePlatform Node
    ep_node_Install
    verify_EP_node

Verify Uninstallation of EnterprisePlatform Node
    EP_uninstall
    verify_node_uninstall

Verify Installation of EnterprisePlatform InformationHub
    EP_InformationHub_Install
    verify_EP_InformationHub

Verify Version Number of InformationHub & IntegrationConnector
    verify_Version_InformationHub
    verify_Version_IntegrationConnector

Verify Uninstallation of EnterprisePlatform InformationHub & IntegrationConnector
    EP_uninstall
    verify_InformationHub_IntegrationConnector_uninstall

Verify Installation of EnterprisePlatform Client
    EP_Client_Install
    verify_EP_Client

Verify Valid Connectivity
    verify_publisher_connection
    verify_projectViewer_connection

Verify Uninstallation of EnterprisePlatform Client
    EP_uninstall
    verify_client_uninstall

[Teardown]   testTeardown