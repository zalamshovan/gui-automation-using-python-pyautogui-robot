*** Settings ***
Library         Screenshot
Library           ../../src.python.test/TCP7_Installation_Guide.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     Uninstall

*** Settings ***
Default Tags  End to End Test: Uninstallation Automation
Test Teardown  run keyword if test failed  Failed Case Handle
...
*** Test Cases ***
[Setup]  testSetup

Verify Removing ShipConstructor
    SC_uninstall
    verifyUninstall

[Teardown]   testTeardown