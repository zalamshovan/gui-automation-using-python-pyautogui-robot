*** Settings ***
Documentation     Test Cases to execute all steps in HVAC
...
Library         Screenshot
Library           ../../src.python.test/TCP2_HVAC_Catalog.py
...
*** Keywords ***
Failed Case Handle
    take screenshot     hvac
    forced_close_application
    testTeardown

*** Settings ***
Default Tags  End to End Test: HVAC
Test Teardown  run keyword if test failed  Failed Case Handle
...
*** Test Cases ***
[Setup]  testSetup

Deploying New Project
    deploy_new_project

Verify Launching of application
	launch_application

Verify Project registration and server choice
	register project

Verify HVAC Stock Catalog Prerequisite
    materials_library
    manufacturers_library

Verify Accessory Packages Library
    creating_new_accessory_type

Verify User Defined Attributes
    creating_uda

Verifying Settings of Autonumber
    settings_of_autonumber

Verify Sheet Stocks Library
    sheet_stocks

Verify Creating New Icons
    creating_new_icon

Verify Creating New Treatments
    creating_new_treatment

Verify Use of Meaningful Name
    meaningful_name

Verify Creating New HVAC Specs
    creating_new_specs

Verify Assigning Stocks to Spec
    assign_stocks_to_spec

Verify Creating New Catalog
    creating_new_catalog

Verify Assigning Stocks to Catalog
    assign_stocks_to_catalog

Vefiry Filtering by Column
    filtering_by_cloumn

Verify Creating New Duct
    creating_new_duct

Verify Creating New Elbow
    creating_elbow

Verify Creating New Flanges
    creating_flanges

Verify Creating New Branch
    creating_branch

Verify Exercise HCM R003
    excercise_hcm_r003

Verify Exercise HCM A002
    excercise_hcm_a002

Verify HVAC Stock Catalog
    connection_parameters

Verify HVAC Stock Connections
    assigning_accessory_packages_to_connections

Restarting Application
    restart_Applicaion

Verify HVAC Model Drawing
    creating_hvac_model_drawing

Verifying Object Snap
    object_snap

Verify System Manager
    creating_systems

Verify Exercise HCM R005
    exercise_hcm_r005

Verify Modeling HVAC
    modeling_hvac

Verify Exercise HCM R006
    exercise_hcm_r006

Verify Creating Bent Ducts
    creating_bent

Verify Creating Polyline Ducts
    polyline_duct

Verify Offset Routing
    offset_routing

Verify OTF Mode
    otf_mode

Verify Exercise HCM R007
    exercise_hcm_r007

Verify Exercise HCM A003
    exercise_hcm_a003

Verify Creating Flange
    creating_flange

Verify Creating Flange OTF
    creating_flange_otf

Verify Creating Tee
    creating_tee_elements

Verify Creating Tee OTF
    creating_tee_elements_otf

Verify Creating Lateral
    creating_lateral

Verify Creating Lateral OTF
    creating_lateral_otf

Verify Creating Cross
    creating_cross

Verify Creating Cross OTF
    creating_cross_otf

Verify Creating Transition
    creating_transition

Verify Creating Transition OTF
    creating_transition_otf

Restarting Application Second Time
	restart_Applicaion

Verify Creating Saddles
    create_saddles

Verify Creating Duct as Saddles
    creating_duct_as_saddle

Verify managing Finishes and Insulations
    managing_finishes_and_insulation

Verify Applying Finishes
    applying_insulation

Verify Applying Insulations
    applying_finishes

Verify Using HVAC UCS Intersection
    creating_ucs_intersection

Verify UCS Intersection Routing
    ucs_intersection_routing

Verify Erasing UCS Intersection
    erasing_ucs_intersection

Verify Stock Constraints
    stock_constraints

Verify Cutting Ducts to Max Length
    cutting_ducts_to_max_length

Restarting Application Third Time
    restart_Applicaion

Verify Locking Anchoring Elements
    locking_anchoring_elements

Verify Connecting and Disconnecting Part
    connecting_and_disconnecting_part

Verify Breaking Duct
    breaking_duct

Verify Merging to Bent Duct
    merging_to_bent_duct

Verify Extracting Center Line
    extracting_center_line

Verify Pantsifying Duct Ends
    pantsifying_duct_ends

Verify Spooling a System
    add_remove_spool_breaks

Verify Spool Manager
    spool_manager

Verify Checkng Local Interferences
    checking_local_interferences

Verify Transferring Parts
    transferring_parts

Verify Connecting Parts
    connecting_parts

Closing Application
	close application

[Teardown]   testTeardown