from IOUtil import mouseLeftClickOn, mouseClickAppears, mouseMoveTo, mouseDoubleClickOn, hotKeyAction, pressAction

from AppUtil import AppUtil

from time import sleep

from screenObjUtil import exists, appears, waitForVanish

from pyautogui import typewrite, press

import Logger


class SSILearningVerifierUtil(object):

    def __init__(self):
        name = self.__class__.__name__
        pass

    def ssi_learning_verifier(self, projectFilePath, list_image, xmlExtractor1, xmlName1):
        obj = AppUtil()
        obj.systemTaskKill("TASKKILL /F /IM SSILearningVerifier_INTERNALONLY_18.200.0.1_NoSubmisison.exe")
        sleep(5)
        obj.launchAnyApp("test.images", "../test.batches/", "Run_Verifier.bat")

        sleep(2)
        if appears("update_ssi_learner", 10):
            mouseMoveTo("update_ssi_learner")
            mouseLeftClickOn("update_ssi_learning_no")
        else:
            raise AssertionError("SSI Learning Verifier Application did not appear.")
        sleep(2)
        hotKeyAction('win', 'up')
        if appears("ssi_learning_ico", 30):
            print("SSI Learning Verifier window appeared")
            hotKeyAction('win', 'up')
            hotKeyAction('win', 'up')
            mouseLeftClickOn("project_browser")
            if appears("register_project_browse"):
                mouseLeftClickOn("register_project_browse")
                if appears("browser_dropdown", 20):
                    mouseLeftClickOn("browser_dropdown")
                    typewrite(projectFilePath, .1)
                    pressAction('enter',1)
                else:
                    raise AssertionError("open_template_browser_window_dropdown did not appear")
            else:
                raise AssertionError("register_project_browse did not appear")
        else:
            raise AssertionError("ssi_learning_ico did not appear")
        if appears("training_file", 10):
            mouseLeftClickOn("training_file")
            press('down', 2, .2)
            press('enter')
            sleep(5)
            mouseLeftClickOn("open_register_project")
            sleep(2)
            if appears("mismatching", 10):
                press('enter')
            if appears("license_veri", 10):
                mouseLeftClickOn("license_veri")
            if appears("checked", 10):
                mouseClickAppears("checked")
            mouseDoubleClickOn("weldManagement")
            if appears(list_image, 10):
                mouseLeftClickOn(list_image)
            else:
                raise AssertionError(list_image + " did not appear")
            mouseLeftClickOn("verify")
            sleep(5)
            if appears("mismatching", 10):
                press('enter')
            sleep(5)
            sleep(5)
            sleep(10)
            if appears("verification_failed"):
                mouseLeftClickOn("no")
                if appears("certified_feedback"):
                    hotKeyAction('win', 'left')
                    sleep(2)
                    press('esc')
                    sleep(2)
                    press('tab', presses=3, interval=.2)
                    press('2')
                    press('tab')
                    hotKeyAction('ctrl', 'a')
                    typewrite('asb', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    hotKeyAction('ctrl', 'a')
                    typewrite('asb', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    hotKeyAction('ctrl', 'a')
                    typewrite('asb', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    typewrite('2', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    hotKeyAction('ctrl', 'a')
                    typewrite('abvs', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    typewrite('2', .2)
                    sleep(1)
                    press('tab')
                    sleep(1)
                    hotKeyAction('ctrl', 'a')
                    typewrite('abvs', .2)
                    sleep(1)
                    mouseLeftClickOn("attach_a_file")
                    sleep(1)
                    if appears("pictures", 10):
                        mouseLeftClickOn("pictures")
                        if appears("testPng", 10):
                            mouseDoubleClickOn("testPng")
                            sleep(1)
                            mouseLeftClickOn('verifier_ok')
                        else:
                            raise AssertionError("testPng did not appear")
                    else:
                        raise AssertionError("pictures did not appear")
                else:
                    raise AssertionError("certified_feedback did not appear")
            elif appears("certified_feedback"):
                if appears("congrats", 10):
                    Logger.info("NO ERROR WAS FOUND")
        else:
            raise AssertionError("training_file did not appear")
        if appears("browse_for_folder"):
            if appears("desktop_node", 5):
                mouseLeftClickOn("desktop_node")
            mouseLeftClickOn("browse_for_folder_ok")
            sleep(5)
            sleep(5)
            mouseLeftClickOn("verifier_close")
            sleep(5)
            obj.XmlAttach(xmlExtractor1, xmlName1)
        else:
            raise AssertionError("browse_for_folder did not appear")
        obj.systemTaskKill("TASKKILL /F /IM SSILearningVerifier_INTERNALONLY_18.200.0.1_NoSubmisison.exe")