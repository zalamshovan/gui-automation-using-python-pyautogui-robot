import config, winreg, codecs, re, os, Logger, pytesseract, glob
from IOUtil import mouseLeftClickOn, mouseMoveTo, mouseMoveToCoordinates, hotKeyAction, pressAction, typeWriteAction, takeScreenShot
from time import sleep
from screenObjUtil import exists, appears, waitForVanish, location
from Initialize import Initialize
from PIL import Image

class AppUtil(object):

    def __init__(self):
        name = self.__class__.__name__
        pass

    # setting image folder for each module by getting the folder name from Module.txt in test.image folder
    def set_path_for_image(self, folder_name):
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation != "test.images":
            os.chdir("../../test.images/")
        file = "Module.txt"
        with open(file, "w") as myfile:
            myfile.write(folder_name)
        obj = Initialize()
        obj.set_image_directory()

    # attach xml file of SSI verifier
    def XmlAttach(self, xmlExtractorBat, xmlName):
        currentWorkingDirectory = os.getcwd()
        os.chdir("../test.batches/")
        os.system(xmlExtractorBat+".bat")
        os.chdir(currentWorkingDirectory)
        sleep(2)
        Logger.verifierLog(xmlPath="ShipConstructorSoftware_AdamRuttan_"+xmlName+".xml")

    # wait until the autocad version appears
    def waitForLicense(self):
        if config.AutoCAD_version == "AutoCAD2019_mechanical":
            if appears("license_trial", 60):
                # license_clock
                mouseLeftClickOn("license_trial")
                mouseLeftClickOn("license_clock")
                mouseMoveToCoordinates(400,400)
            else:
                raise AssertionError("License Trail Image not found.")
        elif config.AutoCAD_version == "AutoCAD19":
            if appears("trial_run", 60):
                mouseLeftClickOn("trial_run")
                mouseMoveToCoordinates(400,400)
            else:
                raise AssertionError("Trail Run Image not found.")
        else:
            raise AssertionError("Nothing is configured in config file or autocad version not matched.")

    # kill system applications by command line
    def systemTaskKill(self, killingTask):
        return os.system(killingTask)

    # launch any application from particular folder
    def launchAnyApp(self, currentLocation, ch_dir, system_run):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == currentLocation:
            os.chdir(ch_dir)
            os.system(system_run)
        else:
            os.chdir("../" + ch_dir)
            os.system(system_run)
        os.chdir(currentWorkingDirectory)

    # select autocad version from the list
    def startSSIFromList(self):
        if appears("start_shipcon", 20):
            print(config.AutoCAD_version)
            if config.AutoCAD_version == "AutoCAD2019_mechanical":
                print("AutoCAD2019_mechanical is selected")
                if appears("autocad_mechanical", 10):
                    mouseLeftClickOn("autocad_mechanical")
            elif config.AutoCAD_version == "AutoCAD2019":
                print("AutoCAD2019 is selected")
                if appears("autocad2019_list", 10):
                    mouseLeftClickOn("autocad2019_list")
                elif appears("autocad2019_list_high", 10):
                    mouseMoveTo("autocad2019_list_high")
                else:
                    raise AssertionError("AutoCAD2019 list was not found.")
            else:
                raise AssertionError("AutoCad version not matched")
            mouseLeftClickOn("start_shipcon")
        else:
            print("Launched AutoCAD 2020 beta version for testing purpose.")
            print(config.AutoCAD_version)
            # raise AssertionError("Ship Consturctor start icon doesn't appeared.")
        waitForVanish("ssi_startup_logo")

    # Match License Status whether it's trial or licensed
    def matchLicenseStatus(self):
        if config.AutoCAD_license_status == "licensed":
            print("AutoCAD is Licensed")
        elif config.AutoCAD_license_status == "trial":
            self.waitForLicense()
        else:
            raise AssertionError("AutoCAD license status doesn't matched.")

    # Check SSI has initiated properly
    def checkSSIInitialization(self):
        if config.AutoCAD_version == "AutoCAD2019_mechanical":
            if appears("autocad_mechanical_icon", timeWait=180):
                print("Application has launched properly")
            else:
                raise AssertionError("ShipConstructor has not launched properly")
        else:
            if appears("autocad_icon", timeWait=180):
                print("Application has launched properly")
            else:
                raise AssertionError("ShipConstructor has not launched properly")
        if appears("ssi_tab", 15):
            mouseMoveTo("ssi_tab")
            if appears("ssi_tab_close", 10):
                mouseLeftClickOn("ssi_tab_close")
                mouseMoveToCoordinates(500, 500)
                sleep(2)
            else:
                raise AssertionError("ssi_tab_close did not appear")
            sleep(3)
            if appears("drawing_tab", 20):
                mouseLeftClickOn("drawing_tab")
                sleep(2)

        mouseMoveToCoordinates(500, 500)
        sleep(2)

    # Launch SSI application
    def launch_app(self):
        # kill tasks with count=1
        self.systemTaskKill("TASKKILL /F /IM acad.exe")
        self.systemTaskKill("TASKKILL /F /IM LMU.exe")

        self.launchAnyApp("test.images", "../test.batches/", "Run_SC.bat")
        sleep(2)
        mouseMoveToCoordinates(400, 400)
        self.startSSIFromList()
        self.matchLicenseStatus()

        if appears("migrate_setting_dialog",10):
            print("Setting Migration dialog appeared")
            mouseMoveTo("migrate_setting_dialog")
            mouseLeftClickOn("migrate_settings_no")
            sleep(2)
        if appears("hull_and_structure_tab",10):
            print("select_shipcon_environment option appeared")
            mouseLeftClickOn("hull_and_structure_tab")
            sleep(2)

        if appears("sc_release_close", 15):
            hotKeyAction("win","up")
            if appears("show_this_next_technote",20):
                mouseLeftClickOn("show_this_next_technote")
            mouseLeftClickOn("sc_release_close")

        self.checkSSIInitialization()
        self.minimize_tabs()

    # To Forced Close the SSI application
    def forced_close(self):
        hotKeyAction('ctrl', 's')
        sleep(4)
        self.systemTaskKill("TASKKILL /F /IM acad.exe")
        self.systemTaskKill("TASKKILL /F /IM LMU.exe")
        sleep(2)
        mouseMoveToCoordinates(760, 385)

    # To Close the SSI application
    def close_app(self):
        sleep(2)
        i = 0
        mouseMoveToCoordinates(760, 385)
        sleep(2)
        while exists("autocad_icon_window") == True:
            i = i + 1
            if appears("cross_button", 5):
                mouseLeftClickOn('cross_button')
            else:
                mouseLeftClickOn("cross_button2")
            sleep(2)
            mouseMoveToCoordinates(760, 385)
            sleep(2)
            if appears("save_yes", 5):
                mouseLeftClickOn("save_yes")
            if appears("exit_warning", 5):
                mouseMoveTo("exit_warning")
                mouseLeftClickOn("exit_warning_yes")
            if i == 5:
                self.forced_close()
                break
        mouseMoveToCoordinates(760, 385)
        sleep(2)

    # for minimizing tab
    def minimize_tabs(self):
        if appears("tab_shrink_region_up", 30):
            mouseLeftClickOn("tab_shrink_region_dropdown", "tab_shrink_region_up")
        elif appears("tab_shrink_region_down", 30):
            mouseLeftClickOn("tab_shrink_region_dropdown","tab_shrink_region_down")
        if appears("panel_titles", 30):
            mouseLeftClickOn("panel_titles")
        else:
            raise AssertionError("Tabs could not be minimized")
        mouseMoveTo("empty_space2")

    # for loading specific ribbon and minimize tab
    def ribbon_load(self, ribbon_name):
        mouseMoveTo("empty_space2")
        typeWriteAction('SCRIBBONLOADTABS')
        pressAction('enter')
        sleep(5)
        if appears("tab_load",10):
            mouseLeftClickOn(ribbon_name)
            sleep(5)
            self.minimize_tabs()
        else:
            raise AssertionError("tab_load window did not appear")

    # clean database before deploying any project
    def delete_database(self, module):
        sleep(1)
        os.system("sqlcmd -S .\SHIPCON_SQL14 -U sa -P ShipCon1 -Q \"DROP DATABASE NewDeploy_" + module + "\"")
        sleep(5)

    # find the SSI folder in registry before cleaning
    def findRegistyFolder(self):
        folderArr = self.returnAllFoldersUnderSSI(winreg.HKEY_CURRENT_USER)
        try:
            if "Constr" in folderArr[0]:
                return folderArr[0]
            else:
                return None
        except IndexError:
            return None

    #Find the folders under SSI from registry
    def returnAllFoldersUnderSSI(self, hkeyName = None):
        parentKey = ""
        if hkeyName is None:
            parentKey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, "Software\SSI",access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)
        else:
            parentKey = winreg.OpenKey(hkeyName, "Software\SSI",access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)
        i = 0
        arr = []
        while True:
           try:
               key = winreg.EnumKey(parentKey, i)
               if "ShipConstructor" in key:
                   arr.append(key)
               elif "EnterprisePlatform2" in key:
                   arr.append(key)
               i += 1
           except WindowsError:
               break
        return arr

    # Find the latest release Year from Registry
    def returnLatestAppYear(self):
        listOfSSIFolders = []
        removeShipConstructor = []
        listOfSSIFolders = self.returnAllFoldersUnderSSI()
        i=0
        while True:
            if i >= len(listOfSSIFolders):
                break
            try:
                if 'ShipConstructor' in listOfSSIFolders[i]:
                    temp = listOfSSIFolders[i].replace('ShipConstructor','',1)
                elif 'EnterprisePlatform' in listOfSSIFolders[i]:
                    temp = listOfSSIFolders[i].replace('EnterprisePlatform','',1)
                if "_" not in temp:
                    removeShipConstructor.append(temp)
                i += 1
            except WindowsError:
                break
        if not removeShipConstructor:
            return None
        else:
            if '20' in removeShipConstructor[0]:
                latestYear = max(removeShipConstructor)
                return latestYear
            else:
                return None

    #Create the latest Version Folder Path
    def createVersionFolder(self):
        obj = AppUtil()
        latestYear = obj.returnLatestAppYear()
        if latestYear is None:
            versionFolder = "ShipConstructor"
        else:
            versionFolder = "ShipConstructor" + latestYear
        return versionFolder

    # Find the latest version Number from Registry
    def getLatestVersion(self):
        obj = AppUtil()
        folderToCollectVersion = obj.createVersionFolder()
        path = "Software\\SSI\\" + folderToCollectVersion
        # Open the key and return the handle object.
        hKey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path ,access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)

        # Read the value.
        result = winreg.QueryValueEx(hKey, "DisplayVersion")
        # Return only the value from the resulting tuple (value, type_as_int).
        fullValueWithVersion = result[0]

        #Remove All the Spaces from version
        fullValueWithVersion = fullValueWithVersion.replace(' ','', 1)
        fullValueWithVersion = fullValueWithVersion.replace(' ', '', 1)

        onlyVersionNumber = fullValueWithVersion.replace('ShipConstructor2019', '', 1)
        return onlyVersionNumber

    # Find the latest version of SSI folder path from Registry
    def getLatestInstalledFolderPathSSI(self):
        obj = AppUtil()
        try:
            folderToCollectVersion = obj.createVersionFolder()
            path = "Software\\SSI\\" + folderToCollectVersion
            # Open the key and return the handle object.
            hKey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path, access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)

            # Read the value.
            result = winreg.QueryValueEx(hKey, "InstallDir")
            # Return only the value from the resulting tuple (value, type_as_int).
            fullValue = result[0]

            return fullValue
        except WindowsError:
            print('Folder Path not found in Software/SSI path an external path has been used to avoid failure.')
            return "ShipConstructor"


    # Find the latest version of EP folder path from Registry
    def getLatestInstalledFolderPathEP(self):
        removeShipConstructor = []
        listOfSSIFolders = self.returnAllFoldersUnderSSI()
        i = 0
        while True:
            if i >= len(listOfSSIFolders):
                break
            try:
                if 'EnterprisePlatform' in listOfSSIFolders[i]:
                    temp = listOfSSIFolders[i]
                if "_" not in temp:
                    removeShipConstructor.append(temp)
                i += 1
            except WindowsError:
                break
        path = "Software\\SSI\\" + removeShipConstructor[0]
        # Open the key and return the handle object.
        hKey = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, path, access=winreg.KEY_READ | winreg.KEY_WOW64_64KEY)

        # Read the value.
        result = winreg.QueryValueEx(hKey, "InstallDir")
        # Return only the value from the resulting tuple (value, type_as_int).
        fullValue = result[0]

        return fullValue

    # getting all the component title id
    def getComponentID(self):
        html = self.readReleaseHtmlFile()
        temp = html.split("class=\"component_links\"")
        TAG_RE = re.compile(r'<[^>]+>')
        startline = html.find(temp[1], 1000)
        endline = html.find("</div>", startline)
        firstpart = html[startline:endline]
        firstp = TAG_RE.sub('', firstpart)
        firstp = re.sub("[\t]*", '', firstp)
        firstp = firstp.replace(" / ","_/_")
        firstp = firstp.replace("|","\n").splitlines()
        temp = []
        for x in firstp:
            if x.__contains__("MISSING COMPONENT"):
                temp.append(x.strip())
            elif x.__contains__(">"):
                continue
            else:
                temp.append(x.strip().replace(" ", "_"))
        return temp

    # Read release.html file to verify the missing components of release notes
    def readReleaseHtmlFile(self):
        currentWorkingDirectory = os.getcwd()
        ssiFolder = self.getLatestInstalledFolderPathSSI()
        SCReleaseNotesHTMLPath = ssiFolder

        os.chdir(SCReleaseNotesHTMLPath)
        file = codecs.open("release.html", "r", "utf-8")
        html = file.read()
        os.chdir(currentWorkingDirectory)
        return html

    # Count the number of Missing Component ID and split them into counted number of ID
    def splitReleaseHTML(self, conId):
        html = self.readReleaseHtmlFile()
        newId = "id=\""+conId+"\""
        temp = html.split(newId)
        countsplitedHTML = html.count(newId)
        countsplitedHTML = countsplitedHTML +1
        TAG_RE = re.compile(r'<[^>]+>')
        if countsplitedHTML==2:
            startline = html.find(temp[1], 1000)
            endline = html.find("tbody>",startline)
            firstpart = html[startline:endline]
            firstp = TAG_RE.sub('', firstpart)
            firstp = re.sub("[\t]*", '', firstp)
            return countsplitedHTML, firstp
        elif countsplitedHTML==3:
            startline = html.find(temp[1], 1000)
            endline = html.find("tbody>", startline)
            firstpart = html[startline:endline]
            firstp = TAG_RE.sub('', firstpart)
            firstp = re.sub("[\t]*", '', firstp)
            startline = html.find(temp[2], 1000)
            endline = html.find("tbody>", startline)
            secondpart = html[startline:endline]
            secondpart = TAG_RE.sub('', secondpart)
            secondpart = re.sub("[\t]*", '', secondpart)
            return countsplitedHTML, firstp, secondpart
        else:
            print("Splitting is not properly done", str(countsplitedHTML))

    # Collect SCnumbers of TechNote Summary
    def getSCNumber_TechNotesSummary(self):
        contentID = self.getComponentID()
        result = []
        for conId in contentID:
            countPart = self.splitReleaseHTML(conId)
            countPart = str(countPart).split(",")
            if countPart[0].__contains__("3") == True:
                countPart, firstPart, secontPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                secontPart = secontPart.splitlines()
                for temp1 in firstPart:
                    if "MISSING TECHNOTE SUMMARY" in temp1:
                        result.append(temp1)
                    elif "MISSING TECHNOTE DETAILS" in temp1:
                        result.append(temp1)
                for temp2 in secontPart:
                    if "MISSING TECHNOTE SUMMARY" in temp2:
                        result.append(temp2)
                    elif "MISSING TECHNOTE DETAILS" in temp2:
                        result.append(temp2)
            else:
                countpart, firstPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                for temp in firstPart:
                    if "MISSING TECHNOTE SUMMARY" in temp:
                        result.append(temp)
                    elif "MISSING TECHNOTE DETAILS" in temp:
                        result.append(temp)
        return result

    # Collect SCnumbers of Unknown Status of TechNote
    def getSCNumber_UnknownStatus(self):
        contentID = self.getComponentID()
        for conId in contentID:
            countPart = self.splitReleaseHTML(conId)
            countPart = str(countPart).split(",")
            if countPart[0].__contains__("3")==True:
                countPart, firstPart, secontPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                secontPart = secontPart.splitlines()
                for temp in firstPart:
                    if "TechNote Status is Unknown" in temp:
                        print(temp)
                for temp in secontPart:
                    if "TechNote Status is Unknown" in temp:
                        print(temp)
            else:
                countpart, firstPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                for temp in firstPart:
                    if "TechNote Status is Unknown" in temp:
                        print(temp)

    # Collect SCnumbers of Missing Summary
    def getSCNumber_MissingSum(self, noMissingSum):
        result = []
        contentID = self.getComponentID()
        for conId in contentID:
            countPart = self.splitReleaseHTML(conId)
            countPart = str(countPart).split(",")
            if countPart[0].__contains__("3") == True:
                countPart, firstPart, secontPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                secontPart = secontPart.splitlines()
                i = 0
                for temp in firstPart:
                    if " " in temp:
                        if noMissingSum in temp and i <= 1:
                            result.append(temp)
                        i = i + 1
                for temp in secontPart:
                    if " " in temp:
                        if noMissingSum in temp and i <= 1:
                            result.append(temp)
                        i = i + 1
            else:
                i = 0
                countpart, firstPart = self.splitReleaseHTML(conId)
                firstPart = firstPart.splitlines()
                for temp in firstPart:
                    if " " in temp:
                        if noMissingSum in temp and i <= 1:
                            result.append(temp)
                        i = i + 1
        return result

    # Programs and Features Window Open from command prompt and search SSI latest version application
    def openApplicationFeatureWindow(self):
        os.system("appwiz.cpl")
        if appears("highlight_programs&feature1",30):
            mouseLeftClickOn("highlight_programs&feature1")
        elif appears("highlight_programs&feature2",30):
            mouseLeftClickOn("highlight_programs&feature2")
        if appears("programsFeatureIcon",30):
            mouseMoveTo("programsFeatureOrgranize")
            mouseLeftClickOn("programsFeatureSearchText2","programsFeatureSearchTextRegion")
            releaseYear = str(self.returnLatestAppYear())
            if releaseYear == str(None):
                typingVariable = "SSI"
                typeWriteAction(typingVariable)
            else:
                leng = releaseYear.__len__()
                leng = leng - 4
                releaseYear = releaseYear[:-leng]
                typingVariable = "SSI " + releaseYear
                typeWriteAction(typingVariable)
        else:
            raise AssertionError("Programs and Feature Window is not found.")

    # To get the version number running a batch file and save the version number in a text file
    # Than read the text file to get the build version number.
    def getBuildVersionNumberByBatch(self):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")

        if "/SCRelease/Latest_installer/Latest_Setup" in config.Path_Installer:
            os.system('BuildVersionNumber_latestsetup.bat')
        elif '/SCRelease/Latest_installer' in config.Path_Installer and not 'Latest_Setup' in config.Path_Installer:
            os.system('BuildVersionNumber_latestinstaller.bat')

        file = open("versionNumber.txt", "r")
        versionTxt = file.read()

        testText = versionTxt.replace(" ", "")
        testText = testText.replace("\n", "")
        testText = "(Build Version Number: "+str(testText)+")"
        os.chdir(currentWorkingDirectory)
        return testText

    # To get the version number from Programs and Features window by taking screenshot and render it to text.
    def getBuildVersionNumberByImage(self):
        currentWorkingDirectory = os.getcwd()
        self.openApplicationFeatureWindow()
        loca2 = location("programsFeatureVersionTitle")
        if loca2 is not None:
            takeScreenShot("programsFeatureVersionTitle", loca2[0], loca2[1] + 20, loca2[2] + 100, loca2[3] + 50)
            os.system(
                "magick convert programsFeatureVersionTitle.png -resize 500% programsFeatureVersionTitleExt.png")
        else:
            raise AssertionError("Programs and Feature Window did not appear")
        text = self.imageReader(image_name="programsFeatureVersionTitleExt.png")
        text = text.replace("Version" + "\n", "")
        testText = text.replace(" ", "")
        hotKeyAction("Alt", "f4")
        testText = "(Build Version Number: " + str(testText) + ")"
        os.chdir(currentWorkingDirectory)
        return testText

    # for reading text from image
    def imageReader(self, image_name):
        filename = image_name
        text = pytesseract.image_to_string(Image.open(filename))
        if text is not None:
            return text
        else:
            return None

    # Minimizing the powershell window or cmd window
    def minimizeRunnerPrompt(self):
        try:
            currentWorkingDirectory = os.getcwd()
            file_1 = currentWorkingDirectory + "\\log.html"
            file_2 = currentWorkingDirectory + "\\report.html"
            os.remove(file_1)
            os.remove(file_2)
        except FileNotFoundError:
            Logger.info("Log.html and Report.html are deleted from robot directory.")
        pressAction('win')
        if appears("powerShell_icon",10):
            if appears("powerShell_window_high"):
                if appears("powerShell_mini_max_region"):
                    mouseLeftClickOn("powerShell_mini","powerShell_mini_max_region")
            elif appears("powerShell_window_unhigh",10):
                mouseLeftClickOn("powerShell_icon")
                if appears("powerShell_mini_max_region",10):
                    mouseLeftClickOn("powerShell_mini","powerShell_mini_max_region")
            else:
                mouseLeftClickOn("powerShell_icon")
                if appears("powerShell_mini_max_region", 10):
                    mouseLeftClickOn("powerShell_mini", "powerShell_mini_max_region")
        elif appears("cmd_icon", 10):
            if appears("cmd_window_high"):
                if appears("cmd_mini_max_region"):
                    mouseLeftClickOn("cmd_mini","cmd_mini_max_region")
            elif appears("cmd_window_unhigh",10):
                mouseLeftClickOn("cmd_icon")
                if appears("cmd_mini_max_region",10):
                    mouseLeftClickOn("cmd_mini","cmd_mini_max_region")
            else:
                mouseLeftClickOn("cmd_icon")
                if appears("cmd_mini_max_region", 10):
                    mouseLeftClickOn("cmd_mini", "cmd_mini_max_region")
        else:
            print("powerShell_icon/cmd_icon didn't appeared.")

    # Maximizing the powershell window or cmd window
    def maximizeRunnerPrompt(self):
        if appears("powerShell_icon",10):
            if appears("powerShell_window_high", 10):
                print("powerShell already appeared.")
            else:
                mouseLeftClickOn("powerShell_icon")
        elif appears("cmd_icon",10):
            if appears("cmd_window_high", 10):
                print("CMD window already appeared.")
            else:
                mouseLeftClickOn("cmd_icon")
        else:
            print("powerShell_icon/cmd_icon didn't appeared.")

        mouseMoveToCoordinates(400,400)

    # after closing explorer.exe it restarts the explorer
    def launchExplorer(self):
        if appears("quick_access_address_bar", 10):
            mouseLeftClickOn("quick_access_address_bar")
            typeWriteAction("cmd")
            pressAction('enter')
        if appears("cmd_window", 10):
            typeWriteAction("start explorer.exe")
            pressAction('enter')
        sleep(10)
        hotKeyAction('alt', 'f4')
        pressAction('esc')
        sleep(10)
        hotKeyAction('alt', 'f4')
        sleep(10)
        hotKeyAction('alt', 'f4')
        sleep(10)

    # find html file path and run build version batch file for Combined report and others
    def runBuildNumberBatchFile(self, path):
        if path == "C:\AutomationLogs\Combined":
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC01_Structure_Modeling" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_StructureModeling.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC02_HVAC_catalog" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_HVAC.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC03_Pipe_Modeling" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_PipeModeling.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC04_ShiCon_Essentials" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_ShipConEssentials.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC05_Pipe_Catalog" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_PipeCatalog.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC06_Production_Documentation" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_ProductionDocumentation.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC07_Installation_Uninstallation" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_Installation_Uninstallation.bat")
            os.chdir(currentWorkingDirectory)
        elif "TC08_Equipment_Catalog" in path:
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.cases/" + "BuildVersionNumber")
            else:
                os.chdir("../" + "BuildVersionNumber")
            os.system("Run_Build_Version_Number_EquipmentCatalog.bat")
            os.chdir(currentWorkingDirectory)
        else:
            print(path)

    # get version number, find html file which one will be updated and call logger file to embed version number in html
    def logVersionNumber(self, path):
        buildVersion = self.getBuildVersionNumberByBatch()
        htmlFile = ''
        for file in glob.glob("*.html"):
            if "log" in file:
                htmlFile = file
                break
        Logger.embedVersionNumberInLog(path, htmlFile, buildVersion)

    # define path of intallation testcase before version embed in log.html file
    def embedBuildNumber_Installation_Uninstallation(self):
        os.chdir("../" + "TC07_Installation_Uninstallation")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of structure modeling before version embed in log.html file
    def embedBuildNumber_StructureModeling(self):
        os.chdir("../" + "TC01_Structure_Modeling")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of hvac before version embed in log.html file
    def embedBuildNumber_HVAC(self):
        os.chdir("../" + "TC02_HVAC_catalog")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of pipe modeling before version embed in log.html file
    def embedBuildNumber_PipeModeling(self):
        os.chdir("../" + "TC03_Pipe_Modeling")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of ShipCon Essentials before version embed in log.html file
    def embedBuildNumber_ShipConEssentials(self):
        os.chdir("../" + "TC04_ShiCon_Essentials")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of pipe catalog before version embed in log.html file
    def embedBuildNumber_PipeCatalog(self):
        os.chdir("../" + "TC05_Pipe_Catalog")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of production documentation before version embed in log.html file
    def embedBuildNumber_ProductionDocumentation(self):
        os.chdir("../" + "TC06_Production_Documentation")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of equipment catalog before version embed in log.html file
    def embedBuildNumber_EquipmentCatalog(self):
        os.chdir("../" + "TC08_Equipment_Catalog")
        path = os.getcwd()
        path = path + "\\"
        self.logVersionNumber(path)

    # define path of combined latest report and embed version number in log.html file
    def embedBuildNumber(self):
        buildVersion = self.getBuildVersionNumberByBatch()
        os.chdir("C:/AutomationLogs/Combined")
        listOfDirectory = []
        for x in os.listdir('.'):
            listOfDirectory.append(x)
        listOfDirectory.sort(reverse=True)
        os.chdir("C:/AutomationLogs/Combined/" + listOfDirectory[0])
        path = os.getcwd()
        path = path + "\\"
        htmlFile = ''
        for file in glob.glob("*.html"):
            if "log" in file:
                htmlFile = file
                break
            elif "Log" in file:
                htmlFile = file
                break
        Logger.embedVersionNumberInLog(path, htmlFile, buildVersion)

    # Insert the installation path of SSI application in a text file
    def setPathFinder(self, path):
        f = open("path.txt", "w+")
        f.write(path)
        f.close()

    # Find the installation path of SSI application
    def getPathFinder(self):
        currentWorkingDirectory = os.getcwd()
        if "test.images" in currentWorkingDirectory:
            os.chdir("../test.cases/TC00_Run_All")
        else:
            os.chdir("../TC00_Run_All")
        for file in glob.glob("*.txt"):
            if "path" in file:
                f = open("path.txt", "r")
                content = f.read()
                f.close()
                os.chdir(currentWorkingDirectory)
                return content
        os.chdir(currentWorkingDirectory)
        return os.getcwd()

    # Remove the text file which contains the installation path of SSI application
    def deletePathFinderFile(self):
        currentWorkingDirectory = os.getcwd()
        if "test.images" in currentWorkingDirectory:
            os.chdir("../test.cases/TC00_Run_All")
        else:
            os.chdir("../TC00_Run_All")
        for file in glob.glob("*.txt"):
            if "path" in file:
                os.remove("path.txt")
        os.chdir(currentWorkingDirectory)

    # check and turn off the hardware acceleration if it is on
    def hardwareaccelerationOFF(self):
        if appears("acceleration_high", 20):
            mouseLeftClickOn("acceleration_high")
            typeWriteAction("GRAPHICSCONFIG")
            pressAction('enter')
            if appears("graphicsconfig_window", 20):
                mouseMoveTo("graphicsconfig_window")
                if appears("on_button", 10):
                    mouseLeftClickOn("on_button")
                    sleep(2)
                    mouseLeftClickOn("okay_gp_window")
                else:
                    raise AssertionError("on_button didn't found.")
            else:
                raise AssertionError("graphicsconfig_window didn't found.")
        elif appears("acceleration_unhigh", 20):
            mouseMoveTo("acceleration_unhigh")

    # check which test cases have been ran in all run suite
    def findTestCasesName(self, whichTestSuite):
        robotFile = self.findRobotFileContent(whichTestSuite)
        #line will save to postString after line number 9.
        postString = robotFile.split("\n")[9:]
        tcArray1 = []
        new_tcArray1 = []
        tcArray2 = []
        new_tcArray2 = []
        tcArray3 = []
        new_tcArray3 = []
        for line in postString:
            if '#' not in line and not line.startswith(' ') and not line.startswith('Creating'):
                if 'Second' in line:
                    tcArray2.append(line)
                elif 'Third' in line:
                    tcArray3.append(line)
                else:
                    tcArray1.append(line)

        for list in tcArray1:
            if list:
                new_tcArray1.append(str(list))
        for list in tcArray2:
            if list:
                new_tcArray2.append(str(list))
        for list in tcArray3:
            if list:
                new_tcArray3.append(str(list))

        if whichTestSuite == 'singlerun':
            return new_tcArray1
        elif whichTestSuite == '2timesrun':
            return new_tcArray1, new_tcArray2
        elif whichTestSuite == '3timesrun':
            return new_tcArray1, new_tcArray2, new_tcArray3

    # return the robot file which was ran from python
    def findRobotFileContent(self, testSuite):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.cases/TC00_Run_All")
        else:
            os.chdir("../../test.cases/TC00_Run_All")

        if testSuite == 'singlerun':
            for file in glob.glob("*.robot"):
                if "RunAll_SingleRun" in file:
                    robotFile = file
                    break
        elif testSuite == '2timesrun':
            for file in glob.glob("*.robot"):
                if "RunAll_2times" in file:
                    robotFile = file
                    break
        elif testSuite == '3timesrun':
            for file in glob.glob("*.robot"):
                if "RunAll_3times" in file:
                    robotFile = file
                    break
        else:
            raise AssertionError('No test suite has been found.')
        os.chdir(currentWorkingDirectory)
        f = open(robotFile, 'r')
        robotfileReader = f.read()
        return robotfileReader

    # create combined report after every suite and every number of times the suite ran
    def create_combined_report(self, timesRun):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")

        if timesRun is not None:
            self.singleRunBatchFiles(timesRun)
        os.chdir(currentWorkingDirectory)

    # batch file run for each times run of the test suite
    def singleRunBatchFiles(self, testCaseRun):
            os.system("XML_folderDelete.bat")
            sleep(10)

            testxml = ''
            stringXML = ''
            testcaseLogPath1 = ''

            for testCase in testCaseRun:
                tc = open("testCaseRun.txt", "w+")
                xml = open("testCaseXML.txt", "w+")
                if "Install Application" == testCase:
                    testxml = '08_Installation.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\08_Installation'
                    stringXML = stringXML + ' .\\' + testxml
                elif "Uninstalling New Installation" == testCase:
                    testxml = '08_Uninstallation.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\08_Uninstallation'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "Structure" in testCase:
                    testxml = '01_StructureModeling.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\01_StructureModeling'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "HVAC" in testCase:
                    testxml = '02_HVAC.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\02_HVAC'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "Pipe Modeling" in testCase:
                    testxml = '03_PipeModeling.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\03_PipeModeling'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "SC_Essentials" in testCase:
                    testxml = '04_SCEssential.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\04_SC_Essentials'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "Pipe Catalog" in testCase:
                    testxml = '05_PipeCatalog.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\05_PipeCatalog'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "Production" in testCase:
                    testxml = '06_ProductDocumentation.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\06_ProdDocumentation'
                    stringXML = stringXML +  ' .\\' + testxml
                elif "Equipment" in testCase:
                    testxml = '07_Equipment_Catalog.xml'
                    testcaseLogPath1 = 'C:\\AutomationLogs\\07_Equipment_Catalog'
                    stringXML = stringXML +  ' .\\' + testxml
                tc.write(testcaseLogPath1)
                tc.close()
                xml.write(testxml)
                xml.close()
                os.system("XML_collector.bat")
                sleep(1)

            pxml = open("generalXML.txt", "w+")
            pxml.write(stringXML)
            pxml.close()
            os.system("Blender.bat")
            sleep(5)

            testcaseLogPath2 = ''

            for testCase in testCaseRun:
                f = open("testCaseRun.txt", "w+")
                if "Install Application" == testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\08_Installation'
                elif "Uninstalling New Installation" == testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\08_Uninstallation'
                elif "Structure" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\01_StructureModeling'
                elif "HVAC" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\02_HVAC'
                elif "Pipe Modeling" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\03_PipeModeling'
                elif "SC_Essentials" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\04_SC_Essentials'
                elif "Pipe Catalog" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\05_PipeCatalog'
                elif "Production" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\06_ProdDocumentation'
                elif "Equipment" in testCase:
                    testcaseLogPath2 = 'C:\\AutomationLogs\\07_Equipment_Catalog'
                f.write(testcaseLogPath2)
                f.close()
                os.system("image_collector.bat")
                sleep(1)

    # store the project package in desired path for each module
    def storeProjectPackage(self, module):
        if (config.ToSaveProjectFile == 'No'):
            print('Project file has not been saved to certain folder for this run.')
        elif (config.ToSaveProjectFile == 'Yes'):
            content = ''
            currentWorkingDirectory = os.getcwd()
            if "test.images" in currentWorkingDirectory:
                os.chdir("../test.batches")
            else:
                os.chdir("../../test.batches")
            for file in glob.glob("*.txt"):
                if "projectfilepath" in file:
                    f = open("projectfilepath.txt", "r")
                    content = f.read()
                    f.close()
                    os.chdir(currentWorkingDirectory)
                    break
            os.chdir(currentWorkingDirectory)

            if content == '':
                content = 'FirstRun'

            storagePath = config.ProjectPath
            storagePath = storagePath + '/' + content
            typeWriteAction('SCPACKAGEPROJECT')
            sleep(2)
            pressAction('enter')
            if appears('package_window', 30):
                if appears('package_browse_button', 5):
                    mouseLeftClickOn('package_browse_button')
                    if appears('browser_adrs_dropdown', 20):
                        mouseLeftClickOn('browser_adrs_dropdown')
                        typeWriteAction(storagePath)
                        pressAction('enter')
                        if appears('project_fileName_region', 5):
                            mouseLeftClickOn('project_fileName_blank', 'project_fileName_region')
                            typeWriteAction(module)
                            pressAction('enter')
                        else:
                            raise AssertionError('project_fileName_region not found.')
                    else:
                        raise AssertionError('browser_adrs_dropdown not found.')
                else:
                    raise AssertionError('package_browse_button not found.')
            else:
                raise AssertionError('package_window not found.')
            if appears('package_ok', 5):
                mouseLeftClickOn('package_ok')
                if appears('file_already_exists', 5):
                    pressAction('enter')
                sleep(5)
                if appears('package_warning', 5):
                    pressAction('enter')
                while 1:
                    if appears("package_message_window"):
                        break

                if appears('package_message_ok', 5):
                    mouseLeftClickOn('package_message_ok')
                else:
                    raise AssertionError('package_message_ok not found.')
            else:
                raise AssertionError('package_ok not found.')
        else:
            print('Please set a legal variable in config.py for ToSaveProjectFile variable.')

    # to set the file path according to number of run time
    def updateProjectFilePath(self, filePath):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")

        projectFilePath = open("projectfilepath.txt", "w+")
        projectFilePath.write(filePath)
        projectFilePath.close()

        os.chdir(currentWorkingDirectory)