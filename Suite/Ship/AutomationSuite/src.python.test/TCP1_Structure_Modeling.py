"""
*** TCP1_Structure_Modeling.py ***

Automation script for Structure Modeling 

Covers the following sections:
- Product Hierarchy
- Construction Lines
- Plate Parts
- Detailing
- Profile Parts
- Curved Plates
- Plank Parts
- Corrugated Plates
- Custom Plate Parts
- Twisted Stiffeners
- Standard Parts and Standard Assemblies
- General Modeling

"""
import os, pyautogui

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

from SSILearningVerifierUtil import SSILearningVerifierUtil

from Structure_modeling_util import create_planar_groups, open_U03_drawing, select_new_plate_option, \
    add_corner_treatment, select_new_stiffener_option, select_new_faceplate_option, select_new_custom_plate_option, no_markline

from time import sleep

from logger_extended import logger_extended

from IOUtil import goDownUntilAppears, mouseMoveTo, mouseMoveToCoordinates, mouseLeftClickOn, mouseRightClickOn, mouseClickAppears, mouseDoubleClickOn, hotKeyAction

from screenObjUtil import exists, appears

from SSIUtil import off_coordinates, maximize_layer_name_column, zoom, change_viewpoint_from_aft_prt_up, maximize, free, zoom_to_ssi_u03_p01, line, conceptual_visual, wireframe2d_visual, reset_viewport, zoom_to_ssi_u03_p07, \
    open_drawing_u03, open_drawing_from_hull, change_viewpoint_plan_looking_down, open_drawing_from_u03, change_viewpoint_body_looking_fwd, \
    change_view, change_viewpoint_angle, open_navigator, change_viewpoint_from_aft_stbd_up, rectang, change_viewpoint_from_fwd_stbd_up, \
    mirror_line, change_viewpoint_from_stbd_up, open_product_hierarchy, open_manager, check_conventions, open_layer_properties, activate_wcs, \
    open_mgi_window, set_layer

import Logger

class TCP1_Structure_Modeling(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP01_StructureModeling')

    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC01_Structure_Modeling")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_structure")
        obj.launch_app()
        obj1.project_deployer(module="Structure")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()

    def register_project(self):
        maximize()
        obj= AppUtil()
        obj.ribbon_load("hull_and_structure_tab")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("structure_deploy", "NewDeploy_Structure")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_Structure")

    def browse_naming_convention(self):
        self.logger.info('Start:\t Checking Module- browse_naming_convention')
        off_coordinates()
        open_manager()
        if appears("general_button"):
            mouseDoubleClickOn('general_button')
        else:
            raise AssertionError("general_button did not appear")
        mouseLeftClickOn('naming_convention_button')
        if appears("naming_convention_window"):
            self.logger.info("Naming conventions window has appeared")
            mouseLeftClickOn("pipe_support_click")
            for i in range(0, 12):
                pyautogui.press("enter")
            mouseLeftClickOn("parts_option", "parts_list_menu")
            mouseLeftClickOn("parts_plus", "parts_plus_region")
            for i in range(0, 12):
                pyautogui.press("enter")
            mouseLeftClickOn("default_plate")
            if appears('assembly_level'):
                mouseLeftClickOn("assembly_level")
                pyautogui.press('tab')
                sleep(2)
                pyautogui.press('tab')
                sleep(2)
                pyautogui.press('left')
                sleep(2)
                check_conventions()
            else:
                raise AssertionError("Assembly level not visible")
        else:
            raise AssertionError("Naming conventions window not available")
        self.logger.info('End:\t Checking Module- browse_naming_convention')

    # Product Hierarchy and Nesting
    def create_nest_test(self):
        self.logger.info('Start:\t Checking Module- create_nest_test')
        open_navigator()
        if appears("navigator_u02_node"):
            mouseLeftClickOn("navigator_u02_node")
        else:
            raise AssertionError("navigator_u02_node did not open")
        mouseLeftClickOn("navigator_nest_node", "navigator_nest_node")
        if appears("u02_folder_region"):
            self.logger.info("U02 folder visible")
            mouseLeftClickOn("u02_folder_region")
            mouseLeftClickOn("new_button")
            if appears("new_plate_nest_drawing_window"):
                self.logger.info("New Plate Window is visible")
                pyautogui.typewrite('Nest-Test', interval=0.1)
                mouseLeftClickOn("new_plate_drawing_ok_button")
                if appears("pl12_region"):
                    mouseLeftClickOn("pl12", "pl12_region")
                    mouseLeftClickOn("select_plate_stock_ok_button")
                    sleep(5)
                    sleep(5)
                    if appears("save_warning"):
                        pyautogui.press('enter')
                    if appears("nest_test_drawing_tab"):
                        self.logger.info("Nest test created")
                        pyautogui.hotkey('ctrl','s')
                        pyautogui.hotkey('ctrl','s')
                        pyautogui.hotkey('ctrl','s')
                        pyautogui.hotkey('ctrl','s')
                        sleep(5)
                    else:
                        raise AssertionError("Nest Test not created")
                else:
                    raise AssertionError("Plate stock window not visible")
            else:
                raise AssertionError("New Plate Window is not visible")
        else:
            raise AssertionError("U02 folder not visible")
        self.logger.info('End:\t Checking Module- create_nest_test')


    def browse_product_hierarchy(self):
        self.logger.info('Start:\t Checking Module- browse_product_hierarchy')
        sleep(2)
        mouseLeftClickOn("profile_plots_button")
        mouseDoubleClickOn("plate_nest")
        mouseDoubleClickOn("insert_part_button")
        if appears("save_warning",10):
            pyautogui.press('enter')

        if appears("product_hierarchy_drop_down_button"):
            mouseLeftClickOn("product_hierarchy_drop_down_button")
            sleep(2)
            mouseDoubleClickOn("cancel_button")
            mouseMoveToCoordinates(500, 400)
            mouseDoubleClickOn("cancel_button")
            free()
            pyautogui.press('esc')
            self.logger.info("Product Hierarchy and Nesting checked")
        else:
            raise AssertionError("Product Hierarchy drop down not visible")
        self.logger.info('End:\t Checking Module- browse_product_hierarchy')

    def check_keep_autonumber(self):
        self.logger.info('Start:\t Checking Module- check_keep_autonumber')
        open_product_hierarchy()
        if appears("options_button"):
            self.logger.info("Product hierarchy window appeared")
            mouseDoubleClickOn("options_button")
        else:
            raise AssertionError("options_button did not appear")
        if appears("naming_option"):
            mouseMoveTo("naming_option")
            if appears("structure_option"):
                mouseMoveTo("structure_option")
                if appears("keep_autonumber_button"):
                    self.logger.info("Keep autonumber button was found and checked")
                    mouseLeftClickOn("keep_autonumber_button")
                    mouseLeftClickOn("product_hierarchy_window")
                    mouseLeftClickOn("close_button_high")
                    free()
                else:
                    raise AssertionError("Keep Autonumber option not visible")
            else:
                raise AssertionError("Structure option not visible")
        else:
            raise AssertionError("Naming option not visible")
        self.logger.info('End:\t Checking Module- check_keep_autonumber')


        # Activate Automatic Beveling

    def activate_automatic_beveling(self):
        self.logger.info('Start:\t Checking Module- activate_automatic_beveling')
        sleep(1)
        open_manager()
        mouseLeftClickOn("structure_button")
        if appears("plates_option"):
            mouseMoveTo("plates_option")
        else:
            raise AssertionError("Plates option not visible")
        if appears("bevel_settings_button"):
            mouseLeftClickOn("bevel_settings_button")
        else:
            raise AssertionError("Bevel Setting option not visible")
        if appears("bevel_settings_window"):
            self.logger.info("Bevel settings window appeared")
            pyautogui.press("enter")
            sleep(.5)
            pyautogui.hotkey('alt', 'f4')
            mouseLeftClickOn("structure_ribbon_button")
            if appears("construction_lines_panel"):
                mouseLeftClickOn("construction_lines_panel")
                mouseMoveTo("show_bevel_drop_down_button")
                # mouseLeftClickOn("show_bevel_drop_down", "show_bevel_drop_down_button")
                mouseLeftClickOn("hovered_drop_down")
                if appears("show_bevel_drop_down_list",5):
                    self.logger.info("Bevel list visible")
                free()
            else:
                raise AssertionError("Contruction lines ribbon not visible")
        else:
            raise AssertionError("Bevel Settings not visible")
        self.logger.info('End:\t Checking Module- activate_automatic_beveling')


    # Create Planar Groups from Hull Drawing
    def create_planar_group_from_hull_drawing(self):
        self.logger.info('Start:\t Checking Module- create_planar_group_from_hull_drawing')
        free()
        pyautogui.click()
        pyautogui.press('esc')
        open_navigator()
        mouseLeftClickOn("ssi_node_unhigh")
        mouseLeftClickOn("new_unit")
        sleep(2)
        pyautogui.typewrite(['U', '0', '3'], interval=0.2)
        sleep(1)
        pyautogui.press('enter')
        if appears('warning',5):
            pyautogui.press('enter')
            if appears('cancel_button',5):
                mouseLeftClickOn('cancel_button')
        sleep(5)
        open_navigator()
        mouseLeftClickOn("hull_node")
        if appears("u03_surface"):
            mouseLeftClickOn("u03_surface")
            mouseLeftClickOn("u03_surface_open")
            if appears("hull_drawing_tab"):
                self.logger.info("Hull drawing tab appeared")
                change_viewpoint_plan_looking_down()
                pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
                pyautogui.press("enter")
                pyautogui.typewrite('32377.4765,4234.2796', interval=0.2)
                pyautogui.press("enter")
                pyautogui.click(button='right', clicks=2, interval=0.25)
                if appears("markline"):
                    no_markline(point1="33925.3855", point2="5748.9332")
                    pyautogui.press("enter")
                else:
                    pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.press('esc')
                sleep(2)
                pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
                pyautogui.press("enter")
                pyautogui.typewrite(
                    ['3', '3', '9', '2', '5', '.', '3', '8', '5', '5', ',', '5', '7', '4', '8', '.',
                     '9', '3', '3', '2'], interval=0.25)
                pyautogui.press("enter")
                pyautogui.click(button='right', clicks=2, interval=0.25)
                if appears("properties_option"):
                    self.logger.info("Properties option appeared")
                    mouseLeftClickOn("properties_option", "properties_option")
                if appears("extracted_line_properties"):
                    sleep(3)
                    pyautogui.hotkey('ctrl', '1')
                    mouseLeftClickOn("structure_ribbon_button", "structure_ribbon_button")
                    mouseLeftClickOn("utilities_pane", "utilities_pane")
                    if appears("create_planar_group_button_region"):
                        mouseLeftClickOn("create_planar_group_drop_down_arrow","create_planar_group_button_region")
                        if appears("create_planar_group_button"):
                            mouseLeftClickOn("create_planar_group_button","create_planar_group_button")
                            if appears("new_deck_group_window"):
                                self.logger.info("New deck group window appeared")
                                if appears("apply_to_all_region",10):
                                    mouseLeftClickOn("apply_to_all_region")
                                    sleep(1)
                                pyautogui.press("enter")
                                if appears('warning',5):
                                    pyautogui.press('enter')
                                    sleep(2)
                                    pyautogui.hotkey('alt', 'f4')
                                else:
                                    if appears("planar_group_created_message"):
                                        self.logger.info("Planar Group Created")
                                        pyautogui.hotkey('alt', 'f4')
                                    else:
                                        raise AssertionError("Planar Group not Created")
                            else:
                                raise AssertionError("New Deck window not visible")
                        else:
                            raise AssertionError("Create Planar group button not visible")
                    else:
                        raise AssertionError("Utilities pane not visible")
                else:
                    raise AssertionError("Properties option not visible ")
            else:
                raise AssertionError("Hull drawing didn't appear")
        else:
            raise AssertionError("U03_SURFACE Didn't appear")
        self.logger.info('End:\t Checking Module- create_planar_group_from_hull_drawing')


    def create_planar_group_from_other_type(self):
        self.logger.info('Start:\t Checking Module- create_planar_group_from_other_type')
        sleep(2)
        open_navigator()
        mouseLeftClickOn("u03_node")
        mouseLeftClickOn("structure_node")
        mouseLeftClickOn("u03_ttop2")
        mouseLeftClickOn("u03_ttop_open")
        if appears("warning2",5):
            pyautogui.press('enter')
        sleep(3)
        if appears("u03_ttop_tab"):
            self.logger.info("U03 TTOP tab appeared")
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(10)
            line("25000","0","40000","0")
            sleep(10)
            sleep(1)
            pyautogui.press("esc")
            pyautogui.press("esc")
            pyautogui.press("esc")
            reset_viewport()
            if appears("home_button", 5):
                mouseLeftClickOn("home_button")
            mouseLeftClickOn("layer_button")
            mouseLeftClickOn("layer_properties_button")
            free()
            sleep(2)
            if appears("new_layer_button"):
                self.logger.info("Layer properties manager appeared")
                mouseLeftClickOn("new_layer_button")
                sleep(1)
                pyautogui.typewrite(['_', 'C', 'L', '-', 's', 'e', 'c', 't', 'i', 'o', 'n'], interval=0.25)
                pyautogui.press("enter")
                self.logger.info("_CL-section layer was added")
                mouseLeftClickOn("layer_properties_manager_close")
                sleep(1)
                free()
                sleep(1)
                pyautogui.press("esc")
                pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
                pyautogui.press("enter")
                pyautogui.typewrite(['2', '5', '0', '0', '0', ',', '0'], interval=0.25)
                pyautogui.press("enter")
                pyautogui.click(button='right', clicks=2, interval=0.25)
                if appears("properties_option"):
                    self.logger.info("Properties option appeared")
                    mouseLeftClickOn("properties_option")
                    mouseLeftClickOn("previous_layer")
                    pyautogui.click()
                    mouseLeftClickOn("new_layer")
                    self.logger.info("Layer was changed")
                    sleep(1)
                    mouseLeftClickOn("extracted_line_properties")
                    mouseLeftClickOn("layer_properties_manager_close")
                    self.logger.info("Successfully moved to new layer")
                    mouseLeftClickOn("structure_ribbon_button")
                    mouseLeftClickOn("utilities_pane")
                    if appears("create_planar_group_button_region"):
                        mouseLeftClickOn("create_planar_group_drop_down_arrow","create_planar_group_button_region")
                        mouseLeftClickOn("create_planar_group_button")
                        if appears("select_planar_group"):
                            mouseLeftClickOn("lngbhd_option_button", "lngbhd_option")
                            pyautogui.press('enter')
                            sleep(2)
                            if appears("new2"):
                                free()
                                if appears("starboard_region7", 4):
                                    if appears("starboard_region6", 4):
                                        pyautogui.press('tab')
                                        pyautogui.press('tab')
                                        pyautogui.press('down')
                                else:
                                    if appears("starboard_region_4", 4):
                                        pyautogui.press('tab')
                                        pyautogui.press('down')
                                    elif appears("starboard_region5", 4):
                                        pyautogui.press('tab')
                                        pyautogui.press('up')
                                        free()
                                    if appears("starboard_region6", 4):
                                        pyautogui.press('tab')
                                        pyautogui.press('down')
                                mouseLeftClickOn("apply_to_all_selected","apply_to_all_selected_region")
                                sleep(1)
                                pyautogui.press('enter')
                                sleep(4)
                                if appears("planar_group_text"):
                                    pyautogui.hotkey('alt', 'f4')
                                    print("Planar Group U03_CL-SECTION created")
                                else:
                                    raise AssertionError("Planar Group U03_CL-SECTION was not created")
                                sleep(3)
                            else:
                                raise AssertionError("New lngbhd menu did not appear")
                        else:
                            raise AssertionError("Select planar group menu didn't appear")
                    else:
                        raise AssertionError("Create Planar group button not visible")
                else:
                    raise AssertionError("Properties option not visible")
            else:
                raise AssertionError("layer properties manager didn't appear")
        else:
            raise AssertionError("u03_TTOP did not appear")
        self.logger.info('End:\t Checking Module- create_planar_group_from_other_type')

    def editing_planar_group_setting(self):
        self.logger.info('Start:\t Checking Module- editing_planar_group_setting')
        sleep(2)
        open_navigator()
        mouseLeftClickOn("u03_ttop_properties")
        if appears("deck_properties"):
            self.logger.info("Deck properties appeared")
            if appears("default_thikness_up_region",4):
                mouseLeftClickOn("default_thikness_up","default_thikness_up_region")
            if appears("default_markside_up_region",4):
                mouseLeftClickOn("default_thikness_up","default_markside_up_region")
            sleep(1)
            pyautogui.press("enter")
            self.logger.info("Successfully checked editing planar groups")
        else:
            raise AssertionError("deck properties didn't appear")

        self.logger.info('End:\t Checking Module- editing_planar_group_setting')


    def understanding_planar_groups(self):
        self.logger.info('Start:\t Checking Module- understanding_planar_groups')
        if appears("shipcon_navi"):
            self.logger.info("Navigator window properly opened")
            mouseLeftClickOn("u03_ttop_open")
            if appears("save_caution",5):
                pyautogui.press('enter')
            sleep(5)
            if appears("u03_ttop_tab"):
                self.logger.info("U03 TTOP tab appeared")
                mouseLeftClickOn('main_panel_new', 'main_panel_new')
                mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
                if appears("cl_section_node"):
                    self.logger.info("Cl-section node appeared")
                    mouseLeftClickOn("cl_section_node")
                    mouseLeftClickOn("u03_ttop_open")
                    if appears("save_caution",5):
                        pyautogui.press('enter')
                    sleep(3)
                    free()
                    if appears("sc_utilites"):
                        mouseLeftClickOn("sc_utilites")
                        mouseLeftClickOn("visual_button")
                        mouseLeftClickOn("activate_ucs")
                        if appears("activate_ucs_menu"):
                            mouseLeftClickOn("activate_ucs_ok")
                            self.logger.info("Activate UCS checking successful")
                            sleep(2)
                            mouseLeftClickOn("home_button")
                            mouseLeftClickOn("layer_button")
                            mouseLeftClickOn("layer_properties_button")
                            sleep(2)
                            if appears("layer_properties_manager_close",5):
                                mouseLeftClickOn("layer_properties_manager_close")
                            elif appears("layer_properties_manager_close_unhighlighted_button", 5):
                                mouseLeftClickOn("layer_properties_manager_close_unhighlighted_button")
                            else:
                                raise AssertionError("layer_properties_manager_close button did not appear")
                            free()
                            free()
                        else:
                            raise AssertionError("activate ucs menu didn't appear")
                    else:
                        raise AssertionError("sc_utilites didnt appear")
                else:
                    raise AssertionError("Cl section node didn't appear")
            else:
                raise AssertionError("U03 tab didn't appear")
        else:
            raise AssertionError("Navigation didn't appear")
        self.logger.info('End:\t Checking Module- understanding_planar_groups')


    def move_planar_group(self):
        self.logger.info('Start:\t Checking Module- move_planar_group')
        sleep(2)
        reset_viewport()
        activate_wcs()
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("utilities_pane")
        if appears("create_planar_group_button_region"):
            mouseLeftClickOn("create_planar_group_drop_down_arrow", "create_planar_group_button_region")
            mouseLeftClickOn("move_planar_group_button")
            if appears("move_planar_group"):
                self.logger.info("Move planar group dialog appeared")
                sleep(1)
                pyautogui.typewrite(['1', '0', '0', '0'], interval=0.25)
                if appears("move_planar_group_port_region",5):
                    mouseLeftClickOn("move_planar_group_port_select","move_planar_group_port_region")
                sleep(1)
                pyautogui.press("enter")
                sleep(3)
                mouseLeftClickOn("utilities_pane")
                sleep(1)
                mouseLeftClickOn("create_planar_group_drop_down_arrow2", "create_planar_group_button_region1")
                mouseLeftClickOn("move_planar_group_button2")
                sleep(2)
                pyautogui.typewrite(['1', '0', '0', '0'], interval=0.25)
                mouseLeftClickOn("move_planar_group_standard_button","move_planar_group_standard_region")
                sleep(1)
                pyautogui.press("enter")
                pyautogui.hotkey('ctrl','s')
                sleep(2)
                self.logger.info("Move planar group successfull")
            else:
                raise AssertionError("move planar group menu didn't appear")
        else:
            raise AssertionError("Create Planar group button not visible")
        self.logger.info('End:\t Checking Module- move_planar_group')


    def exercise_rm_one(self):
        self.logger.info('Start:\t Checking Module- exercise_rm_one')
        sleep(3)
        open_navigator()
        mouseLeftClickOn("u03_node")
        mouseLeftClickOn("hull_node")
        if appears("u03_surface", 10):
            mouseLeftClickOn("u03_surface")
            mouseLeftClickOn("u03_surface_open")
        elif appears("u03_surface_high", 10):
            mouseLeftClickOn("u03_surface_high")
            mouseLeftClickOn("u03_surface_open")
        if appears("warning3_no", 30):
            pyautogui.press('enter')
        change_viewpoint_plan_looking_down()
        create_planar_groups(x1="36000", y1="5848", x2="36005.8076", y2="357.0256" , name="Frame 60")
        sleep(1)
        create_planar_groups(x1="36600", y1="5848", x2="36600.2305", y2="935.5498" , name="Frame 61")
        sleep(1)
        create_planar_groups(x1="37200", y1="5848" , name="Frame 62")
        sleep(1)
        create_planar_groups(x1="37800", y1="5848" , name="Frame 63")
        sleep(1)
        create_planar_groups(x1="38400", y1="5848" , name="Frame 64")
        sleep(1)

        create_planar_groups(x1="33218", y1="750" , name="LngBhd 750p")
        sleep(1)
        create_planar_groups(x1="35700.4661", y1="2506.2719" , name="LngBhd 2500p")
        sleep(1)

        create_planar_groups(x1="38154", y1="6950" , name="Main Deck")
        sleep(1)
        self.logger.info("Excercise Done")
        pyautogui.hotkey('ctrl','s')
        self.logger.info('End:\t Checking Module- exercise_rm_one')

    def exercise_r002(self):
        maximize()
        open_navigator()
        mouseLeftClickOn("u03_node2")
        if appears("structure_node2"):
            mouseLeftClickOn("structure_node2")
        else:
            raise AssertionError("structure_node2 did not appear")
        if appears("u03_f61"):
            mouseDoubleClickOn("u03_f61")
        else:
            raise AssertionError("u03_f61 did not appear")
        if appears("u03_f61_tab"):
            mouseLeftClickOn("u03_f61_tab")
        else:
            raise AssertionError("u03_f61_tab did not appear")
        free()
        sleep(3)
        pyautogui.hotkey('ctrl', '9')
        sleep(5)
        pyautogui.press('enter')
        pyautogui.typewrite('SCMARK',.3)
        sleep(2)
        pyautogui.press('esc')
        sleep(2)
        pyautogui.typewrite('SCMARK',.3)
        sleep(2)
        pyautogui.press('enter')
        if appears("mgi_deck_node",10):
            mouseDoubleClickOn("mgi_deck_node")
        else:
            raise AssertionError("mgi_deck_node did not appear")
        if appears("mgi_u03_maindeck_node",10):
            self.logger.info("mgi_u03_maindeck_node appeared")
            mouseDoubleClickOn("mgi_u03_maindeck_node")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            mouseLeftClickOn("mgi_ok_button")
        else:
            raise AssertionError("mgi_u03_maindeck_node did not appear")
        reset_viewport()
        pyautogui.typewrite('select', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-6137.6842,2754.1318', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('offset', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('500', .2)
        pyautogui.press('enter')
        pyautogui.moveTo(760,521)
        sleep(1)
        pyautogui.click()
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('line', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4000', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-4000,4000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCCONVERTTOCL', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')

        sleep(2)
        free()
        pyautogui.typewrite('select', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('copy',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4000', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-7000,4500', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        reset_viewport()

        free()
        pyautogui.typewrite('select', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4500', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.moveTo(748,359)
        # pyautogui.moveTo(744,358)
        pyautogui.click()
        sleep(1)
        # pyautogui.moveTo(756,358)
        pyautogui.moveTo(776,359)
        sleep(1)
        pyautogui.typewrite('3500',.2)
        sleep(1)
        pyautogui.press('enter')
        if appears("update_related_objects_window",10):
            mouseLeftClickOn("update_related_objects_uncheckall_button")
            mouseLeftClickOn("update_related_objects_ok_button")
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('esc')
            free()
            pyautogui.click()
            pyautogui.press('esc')
            pyautogui.press('esc')
        else:
            raise AssertionError('update_related_objects_window did not appear')
        free()
        pyautogui.typewrite('select', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,4000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.moveTo(461,407)
        # pyautogui.moveTo(470,402)
        pyautogui.click()
        sleep(1)
        pyautogui.moveTo(440, 402)
        sleep(1)
        pyautogui.typewrite('3500', .2)
        pyautogui.press('enter')
        free()
        pyautogui.click()
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.hotkey('ctrl','s')
        sleep(5)

    def exercise_r003(self):
        free()
        pyautogui.typewrite('SCMARK', .2)
        pyautogui.press('enter')
        mouseDoubleClickOn("mgi_deck_node")
        pyautogui.press('down')
        pyautogui.press('right')
        sleep(1)
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('space')
        sleep(1)

        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('space')
        mouseDoubleClickOn("mgi_lngbhd_node")
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('space')
        mouseLeftClickOn("mgi_ok_button")
        sleep(4)
        free()
        pyautogui.typewrite('delete', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-6501.0207,6895.3503', .2)
        pyautogui.press('enter', 2, 0.2)
        sleep(2)
        free()
        pyautogui.typewrite('SCCREATEPLATE',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-364,716',.2)
        pyautogui.press('enter')

        if appears("plate_properties"):
            self.logger.info("Plate properties wondow appeared")
            sleep(1)
            if appears("pl10_2", 40):
                mouseLeftClickOn("pl10_2")
            pyautogui.press('enter')
        else:
            raise AssertionError("plate_properties did not appear")
        self.logger.info('End:\t Checking Module- new_plate_creation')
        free()
        pyautogui.typewrite('SCADDCORNERTREATMENT',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-387.3911,1391.3121',.2)
        pyautogui.press('enter', 2, 0.1)
        if appears("corner_treatment_menu", 50):
            if appears("r50", 20):
                mouseLeftClickOn("r50")
            else:
                mouseLeftClickOn("r50_new")
            pyautogui.press('enter')
        else:
            raise AssertionError("corner_treatment_menu did not appear")
        sleep(1)
        pyautogui.typewrite('-750,-0.0051',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('0,-0.0051',.2)
        pyautogui.press('enter', 2, 0.1)
        sleep(2)
        reset_viewport()
        free()
        reset_viewport()
        pyautogui.typewrite('SCPLATEFLANGE',.2)
        pyautogui.press('enter')
        sleep(1)
        # pyautogui.typewrite('-435.6163,1400.0704',.2)
        pyautogui.typewrite('-387.3911,1391.3121',.2)
        pyautogui.press('enter')

        if appears("select_flange"):
            mouseLeftClickOn("fl_50x10")
            pyautogui.press('tab',presses=8,interval=.2)
            pyautogui.typewrite('40', 0.1)
            pyautogui.press('enter')
        else:
            raise AssertionError("select_flange did not appear")
        if appears("foldline_warning", 50):
            pyautogui.press('enter')

        free()
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-387.3911,-2.7348', interval=0.2)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        mouseLeftClickOn("part_information_button")
        sleep(1)
        mouseRightClickOn('piecemark')
        mouseLeftClickOn("zoom_to_part")
        sleep(1)
        pyautogui.press('esc')
        free()
        pyautogui.press('esc', 3, 0.2)

        free()
        pyautogui.typewrite('SCADDPPGREEN', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-387.3911,-2.7348', .2)
        pyautogui.press('enter')
        sleep(3)
        free()
        # pyautogui.moveTo(648,787)
        pyautogui.moveTo(643,793)
        sleep(2)
        pyautogui.click()
        sleep(2)
        # pyautogui.moveTo(943,787)
        pyautogui.moveTo(950,793)
        sleep(2)
        pyautogui.click()
        free()
        if appears("select_segment_for_green", 50):
            # pyautogui.moveTo(787,787)
            pyautogui.moveTo(796,795)
            sleep(3)
            pyautogui.click()
            # pyautogui.typewrite('-375,-0.0081',.2)
            # pyautogui.press('enter')
        else:
            raise AssertionError("select_segment_for_green did not appear")
        if appears("add_green_menu",10):
            mouseLeftClickOn("_plus50")
            mouseLeftClickOn("remove_green_ok")
        else:
            raise AssertionError("_plus50 did not appear")
        free()
        pyautogui.typewrite('SCMIRRORCL',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('0.1642,770.8841',.2)
        pyautogui.press('enter', 2, 0.1)
        sleep(5)
        free()
        pyautogui.typewrite('circle',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-450,400',.2)
        pyautogui.press('enter')
        sleep(3)
        pyautogui.typewrite('50',.2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCADDOBJECTSTOSTRUCTPART',.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-747.4444,709.3970',.2)
        pyautogui.press('enter', 2, 0.1)
        sleep(1)
        pyautogui.typewrite('-450,450',.2)
        pyautogui.press('enter')
        sleep(3)
        pyautogui.press('enter')
        if appears("update_related_objects_window", 10):
            pyautogui.press('enter')
        else:
            raise AssertionError("update_related_objects_window did not appear")
        pyautogui.hotkey('ctrl', 's')
        sleep(5)

    def exercise_r004(self):
        sleep(5)
        free()
        pyautogui.typewrite('SCUCSLIST',.2)
        pyautogui.press('enter')
        if appears("activate_ucs_menu",10):
            mouseLeftClickOn("activate_from_object")
            pyautogui.typewrite('-747.4444,709.3970', interval=0.2)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('enter')
        else:
            raise AssertionError("activate_ucs_menu did not appear")
        free()
        pyautogui.typewrite('SCDATUM', .1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-747.4444,709.3970', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("datum_menu"):
            self.logger.info("Datum menu appeared")
            mouseLeftClickOn("buttocks", "buttocks_region")
            sleep(1)
            mouseLeftClickOn("datum_scroll")
            pyautogui.click(clicks=6)
            mouseLeftClickOn("waterlines", "waterlines_region")
            sleep(1)
            pyautogui.press('enter')
        else:
            raise AssertionError("datum_menu did not appear")
        free()
        pyautogui.typewrite('line',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-660,1000',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('#-660,100',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        free()
        pyautogui.typewrite('line',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-90,1000',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('#-90,100',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCSTIFF',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-660,1000',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-747.4444,709.3970', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("stiff_properties",10):
            mouseLeftClickOn("aft_side_stiff")
            mouseLeftClickOn("strbrd")

            mouseLeftClickOn("stock_drop_down")
            if appears("hp160x8", 5):
                mouseLeftClickOn("hp160x8")
            elif appears("bulb_flat", 5):
                mouseLeftClickOn("bulb_flat")
                pyautogui.press('right')
                mouseLeftClickOn("hp160x8")
            else:
                pyautogui.press('esc')
                pyautogui.press('esc')
                free()
            pyautogui.press('tab',presses=6,interval=.2)
            pyautogui.press('a')
            pyautogui.press('tab')
            pyautogui.press('a')
            mouseLeftClickOn("stiffeners_ok")
            if appears("update_related_objects_window", 10):
                pyautogui.press('enter')
            else:
                raise AssertionError("update_related_objects_window did not appear")
        else:
            raise AssertionError("stiff_properties did not appear")
        free()
        pyautogui.typewrite('SCSTIFF', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-90,1000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-747.4444,709.3970', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("stiff_properties", 10):
            mouseLeftClickOn("aft_side_stiff")
            mouseLeftClickOn("stock_drop_down")
            if appears("hp160x8", 5):
                mouseLeftClickOn("hp160x8")
            elif appears("bulb_flat", 5):
                mouseLeftClickOn("bulb_flat")
                pyautogui.press('right')
                mouseLeftClickOn("hp160x8")
            else:
                pyautogui.press('esc')
                pyautogui.press('esc')
                free()
            pyautogui.press('tab', presses=6, interval=.2)
            pyautogui.press('f')
            pyautogui.press('tab')
            pyautogui.press('f')
            mouseLeftClickOn("stiffeners_ok")
            if appears("update_related_objects_window", 10):
                pyautogui.press('enter')
            else:
                raise AssertionError("update_related_objects_window did not appear")
        else:
            raise AssertionError("stiff_properties did not appear")
        change_viewpoint_from_aft_prt_up()
        free()
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-387.3911,-2.7348', interval=0.2)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        mouseLeftClickOn("part_information_button")
        sleep(1)
        mouseRightClickOn('piecemark')
        mouseLeftClickOn("zoom_to_part")
        sleep(1)
        pyautogui.press('esc')
        pyautogui.press('esc')
        free()
        pyautogui.typewrite('line',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-660,870',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-690,870',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('line',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-660,635',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-690,635',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('line',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-660,375',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-690,375',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCEXTRADDCUTOUT',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-468.2162,528.8703',.2)
        pyautogui.press('enter')
        if appears("insert_cutout_menu",10):
            mouseDoubleClickOn("insert_cutout_filter")
            pyautogui.typewrite('R30')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('enter')
            free()
            pyautogui.typewrite('130,-10',.2)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(927,328)
            sleep(2)

            pyautogui.click()
            free()
            pyautogui.typewrite('365,-10',.2)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(927,478)
            pyautogui.click()
            sleep(2)
            free()
            pyautogui.typewrite('625,-10',.2)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(927,635)
            sleep(2)
            pyautogui.click()
            pyautogui.press('enter')
            if appears("update_related_objects_window", 10):
                pyautogui.press('enter')
            else:
                raise AssertionError("update_related_objects_window did not appear")
            pyautogui.press('esc')
            pyautogui.hotkey('ctrl','s')
            sleep(5)
        else:
            raise AssertionError("insert_cutout_menu did not appear")

    def exercise_r005(self):
        free()
        pyautogui.typewrite('SCNAVIGATE',.2)
        pyautogui.press('enter')
        if appears("curved_option", 10):
            mouseLeftClickOn("curved_option")
            mouseLeftClickOn("new_curved")
            sleep(1)
            pyautogui.typewrite('_EXERCISES SHELL', interval=.1)
            pyautogui.press('enter')
        sleep(5)
        if appears("u03_exercise_shell"):
            self.logger.info('u03_exercise_shell appeared')
            sleep(10)
            sleep(10)
            pyautogui.hotkey('ctrl', 's')
        else:
            raise AssertionError("u03_exercise_shell did not appear")
        sleep(5)
        sleep(5)
        free()
        pyautogui.typewrite('SCNAVIGATE', .2)
        pyautogui.press('enter')
        if appears("hull_node", 50):
            mouseLeftClickOn("hull_node")
            if appears("u03_shell_from_hull", 50):
                mouseDoubleClickOn("u03_shell_from_hull")
            if appears("u03_shell_high", 50):
                mouseDoubleClickOn("u03_shell_high")
        else:
            raise AssertionError("hull_node did not appear")
        sleep(10)
        sleep(10)
        sleep(10)
        sleep(10)
        sleep(10)
        sleep(10)
        change_viewpoint_plan_looking_down()
        mouseDoubleClickOn("hull_pane")
        free()
        mouseLeftClickOn("hull_utility")
        mouseLeftClickOn("hull_export_to_structure")
        free()
        pyautogui.typewrite('37727,21099', interval=0.2)
        pyautogui.press('enter')
        pyautogui.typewrite('27031,18267', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        # sleep(1)
        # pyautogui.press('enter')
        if appears("choose_group"):
            if appears("ssi_training",10):
                pyautogui.press('tab')
                pyautogui.press('down')
                pyautogui.press('down')
                pyautogui.press('down')
                pyautogui.press('right')
                pyautogui.press('down')
                pyautogui.press('right')
                pyautogui.press('down')
                pyautogui.press('right')
            else:
                raise AssertionError("ssi_training did not appear")
        else:
            raise AssertionError("choose_group window did not appear")
        if appears("u03_exercise_shell_export"):
            mouseDoubleClickOn("u03_exercise_shell_export")
            pyautogui.press('enter')
            sleep(3)
            pyautogui.press('enter')
        else:
            raise AssertionError("u03_exercise_shell_export did not appear")
        if appears("curved_prop"):
            if appears("apply_to_all_click", 5):
                mouseLeftClickOn("apply_to_all_click")
            pyautogui.press('enter')
            sleep(30)
        else:
            raise AssertionError("curved_prop dialog did not appear")
        free()
        pyautogui.typewrite('SCNAVIGATE',.2)
        pyautogui.press('enter')
        if appears("structure_node2"):
            mouseLeftClickOn("structure_node2")
            if appears("u03_exercise_shell_high", 10):
                mouseDoubleClickOn("u03_exercise_shell_high")
        if appears("save_caution", 10):
            pyautogui.press('enter')
        if appears("u03_exercise_shell"):
            self.logger.info('u03_exercise_shell appeared2nd time')
            sleep(10)
            sleep(10)
            mouseDoubleClickOn("structure_ribbon_button")
            change_viewpoint_plan_looking_down()
            pyautogui.hotkey('ctrl', 's')
        else:
            raise AssertionError("u03_exercise_shell did not appear")
        free()
        pyautogui.typewrite("SCEDITCURVEDPLATE",.2)
        pyautogui.press('enter')
        pyautogui.typewrite("28170.9268,20312.0579",.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("curved_prop",10):
            sleep(1)
            mouseLeftClickOn("pl08_curved")
            pyautogui.press('enter')
        else:
            raise AssertionError("curved_prop did not appear")
        free()
        pyautogui.typewrite('circle',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('31188.1197,19952.9257',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('400',.2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('select', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('31588.1197,19952.9257', interval=.2)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("properties2"):
            mouseLeftClickOn("properties2")
            if appears("layer_click"):
                mouseLeftClickOn("layer_click")
                pyautogui.press('right')
                mouseLeftClickOn("layer_properties_manager_close")
            free()
            pyautogui.press('esc')
        free()
        pyautogui.typewrite('SCADDOBJECTSTOSTRUCTPART',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('28170.9268,20312.0579',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('31588.1197,19952.9257',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("select_markline"):
            self.logger.info("select markline window appeared")
            if appears("continuous", 5):
                mouseLeftClickOn("continuous")
            else:
                mouseLeftClickOn("continuous_unhigh")
            sleep(1)
            pyautogui.press('enter')
            free()
        else:
            raise AssertionError("Menu did not appear")
        pyautogui.hotkey('ctrl','s')
        sleep(5)

    def exercise_r006(self):
        free()
        pyautogui.typewrite('SCNAVIGATE',.2)
        pyautogui.press('enter')
        if appears("u03_f61"):
            mouseDoubleClickOn("u03_f61")
        else:
            raise AssertionError("u03_f61 did not appear")
        if appears("u03_f61_tab"):
            mouseLeftClickOn("u03_f61_tab")
        else:
            raise AssertionError("u03_f61_tab did not appear")
        free()
        reset_viewport()
        free()
        pyautogui.typewrite('SCPLANK',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-1625.7844,4246.5738',.2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('-2500,6700',.2)
        pyautogui.press('enter')
        free()
        # pyautogui.moveTo(724,559)
        pyautogui.moveTo(713,588)
        sleep(2)
        pyautogui.click()
        if appears('plank_prop'):
            if appears("6082", 5):
                mouseLeftClickOn("6082")
            if appears("6082_high", 5):
                mouseLeftClickOn("6082_high")
            mouseLeftClickOn("aft_plank")
            pyautogui.press('enter')
        else:
            raise AssertionError("plank_prop did not appear")

    def exercise_r007(self):
        change_viewpoint_body_looking_fwd()
        free()
        pyautogui.typewrite('SCCORRUGATE', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-4553,5441', .2)
        pyautogui.press('enter')
        if appears("corrugated_prop"):
            mouseLeftClickOn("from_corrugated_stock")


            mouseLeftClickOn("corrugated_prop_ok")
        else:
            raise AssertionError("corrugated_prop did not appear")
        free()
        pyautogui.typewrite('-2500,6700', .2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('line', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7300,5000', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('#-5300,7000', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCADDOBJECTSTOSTRUCTPART',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-5668.1681,5805.9133',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-7300,5000',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('circle', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-3500,5700', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('600', .2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('SCADDOBJECTSTOSTRUCTPART', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-5668.1681,5805.9133', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-2900,5700', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(4)
        pyautogui.hotkey('ctrl','s')
        sleep(5)

    def exercise_r008(self):
        change_viewpoint_from_aft_stbd_up()
        free()
        pyautogui.typewrite('zoom',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-1610,-1897',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('1925,3236',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(3)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')
        free()
        pyautogui.typewrite('SCINSERTSTD',.2)
        pyautogui.press('enter')

        if appears("insert_std_part_window"):
            self.logger.info("insert_std_part_window appeared")
            mouseLeftClickOn("k6_20_10")
            mouseLeftClickOn("insert")
            free()
            pyautogui.typewrite('750,1400', .2)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(907,666)
            pyautogui.click()
            if appears("standard_plate_properties"):
                pyautogui.press('enter')
            else:
                raise AssertionError("standard_plate_properties did not appear")
        else:
            raise AssertionError("insert_std_part_window did not appear")
        free()
        pyautogui.typewrite('SCCONVERTTOSTRUCTPART',.2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('895.8647,1201.3679',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("plate_properties"):
            mouseLeftClickOn("pl08")
            pyautogui.press('enter')
        else:
            raise AssertionError("plate_properties did not appear")
        free()
        pyautogui.typewrite('SCSTANDASSEMBLY',.2)
        pyautogui.press('enter')
        if appears("create_std_asmbly"):
            self.logger.info("Create standard assembly dialog appeared")
            mouseLeftClickOn("sa100")
            mouseLeftClickOn("next")
            sleep(2)
            free()
            mouseLeftClickOn("next")
            sleep(2)
            free()
            mouseLeftClickOn("std_dropdown")
            pyautogui.press('u')
            sleep(1)
            pyautogui.press('enter')
            mouseLeftClickOn("finish")
            free()
        else:
            raise AssertionError("create_std_asmbly did not appear")
        pyautogui.typewrite('750,-0.0051',.2)
        pyautogui.press('enter')
        free()
        # pyautogui.moveTo(1012,723)
        pyautogui.moveTo(1011,733)
        pyautogui.click()
        sleep(2)
        # pyautogui.moveTo(840,697)
        pyautogui.moveTo(839,714)
        pyautogui.click()
        free()
        change_viewpoint_from_aft_prt_up()
        free()
        pyautogui.typewrite('zoom', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-1414,-555', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('1649,1584', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(3)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')
        free()
        pyautogui.typewrite('SCSTANDASSEMBLY', .2)
        pyautogui.press('enter')
        if appears("create_std_asmbly"):
            self.logger.info("Create standard assembly dialog appeared")
            mouseLeftClickOn("sa100")
            mouseLeftClickOn("next")
            sleep(2)
            free()
            mouseLeftClickOn("next")
            sleep(2)
            free()
            mouseLeftClickOn("std_dropdown")
            pyautogui.press('u')
            sleep(1)
            pyautogui.press('enter')
            mouseLeftClickOn("finish")
            free()
        else:
            raise AssertionError("create_std_asmbly did not appear")
        pyautogui.typewrite('-750,-0.0051', .2)
        pyautogui.press('enter')
        free()
        # pyautogui.moveTo(649,368)
        pyautogui.moveTo(646,451)
        pyautogui.click()
        sleep(2)
        # pyautogui.moveTo(743,693)
        pyautogui.moveTo(812,749)
        pyautogui.click()
        free()
        pyautogui.hotkey('ctrl','s')
        sleep(5)
        change_viewpoint_body_looking_fwd()
        free()
        pyautogui.typewrite('select', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-7000,6800', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('offset', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('500', .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.moveTo(760, 521)
        sleep(1)
        pyautogui.click()
        pyautogui.press('enter')
        free()
        pyautogui.hotkey('ctrl','s')
        sleep(5)
        pyautogui.hotkey('ctrl','9')
        pyautogui.press('enter')

    def verifyingStructureModeling(self):
        obj = SSILearningVerifierUtil()
        obj.ssi_learning_verifier(projectFilePath="C:/NewDeploy_Structure/", list_image="structure_modeling",
                                  xmlExtractor1="StructureModelingXMLextractor", xmlName1="StructureModeling")

    def mark_group_intersection(self):
        self.logger.info('Start:\t Checking Module- mark_group_intersection')
        open_U03_drawing(drawing_name="u03_f60_node", tab_name="u03_f60_drawing_tab")
        open_mgi_window()
        mouseDoubleClickOn("mgi_deck_node")
        if appears("mgi_u03_maindeck_node"):
            self.logger.info("mgi_u03_maindeck_node appeared")
            mouseDoubleClickOn("mgi_u03_maindeck_node")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            mouseDoubleClickOn("mgi_u03_ttop_node")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            sleep(1)
            mouseLeftClickOn("mgi_ok_button")
            sleep(5)
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.press('esc')
            reset_viewport()
            sleep(2)
            if appears("u03_maindeck_m_on_drawing",10):
                self.logger.info("u03_maindeck_m was opened")
            if appears("u03_ttop_m",10):
                self.logger.info("u03_ttop_m was opened")
            self.logger.info("Planar groups opened properly")
        else:
            raise AssertionError("Maindeck node not visible")
        self.logger.info('End:\t Checking Module- mark_group_intersection')



    def draw_straight_line(self):
        self.logger.info('Start:\t Checking Module- draw_straight_line')
        sleep(1)
        line('-4000','70','-2500','2500')
        self.logger.info("User Defined  line created")
        mouseDoubleClickOn("structure_ribbon_button", "structure_ribbon_button")
        mouseLeftClickOn("contruction_line_pane", "contruction_line_pane")
        if appears("converttoconstructionline_button"):
            self.logger.info("converttoconstructionline_button appeared")
            mouseLeftClickOn("converttoconstructionline_button")
            free()
            sleep(1)
            pyautogui.typewrite(['-', '4', '0', '0', '0', ',', '7', '0'], interval=0.25)
            pyautogui.press('enter')
            pyautogui.press('enter')
        else:
            raise AssertionError("Conversion button not visible")
        self.logger.info('End:\t Checking Module- draw_straight_line')


    def offset_line(self):
        self.logger.info('Start:\t Checking Module- offset_line')
        pyautogui.typewrite(['O', 'F', 'F', 'S', 'E', 'T'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['5', '0', '0'], interval=0.2)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '4', '0', '0', '0', ',', '7', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '0', '0', '0', ',', '1', '0', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        self.logger.info("Offset lin checked")
        self.logger.info('End:\t Checking Module- offset_line')


    def mirror_line(self):
        self.logger.info('Start:\t Checking Module- mirror_line')
        mirror_line()
        sleep(1)
        pyautogui.typewrite(['-', '4', '0', '0', '0', ',', '7', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        self.logger.info("Line properly mirrored")
        mouseMoveTo("empty_space2")
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['3', '2', '0', '4', ',', '1', '3', '5', '9'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("construction_line_info_option"):
            self.logger.info("construction_line_info_option appeared")
            mouseLeftClickOn("construction_line_info_option")
            if appears("construction_line_info_window"):
                mouseLeftClickOn("construction_line_info_window_close_button", "construction_line_info_window_close_button")
            else:
                raise AssertionError("Construction line info window not visible")
        else:
            raise AssertionError("Construction line info option not visible")
        self.logger.info('End:\t Checking Module- mirror_line')


    def copy_line(self):
        self.logger.info('Start:\t Checking Module- copy_line')
        pyautogui.typewrite(['c', 'o', 'p', 'y'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '4', '0', '0', '0', ',', '7', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '0', '0', '0', ',', '4', '0', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '9', '9', '0', ',', '4', '1', '1', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("construction_line_info_option"):
            mouseLeftClickOn("construction_line_info_option", "construction_line_info_option")
            if appears("construction_line_info_window"):
                self.logger.info("Construction line info window appeared")
                mouseDoubleClickOn("identicals_node", "identicals_node")
                mouseLeftClickOn("construction_line_info_window_close_button",
                                 "construction_line_info_window_close_button")
            else:
                raise AssertionError("Construction line info window not visible")
        else:
            raise AssertionError("Construction line info option not visible")
        self.logger.info('End:\t Checking Module- copy_line')


    def stretch_line(self):
        sleep(5)
        free()
        free()
        self.logger.info('Start:\t Checking Module- stretch_line')
        pyautogui.typewrite(['s', 't', 'r', 'e', 't', 'c', 'h'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '9', '9', '0', ',', '4', '1', '1', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '5', '0', '0', ',', '-', '5', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("update_related_objects_window"):
            self.logger.info("update_related_objects_window appeared")
            mouseLeftClickOn("update_related_objects_cancel_button", "update_related_objects_cancel_button")
        else:
            raise AssertionError("Update related objects window not visible")
        pyautogui.typewrite(['s', 't', 'r', 'e', 't', 'c', 'h'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '9', '9', '0', ',', '4', '1', '1', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '5', '0', '0', ',', '-', '5', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("update_related_objects_window"):
            mouseLeftClickOn("update_related_objects_uncheckall_button")
            mouseLeftClickOn("update_related_objects_ok_button")
            pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.5)
            pyautogui.press('enter')
            pyautogui.typewrite(['-', '7', '4', '9', '7', ',', '3', '5', '7', '1'], interval=0.5)
            pyautogui.press('enter')
            pyautogui.click(button='right', clicks=2, interval=0.25)
            if appears("construction_line_info_option"):
                mouseLeftClickOn("construction_line_info_option", "construction_line_info_option")
                if appears("construction_line_info_window"):
                    mouseLeftClickOn("construction_line_info_window_close_button","construction_line_info_window_close_button")
                else:
                    raise AssertionError("Construction line info window not visible")
            else:
                raise AssertionError("Construction line info option not visible")
            pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.1)
            pyautogui.press('enter')
            pyautogui.typewrite(['-', '4', '0', '0', '0', ',', '7', '0'], interval=0.1)
            pyautogui.press('enter')
            pyautogui.click(button='right', clicks=2, interval=0.25)
            if appears("construction_line_info_option"):
                mouseLeftClickOn("construction_line_info_option", "construction_line_info_option")
                if appears("construction_line_info_window"):
                    mouseDoubleClickOn("identicals_node", "identicals_node")
                    mouseLeftClickOn("construction_line_info_window_close_button",
                                     "construction_line_info_window_close_button")
                else:
                    raise AssertionError("Construction line info window not visible")
            else:
                raise AssertionError("Construction line info option not visible")

        else:
            raise AssertionError("Update related objects window not visible")
        self.logger.info('End:\t Checking Module- stretch_line')


    def trim_line(self):
        self.logger.info('Start:\t Checking Module- trim_line')
        pyautogui.typewrite(['O', 'F', 'F', 'S', 'E', 'T'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['5', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '5', '5', '9', '8', ',', '1', '5', '5', '6'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '0', '0', '0', ',', '1', '0', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        self.logger.info('End:\t Checking Module- trim_line')


    def edit_offset(self):
        self.logger.info('Start:\t Checking Module- edit_offset')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '3', '8', '1', ',', '6', '3', '0', '9'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("edit_offset_option"):
            self.logger.info("Edit offset option appeared")
            mouseLeftClickOn("edit_offset_option", "edit_offset_option")
            if appears("edit_properties_window"):
                mouseLeftClickOn("edit_offset_field", "edit_offset_field")
                pyautogui.typewrite(['-'])
                mouseLeftClickOn("edit_offset_ok_button", "edit_offset_ok_button")
                self.logger.info("Offset edited successfully")
            else:
                raise AssertionError("Edit offset window not visible")
        else:
            raise AssertionError("Edit offset option not visible")
        self.logger.info('Start:\t Checking Module- edit_offset')


    def swap_construction_line(self):
        self.logger.info('Start:\t Checking Module- swap_construction_line')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '6', '6', '5', ',', '2', '5', '3', '7'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['d', 'e', 'l', 'e', 't', 'e'], interval=0.5)
        pyautogui.press('enter')
        line('-1703','917','-5688','917')
        mouseDoubleClickOn("structure_ribbon_button", "structure_ribbon_button")
        if appears("plates_pane"):
            self.logger.info("Plates pane appeared")
            mouseLeftClickOn("plates_pane", "plates_pane")
            if appears("new_plate_button"):
                mouseLeftClickOn("new_plate_button", "new_plate_button")
                sleep(1)
                mouseMoveTo("empty_space2")
                pyautogui.typewrite(['-', '3', '0', '5', '4', ',', '1', '1', '4', '8'], interval=0.5)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
            else:
                raise AssertionError("New Plate Button not Visible")
        else:
            raise AssertionError("Plates Plane not visible")
        if appears("plates_pane"):
            mouseLeftClickOn("plates_pane", "plates_pane")
            if appears("new_plate_button"):
                mouseLeftClickOn("new_plate_button", "new_plate_button")
                sleep(1)
                mouseMoveTo("empty_space2")
                pyautogui.typewrite(['-', '4', '1', '9', '6', ',', '1', '1', '4', '3'], interval=0.25)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
            else:
                raise AssertionError("New Plate Button not Visible")
        else:
            raise AssertionError("Plates Plane not visible")
        if appears("contruction_line_pane"):
            mouseLeftClickOn("contruction_line_pane", "contruction_line_pane")
            if appears("swap_button"):
                mouseLeftClickOn("swap_button", "swap_button")
                mouseMoveTo("empty_space2")
                sleep(1)
                pyautogui.typewrite(['-', '7', '3', '9', '7', ',', '1', '3', '9', '7'], interval=0.5)
                pyautogui.press('enter')
                pyautogui.typewrite(['-', '6', '2', '7', '0', ',', '2', '9', '7', '6'], interval=0.5)
                pyautogui.press('enter')
                if appears("structure_parts_window"):
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('enter')
                else:
                    raise AssertionError("Structure Parts Window not available")
            else:
                raise AssertionError("Swap button not available")
        else:
            raise AssertionError("Construction lines panel not available")
        self.logger.info('End:\t Checking Module- swap_construction_line')


    def replace_construction_line(self):
        self.logger.info('Start:\t Checking Module- replace_construction_line')
        free()
        pyautogui.typewrite(['O', 'F', 'F', 'S', 'E', 'T'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['3', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '5', '5', '9', '8', ',', '1', '5', '5', '6'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '0', '0', '0', ',', '1', '0', '0', '0'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mouseLeftClickOn("structure_ribbon_button", "structure_ribbon_button")
        if appears("contruction_line_pane"):
            mouseLeftClickOn("contruction_line_pane", "contruction_line_pane")
            if appears("swap_button"):
                mouseLeftClickOn("replace_dropdown_button", "replace_dropdown_region")
                if appears("replace_button"):
                    mouseLeftClickOn("replace_button")
                    pyautogui.typewrite(['-', '6', '1', '1', '9', ',', '2', '4', '9', '4'], interval=0.5)
                    pyautogui.press('enter')
                    pyautogui.typewrite(['-', '5', '8', '1', '8', ',', '2', '5', '4', '3'], interval=0.5)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('enter')
                    self.logger.info("Successfully replaced")
                    sleep(1)
                    pyautogui.hotkey('ctrl', 'z')
                    self.logger.info("Action Reverted")
                else:
                    raise AssertionError("Replace button didn't appear")
            else:
                raise AssertionError("Swap button not available")
        else:
            raise AssertionError("Construction lines panel not available")
        self.logger.info('End:\t Checking Module- replace_construction_line')


    def manage_relation(self):
        self.logger.info('Start:\t Checking Module- manage_relation')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
        pyautogui.press('enter')
        pyautogui.typewrite(['3', '1', '7', '5', ',', '1', '4', '0', '6'], interval=0.5)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("construction_line_info_option"):
            mouseLeftClickOn("construction_line_info_option", "construction_line_info_option")
            if appears("construction_line_info_window"):
                mouseLeftClickOn("handle_394")
                mouseLeftClickOn("relation_button")
                if appears("relation_window"):
                    mouseLeftClickOn("uncheck_all_button")
                    mouseLeftClickOn("apply_button")
                    sleep(1)
                    pyautogui.hotkey('alt', 'f4')
                    sleep(1)
                    pyautogui.hotkey('alt', 'f4')
                    self.logger.info("Manage relation checked")
                else:
                    raise AssertionError("Relation window did not appear")
            else:
                raise AssertionError("Construction line info window not visible")
        else:
            raise AssertionError("Construction line info option not visible")
        sleep(2)
        pyautogui.press('esc')
        pyautogui.hotkey('ctrl','s')
        self.logger.info('End:\t Checking Module- manage_relation')

    def delete_lines_before_creating_plate_parts(self):
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '6', '9', '9', ',', '1', '3', '9', '8'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '1', '4', '4', ',', '1', '3', '0', '9'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['d', 'e', 'l', 'e', 't', 'e'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '4', '7', '0', '6', ',', '6', '6', '9', '9'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '1', '5', '5', ',', '5', '7', '4', '8'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '3', '2', '7', '8', ',', '1', '2', '3', '8'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '4', '5', '2', ',', '5', '4', '7', '1'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['3', '1', '1', '9', ',', '1', '4', '9', '0'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '2', '2', '8', '9', ',', '1', '8', '9', '2'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '2', '7', '9', '9', ',', '2', '0', '1', '3'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '2', '2', '9', '5', ',', '9', '1', '6'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '8', '4', '6', ',', '1', '3', '9', '9'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['d', 'e', 'l', 'e', 't', 'e'], interval=0.1)
        pyautogui.press('enter')

    def creating_plate_parts(self):
        self.logger.info('Start:\t Checking Module- creating_plate_parts')
        open_U03_drawing(drawing_name="u03_f60_node", tab_name="u03_f60_drawing_tab")
        sleep(1)
        change_viewpoint_body_looking_fwd()
        self.delete_lines_before_creating_plate_parts()
        sleep(1)
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '1', '5', '5', '9', ',', '4', '7'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '6', '7', '3', ',', '2'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mirror_line()
        sleep(1)
        open_mgi_window()
        mouseDoubleClickOn("mgi_deck_node", "mgi_deck_node")
        if appears("mgi_u03_maindeck_node"):
            mouseDoubleClickOn("mgi_u03_maindeck_node", "mgi_u03_maindeck_node")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            mouseDoubleClickOn("mgi_u03_ttop_node", "mgi_u03_ttop_node")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
        else:
            raise AssertionError("mgi_u03_maindeck_node didn't appear")
        mouseDoubleClickOn("mgi_lngbhd_node")
        if appears("mgi_u03_2500p"):
            mouseDoubleClickOn("mgi_u03_2500p")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            mouseDoubleClickOn("mgi_u03_750p")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
        else:
            raise AssertionError("mgi_u03_2500p didn't appear")
        mouseLeftClickOn("mgi_ok_button")
        self.logger.info('End:\t Checking Module- creating_plate_parts')


    def new_plate_creation(self):
        self.logger.info('Start:\t Checking Module- new_plate_creation')
        reset_viewport()
        sleep(1)
        line('0','-0.0144','0','1400')
        self.logger.info("User Defined  line created")
        select_new_plate_option()
        sleep(1)
        free()
        pyautogui.typewrite(['-', '3', '9', '6', ',', '7', '0', '7'], interval=0.2)
        pyautogui.press('enter')
        sleep(1)
        if appears("plate_properties"):
            self.logger.info("Plate properties wondow appeared")
            sleep(1)
            mouseMoveTo("plate_properties_scroll_down")
            pyautogui.click(clicks=5)
            if appears("pl14"):
                mouseLeftClickOn("pl14")
            mouseLeftClickOn("mark_side_button","mark_side_region")
            mouseLeftClickOn("pp_hierarchy")
            mouseLeftClickOn("pp_finishes")
            sleep(1)
            mouseLeftClickOn("pp_user")
            pyautogui.press('enter')
            conceptual_visual()
            if appears("plate_pl14",5):
                self.logger.info("Plate stock pl14 was created")
                wireframe2d_visual()
            else:
                raise AssertionError("Plate stock pl14 was not created")
        self.logger.info('End:\t Checking Module- new_plate_creation')

    def part_information(self):
        self.logger.info('Start:\t Checking Module- part_information')
        # pyautogui.hotkey('ctrl', 'z')
        wireframe2d_visual()
        sleep(2)
        pyautogui.typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '0', '.', '1', '0', '4', '3', ',', '8', '9', '8'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("part_information_button"):
            self.logger.info("Part info button appeared")
            mouseLeftClickOn("part_information_button")
            if appears("part_info_window"):
                self.logger.info("Part info window appeared")
                if appears("pi_construction_line"):
                    mouseLeftClickOn("pi_construction_line_expand","pi_construction_line")
                    mouseLeftClickOn("pi_construction_line2")
                    pyautogui.typewrite(['down', 'down', 'down', 'down'], interval=0.6)
                    mouseRightClickOn("handle_485")
                    if appears("remove_from_the_plate"):
                        self.logger.info("Remove from the plate button appeared and clicked")
                        mouseLeftClickOn("remove_from_the_plate")
                        sleep(1)
                        pyautogui.press('enter')
                        self.logger.info("Part Information checked successfully")
                    else:
                        raise AssertionError("remove_from_the_plate did not appear")
                else:
                    raise AssertionError("Construction line option did not appear")
            else:
                raise AssertionError("Part information window din not appear")
        else:
            raise AssertionError("part_information_button did not appear")
        self.logger.info('End:\t Checking Module- part_information')


    def editing_plate_parts(self):
        self.logger.info('Start:\t Checking Module- editing_plate_parts')
        sleep(1)
        pyautogui.hotkey('ctrl','z')
        sleep(1)
        #100 mm to the left.
        pyautogui.typewrite(['s', 't', 'r', 'e', 't', 'c', 'h'], interval=0.1)
        pyautogui.press('enter')
        # pyautogui.typewrite('-457.5747,1478.2588', interval=0.1)
        pyautogui.typewrite('-238.6505,1956.1968', interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['2', '0', ',', '-', '3', '9'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['0', ',', '-', '0', '.', '0', '1', '4', '4'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '1', '0', '0', ',', '0'], interval=0.1)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.press('enter')
        sleep(3)
        pyautogui.hotkey('ctrl', 'z')
        #50 mm down
        pyautogui.typewrite(['s', 't', 'r', 'e', 't', 'c', 'h'], interval=0.1)
        pyautogui.press('enter')
        # pyautogui.typewrite(['-', '4', '5', '7', '.', '5', '7', '4', '7', ',', '1', '4', '7', '8', '.', '2', '5', '8', '8'], interval=0.1)
        pyautogui.typewrite('-238.6505,1956.1968', interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['2', '0', ',', '-', '3', '9'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['-', '1', '0', '0', ',', '1', '4', '0', '0'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['#', '-', '1', '0', '0', ',', '1', '3', '5', '0'], interval=0.1)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.press('enter')
        pyautogui.hotkey('ctrl','s')
        self.logger.info("Successfully edited")
        self.logger.info('Start:\t Checking Module- editing_plate_parts')


    def open_frame_for_splitting_plate_parts(self):
        self.logger.info('Start:\t Checking Module- open_frame_for_splitting_plate_parts')
        open_navigator()
        mouseDoubleClickOn("u03_ttop3")
        if appears("save_caution"):
            mouseLeftClickOn("save_caution_no")
        if appears("ttop_tab"):
            self.logger.info("TTOP tab appeared")
        else:
            raise AssertionError("TTOP tab did not open")
        open_mgi_window()
        mouseDoubleClickOn("mgi_frame_node")
        if appears("f60"):
            mouseDoubleClickOn("f60")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            mouseDoubleClickOn("f64")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
        else:
            raise AssertionError("f60 didn't appear")
        mouseDoubleClickOn("mgi_lngbhd_node")
        if appears("mgi_u03_cl"):
            mouseDoubleClickOn("mgi_u03_cl")
            pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
        else:
            raise AssertionError("mgi_u03_cl didn't appear")
        mouseLeftClickOn("mgi_ok_button")
        if appears("plates_pane"):
            mouseLeftClickOn("plates_pane", "plates_pane")
            if appears("new_plate_button"):
                mouseLeftClickOn("new_plate_button", "new_plate_button")
                sleep(1)
                free()
                pyautogui.typewrite(['3', '7', '2', '3', '2', ',', '2', '4', '4', '6'], interval=0.1)
                pyautogui.press('enter')
                sleep(1)
                if appears("plate_properties"):
                    sleep(1)
                    mouseLeftClickOn("pl10_button_unhighlighted")
                    mouseLeftClickOn("mark_side_button","mark_side_region")
                    mouseLeftClickOn("throw_dir_button","throw_dir_region")
                    mouseLeftClickOn("plate_properties_ok")
                    # sleep(1)
                    # pyautogui.press('enter')
                    conceptual_visual()
                    if appears("plate_pl10",5):
                        self.logger.info("plate_pl10 was created")
                else:
                    raise AssertionError("plate_properties did not appear")
            else:
                raise AssertionError("New Plate Button not Visible")
        else:
            raise AssertionError("Plates Plane not visible")
        self.logger.info('End:\t Checking Module- open_frame_for_splitting_plate_parts')

    def loading_frames_lngbhd(self):
        self.logger.info('Start:\t Checking Module- loading_frames_lngbhd')
        sleep(5)
        free()
        pyautogui.hotkey('ctrl', 'z')
        free()
        mouseLeftClickOn("utilities_pane")
        mouseLeftClickOn("mark_group_intersection_button", "mark_group_intersection_button")
        if appears("mark_group_intersection_dialog"):
            mouseDoubleClickOn("mgi_frame_node")
            if appears("f62"):
                mouseDoubleClickOn("f62")
                pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
            else:
                raise AssertionError("f62 didn't appear")
            mouseDoubleClickOn("mgi_lngbhd_node")
            if appears("mgi_u03_2500p"):
                mouseDoubleClickOn("mgi_u03_2500p")
                pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
                mouseDoubleClickOn("mgi_u03_750p")
                pyautogui.typewrite(['space', 'down', 'space'], interval=0.25)
                self.logger.info("Frames were imported in the drawing")
            else:
                raise AssertionError("mgi_u03_2500p didn't appear")
            mouseLeftClickOn("mgi_ok_button")
            self.logger.info("Frames were loaded")
        else:
            raise AssertionError("mark_group_intersection_dialog didn't appear")
        self.logger.info('End:\t Checking Module- loading_frames_lngbhd')


    def splitting_plate_parts(self):
        self.logger.info('Start:\t Checking Module- splitting_plate_parts')
        sleep(1)
        reset_viewport()
        mouseLeftClickOn("structure_ribbon_button", "structure_ribbon_button")
        if appears("plates_pane"):
            mouseLeftClickOn("plates_pane", "plates_pane")
            if appears("split_plate"):
                self.logger.info("Split plate option appeared")
                mouseLeftClickOn("split_plate", "split_plate")
                free()
                pyautogui.typewrite('38412.9156,4381.9864', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('34240.1759,5698.6397', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('37202.6152,4478.3018', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('37898.8456,2461.5687', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('37834.6041,750.4100', interval=0.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('480.9472,-1578.6084', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('1711.8032,-1711.9774', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('1785.9512,-3771.7875', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('1785.9512,-5046.2023', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('540.2656,-5046.2023', interval=0.1)
                pyautogui.press('enter')
                pyautogui.typewrite('555.5634,-3897.9669', interval=0.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                free()
                pyautogui.press('esc')
                pyautogui.press('esc')
                conceptual_visual()
                if appears("splitted_plate",5):
                    self.logger.info("Plate parts were splitted")
            else:
                raise AssertionError("Split plate button did not appear")
        else:
            raise AssertionError("Plates pane did not appear")
        self.logger.info('End:\t Checking Module- splitting_plate_parts')


    def boundary_diagnostics(self):
        self.logger.info('Start:\t Checking Module- boundary_diagnostics')
        sleep(5)
        pyautogui.hotkey('ctrl','z')
        open_navigator()
        if appears("u03_f60_node"):
            mouseDoubleClickOn("u03_f60_node")
            if appears("save_caution",5):
                mouseLeftClickOn("save_caution_no")
            if appears("warning4",5):
                pyautogui.press('enter')
                sleep(1)
                if appears("warning4",5):
                    pyautogui.press('enter')
            sleep(5)
            wireframe2d_visual()
            mouseDoubleClickOn("structure_ribbon_button")
            if appears("plates_pane"):
                mouseLeftClickOn("plates_pane", "plates_pane")
                mouseLeftClickOn("invalid_boundary")
                self.logger.info("Invalid boundary diagnostic option was selected")
                free()
                pyautogui.typewrite('-750.5631,900.8032', interval=0.2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("pp_diagnostic"):
                    mouseLeftClickOn("pp_length_button", "pp_length_region")
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite(['1', '0', '0'], interval=0.05)
                    mouseLeftClickOn("pp_circle_button", "pp_circle_region")
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite(['1', '0', '0'], interval=0.05)
                    sleep(1)
                    mouseLeftClickOn("pp_diagnostic_ok")
                    if appears("warning"):
                        pyautogui.press('n')
                        if appears("warning_no",5):
                            mouseLeftClickOn("warning_no")
                    self.logger.info("boundary_diagnostics completed")
                else:
                    raise AssertionError("Diagnostic Menu did not appear")
        else:
            raise AssertionError("u03_f60_node did not appear")
        self.logger.info('End:\t Checking Module- boundary_diagnostics')


    def fixing_invalid_part(self):
        self.logger.info('Start:\t Checking Module- fixing_invalid_part')
        reset_viewport()
        sleep(2)
        pyautogui.moveTo(820,655)
        pyautogui.scroll(5)
        pyautogui.scroll(5)
        pyautogui.scroll(5)
        free()
        pyautogui.typewrite(['s', 'e', 'l', 'e', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('-106.5663,-266.4182',interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('448.9445,1494.0599', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        # pyautogui.typewrite(['s', 't', 'r', 'e', 't', 'c', 'h'], interval=0.1)
        # pyautogui.press('enter')
        # pyautogui.typewrite('0,1350', interval=0.5)
        # pyautogui.press('enter')
        # sleep(1)
        # pyautogui.moveTo(500,500)
        # # pyautogui.typewrite("#0,1400", interval=0.5)
        # pyautogui.typewrite("0,1400", interval=0.5)
        # pyautogui.press('enter')
        sleep(2)
        pyautogui.moveTo(799,422)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(801,414)
        pyautogui.click()
        if appears("update_related_objects2"):
            pyautogui.press('enter')
        pyautogui.press('esc')
        conceptual_visual()
        if appears("plate_pl14",5):
            self.logger.info("Plate stock pl14 was fixed")
        else:
            raise AssertionError("Plate stock pl14 was not fixed")
        self.logger.info('End:\t Checking Module- fixing_invalid_part')

    def deleting_circle(self):
        self.logger.info('Start:\t Checking Module- deleting_circle')
        wireframe2d_visual()
        reset_viewport()
        free()
        pyautogui.typewrite(['s', 'e', 'l', 'e', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('-106.2537,1460.0889', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['d', 'e', 'l', 'e', 't', 'e'],interval=0.1)
        pyautogui.press('enter')
        reset_viewport()
        self.logger.info("Circle is deleted")
        self.logger.info('End:\t Checking Module- deleting_circle')

    def structure_display(self):
        self.logger.info('Start:\t Checking Module- structure_display')
        if appears("structure_ribbon_button",10):
            mouseDoubleClickOn("structure_ribbon_button")
        elif appears("structure_ribbon_button2", 10):
            mouseMoveTo("structure_ribbon_button2")
        else:
            raise AssertionError("structure_ribbon_button didn't appeared.")
        if appears("utilities_pane", 10):
            mouseLeftClickOn("utilities_pane")
            mouseLeftClickOn("toggle_show", "toggle_show_region")
            mouseMoveTo("toggle_show_solid")
            self.logger.info("Toggle show solid appeared")
            free()
            pyautogui.press('esc', presses=2)
            if appears("sc_utilites"):
                mouseLeftClickOn("sc_utilites")
                mouseLeftClickOn("sc_utilites_main")
                if appears("drawing_options_region",10):
                    mouseLeftClickOn("drawing_options_dropdown2","drawing_options_region")
                elif appears("drawing_options_region2",10):
                    mouseLeftClickOn("drawing_options_dropdown2","drawing_options_region2")
                if appears("structure_drawing_option",10):
                    mouseLeftClickOn("structure_drawing_option")
                    sleep(3)
                    pyautogui.press('enter')
                    self.logger.info("Structure display was checked")
                else:
                    raise AssertionError("structure_drawing_option did not appear")
            else:
                raise AssertionError("sc_utilites did not appear")
        else:
            raise AssertionError("Utilities Pane did not appear")
        self.logger.info('End:\t Checking Module- structure_display')


    def part_relation(self):
        self.logger.info('Start:\t Checking Module- part_relation')
        sleep(1)
        pyautogui.typewrite(['s', 'e', 'l', 'e', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('887.5931,-7.7921',interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('2331.2887,235.2543',interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite(['d', 'e', 'l', 'e', 't', 'e'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite(['s', 'e', 'l', 'e', 't'], interval=0.1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2.1618,997.0606', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mirror_line()
        sleep(2)
        pyautogui.hotkey('ctrl', 's')
        self.logger.info("Part relation is checked")
        self.logger.info('End:\t Checking Module- part_relation')


    def create_flanges(self):
        self.logger.info('Start:\t Checking Module- create_flanges')
        open_drawing_from_u03("u03_f60_node")
        sleep(5)
        if appears("u03_f60_drawing_tab"):
            self.logger.info("U03 F60 tab appeared")
            free()
            mouseLeftClickOn("structure_ribbon_button")
            mouseLeftClickOn("plates_pane")
            mouseLeftClickOn("add_flange_drop_down","add_flange_region")
            mouseLeftClickOn("add_flange_button")
            free()
            pyautogui.typewrite('-408.0670,1399.0149',interval=.2)
            pyautogui.press('enter')
            if appears("select_flange"):
                pyautogui.press('enter')
                sleep(1)
                if appears("foldline_warning", 10):
                    pyautogui.press('enter')
                if appears("warning_flange",10):
                    mouseLeftClickOn("warning_flange_no")
                if appears("update_related_objects2",10):
                    pyautogui.press('enter')
                change_view("conceptual_visual")
                free()
                if appears("plate_pl14",10):
                    self.logger.info("Plate stock pl14 was fixed")
                mouseMoveTo("flange_zoom")
            else:
                raise AssertionError("select_flange window did not appear")
        else:
            raise AssertionError("u03_f60_drawing_tab did not appear")
        self.logger.info('End:\t Checking Module- create_flanges')


    def modify_flanges(self):
        self.logger.info('Start:\t Checking Module- modify_flanges')
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("plates_pane")
        if appears("add_flange_region"):
            mouseLeftClickOn("add_flange_drop_down", "add_flange_region")
            mouseLeftClickOn("edit_flange")
        else:
            mouseLeftClickOn("add_flange_drop_down", "flange_modify")
            mouseLeftClickOn("edit_flange2")
        free()
        sleep(1)
        pyautogui.typewrite('-395.1985,687.0111', interval=.3)
        pyautogui.press('enter')
        if appears("flange_list"):
            mouseRightClickOn("select_flange_from_list")
            mouseLeftClickOn("flange_properties")
            if appears("flange_properties_scroll_down"):
                self.logger.info("Flange properties appeared")
                mouseLeftClickOn("flange_properties_scroll_down")
                mouseLeftClickOn("fl95")
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.hotkey('alt','f4')
                self.logger.info("Successfully modified")
            else:
                raise AssertionError("fl95 did not appear")
        else:
            raise AssertionError("flange_list did not appear")
        self.logger.info('End:\t Checking Module- modify_flanges')


    def add_green_material(self):
        self.logger.info('Start:\t Checking Module- add_green_material')
        change_viewpoint_body_looking_fwd()
        change_view("2d_wireframe")
        sleep(1)
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("plates_pane")
        if appears("add_green_region"):
            mouseLeftClickOn("add_flange_drop_down", "add_green_region")
            mouseLeftClickOn("add_green")
            free()
            pyautogui.typewrite('-751.8638,663.5256',interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.typewrite('-750,-0.0067',interval=.1)
            pyautogui.press('enter')
            pyautogui.typewrite('0.0,-0.0144', interval=.1)
            pyautogui.press('enter')
            pyautogui.typewrite('0.0,11.4734', interval=.1)
            pyautogui.press('enter')
            if appears("add_green_menu"):
                self.logger.info("Add green menu appeared")
                mouseLeftClickOn("plus50")
                mouseLeftClickOn("green_ok")
                self.logger.info("successfully added green part")
            else:
                raise AssertionError("Green part menu did not appear")
        self.logger.info('End:\t Checking Module- add_green_material')


    def modify_green_material(self):
        self.logger.info('Start:\t Checking Module- modify_green_material')
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("plates_pane")
        if appears("add_green_region"):
            mouseLeftClickOn("add_flange_drop_down", "add_green_region")
            mouseLeftClickOn("edit_green_button")
            free()
            pyautogui.typewrite('-751.8638,663.5256',interval=0.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            if appears("edit_green"):
                self.logger.info("Edit green dialog appeared")
                mouseLeftClickOn("remove_green")
                sleep(1)
                mouseLeftClickOn("remove_green_ok")
                self.logger.info("Green part has been removed successfully")
            else:
                raise AssertionError("Edit menu did not appear")
        self.logger.info('End:\t Checking Module- modify_green_material')

    def add_contour_construction_lines(self):
        self.logger.info('Start:\t Checking Module- add_contour_construction_lines')
        mouseLeftClickOn("plates_pane")
        mouseLeftClickOn("new_plate_button")
        free()
        pyautogui.typewrite('-3666.3051,1066.2859',interval=0.1)
        pyautogui.press('enter')
        if appears("plate_menu"):
            if appears("pl10_button_unhighlighted",10):
                mouseLeftClickOn("pl10_button_unhighlighted")
            mouseLeftClickOn("pp_hierarchy")
            mouseLeftClickOn("plate_tab")
            mouseLeftClickOn("drop_down")
            if appears("p07",5):
                mouseLeftClickOn("p07")
            else:
                pyautogui.press('esc')
            sleep(1)
            mouseLeftClickOn("plate_properties_ok")
            conceptual_visual()
            if appears("plate_pl10",5):
                self.logger.info('Plate PL10 was created')
            wireframe2d_visual()
            sleep(4)
            line('-5092.521','617.0532','-4022.8591','1840.8251')
            free()
            sleep(2)
            pyautogui.typewrite('circle', interval=0.1)
            pyautogui.press('enter')
            pyautogui.typewrite('#-3000,1000', interval=0.1)
            pyautogui.press('enter')
            pyautogui.typewrite('200', interval=0.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.press('esc')
            self.logger.info("Adding contour lines Successfully done")
        else:
            raise AssertionError("Plate menu didn't appear")
        self.logger.info('End:\t Checking Module- add_contour_construction_lines')


    def add_user_construction_lines(self):
        self.logger.info('Start:\t Checking Module- add_user_construction_lines')
        mouseLeftClickOn("parts_pane")
        if appears("add_object"):
            mouseLeftClickOn("add_object")
            free()
            pyautogui.typewrite('-4703.6247,1396.0637',interval=0.3)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('-4596.5137,1191.7316', interval=0.3)
            pyautogui.press('enter')
            pyautogui.typewrite('-2827.1516,1100.9101', interval=0.3)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("markline_style"):
                self.logger.info("markline style dialog appeared")
                mouseLeftClickOn("markline_style_ok")
            sleep(1)
            change_view("conceptual_visual")
            sleep(1)
            self.logger.info("User construction lines added")
        else:
            raise AssertionError("No button found named add object")
        self.logger.info('End:\t Checking Module- add_user_construction_lines')


    def add_mark_lines(self):
        self.logger.info('Start:\t Checking Module- add_mark_lines')
        change_view("2d_wireframe")
        sleep(5)
        set_layer("draft_mark")
        mouseMoveToCoordinates(400,100)
        sleep(2)
        free()
        line('-4143.9846','1279.7944','-4143.9846','849.8447')
        if appears("structure_ribbon_button"):
            mouseDoubleClickOn("structure_ribbon_button")
            if appears("parts_pane"):
                mouseLeftClickOn("parts_pane")
                if appears("add_object"):
                    mouseLeftClickOn("add_object")
                    free()
                    sleep(5)
                    free()
                    pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('-4143.9846,900.8447', interval=0.3)
                    pyautogui.press('enter')
                    pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    if appears("select_markline"):
                        self.logger.info("select markline window appeared")
                        if appears("continuous",10):
                            mouseLeftClickOn("continuous")
                        else:
                            mouseLeftClickOn("continuous_unhigh")
                        sleep(1)
                        pyautogui.press('enter')
                    else:
                        raise AssertionError("Menu did not appear")
                    self.logger.info("Markline added")
                else:
                    raise AssertionError("Parts pane did not appear")
            else:
                raise AssertionError("No button found named add object")
        else:
            raise AssertionError("Structure tab did not found")
        self.logger.info('End:\t Checking Module- add_mark_lines')

    def add_dynamic_marking_blocks(self):
        self.logger.info('Start:\t Checking Module- add_dynamic_marking_blocks')
        mouseLeftClickOn("parts_pane")
        if appears("add_dynamic"):
            mouseLeftClickOn("add_dynamic")
            mouseLeftClickOn("dynamic_block")
            pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
            pyautogui.press('enter')
            if appears("dynamic_menu"):
                mouseLeftClickOn("grinding")
                mouseLeftClickOn("grinding_ok")
                sleep(1)
                free()
                pyautogui.typewrite('657.5658,-266.0949',interval=0.2)
                pyautogui.press('enter')
                sleep(1)
                if appears("dynamic_edit"):
                    self.logger.info("Dynamic edit option appeared")
                    mouseLeftClickOn("dynamic2_button1_button","dynamic2_button1")
                    pyautogui.typewrite('1',interval=.2)
                    mouseLeftClickOn("dynamic2_button2")
                    pyautogui.typewrite('1', interval=.2)
                    sleep(1)
                    mouseLeftClickOn("dynamic_ok")
                    self.logger.info("Dynamic marking blocks done")
                else:
                    raise AssertionError("dynamic_edit menu did not appear")
            else:
                raise AssertionError("Dynamic menu did not appear")
        else:
            raise AssertionError("No button found named add dynamic")
        self.logger.info('End:\t Checking Module- add_dynamic_marking_blocks')

    def manage_datum_lines(self):
        self.logger.info('Start:\t Checking Module- manage_datum_lines')
        mouseLeftClickOn("plates_pane")
        if appears("datum_lines"):
            mouseLeftClickOn("datum_lines")
            free()
            pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("datum_menu"):
                self.logger.info("Datum menu appeared")
                mouseLeftClickOn("buttocks","buttocks_region")
                sleep(1)
                mouseLeftClickOn("datum_scroll")
                pyautogui.click(clicks=6)
                mouseLeftClickOn("waterlines", "waterlines_region")
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.typewrite('move',interval=.2)
                pyautogui.press('enter')
                pyautogui.typewrite('-3323.1639,1003.0055', interval=.3)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('-2500,272.2582', interval=.3)
                pyautogui.press('enter')
                pyautogui.typewrite('#-2500,272.2592', interval=.3)
                pyautogui.press('enter')
                self.logger.info("Datum lines managed")
                sleep(2)
                pyautogui.hotkey('ctrl', 'z')
            else:
                raise AssertionError("datum_menu did not appear")
        else:
            raise AssertionError("datum_lines option did not appear")
        self.logger.info('End:\t Checking Module- manage_datum_lines')

    def orientation_icons(self):
        self.logger.info('Start:\t Checking Module- orientation_icons')
        sleep(1)
        pyautogui.typewrite('select',interval=.1)
        pyautogui.press('enter')
        # pyautogui.typewrite('-3757.9401,1400.7431', interval=0.1)
        pyautogui.typewrite('-3429.1647,1402.7936', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        # pyautogui.click(button='right', clicks=2, interval=0.25)
        pyautogui.click(button='right')
        if appears("edit_properties2"):
            mouseLeftClickOn("edit_properties2")
            if appears("plate_properties"):
                mouseLeftClickOn("insert_orientation","insert_orientation_region")
                sleep(1)
                pyautogui.press('enter')
                self.logger.info("Orientation icons added")
            else:
                raise AssertionError("Plate Properties did not appear")
        else:
            raise AssertionError("Properties option did not appear")
        self.logger.info('End:\t Checking Module- orientation_icons')

    def editing_piecemarks(self):
        self.logger.info('Start:\t Checking Module- editing_piecemarks')
        sleep(5)
        pyautogui.typewrite('move', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-3323.1639,1003.0055', interval=.3)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('-2500,272.2582', interval=.3)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('#-2500,272.2592', interval=.3)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('rotate', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-3323.1639,1003.0055', interval=.3)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('-2500,272.2582', interval=.3)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('15', interval=.3)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.press('esc', 3)
        pyautogui.hotkey('ctrl', 'z')
        pyautogui.hotkey('ctrl', 'z')
        sleep(2)
        pyautogui.typewrite('select',interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-3323.1639,1003.0055', interval=.3)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("part_information_button"):
            mouseLeftClickOn("part_information_button")
            if appears("part_info_window2"):
                mouseRightClickOn("piecemark")
                if appears("edit_prop"):
                    mouseLeftClickOn("edit_prop")
                    pyautogui.typewrite('30',interval=.2)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.hotkey('esc')
                    pyautogui.hotkey('ctrl','s')
                    self.logger.info("Piecemark is edited")
                else:
                    raise AssertionError("edit_properties did not appear")
            else:
                raise AssertionError("Plate Properties did not appear")
        else:
            raise AssertionError("Properties option did not found")
        self.logger.info('End:\t Checking Module- editing_piecemarks')

    def adding_corner_treatments(self):
        self.logger.info('Start:\t Checking Module- adding_corner_treatments')
        # open_drawing_from_u03("u03_f60_node")
        add_corner_treatment()
        pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("corner_treatment_menu"):
            self.logger.info("Corner treatment menu appeared")
            if appears("r50_new",5):
                mouseLeftClickOn("r50_new")
            else:
                mouseLeftClickOn("r50")
            sleep(1)
            pyautogui.press('enter')
            pyautogui.typewrite('-4408.1709,1400', interval=0.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            self.logger.info("Corner treatment added")
        else:
            raise AssertionError("corner_treatment_menu did not appear")
        self.logger.info('End:\t Checking Module- adding_corner_treatments')

    def moving_construction_lines(self):
        self.logger.info('Start:\t Checking Module- moving_construction_lines')
        pyautogui.typewrite('stretch',interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-4328.0541,1490.0693', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-4408.1709,1400', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-4408.1709,1420', interval=.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.hotkey('ctrl', 'z')
        self.logger.info("Construction lines were moved")
        self.logger.info('End:\t Checking Module- moving_construction_lines')

    def remove_corner_treatments(self):
        self.logger.info('Start:\t Checking Module- remove_corner_treatments')
        mouseLeftClickOn("plates_pane")
        if appears("corner_treatment_region"):
            mouseLeftClickOn("corner_treatment_drop","corner_treatment_region")
            if appears("remove_corner"):
                self.logger.info("Remove corner option appeared")
                mouseMoveTo("remove_corner")
                free()
                pyautogui.press('esc')
                pyautogui.press('esc')
                sleep(1)
                self.logger.info("Corner treatment was removed")
            else:
                raise AssertionError("Remove corner option did not appear")
        else:
            raise AssertionError("corner_treatment option did not found")
        self.logger.info('End:\t Checking Module- remove_corner_treatments')

    def bevel_standards(self):
        self.logger.info('Start:\t Checking Module- bevel_standards')
        mouseDoubleClickOn("shipconstructor_tab_new")
        if appears("manage_pane"):
            mouseLeftClickOn("manage_pane")
            if appears("manage_button"):
                mouseLeftClickOn("manage_button")
                if appears("shipcon_manager"):
                    self.logger.info("Shipcon manager appeared")
                    mouseLeftClickOn("structure_button")
                    mouseLeftClickOn("plates_option")
                    mouseLeftClickOn("bevel_standards")
                    if appears("bevel_standards_window"):
                        if appears("pl06"):
                            mouseLeftClickOn("pl06")
                            pyautogui.press('right')
                            for i in range(0, 9):
                                pyautogui.typewrite('30',interval=0.1)
                                pyautogui.press('right')
                                pyautogui.typewrite('2',interval=0.1)
                                pyautogui.press('down')
                                pyautogui.press('left')
                            sleep(1)
                            mouseLeftClickOn("bevel_ok")
                            sleep(1)
                            pyautogui.hotkey('alt', 'f4')
                            self.logger.info("Bevel standard was checked")
                    else:
                        raise AssertionError("bevel_standards_window did not appear")
                else:
                    raise AssertionError("shipcon_manager window did not appear")
            else:
                raise AssertionError("Manaer button did not appear")
        else:
            raise AssertionError("Manage pane did not appear")
        self.logger.info('End:\t Checking Module- bevel_standards')

    def adding_bevel_standards(self):
        self.logger.info('Start:\t Checking Module- adding_bevel_standards')
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("parts_pane")
        if appears("bevel_standards_region"):
            mouseLeftClickOn("bevel_standards_drop","bevel_standards_region")
            mouseLeftClickOn("create_bevel")
            free()
            pyautogui.typewrite('-3757.9401,1400.7431', interval=0.1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.typewrite('-2500,1400', interval=0.1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(1323, 720)
            pyautogui.click()
            pyautogui.typewrite('-2502.1931,801.4675', interval=0.1)
            pyautogui.press('enter')
            if appears("bevel_codes",15):
                self.logger.info("Bevel codes window appeared")
                mouseLeftClickOn("ts30ns")
                sleep(1)
                mouseLeftClickOn("bevel_codes_ok")
                self.logger.info("Bevel standard was checked")
            else:
                raise AssertionError("bevel_codes window did not open")
        else:
            raise AssertionError("bevel_standards_region did not appear")
        self.logger.info('End:\t Checking Module- adding_bevel_standards')


    def removing_bevel_standards(self):
        self.logger.info('Start:\t Checking Module- removing_bevel_standards')
        mouseLeftClickOn("parts_pane")
        if appears("bevel_standards_region"):
            self.logger.info('Bevel option region appeared')
            mouseLeftClickOn("bevel_standards_drop","bevel_standards_region")
            free()
            pyautogui.press('esc')
            pyautogui.press('esc')
            self.logger.info("Bevel standard was removed")
        else:
            raise AssertionError("bevel_standards_region did not appear")
        self.logger.info('End:\t Checking Module- removing_bevel_standards')


    def bevel_solids(self):
        self.logger.info('Start:\t Checking Module- bevel_solids')
        mouseLeftClickOn("structure_main_pane")
        if appears("drawing_options_region2", 10):
            mouseLeftClickOn("drawing_options_dropdown3", "drawing_options_region2")
            if appears("structure_drawing_option"):
                self.logger.info("Structure drawing option appeared")
                mouseLeftClickOn("structure_drawing_option")
                if appears("bevel_solid_hide_region"):
                    mouseLeftClickOn("bevel_solid_hide","bevel_solid_hide_region")
                    sleep(1)
                    pyautogui.press('s')
                    mouseLeftClickOn("bevel_solid_ok")
                else:
                    pyautogui.press('enter')
                change_view("conceptual_visual")
                sleep(4)
                change_view("2d_wireframe")
                free()
                reset_viewport()
            else:
                raise AssertionError("structure_drawing_option did not appear")
        else:
            raise AssertionError("drawing_options_region2 did not appear")
        self.logger.info('End:\t Checking Module- bevel_solids')


    def weld_shrinkage(self):
        self.logger.info('Start:\t Checking Module- weld_shrinkage')
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("plates_pane")
        if appears("datum_region"):
            mouseLeftClickOn("datum_drop","datum_region")
            if appears("weld_shrinkage"):
                mouseLeftClickOn("weld_shrinkage")
                self.logger.info("Weld shrinkage option appeared")
            else:
                raise AssertionError("weld_shrinkage did not appeared")
            free()
            pyautogui.typewrite('-3757.9401,1400.7431', interval=0.3)
            pyautogui.press('enter')
            if appears("weld_shrinkage_window"):
                self.logger.info("Weld shrinkage window appeared")
                mouseLeftClickOn("apply_weld")
                pyautogui.press('tab')
                pyautogui.press('tab')
                pyautogui.press('3')
                pyautogui.press('tab')
                pyautogui.press('1')
                pyautogui.press('enter')
                self.logger.info("Weld shrinkage added")
            else:
                raise AssertionError("weld_shrinkage_window did not open")
        else:
            raise AssertionError("datum_region did not appear")
        self.logger.info('End:\t Checking Module- weld_shrinkage')

    def bevel_angles(self):
        self.logger.info('Start:\t Checking Module- bevel_angles')
        mouseLeftClickOn("structure_main_pane")
        if appears("structure_drawing_option2"):
            mouseLeftClickOn("structure_drawing_option2")
            if appears("bevel_text"):
                mouseLeftClickOn("bevel_text_show","bevel_text")
                sleep(1)
                pyautogui.press('h')
            mouseLeftClickOn("bevel_solid_ok")
            pyautogui.hotkey('ctrl','s')
        else:
            raise AssertionError("structure_drawing_option2 did not appear")
        self.logger.info('End:\t Checking Module- bevel_angles')

    def menu_check_for_line1(self):
        # if appears("aft_side_region",3):
        #     mouseLeftClickOn("aft_side", "aft_side_region")
        mouseLeftClickOn("stock_drop_down_2")
        if appears("hp120x7",5):
            mouseLeftClickOn("hp120x7")
        elif appears("bulb_flat",3):
            mouseLeftClickOn("bulb_flat")
            pyautogui.press('right')
            pyautogui.press('down')
            pyautogui.press('enter')
        free()
        if appears("flange_dir_region",5):
            mouseLeftClickOn("flange_dir_star")
        mouseLeftClickOn("aft_side_click")
        free()
        if appears("mid_symbol",3):
            mouseLeftClickOn("mid_symbol")
            pyautogui.press('s')
            pyautogui.press('s')
            pyautogui.press('enter')
        free()
        if appears("stiff_plate_selected",5):
            mouseLeftClickOn("stiff_plate_selected")
            mouseLeftClickOn("stiffener_markline")
            free()
        # mouseLeftClickOn("stock_drop_down")
        if appears("start_none",3):
            mouseLeftClickOn("start_none")
            pyautogui.press('down')
            pyautogui.press('enter')
        free()
        if appears("start_none",3):
            mouseLeftClickOn("start_none")
            # mouseLeftClickOn("c30", "end_region2")
            pyautogui.press('down')
            pyautogui.press('enter')
        if appears("green_100",5):
            mouseLeftClickOn("green_100")
            pyautogui.press('up')
            pyautogui.press('enter')
            pyautogui.press('tab')
            pyautogui.press('up')
        mouseLeftClickOn("stiffeners_ok")

    # self.menu_check_for_line1()

    def create_stiffeners(self):
        self.logger.info('Start:\t Checking Module- create_stiffeners')
        open_U03_drawing(drawing_name="u03_f60_node", tab_name="u03_f60_drawing_tab")
        sleep(1)
        if appears("u03_f60_drawing_tab"):
            reset_viewport()
            mouseDoubleClickOn("sc_utilites")
            mouseLeftClickOn("visual_button")
            if appears("activate_ucs"):
                mouseLeftClickOn("activate_ucs")
                if appears("activate_ucs_menu"):
                    mouseLeftClickOn("activate_from_object")
                    pyautogui.typewrite('-751.8995,1081.9336',interval=0.2)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('enter')


                    zoom_to_ssi_u03_p01()
                    sleep(1)
                    pyautogui.press('esc')
                    line('-683.2019','298.1024','-683.2019','1115.6280')
                    select_new_stiffener_option()
                    pyautogui.typewrite('-683.2019,595.9953',interval=.2)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('-751.8995,1081.9336', interval=.2)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    if appears("stiff_properties"):
                        self.menu_check_for_line1()
                        sleep(4)
                        pyautogui.press('enter')
                        sleep(1)
                        conceptual_visual()
                        sleep(2)
                        wireframe2d_visual()
                        reset_viewport()
                        zoom_to_ssi_u03_p01()
                        pyautogui.press('esc')
                        line('-568.8870','396.6534','-568.8870','974.5208')
                        sleep(1)
                        mouseDoubleClickOn("structure_ribbon_button")
                        select_new_stiffener_option()
                        free()
                        pyautogui.typewrite('-568.8870,445.6534', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        if appears("stiff_properties"):
                            if appears("web_dir1", 10):
                                mouseLeftClickOn("web_dir1_aft", "web_dir1")
                            if appears("flange_dir1", 10):
                                mouseLeftClickOn("flange_dir1_star", "flange_dir1")
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("Stiffeners properties for line 2 did not appear")
                    else:
                        raise AssertionError("Stiffeners properties for line 1 did not appear")
            else:
                raise AssertionError("activate_ucs did not appear")
        else:
            raise AssertionError("u03_f60_drawing_tab did not appear")
        self.logger.info('End:\t Checking Module- create_stiffeners')

    def loose_stiffeners_to_plate(self):
        self.logger.info('Start:\t Checking Module- loose_stiffeners_to_plate')
        if appears("stiffeners"):
            mouseLeftClickOn("stiffeners")
        else:
            raise AssertionError("stiffeners pane did not appear")
        if appears("attach_stiffeners"):
            mouseLeftClickOn("attach_stiffeners")
            free()
            pyautogui.typewrite('-564.4041,598.2351',interval=.2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.typewrite('-751.8995,1081.9336', interval=0.2)
            pyautogui.press('enter')
            sleep(1)

            pyautogui.press('enter')
            if appears("select_markline_menu"):
                mouseLeftClickOn("select_markline_menu_ok")
                sleep(1)
                pyautogui.press('enter')
            else:
                raise AssertionError("select_markline_menu did not appear")
        else:
            raise AssertionError("attach_stiffeners did not open")
        self.logger.info('End:\t Checking Module- loose_stiffeners_to_plate')

    def editing_stiffeners(self):
        self.logger.info('Start:\t Checking Module- editing_stiffeners')
        mouseLeftClickOn("stiffeners")
        if appears("edit_sniffeners"):
            mouseLeftClickOn("edit_sniffeners")
            free()
            pyautogui.typewrite('-564.4041,598.2351', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("stiff_properties"):
                # mouseLeftClickOn("len1")
                pyautogui.press('tab',presses=11,interval=.1)
                sleep(1)
                pyautogui.typewrite('-10',interval=.1)
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.typewrite('-100', interval=.1)
                sleep(1)
                pyautogui.press('enter')
                sleep(3)
                pyautogui.press('enter')
            else:
                raise AssertionError("Stiffeners properties did not appear")
        else:
            raise AssertionError("edit_sniffeners did not appear")
        self.logger.info('End:\t Checking Module- editing_stiffeners')

    def creating_face_plates(self):
        self.logger.info('Start:\t Checking Module- creating_face_plates')
        sleep(2)
        reset_viewport()
        zoom_to_ssi_u03_p07()
        select_new_faceplate_option()
        pyautogui.typewrite('-3325.2534,1391.5924', interval=0.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-4117.9085,1399.8279', interval=.2)
        pyautogui.press('enter')
        if appears("faceplates_properties"):
            mouseLeftClickOn("stock_drop_down_2")
            if appears("fp_fb120",10):
                mouseLeftClickOn("fp_fb120")
            else:
                pyautogui.press('esc')
            pyautogui.press('tab',presses=5, interval=.1)
            sleep(1)
            pyautogui.press('up',presses=5, interval=.1)
            sleep(1)
            pyautogui.press('tab')
            sleep(1)
            pyautogui.press('up',presses=5, interval=.1)
            sleep(1)
            if appears("fp_center",3):
                mouseLeftClickOn("fp_center_button","fp_center")
            if appears("fp_show",3):
                mouseLeftClickOn("fp_show_button","fp_show")
            mouseLeftClickOn("fp_ok")
            sleep(3)
            pyautogui.moveTo(350,135)
            sleep(1)
            pyautogui.click()
            sleep(2)
            pyautogui.moveTo(1473,128)
            sleep(1)
            pyautogui.click()
            sleep(2)
            pyautogui.moveTo(895,112)
            sleep(1)
            if appears("thickness_direction",10):
                pyautogui.click()
            else:
                raise AssertionError("thickness_direction option did not appear")
            sleep(3)
            conceptual_visual()
            sleep(2)
            wireframe2d_visual()
        else:
            raise AssertionError("faceplates_properties did not appear")
        self.logger.info('End:\t Checking Module- creating_face_plates')

    def modifying_face_plates(self):
        self.logger.info('Start:\t Checking Module- modifying_face_plates')
        select_new_faceplate_option()
        free()
        pyautogui.typewrite('-4365.9488,766.4198', interval=0.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-3167.0871,888.6262', interval=0.2)
        pyautogui.press('enter')
        if appears("faceplates_properties"):
            mouseLeftClickOn("fp_ok")
            pyautogui.typewrite('-3000.00,1200.00', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-3000.00,800.00', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-3200.00,1000.00', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-3060.2919,995.3417', interval=.2)
            pyautogui.press('enter')
            mouseLeftClickOn("faceplates_button")
            if appears("fp_rotate"):
                mouseLeftClickOn("fp_rotate")
                mouseMoveTo("empty_space2")
                pyautogui.typewrite('-3192.9246,1002.2266', interval=.2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('-3000.00,1000.00', interval=.2)
                pyautogui.press('enter')
                pyautogui.press('esc')
                pyautogui.press('esc')
                reset_viewport()
            else:
                raise AssertionError("fp_rotate button did not found")
        else:
            raise AssertionError("Faceplate properties did not appear")
        self.logger.info('End:\t Checking Module- modifying_face_plates')

    def trimming_profile(self):
        self.logger.info('Start:\t Checking Module- trimming_profile')
        sleep(3)
        reset_viewport()
        select_new_stiffener_option()
        sleep(10)
        pyautogui.typewrite('-6135.2451,2543.0176', interval=.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('s')
        pyautogui.press('enter')
        sleep(5)
        pyautogui.moveTo(336,251)
        # pyautogui.moveTo(364,252)
        sleep(1)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(437,590)
        # pyautogui.moveTo(455,577)
        sleep(1)
        pyautogui.click()
        pyautogui.press('enter')
        if appears("stiff_properties"):
            mouseLeftClickOn("stock_drop_down")
            if appears("200x10",5):
                mouseLeftClickOn("200x10")
            elif appears("bulb_flat",5):
                mouseLeftClickOn("bulb_flat")
                pyautogui.press('right')
                mouseLeftClickOn("200x10")
            else:
                free()
                pyautogui.press('esc')

            pyautogui.press('tab',presses=6,interval=.1)
            pyautogui.press('up',presses=5)
            sleep(2)
            pyautogui.press('tab')
            sleep(1)
            pyautogui.press('up',presses=5)
            if appears("starboard_region_9",5):
                mouseLeftClickOn("starboard")
            # if appears("web_dir_up",5):
            #     mouseLeftClickOn("web_dir_up")
            sleep(1)
            # pyautogui.press('enter')
            mouseLeftClickOn("sp_ok")
            sleep(5)


            free()
            pyautogui.typewrite('select',interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-6786.7003,6601.1454', interval=.2)
            pyautogui.press('enter')
            pyautogui.click(button='right', clicks=2, interval=0.25)
            if appears("trim"):
                mouseLeftClickOn("trim")
                if appears("new_trim"):
                    mouseLeftClickOn("new_trim")
                    sleep(1)
                    if appears("start_trim_new",3):
                        mouseLeftClickOn("start_trim_new")
                        pyautogui.typewrite('e',interval=.2)
                    elif appears("start_trim",3):
                        mouseLeftClickOn("start_trim")
                        pyautogui.typewrite('e', interval=.2)
                    if appears("custom_trim_new",3):
                        mouseLeftClickOn("custom_trim_new")
                        pyautogui.typewrite('d', interval=.2)
                    elif appears("custom_trim",3):
                        mouseLeftClickOn("custom_trim")
                        pyautogui.typewrite('d', interval=.2)
                    mouseLeftClickOn("pick_trim")
                    sleep(1)
                    pyautogui.typewrite('-6636.3501,6700.00',interval=.2)
                    pyautogui.press('enter')
                    if appears("trim_warning",5):
                        pyautogui.press("enter")
                    if appears("6700_new",5):
                        mouseDoubleClickOn("6700_new")
                    elif appears("6700",5):
                        mouseDoubleClickOn("6700")
                    pyautogui.typewrite('6400.0000',interval=.2)
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.press('enter')
                    sleep(1)
                    self.logger.info("Trimming Done")
                    sleep(2)
                    line('-6073.5356','1238.4229','-5040.6735','2157.5687')
                    pyautogui.typewrite('select', interval=.2)
                    pyautogui.press('enter')
                    pyautogui.typewrite('-6399.1266,4520.6526', interval=.2)
                    pyautogui.press('enter')
                    pyautogui.click(button='right', clicks=2, interval=0.25)
                    if appears("trim"):
                        mouseLeftClickOn("trim")
                        if appears("new_trim"):
                            mouseLeftClickOn("new_trim")
                            sleep(1)
                            mouseLeftClickOn("pick_trim")
                            pyautogui.typewrite('-6073.5356,1238.4229', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('-5040.6735,2157.5687', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            if appears("trim_warning", 10):
                                pyautogui.press('enter')
                            if appears("trim_warning_yes",10):
                                mouseLeftClickOn("trim_warning_yes")
                            if appears("stiff_trim"):
                                pyautogui.press('enter')
                                sleep(2)
                                pyautogui.typewrite('select', interval=.2)
                                pyautogui.press('enter')
                                pyautogui.typewrite('-6399.1266,4520.6526', interval=.2)
                                pyautogui.press('enter')
                                pyautogui.press('enter')
                                mouseDoubleClickOn("sc_utilites")
                                mouseLeftClickOn("sc_utilities_tools_button")
                                if appears("sc_utilities_mirror_button"):
                                    mouseLeftClickOn("sc_utilities_mirror_button")
                                    self.logger.info("Mirroring done successfully")
                                    free()
                                    sleep(1)
                                    pyautogui.typewrite('select', interval=.2)
                                    pyautogui.press('enter')
                                    pyautogui.typewrite('5731.0628,2062.9501', interval=.2)
                                    pyautogui.press('enter')
                                    pyautogui.click(button='right', clicks=2, interval=0.25)
                                    if appears("trim"):
                                        mouseLeftClickOn("trim")
                                        if appears("custom_trim2"):
                                            mouseLeftClickOn("custom_trim2")
                                            pyautogui.press('d')
                                            sleep(3)
                                            mouseLeftClickOn("pick_trim")
                                            pyautogui.typewrite('5704.2384,1400.00',interval=.2)
                                            pyautogui.press('enter')
                                            sleep(4)
                                            pyautogui.press('enter')
                                            sleep(4)
                                            pyautogui.press('enter')
                                            sleep(4)
                                            pyautogui.press('enter')
                                            sleep(3)
                                            pyautogui.press('esc')
                                            self.logger.info("Profile was trimmed")
                                    else:
                                        raise AssertionError("trim menu did not appear for the mirrored line")
                                else:
                                    raise AssertionError("Mirroring did not completed")
                            else:
                                raise AssertionError("Trims menu did not appear")
                    else:
                        raise AssertionError("trim option did not appear for the 2nd time")
            else:
                raise AssertionError("trim option did not appear")
        else:
            raise AssertionError("Stiffeners properties did not appear")
        self.logger.info('End:\t Checking Module- trimming_profile')


    def adding_cutouts_in_profiles(self):
        self.logger.info('Start:\t Checking Module- adding_cutouts_in_profiles')
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("stiffeners")
        if appears("insert_cutout_region_new"):
            mouseLeftClickOn("insert_cutout","insert_cutout_region_new")
            mouseLeftClickOn("insert_profile_cutout")
            free()
            pyautogui.typewrite('-6048.1450,2899.5362', interval=0.2)
            pyautogui.press('enter')
            if appears("insert_cutout_menu"):
                if appears("insert_cutout_filter"):
                    mouseLeftClickOn("insert_cutout_filter")
                    pyautogui.typewrite('R30',.3)
                    pyautogui.press('down')
                    pyautogui.press('enter')
                else:
                    raise AssertionError("insert_cutout_filter option did not appear")
                pyautogui.typewrite('2065.8394,613.6485', interval=.2)
                pyautogui.press('enter')
                pyautogui.press('0')
                pyautogui.press('enter')
                pyautogui.press('esc')
                sleep(1)
                pyautogui.press('enter')
            else:
                raise AssertionError("insert_cutout_menu did not appear")
        else:
            raise AssertionError("insert_cutout_region did not appear")
        self.logger.info('End:\t Checking Module- adding_cutouts_in_profiles')

    def editing_cutouts(self):
        self.logger.info('Start:\t Checking Module- editing_cutouts')
        mouseLeftClickOn("stiffeners")
        if appears("insert_cutout_region_new"):
            mouseLeftClickOn("insert_cutout", "insert_cutout_region_new")
            mouseLeftClickOn("edit_cutout")
            free()
            pyautogui.typewrite('-6048.1450,2899.5362', interval=0.2)
            pyautogui.press('enter')
            if appears("cutout_list"):
                mouseRightClickOn("r30_cut")
                mouseLeftClickOn("cut_prop")
                if appears("edit_cut_menu"):
                    sleep(2)
                    pyautogui.press('enter')
                    pyautogui.hotkey('alt','f4')
                    if appears("update_related_objects_window"):
                        pyautogui.press('enter')
                        free()
                    else:
                        raise AssertionError("update_related_objects_window did not appear")
                else:
                    raise AssertionError("edit_cut_menu did not appear")
            else:
                raise AssertionError("cutout_list did not appear")
        else:
            raise AssertionError("insert_cutout_region did not appear")
        self.logger.info('End:\t Checking Module- editing_cutouts')

    def inserting_welding_seam_reliefs(self):
        self.logger.info('Start:\t Checking Module- inserting_welding_seam_reliefs')
        zoom_to_ssi_u03_p01()
        sleep(1)
        line('-725.7898','674.3881','-490.4356','840.1331')
        mouseLeftClickOn("stiffeners")
        if appears("edit_cutout_region"):
            mouseLeftClickOn("insert_cutout", "edit_cutout_region")
            mouseLeftClickOn("insert_weld")
            free()
            pyautogui.typewrite('-676.4775,813.2555', interval=0.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-568.8870,837.8933', interval=0.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('-725.7898,674.3881', interval=0.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-490.4356,840.1331', interval=0.2)
            pyautogui.press('enter')
            if appears("insert_cutout2"):
                if appears("r30"):
                    pyautogui.press('enter')
                else:
                    mouseLeftClickOn("cutout_down")
                    pyautogui.click(clicks=6)
                    mouseLeftClickOn("r30_non_selected")
                    pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
                pyautogui.press('enter')
            else:
                raise AssertionError("insert_cutout menu did not appear")
        else:
            raise AssertionError("insert_cutout_region did not appear")
        self.logger.info('End:\t Checking Module- inserting_welding_seam_reliefs')

    def extracting_lines_from_siffners(self):
        self.logger.info('Start:\t Checking Module- extracting_lines_from_siffners')
        reset_viewport()
        sleep(1)
        mouseLeftClickOn("stiffeners")
        if appears("extract_neutral"):
            mouseLeftClickOn("extract_neutral")
            free()
            pyautogui.typewrite('-6363.7135,4365.8227', interval=.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.hotkey('ctrl', 's')
        else:
            raise AssertionError("extract_neutral did not appear")
        self.logger.info('End:\t Checking Module- extracting_lines_from_siffners')

    def open_curved(self):
        self.logger.info('Start:\t Checking Module- open_curved')
        mouseLeftClickOn('shipconstructor_tab_new')
        if appears("main_panel_new"):
            mouseLeftClickOn('main_panel_new', 'main_panel_new')
            mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
            if appears("structure_node2"):
                mouseLeftClickOn("structure_node2")
                if appears("u03_shell_from_hull",5):
                    mouseDoubleClickOn("u03_shell_from_hull")
                if appears("u03_shell_high",5):
                    mouseDoubleClickOn("u03_shell_high")
            if appears("save_caution",10):
                pyautogui.press('enter')
        else:
            raise AssertionError("main_panel did not appear")
        self.logger.info('End:\t Checking Module- open_curved')

    def create_curved_plates(self):
        self.logger.info('Start:\t Checking Module- create_curved_plates')
        open_drawing_u03()
        if appears("curved_option",10):
            mouseLeftClickOn("curved_option")
            mouseLeftClickOn("new_curved")
            sleep(1)
            pyautogui.typewrite('_SHELL',interval=.1)
            pyautogui.press('enter')
        sleep(5)
        if appears("u03_shell_tab"):
            self.logger.info('u03_shell_tab appeared')
            pyautogui.hotkey('ctrl','s')
            sleep(5)
            open_drawing_from_hull("u03_shell_from_hull")
            sleep(30)
            change_viewpoint_plan_looking_down()
            mouseDoubleClickOn("hull_pane")
            mouseLeftClickOn("hull_utility")
            mouseLeftClickOn("hull_export_to_structure")
            free()
            pyautogui.typewrite('25980.3245,22299.4292',interval=0.2)
            pyautogui.press('enter')
            pyautogui.typewrite('42997.3324,856.5936',interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            if appears("choose_group"):
                if appears("ssi_training"):
                    pyautogui.press('tab')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('down')
                    pyautogui.press('right')

                    if appears("u03_shell_",10):
                        mouseDoubleClickOn("u03_shell_")
                    else:
                        raise AssertionError("u03_shell_ did not appear")
                    pyautogui.press('enter')
                    sleep(5)
                    pyautogui.press('enter')
                if appears("curved_prop"):
                    sleep(2)
                    mouseLeftClickOn("fp_prod_hier")
                    mouseLeftClickOn("pp_unassigned")
                    sleep(1)
                    mouseLeftClickOn("curved_plates_tab")
                    mouseLeftClickOn("generate")
                    if appears("apply_to_all_click",5):
                        mouseLeftClickOn("apply_to_all_click")
                    pyautogui.press('enter')
                    sleep(10)
                    self.open_curved()
                    sleep(10)
                else:
                    raise AssertionError("curved_properties did not appear")
            else:
                raise AssertionError("choose_group menu did not appear")
        else:
            raise AssertionError("u03_shell_tab did not appear")
        self.logger.info('End:\t Checking Module- create_curved_plates')

    def editing_surface_properties(self):
        self.logger.info('Start:\t Checking Module- editing_surface_properties')
        if appears("u03_shell_tab"):
            free()
            sleep(2)
            reset_viewport()
            sleep(2)
            pyautogui.typewrite('select',interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('28186.9739,20572.3710',interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseDoubleClickOn("structure_ribbon_button")
            mouseLeftClickOn("curved_plate_button")
            if appears("surface_properties"):
                mouseLeftClickOn("surface_properties")
                if appears("surface_properties2"):
                    sleep(2)
                    pyautogui.hotkey('alt', 'f4')
                else:
                    raise AssertionError("surface_properties menu did not appear")
            else:
                raise AssertionError("surface_properties did not appear")
        else:
            raise AssertionError("U03 tab did not open")
        self.logger.info('End:\t Checking Module- editing_surface_properties')

    def editing_marklines(self):
        self.logger.info('Start:\t Checking Module- editing_marklines')
        free()
        if appears("curved_plate_button"):
            mouseLeftClickOn("curved_plate_button")
            if appears("markline_prop"):
                mouseLeftClickOn("markline_prop")
                if appears("edit_marklines"):
                    pyautogui.press('tab')
                    mouseClickAppears("expand")
                    mouseLeftClickOn("mark_down_arrow")
                    pyautogui.click(clicks=5)
                    mouseClickAppears("expand")
                    mouseRightClickOn("f48_markline")
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.hotkey('alt', 'f4')
                    mouseRightClickOn("f48_markline_high")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.press('enter')
                    mouseRightClickOn("f48_markline_high")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("label_prop")
                    mouseDoubleClickOn("text_size")
                    pyautogui.typewrite('50', interval=.2)
                    pyautogui.press('enter')
                    sleep(3)
                    mouseRightClickOn("f48_markline_high")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("name_mark")
                    pyautogui.click(clicks=2)
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    mouseLeftClickOn("offset_save")
                    sleep(2)
                    pyautogui.press('enter')
                    if appears("warning5", 30):
                        pyautogui.press('left')
                        pyautogui.press('enter')
                    if appears("offset_off", 10):
                        sleep(2)
                        pyautogui.hotkey('alt', 'f4')
                    else:
                        self.logger.info("Notepad did not open")
                    if appears("config_offset_output", 10):
                        mouseRightClickOn("config_offset_output")
                        pyautogui.press('down')
                        pyautogui.press('down')
                        pyautogui.press('enter')
                    # sleep(3)
                    # pyautogui.hotkey('alt', 'f4')
                    # sleep(4)
                    free()
                    if appears("edit_marklines_close",10):
                        mouseLeftClickOn("edit_marklines_close")
                    if appears("edit_marklines_ico", 10):
                        mouseLeftClickOn("edit_marklines_ico")
                        # pyautogui.click()
                        pyautogui.hotkey('alt', 'f4')
                        # mouseLeftClickOn("marklines_close")

                else:
                    raise AssertionError("edit_marklines window did not appear")
            else:
                raise AssertionError("markline_prop did not appear")
        else:
            raise AssertionError("curved_plate_button did not appear")
        self.logger.info('End:\t Checking Module- editing_marklines')

    def replacing_outer_toolpath(self):
        self.logger.info('Start:\t Checking Module- replacing_outer_toolpath')
        free()
        pyautogui.press('esc')
        change_viewpoint_plan_looking_down()
        sleep(1)
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('28186.9739,20572.3710', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mouseLeftClickOn("curved_plate_button")
        if appears("extract_curved"):
            mouseLeftClickOn("extract_curved")
            sleep(3)
            free()
            pyautogui.press('esc')
            if appears("zoom_here"):
                mouseMoveTo("zoom_here")
            pyautogui.scroll(5)
            pyautogui.scroll(5)
            pyautogui.scroll(5)
            pyautogui.scroll(5)
            pyautogui.typewrite('select', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('27725.7412,20578.0214', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.moveTo(734,490)
            pyautogui.click()
            sleep(2)
            pyautogui.typewrite('#27646.0292,20616.9530', interval=.2)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("curved_plate_button")
            if appears("outer_toolpath"):
                mouseLeftClickOn("outer_toolpath")
                free()
                pyautogui.typewrite('27725.9353,20575.7440', interval=.1)
                pyautogui.press('enter')
                pyautogui.typewrite('27646.0292,20616.9530', interval=.1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.press('enter')
                self.logger.info("Outer toolpath was replaced")
            else:
                self.logger.info("outer_toolpath option did not found")
        else:
            raise AssertionError("extract option did not appear")
        self.logger.info('End:\t Checking Module- replacing_outer_toolpath')

    def adding_object_to_curved_plate(self):
        self.logger.info('Start:\t Checking Module- adding_object_to_curved_plate')
        pyautogui.typewrite('rectang', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('28479.1245,19648.0158', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('578.4749,710.709', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('select', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('28479.1245,19648.0158', interval=.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("properties2"):
            mouseLeftClickOn("properties2")
            if appears("layer_click"):
                mouseLeftClickOn("layer_click")
                pyautogui.press('right')
                mouseLeftClickOn("properties_close_button")
            free()
            pyautogui.press('esc')
            mouseLeftClickOn("parts_pane")
            if appears("add_object"):
                mouseLeftClickOn("add_object")
                free()
                pyautogui.typewrite('28076.0937,20590.8999', interval=.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('28479.1245,19648.0158', interval=.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("select_markline2"):
                    if appears("continuous", 2):
                        mouseLeftClickOn("continuous")
                    else:
                        mouseLeftClickOn("continuous_unhigh")
                    pyautogui.press('enter')
                    sleep(3)
                    free()
            else:
                raise AssertionError("add_object option did not found")
        else:
            raise AssertionError("properties option did not appear")
        self.logger.info('End:\t Checking Module- adding_object_to_curved_plate')

    def thinning_production(self):
        self.logger.info('Start:\t Checking Module- thinning_production')
        mouseLeftClickOn("curved_plate_button")
        if appears("thin_profile"):
            mouseLeftClickOn("thin_profile")
            free()
            pyautogui.typewrite('28076.0937,20590.8999', interval=.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('enter')
        else:
            raise AssertionError("Thin option did not found")
        self.logger.info('End:\t Checking Module- thinning_production')

    def extracting_production_information(self):
        self.logger.info('Start:\t Checking Module- extracting_production_information')
        mouseLeftClickOn("curved_plate_button")
        if appears("extract_product_info"):
            mouseLeftClickOn("extract_product_info")
            free()
            pyautogui.typewrite('28076.0937,20590.8999', interval=.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            free()
            pyautogui.press('esc')
            pyautogui.typewrite('select', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('28076.0937,20590.8999', interval=.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseDoubleClickOn("sc_utilites")
            if appears("visual_sc"):
                mouseLeftClickOn("visual_sc")
                if appears("hide_object"):
                    mouseLeftClickOn("hide_object")
                    free()
                    pyautogui.typewrite('select', interval=.2)
                    pyautogui.press('enter')
                    pyautogui.typewrite('27135.9490,21373.7754', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('38992.4183,18584.6724', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('delete', interval=.2)
                    pyautogui.press('enter')
                    mouseLeftClickOn("visual_sc")
                    mouseLeftClickOn("hide_obj_selected")
                    # mouseLeftClickOn("hovered_drop_down_region2")
                    mouseLeftClickOn("unhide_obj")
                    free()
                    pyautogui.hotkey('ctrl','s')
                    sleep(5)
                else:
                    raise AssertionError("visual_sc option did not appear")
            else:
                raise AssertionError("hide_object button did not appear")
        self.logger.info('End:\t Checking Module- extracting_production_information')


    def creating_plank(self):
        self.logger.info('Start:\t Checking Module- creating_plank')
        open_drawing_from_u03("u03_f64_node")
        sleep(4)
        if appears("u03_shell_tab"):
            change_viewpoint_body_looking_fwd()
            mouseLeftClickOn("structure_ribbon_button")
            if appears("utilities_pane"):
                mouseLeftClickOn("utilities_pane")
                mouseLeftClickOn("mark_group_intersection_button")
                if appears("mark_group_intersection_dialog"):
                    mouseDoubleClickOn("mgi_lngbhd_node")
                    mouseLeftClickOn("mgi_u03_2500p")
                    pyautogui.press('right')
                    pyautogui.press('space')
                    pyautogui.press('down')
                    pyautogui.press('space')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('space')
                    pyautogui.press('down')
                    pyautogui.press('space')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('space')
                    pyautogui.press('down')
                    pyautogui.press('space')
                    sleep(2)
                    pyautogui.press('enter')
                    self.logger.info('Mark Group Intersections line appeared')
                    if appears("plank_pane"):
                        mouseLeftClickOn("plank_pane")
                        mouseLeftClickOn("new_plank")
                        free()
                        pyautogui.typewrite('-4380.6636,4046.9616',interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('-2500,6700',interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('#-2500,4712', interval=.2)
                        pyautogui.press('enter')
                        if appears('plank_prop'):
                            if appears("6082",3):
                                mouseLeftClickOn("6082")
                            if appears("6082_high",3):
                                mouseLeftClickOn("6082_high")
                            pyautogui.press('enter')
                            sleep(5)
                            conceptual_visual()
                            if appears("plank_part_color",5):
                                self.logger.info("Plank part was created")
                        else:
                            raise AssertionError("plank_properties window did not appear")
                    else:
                        raise AssertionError("plank_pane did not appear")
                else:
                    raise AssertionError("mark_group_intersection_dialog did not appear")
            else:
                raise AssertionError("utilities_pane did not appear")
        else:
            raise AssertionError("u03_f64 did not open")
        self.logger.info('End:\t Checking Module- creating_plank')

    def editing_plank(self):
        self.logger.info('Start:\t Checking Module- editing_plank')
        wireframe2d_visual()
        mouseLeftClickOn("plank_pane")
        if appears("edit_plank"):
            mouseLeftClickOn("edit_plank")
            free()
            pyautogui.typewrite('-4762.4069,5501.6484', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("edit_plank_menu"):
                sleep(2)
                pyautogui.press('enter')
                self.logger.info('Plank part editing done')
            else:
                raise AssertionError("edit_plank_menu did not appear")
        else:
            raise AssertionError("edit_plank option did not found")
        self.logger.info('End:\t Checking Module- editing_plank')

    def editing_plank_collection(self):
        self.logger.info('Start:\t Checking Module- editing_plank_collection')
        mouseLeftClickOn("plank_pane")
        if appears("edit_plank_collection"):
            mouseLeftClickOn("edit_plank_collection")
            free()
            pyautogui.typewrite('-4762.4069,5501.6484', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("plank_properties"):
                sleep(2)
                mouseLeftClickOn("product_hierarchy_tab")
                mouseLeftClickOn("pp_unassigned")
                sleep(2)
                mouseLeftClickOn("pp_ok")
                self.logger.info('Plank part collection editing done')
            else:
                raise AssertionError("plank_properties did not appear")
        else:
            raise AssertionError("edit_plank_collection option did not found")
        self.logger.info('End:\t Checking Module- editing_plank_collection')

    def splitting_plank_collections(self):
        self.logger.info('Start:\t Checking Module- splitting_plank_collections')
        mouseLeftClickOn("plank_pane")
        if appears("split_plank"):
            mouseLeftClickOn("split_plank")
            free()
            pyautogui.typewrite('-7311.7302,6855.4768', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('-1058.1189,2231.4571', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            self.logger.info('Splitting done')
            reset_viewport()
            conceptual_visual()
            if appears("plank_part_splitted",5):
                self.logger.info("Plank part was splitted")
        else:
            raise AssertionError("split_plank option did not appear")
        self.logger.info('End:\t Checking Module- splitting_plank_collections')

    def deleting_plank(self):
        self.logger.info('Start:\t Checking Module- deleting_plank')
        wireframe2d_visual()
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('-4762.4069,5501.6484', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete', interval=.2)
        pyautogui.press('enter')
        reset_viewport()
        conceptual_visual()
        free()
        if appears("plank_part_deleted",5):
            self.logger.info("Plank part was deleted")
        self.logger.info('End:\t Checking Module- deleting_plank')

    def deleting_plank_collection(self):
        self.logger.info('Start:\t Checking Module- deleting_plank_collection')
        wireframe2d_visual()
        mouseLeftClickOn("plank_pane")
        if appears("delete_plank_collection"):
            mouseLeftClickOn("delete_plank_collection")
            free()
            pyautogui.typewrite('-3256.3996,4214.7131', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('enter')
            mouseLeftClickOn("plank_pane")
            mouseLeftClickOn("delete_plank_collection")
            free()
            pyautogui.typewrite('-3256.0133,2170.1466', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('enter')
            pyautogui.press("Plank part collection deleted")
            pyautogui.hotkey('ctrl', 's')
        else:
            raise AssertionError("delete_plank_collection option did not found")
        self.logger.info('End:\t Checking Module- deleting_plank_collection')



    def creating_corrugated(self):
        self.logger.info('Start:\t Checking Module- creating_corrugated')
        open_drawing_from_u03("u03_f64_node")
        sleep(4)
        if appears("u03_shell_tab"):
            mouseLeftClickOn("structure_ribbon_button")
            mouseLeftClickOn("corrugated_pane")
            if appears("corrugated_new"):
                mouseLeftClickOn("corrugated_new")
                free()
                pyautogui.typewrite('-4380.6636,4046.9616', interval=.2)
                pyautogui.press('enter')
                if appears("corrugated_prop"):
                    if appears("corrugated_from_region",5):
                        mouseLeftClickOn("corrugated_from", "corrugated_from_region")
                    if appears("1220x85",5):
                        mouseLeftClickOn("1220x85")
                    if appears("show_expand_region",5):
                        mouseLeftClickOn("show_expand", "show_expand_region")
                    if appears("forward_region",5):
                        mouseLeftClickOn("forward", "forward_region")
                    mouseLeftClickOn("corrugated_prop_ok")
                    pyautogui.typewrite('-2500.00,1400.00', interval=.2)
                    pyautogui.press('enter')
                    sleep(3)
                    change_view("conceptual_visual")
                    sleep(2)
                    free()
                    if appears("corrugated_plate",5):
                        self.logger.info("corrugated_plate was created")
                else:
                    raise AssertionError("corrugated_properties did not appear")
            else:
                raise AssertionError("corrugated_new button did not appear")
        else:
            raise AssertionError("u03_f64 did not open")
        self.logger.info('End:\t Checking Module- creating_corrugated')


    def mgi_select(self):
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('down')
        pyautogui.press('right')
        pyautogui.press('space')
        pyautogui.press('down')
        pyautogui.press('space')
        sleep(2)
        if appears("select_plane_check",5):
            mouseLeftClickOn("select_plane_check")
        pyautogui.press('enter')

    def creating_corrugated_plate_from_plate_stock(self):
        self.logger.info('Start:\t Checking Module- creating_corrugated_plate_from_plate_stock')
        wireframe2d_visual()
        sleep(2)
        change_viewpoint_body_looking_fwd()
        open_drawing_from_u03("u03_2500p")
        sleep(4)
        if appears("u03_2500p_tab"):
            sleep(10)
            sleep(10)
            sleep(10)
            mouseLeftClickOn("structure_ribbon_button")
            if appears("utilities_pane"):
                mouseLeftClickOn("utilities_pane")
                mouseLeftClickOn("mark_group_intersection_button")
            if appears("mark_group_intersection_dialog"):
                mouseDoubleClickOn("mgi_frame_node")
            else:
                raise AssertionError("mark_group_intersection_dialog did not appear")
            self.mgi_select()
            change_viewpoint_angle()
            sleep(2)
            free()
            pyautogui.typewrite('offset', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('500', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('35649.7350,6699.6338', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('37515.2167,6176.4992', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('esc')
            mouseDoubleClickOn("sc_utilites")
            mouseLeftClickOn("visual_sc")
            if appears("activate_ucs"):
                mouseLeftClickOn("activate_ucs")
                if appears("world_cs",10):
                    mouseDoubleClickOn("world_cs")
                else:
                    mouseDoubleClickOn("activate_ucs_ok")
            else:
                raise AssertionError("activate_ucs did not appear")
            free()
        else:
            raise AssertionError("u03_2500p_tab tab did not appear")
        self.logger.info('End:\t Checking Module- creating_corrugated_plate_from_plate_stock')

    def drawing_polyline(self):
        sleep(4)
        pyautogui.typewrite('polyline', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#38400,2500,6200', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#37200,2500', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('#36000,2500', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('esc')
        sleep(1)
        pyautogui.typewrite('ORTHO', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('ON', interval=.2)
        pyautogui.press('enter')

    def stretch_vertex(self):
        self.logger.info('Start:\t Checking Module- stretch_vertex')
        sleep(3)
        reset_viewport()
        free()
        zoom('38892','-2321','47306','-4123')
        pyautogui.typewrite('select', interval=.05)
        pyautogui.press('enter')
        pyautogui.typewrite('42617.2225,-3710.6429', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.moveTo(625,494)
        pyautogui.click()
        pyautogui.typewrite('#36000,2253.9322', interval=.2)
        pyautogui.press('enter')
        free()
        mouseDoubleClickOn("sc_utilites")
        mouseLeftClickOn("visual_sc")
        mouseLeftClickOn("activate_ucs")
        if appears("ucs_2500"):
            mouseDoubleClickOn("ucs_2500")
        else:
            raise AssertionError("ucs_2500 did not appear")
        free()
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('37186.3689,4710.5712', interval=.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete', interval=.2)
        pyautogui.press('enter')
        self.logger.info("Vertex was stratched")
        self.logger.info('End:\t Checking Module- stretch_vertex')

    def new_corrugated(self):
        self.logger.info('Start:\t Checking Module- new_corrugated')
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("corrugated_pane")
        if appears("corrugated_new"):
            mouseLeftClickOn("corrugated_new")
            free()
            pyautogui.typewrite('36758.7424,6491.6932', interval=.2)
            pyautogui.press('enter')
            if appears("corrugated_prop"):
                if appears("from_flat_plate_region",5):
                    mouseLeftClickOn("from_flat_plate_region", "from_flat_plate_region")
                if appears("pl08",5):
                    mouseLeftClickOn("pl08")
                if appears("show_expand_region",5):
                    mouseLeftClickOn("show_expand_region")
                mouseLeftClickOn("corrugated_prop_ok")
                if appears("select_corrugation",30):
                    # pyautogui.typewrite('36884.8362,6253.5527', interval=.2)
                    pyautogui.typewrite('36219.2498,6369.4024', interval=.2)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('a')
                    pyautogui.press('enter')
                    sleep(4)
                    conceptual_visual()
                    free()
                    if appears("new_corrugated_color",5):
                        self.logger.info("New corrugated plate was created")
                else:
                    raise AssertionError("select_corrugation profile line did not appear")
            else:
                raise AssertionError("corrugated_properties did not appear")
        else:
            raise AssertionError("corrugated_new button did not appear")
        self.logger.info('End:\t Checking Module- new_corrugated')


    def modifying_corrugated_plate(self):
        self.logger.info('Start:\t Checking Module- modifying_corrugated_plate')
        sleep(5)
        free()
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')

        wireframe2d_visual()
        free()
        mouseLeftClickOn("corrugated_pane")
        if appears("corrugated_edit"):
            mouseLeftClickOn("corrugated_edit")
            free()
            pyautogui.typewrite('35752.1716,6751.1126', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("corrugated_prop"):
                if appears("pl07",5):
                    mouseLeftClickOn("pl07")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    sleep(2)
                pyautogui.press('enter')
            else:
                raise AssertionError("corrugated_properties did not appear")
        else:
            raise AssertionError("corrugated_edit button did not found")
        self.logger.info('End:\t Checking Module- modifying_corrugated_plate')

    def adding_objects_to_corrugated_plate(self):
        self.logger.info('Start:\t Checking Module- adding_objects_to_corrugated_plate')
        sleep(1)
        line('36224.7087', '6286.7575', '36224.7087', '6588.0328')
        pyautogui.typewrite('circle', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('36434.9159,6427.6079', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('80', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('select', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('36224.7087,6445.1257', interval=.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("properties2"):
            mouseLeftClickOn("properties2")
            if appears("layer_click"):
                mouseLeftClickOn("layer_click")
                pyautogui.press('right')
                mouseLeftClickOn("properties_close_button")
            else:
                raise AssertionError("Layer option did not appear")
            free()
            pyautogui.press('esc')
            mouseLeftClickOn("parts_pane")
            if appears("add_object"):
                mouseLeftClickOn("add_object")
                free()
                pyautogui.typewrite('35752.1716,6751.1126', interval=.2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('36224.7087,6445.1257', interval=.1)
                pyautogui.press('enter')
                pyautogui.typewrite('36357.0614,6432.8483', interval=.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
                if appears("add_obj_corru"):
                    mouseLeftClickOn("continuous")
                    if appears("forward_region2",5):
                        mouseLeftClickOn("forward2", "forward_region2")
                    sleep(1)
                    pyautogui.press('enter')
                else:
                    raise AssertionError("Markline style menu did not appear")
            else:
                raise AssertionError("add_object did not found")
        else:
            raise AssertionError("Properties option did not found")
        self.logger.info('End:\t Checking Module- adding_objects_to_corrugated_plate')

    def extracting_production_information2(self):
        self.logger.info('Start:\t Checking Module- extracting_production_information2')
        mouseLeftClickOn("corrugated_pane")
        if appears("extract_prod_info_button"):
            mouseLeftClickOn("extract_prod_info_button")
            free()
            pyautogui.typewrite('35752.1716,6751.1126', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
        else:
            raise AssertionError("extract_prod_info_button did not appear")
        self.logger.info('End:\t Checking Module- extracting_production_information2')

    def extracting_the_cross_section(self):
        self.logger.info('Start:\t Checking Module- extracting_the_cross_section')
        mouseLeftClickOn("corrugated_pane")
        if appears("extract_cross_sec"):
            mouseLeftClickOn("extract_cross_sec")
            free()
            pyautogui.typewrite('35752.1716,6751.1126', interval=.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.hotkey('ctrl','s')
        else:
            raise AssertionError("extract_cross_sec did not appear")
        self.logger.info('End:\t Checking Module- extracting_the_cross_section')

    def drawings_before_creating_custom_plate_parts(self):
        if appears("u03_shell_tab"):
            self.logger.info("U03 Main Deck appeared")
            pyautogui.typewrite('polyline', interval=.2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('30000,3000', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#30000,3010', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#31002.6796,3010', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#31871.0001,2508.6602', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#31866,2500', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#31000,3000', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('#30000,3000', interval=.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('esc')
            sleep(1)
            change_viewpoint_from_aft_stbd_up()
            free()
            sleep(1)
            pyautogui.typewrite('EXTRUDE', interval=.2)
            pyautogui.press('enter')
            pyautogui.typewrite('30000,3000', interval=.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(1)
            sleep(1)
            pyautogui.typewrite('1000', interval=.2)
            pyautogui.press('enter')
            sleep(1)
            rectang('30000', '1000', '32000', '2000')
            sleep(1)
        else:
            raise AssertionError("u03_main_deck did not appear")

    def creating_custom_plate_parts(self):
        self.logger.info('Start:\t Checking Module- creating_custom_plate_parts')
        open_drawing_from_u03("u03_main_deck")
        sleep(4)
        self.drawings_before_creating_custom_plate_parts()
        sleep(1)
        select_new_custom_plate_option()
        pyautogui.typewrite('30000,3000', interval=.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('30985.5543,1534.8341', interval=.1)
        pyautogui.press('enter')
        if appears("custom_plate_prop"):
            self.logger.info("Custom plate properties appeared")
            sleep(1)
            pyautogui.press('enter')
        else:
            raise AssertionError("custom_plate_properties did not appear")
        self.logger.info('End:\t Checking Module- creating_custom_plate_parts')

    def convert_plate_part(self):
        self.logger.info('Start:\t Checking Module- convert_plate_part')
        rectang('30000', '0', '32000', '-1000')
        pyautogui.typewrite('delete', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('33090.1835,6988.1235', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        change_viewpoint_from_aft_stbd_up()
        free()
        sleep(1)
        mouseLeftClickOn("plates_pane")
        if appears("new_plate_button"):
            mouseLeftClickOn("new_plate_button")
            free()
            pyautogui.typewrite('31037.7957,-528.4606', interval=.1)
            pyautogui.press('enter')
            if appears("plate_properties"):
                self.logger.info("Plate peroperties appeared")
                if appears("select_down_region",5):
                    mouseLeftClickOn("select_down", "select_down_region")
                pyautogui.press('enter')
                mouseLeftClickOn("custom_plate_pane")
                if appears("plate_part_convert_button"):
                    self.logger.info("plate_part_convert_button appeared")
                    mouseLeftClickOn("plate_part_convert_button")
                    free()
                    pyautogui.typewrite('30944.4718,-1.9286', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("custom_plate_pane")
                    if appears("custom_plate_extract"):
                        self.logger.info("custom_plate_extract appeared")
                        mouseLeftClickOn("custom_plate_extract")
                        free()
                        pyautogui.typewrite('30944.4718,-1.9286', interval=.1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        sleep(1)
                        pyautogui.typewrite('select', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('30944.4718,-1.9286', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('move', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('30000,-1000', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('#33000,-1000', interval=.2)
                        pyautogui.press('enter')
                        line('33000', '-200', '33200', '0')
                        pyautogui.typewrite('slice', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('32999.5561,-412.7951', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('#33000,-200', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.typewrite('#33200,0', interval=.2)
                        pyautogui.press('enter')
                        pyautogui.moveTo(1426, 472)
                        pyautogui.typewrite('33297.623,-288.0463', interval=.2)
                        pyautogui.press('enter')
                        mouseLeftClickOn("custom_plate_pane")
                        if appears("custom_plate_replace"):
                            self.logger.info("Custom plate replace appeared")
                            mouseLeftClickOn("custom_plate_replace")
                            free()
                            pyautogui.typewrite('30944.4718,-1.9286', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('33000,-1000', interval=.1)
                            pyautogui.press('enter')

                            pyautogui.typewrite('select', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('30944.4718,-1.9286', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.press('esc')
                            pyautogui.typewrite('polyline', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#33000,-1000', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#33000,-200', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#33200,0', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#35000,0', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#35000,-1000', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#33000,-1000', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.press('esc')
                            pyautogui.typewrite('select', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('33000,-1000', interval=.1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            pyautogui.typewrite('move', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('33000,-1000', interval=.2)
                            pyautogui.press('enter')
                            pyautogui.typewrite('#30000,-1000', interval=.2)
                            pyautogui.press('enter')
                            sleep(2)
                            mouseLeftClickOn("custom_plate_pane")
                            if appears("custom_plate_replace_toolpath"):
                                mouseLeftClickOn("custom_plate_replace_toolpath")
                                free()
                                pyautogui.typewrite('30008.2452,-8.2962', interval=.2)
                                pyautogui.press('enter')
                                pyautogui.typewrite('30667.7410,9.1578', interval=.2)
                                pyautogui.press('enter')
                                pyautogui.press('enter')
                                sleep(3)
                            else:
                                raise AssertionError("custom_plate_replace_toolpath did not found")
                        else:
                            raise AssertionError("custom_plate_replace button did not appear")
                    else:
                        raise AssertionError("custom_plate_extract button did not found")
                else:
                    raise AssertionError("plate_part_convert_button did not appear")
            else:
                raise AssertionError("plate_properties did not appear")
        else:
            raise AssertionError("new_plate_button did not appear")
        self.logger.info('End:\t Checking Module- convert_plate_part')

    def creating_twisted_stiffener(self):
        self.logger.info('Start:\t Checking Module- creating_twisted_stiffener')
        open_drawing_from_hull("u03_training_shell")
        sleep(3)
        if appears("u03_training_shell_tab"):
            self.logger.info("U03 Training Shell tab appeared")
            mouseLeftClickOn("home_button")
            if appears("layer_button"):
                mouseLeftClickOn("layer_button")
                mouseLeftClickOn("layer_properties_button")
                mouseMoveToCoordinates(400,500)
                if appears("layer_name"):
                    mouseRightClickOn("layer_name")
                else:
                    raise AssertionError("layer_name did not appear")
                if appears("maximize_column",10):
                    mouseLeftClickOn("maximize_column")
                if appears("refline_layer"):
                    self.logger.info("Refline layer appeared")
                    mouseLeftClickOn("refline_layer")
                    pyautogui.press('right')
                    pyautogui.press('space')
                    pyautogui.press('right')
                    pyautogui.press('space')
                    mouseLeftClickOn("layer_properties_manager_close")
                    free()
                    if appears("hull_pane"):
                        mouseLeftClickOn("hull_pane")
                        mouseLeftClickOn("stringers")
                        if appears("export_stringer"):
                            self.logger.info("Export stringer appeared")
                            mouseLeftClickOn("export_stringer")
                            free()
                            sleep(1)
                            pyautogui.typewrite('32664.6365,45717.7184', .1)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.typewrite('34434.6287,46805.0165', .1)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.typewrite('35002.3621,46655.3790', .1)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.typewrite('35035.7582,46122.9717', .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            sleep(1)
                            if appears("choose_group2"):
                                self.logger.info("Choose group dialog appeared")
                                mouseDoubleClickOn("u03")
                                pyautogui.press('down')
                                pyautogui.press('right')
                                pyautogui.press('down')
                                pyautogui.press('down')
                                pyautogui.press('down')
                                pyautogui.press('down')
                                pyautogui.press('down')
                                pyautogui.press('right')
                                if appears("u03_shell_", 10):
                                    mouseLeftClickOn("u03_shell_")
                                else:
                                    raise AssertionError("u03_shell_ did not appear")
                                pyautogui.press('enter')
                                if appears("twisted_stiffener_prop"):
                                    self.logger.info("Twisted stiffener properties appeared")
                                    if appears("apply_to_all_click", 5):
                                        mouseLeftClickOn("apply_to_all_click")
                                    pyautogui.press('enter')
                                    sleep(6)
                                    mouseLeftClickOn("navigation")
                                    if appears("navigator_button_new"):
                                        mouseLeftClickOn("navigator_button_new")
                                        if appears("shipcon_navi"):
                                            mouseLeftClickOn("structure_node2")
                                            mouseDoubleClickOn("u03_shell")
                                            sleep(2)
                                            pyautogui.press('enter')
                                            self.logger.info("Twisted stiffeners were created")
                                        else:
                                            raise AssertionError("shipcon_navigator did not open")
                                    else:
                                        raise AssertionError("navigator_button did not appear")
                                else:
                                    raise AssertionError("twisted_stiffener_properties did not appear")
                            else:
                                raise AssertionError("choose_group window did not appear")
                        else:
                            raise AssertionError("export_stringer did not appear")
                    else:
                        raise AssertionError("hull_pane did not appear")
                else:
                    raise AssertionError("refline_layer did not appear")
            else:
                raise AssertionError("layer_button did not appear")
        else:
            raise AssertionError("u03_training_shell_tab did not open")
        self.logger.info('End:\t Checking Module- creating_twisted_stiffener')

    def editing_twisted(self):
        self.logger.info('Start:\t Checking Module- editing_twisted')
        change_viewpoint_from_aft_stbd_up()
        free()
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("stiffeners")
        if appears("edit_twisted_stiffener"):
            self.logger.info("Edit twisted stiffener option appeared")
            mouseLeftClickOn("edit_twisted_stiffener")
            free()
            pyautogui.typewrite('45294.0555,13075.6712', .2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("twisted_stiffener_prop"):
                self.logger.info("twisted stiffener properties appeared")
                if appears("drop_down2",3):
                    mouseLeftClickOn("drop_down2")
                    mouseDoubleClickOn("bulb_flat")
                    mouseLeftClickOn("hp200x10")
                if appears("start1",3):
                    mouseLeftClickOn("none", "start1")
                    pyautogui.press('c')
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.press('tab')
                    pyautogui.typewrite('-100', .1)
                if appears("end1",3):
                    mouseLeftClickOn("none", "end1")
                    pyautogui.press('c')
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.press('tab')
                    pyautogui.typewrite('-100', .1)
                sleep(1)
                pyautogui.press('enter')
                sleep(3)
            else:
                raise AssertionError("twisted_stiffener_properties did not appear")
        else:
            raise AssertionError("edit_twisted_stiffener did not appear")
        self.logger.info('End:\t Checking Module- editing_twisted')

    def cleaning_geometry(self):
        self.logger.info('Start:\t Checking Module- cleaning_geometry')
        mouseLeftClickOn("stiffeners")
        if appears("cleaning_geom"):
            self.logger.info("Cleaning geometry option appeared")
            mouseLeftClickOn("cleaning_geom")
            free()
            pyautogui.typewrite('45294.0555,13075.6712', .2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('.5', .2)
            pyautogui.press('enter')
            sleep(3)
        else:
            raise AssertionError("cleaning_geometry did not appear")
        self.logger.info('End:\t Checking Module- cleaning_geometry')

    def inserting_standard_part(self):
        self.logger.info('Start:\t Checking Module- inserting_standard_part')
        open_drawing_from_u03("u03_f60_node")
        sleep(5)
        if appears("u03_f60_drawing_tab"):
            self.logger.info("U03 F60 tab appeared")
            change_viewpoint_body_looking_fwd()
            mouseLeftClickOn("structure_ribbon_button")
            mouseLeftClickOn("stiffeners")
            if appears("new_stiffeners2"):
                mouseLeftClickOn("new_stiffeners2")
                mouseLeftClickOn("new_stiffeners_from_point")
                free()
                pyautogui.typewrite('-6976.7717,6700.00', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('-750.00,6700.00', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("stiff_properties"):
                    self.logger.info("Stiffener properties appeared")
                    self.logger.info("Stiffener properties appeared")
                    mouseLeftClickOn("stock_drop_down")
                    if appears("200x10", 5):
                        mouseLeftClickOn("200x10")
                    elif appears("bulb_flat", 5):
                        mouseLeftClickOn("bulb_flat")
                        pyautogui.press('right')
                        mouseLeftClickOn("200x10")
                    else:
                        free()
                        pyautogui.press('esc')

                    pyautogui.press('tab', presses=6, interval=.1)
                    pyautogui.press('up', presses=5)
                    sleep(2)
                    pyautogui.press('tab')
                    sleep(1)
                    pyautogui.press('up', presses=5)
                    sleep(2)
                    mouseLeftClickOn("stiffeners_ok")
                    free()
                    mouseLeftClickOn("stiffeners")
                    if appears("stiffeners_from_points"):
                        mouseLeftClickOn("stiffeners_from_points")
                        free()
                        pyautogui.typewrite('-2500,1400', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('-2500,6700', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        if appears("stiff_properties"):
                            mouseLeftClickOn("web_direction_starboard")
                            if appears("port",3):
                                mouseLeftClickOn("port")
                            if appears("end2",3):
                                mouseDoubleClickOn("end2")
                                pyautogui.typewrite('-200', .2)
                                pyautogui.press('enter')
                            # if appears("aft_port_region", 3):
                            #     mouseLeftClickOn("aft_port_region_port")
                            sleep(1)
                            pyautogui.press('enter')
                            sleep(2)
                            mouseLeftClickOn("parts_pane")
                            if appears("insert_std_part"):
                                mouseLeftClickOn("insert_std_part")
                                if appears("insert_std_part_window"):
                                    self.logger.info("insert_std_part_window appeared")
                                    mouseLeftClickOn("162_8")
                                    mouseLeftClickOn("insert")
                                    pyautogui.typewrite('-2500,6500', .2)
                                    pyautogui.press('enter')
                                    sleep(2)
                                    pyautogui.moveTo(435, 254)
                                    pyautogui.press('enter')
                                    sleep(2)
                                    pyautogui.press('enter')
                                    change_view("conceptual_visual")
                                else:
                                    raise AssertionError("insert_std_part_window did not appear")
                            else:
                                raise AssertionError("insert_std_part did not appear")
                        else:
                            raise AssertionError("stiff_properties did not appear")
                    else:
                        raise AssertionError("stiffeners_from_points did not appear")
                else:
                    raise AssertionError("stiff_properties did not appear")
            else:
                raise AssertionError("new_stiffeners did not appear")
        else:
            raise AssertionError("u03_f60_drawing_tab did not open")
        self.logger.info('End:\t Checking Module- inserting_standard_part')

    def inserting_standard_part_on_stiffener(self):
        self.logger.info('Start:\t Checking Module- inserting_standard_part_on_stiffener')
        free()
        pyautogui.typewrite('delete', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('-2627.6479,6356.4913', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("parts_pane")
        if appears("insert_std_part_region"):
            mouseLeftClickOn("insert_std_part_drop", "insert_std_part_region")
            if appears("insert_std_part_stiffener"):
                self.logger.info("Insert standard part on stiffener option appeared")
                mouseLeftClickOn("insert_std_part_stiffener")
                free()
                pyautogui.typewrite('-2409.9859,5678.2671', .2)
                # pyautogui.typewrite('-2491.3915,5541.3746', .2)
                pyautogui.press('enter')
                # pyautogui.typewrite('-1936.6908,6700.3477', .2)
                pyautogui.typewrite('-3151.5079,6609.4188', .2)
                pyautogui.press('enter')
                if appears("insert_std_part_window"):
                    if appears("12_7_3",4):
                        pyautogui.press('enter')
                    else:
                        mouseDoubleClickOn("12_7")
                    sleep(2)
                    if appears("bracket_location"):
                        mouseDoubleClickOn("flange_offset", "flange_offset_region")
                        pyautogui.press('5')
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        if appears("plate_properties2"):
                            mouseLeftClickOn("forward4_click", "forward4")
                            sleep(1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("plate_properties did not appear")
                    else:
                        raise AssertionError("bracket_location window did not appear")
                else:
                    raise AssertionError("insert_std_part_window did not appear")
            else:
                raise AssertionError("insert_std_part_stiffener did not appear")
        else:
            raise AssertionError("insert_std_part did not appear")
        self.logger.info('End:\t Checking Module- inserting_standard_part_on_stiffener')

    def modify_standard(self):
        self.logger.info('Start:\t Checking Module- modify_standard')
        mouseDoubleClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("manage_pane")
        if appears("manage_button"):
            mouseLeftClickOn("manage_button")
            if appears("shipcon_manager"):
                self.logger.info("SC Manager appeared")
                mouseLeftClickOn("structure_button")
                if appears("std_parts"):
                    mouseLeftClickOn("std_parts")
                    mouseLeftClickOn("12_7_2")
                    mouseMoveTo("edit_std_part")
                    sleep(1)
                    pyautogui.hotkey('alt', 'f4')
                else:
                    raise AssertionError("std_parts did not appear")
            else:
                raise AssertionError("shipcon_manager window did not open")
        else:
            raise AssertionError("manage_button did not appear")
        self.logger.info('End:\t Checking Module- modify_standard')

    def convert_standard_to_structure_part(self):
        self.logger.info('Start:\t Checking Module- convert_standard_to_structure_part')
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("parts_pane")
        if appears("conver_to_structure"):
            mouseLeftClickOn("conver_to_structure")
            self.logger.info("Convert to structure option appeared")
            free()
            pyautogui.typewrite('-2198.1435,6402.3008', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('enter')
        else:
            raise AssertionError("conver_to_structure did not appear")
        self.logger.info('End:\t Checking Module- convert_standard_to_structure_part')

    def inserting_std_assembly(self):
        self.logger.info('Start:\t Checking Module- inserting_std_assembly')
        change_viewpoint_from_fwd_stbd_up()
        pyautogui.hotkey('ctrl', '9')
        pyautogui.press('enter')
        change_view("2d_wireframe")
        mouseDoubleClickOn("multi_discipline")
        mouseLeftClickOn("std_assembly_button")
        if appears("insert_std_assmbly"):
            mouseLeftClickOn("insert_std_assmbly")
            if appears("create_std_asmbly"):
                self.logger.info("Create standard assembly dialog appeared")
                mouseLeftClickOn("sa100")
                mouseLeftClickOn("next")
                sleep(2)
                free()
                mouseLeftClickOn("next")
                sleep(2)
                free()
                mouseLeftClickOn("finish")
                sleep(6)
                pyautogui.press('x')
                pyautogui.press('enter')
                sleep(2)
                pyautogui.typewrite('750,-0.0067,14.00', .1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.moveTo(694,627)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.moveTo(694,627)
                pyautogui.press('enter')
                pyautogui.hotkey('ctrl', '9')
                pyautogui.press('enter')
            else:
                raise AssertionError("create_std_asmbly did not found")
        else:
            raise AssertionError("insert_std_assmbly button did not appear")
        self.logger.info('End:\t Checking Module- inserting_std_assembly')

    def editing_standard_assembly(self):
        self.logger.info('Start:\t Checking Module- editing_standard_assembly')
        mouseLeftClickOn("std_assembly_button")
        if appears("std_assembly_edit"):
            mouseLeftClickOn("std_assembly_edit")
            if appears("warning6",10):
                pyautogui.press('enter')
                free()
                pyautogui.typewrite('1032.2800,164.6828', .2)
                pyautogui.press('enter')
                if appears("edit_std_window"):
                    self.logger.info("Edit standard window appeared")
                    self.logger.info("Edit")
                    mouseLeftClickOn("std_dropdown")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    mouseLeftClickOn("finish2")
                else:
                    raise AssertionError("edit_std_window did not appear")
        else:
            raise AssertionError("std_assembly_edit did not appear")
        self.logger.info('End:\t Checking Module- editing_standard_assembly')

    def anchoring_standard_assemblies(self):
        self.logger.info('Start:\t Checking Module- anchoring_standard_assemblies')
        mouseDoubleClickOn("multi_discipline")
        mouseLeftClickOn("std_assembly_button")
        if appears("anchor"):
            mouseLeftClickOn("anchor")
            free()
            pyautogui.typewrite('1032.2800,164.6828', .2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("std_assembly_button")
            if appears("unanchor"):
                mouseLeftClickOn("unanchor")
                free()
                pyautogui.typewrite('1032.2800,164.6828', .2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.press('esc')
                sleep(2)
                pyautogui.hotkey('ctrl','s')
                sleep(8)
            else:
                raise AssertionError("unanchor did not appear")
        else:
            raise AssertionError("anchor did not appear")
        self.logger.info('End:\t Checking Module- anchoring_standard_assemblies')

    def managing_structure_drawing_option(self):
        self.logger.info('Start:\t Checking Module- managing_structure_drawing_option')
        open_drawing_from_u03("u03_f60_node")
        sleep(5)
        if appears("u03_f60_drawing_tab"):
            self.logger.info("U03 F60 tab appeared")
            mouseDoubleClickOn("structure_ribbon_button")
            mouseLeftClickOn("structure_main_pane")
            if appears("drawing_options_region2"):
                mouseLeftClickOn("drawing_options_dropdown3", "drawing_options_region2")
                mouseLeftClickOn("structure_drawing_option")
                if appears("structure_display_window"):
                    self.logger.info("Structure display window appeared")
                    if appears("start_end_label",3):
                        mouseDoubleClickOn("start_end_label_show", "start_end_label")
                    free()
                    mouseLeftClickOn("struc_display_okay")
                else:
                    raise AssertionError("structure_display_window did not appear")
            else:
                raise AssertionError("drawing_options did not appear")
        else:
            raise AssertionError("u03_f60_drawing_tab did not open")
        self.logger.info('End:\t Checking Module- managing_structure_drawing_option')


    def copying_moving_mirroring(self):
        self.logger.info('Start:\t Checking Module- copying_moving_mirroring')
        change_viewpoint_body_looking_fwd()
        sleep(1)
        mouseLeftClickOn("structure_main_pane")
        if appears("structure_drawing_option2"):
            mouseLeftClickOn("structure_drawing_option2")
            if appears("structure_display_window"):
                # mouseDoubleClickOn("hide_option", "bevel_solid_hide_region2")
                if appears("production_hide",3):
                    mouseDoubleClickOn("hide_option", "production_hide")
                if appears("start_label_show",3):
                    mouseDoubleClickOn("show_option", "start_label_show")
                mouseLeftClickOn("struc_display_okay")
            else:
                raise AssertionError("structure_display_window did not appear")
        else:
            raise AssertionError("drawing_options did not appear")
        self.logger.info('End:\t Checking Module- copying_moving_mirroring')

    def mirroring(self):
        self.logger.info('Start:\t Checking Module- mirroring')
        sleep(2)
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-3392.4679,1012.1646',.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mirror_line()
        pyautogui.typewrite('select', interval=.2)
        pyautogui.press('enter')
        pyautogui.typewrite('3511.2816,508.6177', interval=.2)
        # pyautogui.typewrite('2507.1101,845.1216', interval=.2)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("part_information_button"):
            mouseLeftClickOn("part_information_button")
            sleep(2)
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('esc')
            self.logger.info("Mirroring done")
        else:
            raise AssertionError("part_information_button did not appear")
        self.logger.info('End:\t Checking Module- mirroring')

    def moving_copying_part(self):
        self.logger.info('Start:\t Checking Module- moving_copying_part')
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2302.0986,5341.7420', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2193.5821,6395.1153', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('move', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2100,6500,2', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('#-2500,6500,2', .1)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('copy', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2705.1595,5496.6499', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2595.6431,6410.6061', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('-2900,6500', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('#-4035,6500', .1)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.press('esc')
        self.logger.info("Moving copying part done")
        self.logger.info('End:\t Checking Module- moving_copying_part')

    def editing_parts_with_identicals(self):
        self.logger.info('Start:\t Checking Module- editing_parts_with_identicals')
        sleep(2)
        pyautogui.typewrite('select', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('-2705.1595,5496.6499', interval=.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("edit_prop2"):
            mouseLeftClickOn("edit_prop2")
            if appears("none"):
                mouseLeftClickOn("none")
                pyautogui.click()
                pyautogui.press('tab')
                pyautogui.press('tab')
                pyautogui.typewrite('-30', .1)
                pyautogui.press('tab')
                pyautogui.typewrite('-200', .1)
                pyautogui.press('enter')
                if appears("ssi_u03_region",5):
                    mouseLeftClickOn("ssi_u03", "ssi_u03_region")
                    pyautogui.press('enter')
                else:
                    pyautogui.press('enter')
        else:
            raise AssertionError("edit_prop did not appear")
        sleep(2)
        pyautogui.typewrite('select', interval=.1)
        pyautogui.press('enter')
        pyautogui.typewrite('3449.2722,508.6177', interval=.1)
        # pyautogui.typewrite('2507.1101,845.1216', interval=.2)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("edit_prop2"):
            mouseLeftClickOn("edit_prop2")
            mouseLeftClickOn("pl08_2")
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            self.logger.info("Editing part with identicals done")
        else:
            raise AssertionError("edit_prop did not appear")
        self.logger.info('End:\t Checking Module- editing_parts_with_identicals')


    def replacing_parts_to_other_planar_group(self):
        self.logger.info('Start:\t Checking Module- replacing_parts_to_other_planar_group')
        zoom_to_ssi_u03_p01()
        pyautogui.press('esc')
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("utilities_pane")
        if appears("replace_parts"):
            mouseLeftClickOn("replace_parts")
            free()
            pyautogui.typewrite('-751.8995,1081.9336', interval=0.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            sleep(3)
            if appears("replicate_window"):
                self.logger.info("Replicate window appeared")
                mouseLeftClickOn("frame_expand_click", "frame_expand")
                pyautogui.press('down')
                pyautogui.press('down')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                mouseLeftClickOn("ok")
                sleep(10)
                change_viewpoint_body_looking_fwd()
                sleep(1)
                mouseLeftClickOn("structure_main_pane")
                if appears("navigator_button_new"):
                    mouseLeftClickOn("navigator_button_new")
                    if appears("u03_f62"):
                        mouseDoubleClickOn("u03_f62")
                    else:
                        raise AssertionError("u03_f62 did not appear")
                    sleep(1)
                    if appears("save_warning",5):
                        pyautogui.press('enter')
                    sleep(20)
                    self.logger.info("U03 F62 tab appeared")
                    change_viewpoint_body_looking_fwd()
                    pyautogui.typewrite('select', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('-435.0264,1345.1200', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.click(button='right', clicks=2, interval=0.25)
                    if appears("edit_prop2"):
                        mouseLeftClickOn("edit_prop2")
                        mouseLeftClickOn("product_hierarchy_tab")
                        sleep(1)
                        pyautogui.press('enter')
                        sleep(1)
                        pyautogui.press('enter')
                    else:
                        raise AssertionError("edit_prop did not appear")
                    pyautogui.typewrite('select', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('-435.0264,1345.1200', interval=.1)
                    pyautogui.press('enter')
                    pyautogui.click(button='right', clicks=2, interval=0.25)
                    if appears("part_information_button"):
                        mouseLeftClickOn("part_information_button")
                        sleep(2)
                        pyautogui.press('enter')
                        conceptual_visual()
                        if appears("replaced_part",5):
                            self.logger.info("Part was replaced to U03 F62")
                        else:
                            raise AssertionError("Part was not replaced to U03 F62")
                        wireframe2d_visual()
                    else:
                        raise AssertionError("edit_prop did not appear")
                else:
                    raise AssertionError("navigator_button_new did not found")
            else:
                raise AssertionError("replicate_window did not appear")
        else:
            raise AssertionError("replace_parts did not appear")
        self.logger.info('End:\t Checking Module- replacing_parts_to_other_planar_group')

    def transferring_parts_to_other_planar_groups(self):
        self.logger.info('Start:\t Checking Module- transferring_parts_to_other_planar_groups')
        mouseLeftClickOn("structure_main_pane")
        if appears("navigator_button_new"):
            mouseLeftClickOn("navigator_button_new")
            if appears("u03_f60_node"):
                mouseDoubleClickOn("u03_f60_node")
            else:
                raise AssertionError("u03_f60_node did not appear")
            sleep(1)
            if appears("save_warning",5):
                pyautogui.press('enter')
            if appears("u03_f60_drawing_tab"):
                sleep(20)
            else:
                raise AssertionError("u03_f60_drawing_tab did not appear")
            self.logger.info("U03 F60 tab appeared")
            mouseLeftClickOn("utilities_pane")
            if appears("replicate_region"):
                mouseLeftClickOn("replicate_drop", "replicate_region")
                mouseLeftClickOn("transfer_object")
                free()
                pyautogui.typewrite('309.7474,1473.0005', interval=0.2)
                # pyautogui.typewrite('463.6975,1347.8591', interval=0.2)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('enter')
                if appears("transfer_window"):
                    mouseLeftClickOn("frame_expand_click", "frame_expand")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('enter')
                    mouseLeftClickOn("ok")
                    sleep(1)
                    pyautogui.press('enter')
                    sleep(3)
                    mouseLeftClickOn("structure_main_pane")
                    if appears("navigator_button_new"):
                        mouseLeftClickOn("navigator_button_new")
                        if appears("u03_f62"):
                            mouseDoubleClickOn("u03_f62")
                        else:
                            raise AssertionError("u03_f62 did not appear")
                        sleep(1)
                        if appears("save_warning",5):
                            pyautogui.press('enter')
                        sleep(20)
                        if appears("warning9",5):
                            pyautogui.press('enter')
                        sleep(1)
                        change_viewpoint_from_fwd_stbd_up()
                        sleep(4)
                        pyautogui.typewrite('select', interval=.1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('1572.1130,2658.0847', interval=.1)
                        pyautogui.press('enter')
                        pyautogui.click(button='right', clicks=2, interval=0.25)
                        if appears("edit_prop2"):
                            mouseLeftClickOn("edit_prop2")
                            sleep(5)
                            mouseLeftClickOn("override_region")
                            pyautogui.hotkey('shift', 'tab')
                            pyautogui.typewrite('Transferred', .1)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.press('esc')
                            pyautogui.press('esc')
                            pyautogui.press('esc')
                            change_viewpoint_body_looking_fwd()
                            conceptual_visual()
                            if appears("replaced_part", 5):
                                self.logger.info("Part was replaced to U03 F62")
                            else:
                                raise AssertionError("Part was not Transferred to U03 F62")
                            wireframe2d_visual()
                            pyautogui.hotkey('ctrl', 's')
                            self.logger.info("Transferring part to other drawing done")
                        else:
                            raise AssertionError("edit_prop did not appear")
                else:
                    raise AssertionError("transfer_window did not appear")
            else:
                raise AssertionError("replicate region did not found")
        self.logger.info('End:\t Checking Module- transferring_parts_to_other_planar_groups')


    def automatic_cutouts(self):
        self.logger.info('Start:\t Checking Module- automatic_cutouts')
        open_drawing_from_u03("u03_f60_node")
        sleep(5)
        if appears("u03_f60_drawing_tab"):
            sleep(5)
            free()
            sleep(2)
            line('0', '0', '5360', '0')
            mouseDoubleClickOn("structure_ribbon_button")
            mouseLeftClickOn("utilities_pane")
            if appears("create_planar"):
                mouseLeftClickOn("create_planar")
                free()
                pyautogui.typewrite('2733,0', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("select_planar_group_type"):
                    mouseLeftClickOn("deck_group")
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.typewrite('_BOTTOM', .1)
                    pyautogui.press('tab')
                    pyautogui.press('down')
                    sleep(1)
                    pyautogui.press('enter')
                    if appears("planar_group_created_message2"):
                        self.logger.info("Planar group was created")
                        pyautogui.hotkey('alt', 'f4')
                        sleep(1)
                        #mirroring
                        pyautogui.typewrite('select', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('-3.5344,949.6742', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        mirror_line()
                        sleep(1)
                        mouseDoubleClickOn("structure_ribbon_button")
                        mouseLeftClickOn("structure_main_pane")
                        if appears("navigator_button_new"):
                            mouseLeftClickOn("navigator_button_new")
                            if appears("u03_bottom"):
                                mouseDoubleClickOn("u03_bottom")
                            else:
                                raise AssertionError("u03_bottom did not appear")
                            if appears("warning7"):
                                pyautogui.press('enter')
                            sleep(20)
                            self.logger.info("U03_bottom drawing appeared")
                            change_viewpoint_from_stbd_up()
                            line('34900', '250', '39000', '250')
                            line('34900', '500', '39000', '500')
                            change_viewpoint_from_stbd_up()
                            mouseLeftClickOn("stiffeners")
                            if appears("new_stiffeners"):
                                mouseLeftClickOn("new_stiffeners")
                                free()
                                pyautogui.typewrite('36163.5539,507.6145', .1)
                                pyautogui.press('enter')
                                pyautogui.typewrite('36175.8903,246.1058', .1)
                                pyautogui.press('enter')
                                pyautogui.press('enter')
                                pyautogui.press('enter')
                                if appears("stiff_prop_2"):
                                    if appears("stock_drop_down",2):
                                        mouseLeftClickOn("stock_drop_down")
                                        mouseLeftClickOn("hp160x8")
                                    if appears("up_down",2):
                                        mouseLeftClickOn("up", "up_down")
                                    pyautogui.press('enter')
                                    sleep(4)
                                    mouseLeftClickOn("structure_main_pane")
                                    if appears("navigator_button_new"):
                                        mouseLeftClickOn("navigator_button_new")
                                        if appears("u03_f60_node"):
                                            mouseDoubleClickOn("u03_f60_node")
                                        else:
                                            raise AssertionError("u03_f60_node did not appear")
                                        if appears("warning7"):
                                            pyautogui.press('enter')
                                        sleep(10)
                                        mouseLeftClickOn("plates_pane")
                                        if appears("manage_cutout"):
                                            mouseLeftClickOn("manage_cutout")
                                            free()
                                            # pyautogui.typewrite('-748.0930,982.2422', .1)
                                            pyautogui.typewrite('-366.8149,5.4594', .1)
                                            pyautogui.press('enter')
                                            pyautogui.press('enter')
                                            sleep(2)
                                            if appears("uncheck_all_button2"):
                                                self.logger.info("Update related part window appeared")
                                                mouseLeftClickOn("uncheck_all_button2")
                                                mouseLeftClickOn("ok3")
                                            else:
                                                raise AssertionError("Update related part window did not appear")
                                            sleep(15)
                                            pyautogui.press('enter')
                                            sleep(3)
                                            if appears("ssi0201"):
                                                mouseLeftClickOn("ssi0201")
                                                mouseLeftClickOn("delete_cutout")
                                                mouseLeftClickOn("ssi_s38")
                                                mouseLeftClickOn("set_cutout")
                                                if appears("tight_cutout"):
                                                    mouseLeftClickOn("tight_cutout")
                                                    pyautogui.press('enter')
                                                    if appears("cutout_warning2", 10):
                                                        pyautogui.press('enter')
                                                else:
                                                    raise AssertionError("tight_cutout option did not found")
                                                mouseLeftClickOn("ok2")
                                                self.logger.info("Automatic cutout done")
                                            else:
                                                raise AssertionError("ssi0201 did not appear")
                                        else:
                                            raise AssertionError("manage_cutout did not appear")
                                else:
                                    raise AssertionError("stiff_prop did not appear")
                            else:
                                raise AssertionError("new_stiffeners did not appear")
                    else:
                        raise AssertionError("planar_group_created_message did not appear")
            else:
                raise AssertionError("create_planar option did not found")
        self.logger.info('End:\t Checking Module- automatic_cutouts')


    def identical_part_names_the_same(self):
        self.logger.info('Start:\t Checking Module- identical_part_names_the_same')
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('593.4310,1390.1517', .1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("part_information_button"):
            mouseLeftClickOn("part_information_button")
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('esc')
            mouseLeftClickOn("parts_pane")
            if appears("making_identical_same"):
                mouseLeftClickOn("making_identical_same")
                free()
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('593.4310,1390.1517', .1)
                pyautogui.press('enter')
                pyautogui.click(button='right', clicks=2, interval=0.25)
                if appears("part_information_button"):
                    self.logger.info("Part information button appeared")
                    mouseLeftClickOn("part_information_button")
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    sleep(3)
                    pyautogui.press('esc')
                    pyautogui.press('esc')
        else:
            raise AssertionError("edit_prop did not appear")
        self.logger.info('Start:\t Checking Module- identical_part_names_the_same')

    def showing_list_of_parts(self):
        self.logger.info('Start:\t Checking Module- showing_list_of_parts')
        free()
        sleep(5)
        mouseLeftClickOn("parts_pane")
        if appears("part_list"):
            mouseLeftClickOn("part_list")
            if appears("parts_list_window"):
                self.logger.info("Parts list window appeared")
                mouseLeftClickOn("name_part_sort")
                goDownUntilAppears("u03_p04", "slide_down")
                if appears("u03_p04"):
                    self.logger.info("U03_P04 part found")
                    mouseRightClickOn("u03_p04")
                    mouseLeftClickOn("zoom_to_p04")
                    pyautogui.hotkey('alt', 'f4')
                    mouseLeftClickOn("parts_pane")
                    sleep(1)
                    mouseLeftClickOn("part_list")
                    if appears("parts_list_window"):
                        # mouseLeftClickOn("name_part_sort")
                        goDownUntilAppears("u03_p04", "slide_down")

                    else:
                        raise AssertionError("parts_list_window did not appear")
                    if appears("u03_p04"):
                        mouseRightClickOn("u03_p04")
                        mouseLeftClickOn("view_in_nest")
                        if appears("warning8"):
                            pyautogui.press('enter')
                        sleep(1)
                        mouseLeftClickOn("parts_list_window_unhigh")
                        if appears("u03_p04_high",4):
                            mouseRightClickOn("u03_p04_high")
                        elif appears("u03_p04_high2",4):
                            mouseRightClickOn("u03_p04_high2")
                        else:
                            pyautogui.hotkey('alt','f4')
                            raise AssertionError("u03_p04_high did not appear")
                        if appears("delete_part"):
                            mouseLeftClickOn("delete_part")
                        else:
                            raise AssertionError("delete_part did not appear")
                        mouseLeftClickOn("parts_list_window_unhigh")
                        pyautogui.hotkey('alt', 'f4')
                        self.logger.info("Showing list of parts completed")
                    else:
                        raise AssertionError("u03_p04 did not appear")
                else:
                    raise AssertionError("u03_p04 did not appear")
            else:
                raise AssertionError("parts_list_window did not appear")
        else:
            raise AssertionError("part_list did not appear")
        pyautogui.hotkey('ctrl', 'z')
        sleep(4)
        self.logger.info('End:\t Checking Module- showing_list_of_parts')


    def checking_planar_group(self):
        self.logger.info('Start:\t Checking Module- checking_planar_group')
        mouseLeftClickOn("utilities_pane")
        if appears("check_planar"):
            mouseLeftClickOn("check_planar")
            free()
            if appears("yes_button3",10):
                mouseLeftClickOn("yes_button3")
            if appears("warning_5",15):
                mouseLeftClickOn("warning_5")
                mouseLeftClickOn("save_yes_4")
            sleep(3)
            if appears("planar_group_text"):
                self.logger.info("Planar group was checked")
                sleep(1)
                pyautogui.hotkey('alt', 'f4')
            else:
                raise AssertionError("planar_group_text did not appear")
        else:
            raise AssertionError("check_planar did not appear")
        self.logger.info('End:\t Checking Module- checking_planar_group')

    def showing_unused(self):
        self.logger.info('Start:\t Checking Module- showing_unused')
        mouseLeftClickOn("utilities_pane")
        if appears("show_unused"):
            mouseLeftClickOn("show_unused")
            if appears("unused_window"):
                self.logger.info("Unused parts window appeared")
                mouseLeftClickOn("group_pane")
                mouseMoveTo("erase")
                pyautogui.click(clicks=20, interval=.2)
                pyautogui.hotkey('alt', 'f4')
            else:
                raise AssertionError("unused_window did not appear")
        else:
            raise AssertionError("show_unused did not appear")
        self.logger.info('End:\t Checking Module- showing_unused')

    def part_info(self):
        self.logger.info('Start:\t Checking Module- part_info')
        free()
        reset_viewport()
        sleep(1)
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        # pyautogui.typewrite('-627.9110,1461.0277', interval=.1)
        pyautogui.typewrite('-306.5433,1332.1759', interval=.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        if appears("part_information_button"):
            self.logger.info("Part information option appeared")
            mouseLeftClickOn("part_information_button")
            sleep(1)
            mouseRightClickOn("piecemark")
            mouseLeftClickOn("edit_prop_piecemark")
            pyautogui.typewrite('40', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.press('esc')
            sleep(1)
            pyautogui.typewrite('select', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('-306.5433,1332.1759', interval=.1)

            # pyautogui.typewrite('-627.9110,1461.0277', interval=.1)
            pyautogui.press('enter')
            pyautogui.click(button='right', clicks=2, interval=0.25)
            if appears("part_information_button"):
                mouseLeftClickOn("part_information_button")
                mouseRightClickOn("orientation")
                mouseLeftClickOn("edit_prop_piecemark")
                pyautogui.typewrite('100', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("part_information_button")
                mouseRightClickOn("weld_shrinkage_option")
                mouseLeftClickOn("edit_prop_piecemark")
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.press('space')
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.press('up')
                sleep(1)
                pyautogui.press('tab')
                sleep(1)
                pyautogui.typewrite('0.5', .1)
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('right')
                sleep(1)
                mouseRightClickOn("u03_s01")
                mouseLeftClickOn("edit_prop_piecemark")
                sleep(1)
                pyautogui.press('esc')
                mouseRightClickOn("u03_s01")
                mouseLeftClickOn("part_relation")
                sleep(1)
                pyautogui.press('esc')
                sleep(1)
                mouseLeftClickOn("structure_cutout")
                sleep(1)
                pyautogui.press('right')
                sleep(1)
                mouseLeftClickOn("marklines")
                sleep(1)
                pyautogui.press('right')
                sleep(1)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('right')
                sleep(1)
                sleep(1)
                mouseRightClickOn("handle_867")
                mouseLeftClickOn("edit_prop_piecemark")
                sleep(1)
                pyautogui.press('enter')
                mouseLeftClickOn("construction_line")
                pyautogui.press('right')
                mouseRightClickOn("handle_37b")
                sleep(1)
                pyautogui.press('esc')
                pyautogui.press('esc')
            else:
                raise AssertionError("part_information_button did not appear")
        else:
            raise AssertionError("part_information_button did not appear")
        self.logger.info('End:\t Checking Module- part_info')

    def extract_components(self):
        self.logger.info('Start:\t Checking Module- extract_components')
        change_viewpoint_body_looking_fwd()
        mouseLeftClickOn("structure_ribbon_button")
        mouseLeftClickOn("parts_pane")
        if appears("extract_components"):
            mouseLeftClickOn("extract_components")
            free()
            pyautogui.typewrite('-2500.6689,1141.0679', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("home_button"):
                mouseDoubleClickOn("home_button")
                if appears("layer_button"):
                    mouseLeftClickOn("layer_button")
                    mouseLeftClickOn("layer_properties_button")
                    free()
                    if appears("layer_name",10):
                        mouseRightClickOn("layer_name")
                    else:
                        raise AssertionError("layer_name did not appear")
                    if appears("maximize_column",7):
                        mouseLeftClickOn("maximize_column")
                    else:
                        pyautogui.press('esc')
                    if appears("extracted_part",5):
                        mouseDoubleClickOn("extracted_part")
                        pyautogui.click(button='right')
                        if appears("select_all_but_current",5):
                            mouseLeftClickOn("select_all_but_current")
                            mouseLeftClickOn("layer_off")
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("select_all_but_current did not found")
                        if appears("layer_properties_manager_close",5):
                            mouseLeftClickOn("layer_properties_manager_close")
                        else:
                            mouseLeftClickOn("layer_properties_close_unhigh")
                        free()
                        pyautogui.typewrite('select', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('-5084.4417,1865.7964', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('-2047.3755,-21.5174', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('delete', .1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("layer_button")
                        mouseLeftClickOn("layer_properties_button")
                        if appears("extracted_part_unhigh",5):
                            mouseRightClickOn("extracted_part_unhigh")
                            if appears("select_all_but_current",5):
                                mouseLeftClickOn("select_all_but_current")
                                mouseLeftClickOn("layer_on")
                                mouseLeftClickOn("layer_properties_manager_close")
                                free()
                            else:
                                raise AssertionError("select_all_but_current did not found")
                        else:
                            raise AssertionError("extracted_part_unhigh did not appear")
                    else:
                        raise AssertionError("extracted_part did not appear")
                else:
                    raise AssertionError("layer_button did not appear")
            else:
                raise AssertionError("home_button did not appear")
        else:
            raise AssertionError("extract_components did not appear")
        self.logger.info('End:\t Checking Module- extract_components')

    def add_manual_cutout(self):
        self.logger.info('Start:\t Checking Module- add_manual_cutout')
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("parts_pane")
        if appears("dynamic_block_region"):
            mouseLeftClickOn("dynamic_block_region_drop_down", "dynamic_block_region")
            if appears("add_manual_cut"):
                mouseLeftClickOn("add_manual_cut")
                self.logger.info("add_manual_cut appeared")
            else:
                raise AssertionError("add_manual_cut did not appear")
            free()
            # pyautogui.typewrite('-627.9110,1461.0277', .1)
            pyautogui.typewrite('-306.5433,1332.1759', .1)
            pyautogui.press('enter')
            if appears("hp200x10nt_high",4):
                pyautogui.press('enter')
            else:
                mouseLeftClickOn("enter_filter")
                pyautogui.typewrite('hp', .1)
                mouseDoubleClickOn("hp200x10nt")
            pyautogui.typewrite('-750.00,0.0078', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.press('esc')
            pyautogui.hotkey('ctrl','s')
            self.logger.info("Manual cutout was added")
        else:
            raise AssertionError("dynamic_block_region did not appear")
        self.logger.info('End:\t Checking Module- add_manual_cutout')

class TCP1_Structure_Modeling_Error(Exception):
    pass
