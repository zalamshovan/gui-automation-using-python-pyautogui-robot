"""
*** TCP7_Installation_Guide.py ***

Automation script for Installation Guide (Page 1 - 90)

Covers the following sections:
- Install ShipConstructor Client, Server, or Standalone (pg 13-16), specifically steps 1, 2, 4a, & 5 which are required to perform a Client installation of ShipConstructor
- Connecting to your first project (pg 17-20)
- Modify ShipConstructor Installation (pg 20-21)
- Removing ShipConstructor (pg 21-22)


"""

import os, glob, config, datetime, pyautogui
import subprocess
from os import listdir
from os.path import isfile, join
from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

from time import sleep

from logger_extended import logger_extended

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseDoubleClickOn, hotKeyAction, pressAction, goDownUntilAppears, mouseMoveToCoordinates, takeScreenShot, typeWriteAction

from screenObjUtil import exists, appears, waitForVanish, waitToAppear, location

from Logger import write, addScreenShot

from SSIUtil import free, maximize

from time_tracker import my_timer


class TCP7_Installation_Guide(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    logger = logger_extended().get_logger('TCP10_Installation_Guide')
    pyVersionValue = None

    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.deletePathFinderFile()
        obj.runBuildNumberBatchFile("TC07_Installation_Uninstallation")

    # Setting up image directory
    @my_timer
    def image_directory(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")

    def releaseNote(self):
        if appears("SC_release_note"):
            print("SC Release note appeared")
        else:
            raise AssertionError("SC_release_note did not appear")

    # take screenshot on success after version number match and log into robot report if it ran from all_run robot
    def versionSuccessScreenshotAllRunRobot(self, logoName, screenshotName):
        currentWorkingDirectory = os.getcwd()
        newList = []
        newPath = "C:/AutomationLogs/08_Installation"
        os.chdir(newPath)
        for x in os.listdir('.'):
            newList.append(x)
        newList.sort(reverse=True)
        newPath = newPath + "/" + newList[0]
        os.chdir(currentWorkingDirectory)
        if appears(logoName):
            locationApp = location(logoName)
            currentWorkingDirectory = os.getcwd()
            os.chdir(newPath)
            if 'sc_about_logo' == logoName:
                takeScreenShot(screenshotName, locationApp[0] - 10, locationApp[1] - 10, locationApp[2] + 830,
                               locationApp[3] + 550)
            else:
                takeScreenShot(screenshotName, locationApp[0], locationApp[1], locationApp[2] + 1300,
                               locationApp[3] + 820)
            os.chdir(currentWorkingDirectory)
            addScreenShot(screenshotName, newPath)
        else:
            raise AssertionError(logoName + " didn't appeared.")

    # take screenshot on success after version number match and log into robot report
    def versionSuccessScreenshot(self, logoName, screenshotName, currentLocation, ch_dir):
        if appears(logoName):
            locationApp = location(logoName)
            screenshotDateTime = str(datetime.datetime.now().strftime ("%Y%m%d%H%M%S"))
            screenshotDateTime = screenshotDateTime.replace(" ", "")
            screenshotName = screenshotName + screenshotDateTime
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == currentLocation:
                os.chdir(ch_dir)
                if 'sc_about_logo' == logoName:
                    takeScreenShot(screenshotName, locationApp[0] - 10, locationApp[1] - 10, locationApp[2] + 830,
                                   locationApp[3] + 550)
                else:
                    takeScreenShot(screenshotName, locationApp[0], locationApp[1], locationApp[2] + 1300,
                                  locationApp[3] + 820)
            else:
                os.chdir("../" + ch_dir)
                if 'sc_about_logo' == logoName:
                    takeScreenShot(screenshotName, locationApp[0] - 10, locationApp[1] - 10, locationApp[2] + 830,
                                   locationApp[3] + 550)
                else:
                    takeScreenShot(screenshotName, locationApp[0], locationApp[1], locationApp[2] + 1300,
                                  locationApp[3] + 820)
            os.chdir(currentWorkingDirectory)
            addScreenShot(screenshotName)
        else:
            raise AssertionError(logoName+" didn't appeared.")

    # checking version number on Release Note
    def releaseVersion(self):
        obj = AppUtil()
        path = obj.getPathFinder()
        if appears("SC_release_note"):
            mouseMoveTo("SC_release_note")
            pyautogui.click()
            sleep(2)
            pyautogui.hotkey('win', 'up')
            pyautogui.hotkey('win', 'up')
            sleep(2)
            loca = location("sc_on_release_note")
        elif appears("SC_release_note_unhigh"):
            mouseMoveTo("SC_release_note_unhigh")
            pyautogui.click()
            sleep(2)
            pyautogui.hotkey('win', 'up')
            pyautogui.hotkey('win', 'up')
            sleep(2)
            loca = location("sc_on_release_note")
        else:
            mouseLeftClickOn("sc_on_release_note")
            if appears("SC_release_note"):
                mouseMoveTo("SC_release_note")
                pyautogui.click()
                sleep(2)
                pyautogui.hotkey('win', 'up')
                sleep(2)
                loca = location("sc_on_release_note")
            else:
                raise AssertionError("SC_release_note did not appear")
        if loca is not None:
            takeScreenShot("ReleaseNote", loca[0], loca[1]+25, loca[2]+ 600, loca[3]+30)
            os.system("magick convert ReleaseNote.png -resize 400% ReleaseNoteExt.png")
        else:
            raise AssertionError("sc_2019_on_release_note did not appear")
        text = obj.imageReader(image_name="ReleaseNoteExt.png")
        versionYear = obj.returnLatestAppYear()
        leng = versionYear.__len__()
        leng = leng - 4
        versionYear = versionYear[:-leng]
        versionNo = obj.getLatestVersion()
        versionNoSkipDot = versionNo.replace(".", "")
        if versionYear in text:
            print("Version year "+versionYear+" matched")
            if versionNo in text or versionNoSkipDot in text:
                print("PASSED")
                pyautogui.moveTo(400, 400)
                pyautogui.hotkey('win', 'up')
                if appears("sc_on_release_note_autocadIcon"):
                    if "\TC00_Run_All" in path:
                        self.versionSuccessScreenshotAllRunRobot(logoName="sc_on_release_note_autocadIcon", screenshotName="About_releaseVersion_Screenshot")
                    else:
                        self.versionSuccessScreenshot(logoName="sc_on_release_note_autocadIcon", screenshotName="About_releaseVersion_Screenshot", currentLocation="test.images", ch_dir="../test.cases/TC07_Installation_Uninstallation/VersionCheckingScreenShot/")
                elif appears("sc_on_release_note_autocadIcon_new"):
                    if "\TC00_Run_All" in path:
                        self.versionSuccessScreenshotAllRunRobot(logoName="sc_on_release_note_autocadIcon_new", screenshotName="About_releaseVersion_Screenshot")
                    else:
                        self.versionSuccessScreenshot(logoName="sc_on_release_note_autocadIcon_new", screenshotName="About_releaseVersion_Screenshot", currentLocation="test.images", ch_dir="../test.cases/TC07_Installation_Uninstallation/VersionCheckingScreenShot/")
                print("Version Number: " + versionNo + " matched." + " Found text is \"" + text + "\"")
            else:
                print("FAILED")
                pyautogui.moveTo(400, 400)
                print(text)
                raise AssertionError(
                    "Version Number: " + versionNo + " Did not match. Found text is \"" + text + "\"")
        else:
            raise AssertionError(
                "Version Year: " + versionYear + " Did not match. Found text is \"" + text + "\"")
        if appears("sc_release_close", 15):
            hotKeyAction("win","up")
            if appears("show_this_next_technote"):
                mouseLeftClickOn("show_this_next_technote")
            mouseLeftClickOn("sc_release_close")
        else:
            pressAction('tab')
            pressAction('enter')
        if appears("ssi_tab", 15):
            mouseMoveTo("ssi_tab")
            if appears("ssi_tab_close", 10):
                mouseLeftClickOn("ssi_tab_close")
            else:
                raise AssertionError("ssi_tab_close did not appear")
            sleep(3)
        if appears("drawing_tab", 20):
            mouseLeftClickOn("drawing_tab")

    def checkTechNotes(self):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.cases/TC07_Installation_Uninstallation/")
            file = open("_ReleaseNotes.txt", "r")
        else:
            os.chdir("../TC07_Installation_Uninstallation/")
            file = open("_ReleaseNotes.txt", "r")
        os.chdir(currentWorkingDirectory)

        releasesFromText = file.read().splitlines()

        print(str(len(releasesFromText))+" Items were found in the _ReleaseNotes.txt")

        obj = AppUtil()
        ssiFolder = obj.getLatestInstalledFolderPathSSI()
        SCReleaseNotesPath = ssiFolder + "TechNotes"

        filesInsideDir = [f for f in listdir(SCReleaseNotesPath) if isfile(join(SCReleaseNotesPath, f))]

        print(str(len(filesInsideDir)) + " Items were found in the " + SCReleaseNotesPath + " directory.")

        unmatched=0
        added=0
        flag = 0

        if (len(releasesFromText) == len(filesInsideDir)):
            temp = ""
            for i in range(len(releasesFromText)):
                for j in range(len(filesInsideDir)):
                    flag=0
                    if releasesFromText[i] in filesInsideDir[j]:
                        temp+="<tr><td align=\"center\" bgcolor=\"#cef3ff\">"+releasesFromText[i]+"</td><td align=\"center\" bgcolor=\"#9fff8e\"> FOUND </td></tr>"
                        flag=1
                        break
                if flag==0:
                    unmatched=unmatched+1
                    temp+="<tr><td align=\"center\" bgcolor=\"#cef3ff\">" + releasesFromText[i] + "</td><td align=\"center\" bgcolor=\"#f48942\"> NOT FOUND </td></tr>"
            write("<table style = \"width:30%\" border=\"1\">"
                  "<tr>"
                  "<th bgcolor=\"#77daff\"> TECHNOTE VERSION </th>"
                  "<th bgcolor=\"#77daff\"> STATUS </th>"
                  "</tr>"+ temp +
                  "</table>"
                  "", level='INFO', html=True)
        elif (len(releasesFromText) > len(filesInsideDir)):
            temp = ""
            for i in range(len(releasesFromText)):
                for j in range(len(filesInsideDir)):
                    flag = 0
                    if releasesFromText[i] in filesInsideDir[j]:
                        temp += "<tr><td align=\"center\" bgcolor=\"#cef3ff\">" + releasesFromText[i] + "</td><td align=\"center\" bgcolor=\"#9fff8e\"> FOUND </td></tr>"
                        flag = 1
                        break
                if flag == 0:
                    unmatched = unmatched + 1
                    temp += "<tr><td align=\"center\" bgcolor=\"#cef3ff\">" + releasesFromText[i] + "</td><td align=\"center\" bgcolor=\"#f48942\"> NOT FOUND </td></tr>"
            write("<table style = \"width:30%\" border=\"1\">"
                  "<tr>"
                  "<th bgcolor=\"#77daff\"> TECHNOTE VERSION </th>"
                  "<th bgcolor=\"#77daff\"> STATUS </th>"
                  "</tr>" + temp +
                  "</table>"
                  "", level='INFO', html=True)
        elif (len(releasesFromText) < len(filesInsideDir)):
            temp = ""
            for i in range(len(filesInsideDir)):
                for j in range(len(releasesFromText)):
                    flag = 0
                    if filesInsideDir[i] in releasesFromText[j]:
                        temp += "<tr><td align=\"center\" bgcolor=\"#cef3ff\">" + releasesFromText[j] + "</td><td align=\"center\" bgcolor=\"#9fff8e\"> FOUND </td></tr>"
                        flag = 1
                        break
                if flag == 0:
                    currentWorkingDirectory = os.getcwd()
                    baseLocation = os.path.basename(os.getcwd())
                    if baseLocation == "test.images":
                        os.chdir("../test.cases/TC07_Installation_Uninstallation/")
                        reWrite = open("_ReleaseNotes.txt", "a+")
                    else:
                        os.chdir("../TC07_Installation_Uninstallation/")
                        reWrite = open("_ReleaseNotes.txt", "a+")
                    reWrite.write(filesInsideDir[i]+ "\n")
                    reWrite.close()
                    os.chdir(currentWorkingDirectory)
                    added = added + 1
                    temp += "<tr><td align=\"center\" bgcolor=\"#cef3ff\">" + filesInsideDir[i] + "</td><td align=\"center\" bgcolor=\"#f48942\"> ADDED </td></tr>"
            write("<table style = \"width:30%\" border=\"1\">"
                  "<tr>"
                  "<th bgcolor=\"#77daff\"> TECHNOTE VERSION </th>"
                  "<th bgcolor=\"#77daff\"> STATUS </th>"
                  "</tr>" + temp +
                  "</table>"
                  "", level='INFO', html=True)

        if unmatched>0:
            self.logger.error(str(unmatched)+" file(s) missing in " + SCReleaseNotesPath + " directory.")
        if added>0:
            self.logger.warn(str(added)+" file(s) added in the _ReleaseNotes.txt file")

    # checking release version in SC About
    def SCAboutVersion(self):
        obj = AppUtil()
        path = obj.getPathFinder()
        # obj.minimize_tabs()
        sleep(4)
        pyautogui.moveTo(400, 400)
        pyautogui.click()
        pressAction('esc',2)
        pyautogui.typewrite('SCABOUT', .2)
        pressAction('enter')
        if appears("about_sc", 30):
            print("about_sc dialog appeared")
            if appears("version",10):
                mouseMoveTo("version")
            else:
                raise AssertionError("version did not appear")
            loca2 = location("version")
            if loca2 is not None:
                takeScreenShot("Version", loca2[0], loca2[1], loca2[2] + 400, loca2[3])
                os.system("magick convert Version.png -resize 450% VersionExt.png")
            else:
                raise AssertionError("sc_2019_on_release_note did not appear SC About")
            text = obj.imageReader(image_name="VersionExt.png")
            versionYear = obj.returnLatestAppYear()
            leng = versionYear.__len__()
            leng = leng - 4
            versionYear = versionYear[:-leng]
            versionNo = obj.getLatestVersion()
            versionNoSkipDot = versionNo.replace(".", "")
            # versionNoBeforeDot = versionNo.split('.')[0]
            if versionYear in text:
                print("Version year "+versionYear+" matched")
                if versionNo in text or versionNoSkipDot in text:# or versionNoBeforeDot in text:
                    print("PASSED")
                    pyautogui.moveTo(400, 400)
                    if "\TC00_Run_All" in path:
                        self.versionSuccessScreenshotAllRunRobot(logoName="sc_about_logo", screenshotName="About_SCVersion_Screenshot")
                    else:
                        self.versionSuccessScreenshot(logoName="sc_about_logo", screenshotName="About_SCVersion_Screenshot", currentLocation="test.images", ch_dir="../test.cases/TC07_Installation_Uninstallation/VersionCheckingScreenShot/")
                    print("Version Number: " + versionNo + " matched." + " Found text is \"" + text + "\"")
                else:
                    print("FAILED")
                    pyautogui.moveTo(400, 400)
                    print(text)
                    raise AssertionError(
                        "Version Number: " + versionNo + " Did not match. Found text is \"" + text + "\"")
            else:
                raise AssertionError(
                    "Version Year: " + versionYear + " Did not match. Found text is \"" + text + "\"")
            pressAction('enter')
            sleep(5)
        else:
            raise AssertionError("about_sc did not appear")

    def deployTestProject(self):
        while(1):
            if appears("unselect_project_icon", 10):
                mouseDoubleClickOn("unselect_project_icon")
                break
            elif appears("select_project_icon", 10):
                mouseDoubleClickOn("select_project_icon")
                break
            elif appears("no_registered_project", 10):
                obj = AppUtil()
                var = obj.systemTaskKill('rd /s /q C:\\NewDeploy_pipeCatalog')
                if var == 0:
                    self.logger.info("NewDeploy_pipeCatalog deploy folder was deleted")
                else:
                    self.logger.info("NewDeploy_pipeCatalog deploy folder was not found")
                obj1 = ProjectRegisterDeployDeleteUtil()
                obj1.project_deployer(module="pipeCatalog")
                obj.launch_app()
                mouseLeftClickOn('shipconstructor_tab_new')
                sleep(5)
                maximize()
                obj.ribbon_load("hull_and_structure_tab")
                obj1.register_project("deploy_pipe_catalog_list", "NewDeploy_pipeCatalog")
                break
            else:
                self.logger.info("One Project is already deployed.")
                mouseLeftClickOn("cancel_register_project")

    # checking Report verification from SC REPORT
    def SCReportVerification(self):
        obj = AppUtil()
        obj.minimize_tabs()
        mouseLeftClickOn('shipconstructor_tab_new')
        if appears("main_panel_new", 10):
            mouseLeftClickOn('main_panel_new')
            mouseLeftClickOn('navigator_button_new_18')
            if appears("register_project_dialog", 10):
                mouseMoveTo("register_project_dialog")
                self.deployTestProject()
            else:
                raise AssertionError("register_project_dialog didn't appear.")
        pyautogui.moveTo(400, 400)
        pyautogui.click()
        pressAction('esc',2)
        sleep(2)
        pyautogui.typewrite('SCREPORT', .2)
        sleep(2)
        pressAction('enter')
        if appears("report_generation_window"):
            mouseMoveTo("report_generation_window")
            hotKeyAction("win", "up")
            if appears("report_run_button", 10):
                mouseLeftClickOn("report_run_button")
            if appears("close_table_content"):
                mouseMoveTo("generated_report_upDown")
                mouseLeftClickOn("close_table_content")
        else:
            raise AssertionError("report_generation_window doesn't appeared.")
        if appears("register_down_arrow"):
            goDownUntilAppears("end_of_page", "register_down_arrow")
        mouseMoveToCoordinates(10,10)
        if appears("report_warning_message",10):
            mouseLeftClickOn("report_cross")
            os.system("TASKKILL /F /IM Report.exe")
            raise AssertionError("A Warning message is showing on the footer: This document was created using an Evaluation version of ActiveReports")
        else:
            mouseLeftClickOn("report_cross")
            os.system("TASKKILL /F /IM Report.exe")
            self.logger.info("Report has been generate successflly.")
        os.system("TASKKILL /F /IM acad.exe")
        os.system("TASKKILL /F /IM Report.exe")

    def closeThisApplication(self):
        cmd = 'WMIC PROCESS get Caption,Commandline,Processid'
        proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        for line in proc.stdout:
            if "NCPyros.exe" in str(line):
                os.system("TASKKILL /F /IM NCPyros.exe")
                print("NCPyros application closed.")
            elif "InverseBend.exe" in str(line):
                os.system("TASKKILL /F /IM InverseBend.exe")
                print("InverseBend application closed.")
            elif "LinesFairing.exe" in str(line):
                os.system("TASKKILL /F /IM LinesFairing.exe")
                print("LinesFairing application closed.")
            elif "LoftSpace.exe" in str(line):
                os.system("TASKKILL /F /IM LoftSpace.exe")
                print("LoftSpace application closed.")
            elif "Pinjigs.exe" in str(line):
                os.system("TASKKILL /F /IM Pinjigs.exe")
                print("Pinjigs application closed.")
            elif "PlateExpand.exe" in str(line):
                os.system("TASKKILL /F /IM PlateExpand.exe")
                print("PlateExpand application closed.")
            elif "PrintOffsets.exe" in str(line):
                os.system("TASKKILL /F /IM PrintOffsets.exe")
                print("PrintOffsets application closed.")
            elif "ShellExpand.exe" in str(line):
                os.system("TASKKILL /F /IM ShellExpand.exe")
                print("ShellExpand application closed.")
            elif "StringerCutouts.exe" in str(line):
                os.system("TASKKILL /F /IM StringerCutouts.exe")
                print("StringerCutouts application closed.")
            elif "acad.exe" in str(line):
                os.system("TASKKILL /F /IM acad.exe")
                os.system("TASKKILL /F /IM LMU.exe")
                print("acad application closed.")
            elif "Administrator.exe" in str(line):
                os.system("TASKKILL /F /IM Administrator.exe")
                print("Administrator application closed.")

    # checking release note in NCPyros about
    def AboutNCPyros(self):
        os.system("TASKKILL /F /IM NCPyros.exe")
        obj = AppUtil()
        path = obj.getPathFinder()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SC_NCPyros.bat")
        # if appears("licensing_done", 50):
        #     mouseLeftClickOn("licensing_done")
        if appears("nc_pyros_window"):
            print("nc_pyros_window appeared")
            if appears("nc_pyros_help"):
                mouseLeftClickOn("nc_pyros_help")
            if appears("nc_pyros_about"):
                mouseLeftClickOn("nc_pyros_about")
            else:
                mouseLeftClickOn("nc_pyros_help")
                mouseLeftClickOn("nc_pyros_about")
        if appears("about_nc_pyros_window"):
            self.logger.info("about_nc_pyros_window appeared")
            if appears("nc_pyros_version",10):
                mouseMoveTo("nc_pyros_version")
            else:
                raise AssertionError("nc_pyros_version did not appear")
            loca2 = location("nc_pyros_version")
            if loca2 is not None:
                takeScreenShot("nc_pyros_version", loca2[0], loca2[1], loca2[2] + 400, loca2[3])
                os.system("magick convert nc_pyros_version.png -resize 500% nc_pyros_versionExt.png")
            else:
                raise AssertionError("sc_2019_on_release_note did not appear in NCPyros")
            text = obj.imageReader(image_name="nc_pyros_versionExt.png")
            obj = AppUtil()
            versionYear = obj.returnLatestAppYear()
            leng = versionYear.__len__()
            leng = leng - 4
            versionYear = versionYear[:-leng]
            versionNo = obj.getLatestVersion()
            versionNoSkipDot = versionNo.replace(".", "")
            versionNoBeforeDot = versionNo.split('.')[0]
            if versionYear in text:
                print("Version year " + versionYear + " matched")
                if versionNo in text or versionNoSkipDot in text or versionNoBeforeDot in text:
                    print("PASSED")
                    pyautogui.moveTo(400, 400)
                    if "\TC00_Run_All" in path:
                        self.versionSuccessScreenshotAllRunRobot(logoName="nc_logo", screenshotName="About_NCPyros_Screenshot")
                    else:
                        self.versionSuccessScreenshot(logoName="nc_logo", screenshotName="About_NCPyros_Screenshot", currentLocation= "test.images", ch_dir= "../test.cases/TC07_Installation_Uninstallation/VersionCheckingScreenShot/")
                    print("Version Number: " + versionNo + " matched." + " Found text is \"" + text + "\"")
                else:
                    print("FAILED")
                    pyautogui.moveTo(400, 400)
                    print(text)
                    raise AssertionError(
                        "Version Number: " + versionNo + " Did not match. Found text is \"" + text + "\"")
            else:
                raise AssertionError(
                    "Version Year: " + versionYear + " Did not match. Found text is \"" + text + "\"")
            pressAction('enter')
        else:
            raise AssertionError("about_nc_pyros_window did not appear")
        self.closeThisApplication()

    #to handle shipcam warning messages
    def cancelShipCamWarning(self):
        if appears("shipcam_warn", 20):
            mouseLeftClickOn("shipcam_warn")
            pressAction('tab')
            pressAction('enter')
        if appears('shipcam_open_win', 20):
            pressAction('tab', 3)
            pressAction('enter')

    # to open applications of ShipCam and open about popup
    def openShipCamApplications(self, windowName):
        self.cancelShipCamWarning()
        if appears("licensing_logo", 10):
            mouseLeftClickOn("licensing_logo")
            if appears("licensing_done", 10):
                mouseLeftClickOn("licensing_done")
            else:
                raise AssertionError("licensing_done did not appear")
            pyautogui.moveTo(400, 400)
            if appears("No_Button_CreateWindow", 10):
                mouseLeftClickOn("No_Button_CreateWindow")
            else:
                raise AssertionError("No_Button_CreateWindow did not appear")
            if appears("cancel_button_CreateWindow", 10):
                mouseLeftClickOn("cancel_button_CreateWindow")
            else:
                raise AssertionError("cancel_button_CreateWindow did not appear")
        else:
            self.logger.warn("licensing window did not appear")
        if appears(windowName, 10):
            self.logger.info(windowName + " appeared")
            if appears("help_button", 10):
                mouseLeftClickOn("help_button")
            else:
                raise AssertionError("help button did not appear")
            if appears("about_button", 10):
                mouseLeftClickOn("about_button")
            else:
                raise AssertionError("about_button did not appear")
        else:
            raise AssertionError(windowName + " did not appear")

    # to match version number of ShipCam applications and take screenshot on success
    def matchVersionNumber(self, logo, screenShotName):
        obj = AppUtil()
        path = obj.getPathFinder()
        if appears("about_ShipCam_window"):
            print("about_ShipCam_window appeared")
            if appears("version",10):
                mouseMoveTo("version")
            else:
                raise AssertionError("version did not appear")
            loca2 = location("version")
            if loca2 is not None:
                takeScreenShot("version", loca2[0], loca2[1], loca2[2] + 200, loca2[3])
                os.system("magick convert version.png -resize 450% versionExt.png")
            else:
                raise AssertionError("Version did not appear in about version window.")
            text = obj.imageReader(image_name="versionExt.png")
            versionYear = obj.returnLatestAppYear()
            leng = versionYear.__len__()
            leng = leng - 4
            versionYear = versionYear[:-leng]
            versionNo = obj.getLatestVersion()
            versionNoSkipDot = versionNo.replace(".", "")
            # versionNoBeforeDot = versionNo.split('.')[0]
            if versionYear in text:
                print("Version year " + versionYear + " matched")
                if versionNo in text or versionNoSkipDot in text:
                    print("PASSED")
                    pyautogui.moveTo(400, 400)
                    if "\TC00_Run_All" in path:
                        self.versionSuccessScreenshotAllRunRobot(logoName=logo, screenshotName=screenShotName)
                    else:
                        self.versionSuccessScreenshot(logoName = logo, screenshotName = screenShotName, currentLocation= "test.images", ch_dir= "../test.cases/TC07_Installation_Uninstallation/VersionCheckingScreenShot/")
                    print("Version Number: " + versionNo + " matched." + " Found text is \"" + text + "\"")
                else:
                    print("FAILED")
                    pyautogui.moveTo(400, 400)
                    print(text)
                    raise AssertionError("Version Number: " + versionNo + " Did not match. Found text is \"" + text + "\"")
            else:
                raise AssertionError("Version Year: " + versionYear + " Did not match. Found text is \"" + text + "\"")
            pressAction('enter')
        else:
            raise AssertionError("about_ShipCam_window did not appear")
        self.closeThisApplication()

    # checking release note in ShipCAM\InverseBend about
    def AboutInverseBend(self):
        os.system("TASKKILL /F /IM InverseBend.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_InverseBend.bat")
        self.openShipCamApplications("inversebend_logo")
        self.matchVersionNumber("inversebend_logo", "About_InverseBend_Screenshot")

    # checking release note in ShipCAM\LinesFairing about
    def AboutLinesFairing(self):
        os.system("TASKKILL /F /IM LinesFairing.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_LinesFairing.bat")
        self.openShipCamApplications("LinesFairing_logo")
        self.matchVersionNumber("LinesFairing_logo", "About_LinesFairing_Screenshot")

    # checking release note in ShipCAM\LoftSpace about
    def AboutLoftSpace(self):
        os.system("TASKKILL /F /IM LoftSpace.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_LoftSpace.bat")
        self.openShipCamApplications("LoftSpace_logo")
        self.matchVersionNumber("LoftSpace_logo", "About_LoftSpace_Screenshot")

    # checking release note in ShipCAM\Pinjigs about
    def AboutPinjigs(self):
        os.system("TASKKILL /F /IM Pinjigs.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_Pinjigs.bat")
        self.openShipCamApplications("Pinjigs_logo")
        self.matchVersionNumber("Pinjigs_logo", "About_Pinjigs_Screenshot")

    # checking release note in ShipCAM\PlateExpand about
    def AboutPlateExpand(self):
        os.system("TASKKILL /F /IM PlateExpand.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_PlateExpand.bat")
        self.openShipCamApplications("PlateExpand_logo")
        self.matchVersionNumber("PlateExpand_logo", "About_PlateExpand_Screenshot")

    # checking release note in ShipCAM\PrintOffsets about
    def AboutPrintOffsets(self):
        os.system("TASKKILL /F /IM PrintOffsets.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_PrintOffsets.bat")
        self.openShipCamApplications("PrintOffsets_logo")
        self.matchVersionNumber("PrintOffsets_logo", "About_PrintOffsets_Screenshot")

    # checking release note in ShipCAM\ShellExpand about
    def AboutShellExpand(self):
        os.system("TASKKILL /F /IM ShellExpand.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_ShellExpand.bat")
        self.openShipCamApplications("ShellExpand_logo")
        self.matchVersionNumber("ShellExpand_logo", "About_ShellExpand_Screenshot")

    # checking release note in ShipCAM\StringerCutouts about
    def AboutStringerCutouts(self):
        os.system("TASKKILL /F /IM StringerCutouts.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_ShipCAM_StringerCutouts.bat")
        self.openShipCamApplications("StringerCutouts_logo")
        self.matchVersionNumber("StringerCutouts_logo", "About_StringerCutouts_Screenshot")

    # Executing SC for version checking
    def RunApplication(self):
        os.system("TASKKILL /F /IM acad.exe")
        os.system("TASKKILL /F /IM LMU.exe")

        obj = AppUtil()

        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
            os.system("Run_SC.bat")
        else:
            os.chdir("../../test.batches/")
            os.system("Run_SC.bat")
        os.chdir(currentWorkingDirectory)
        sleep(2)
        pyautogui.moveTo(400, 400)


        obj.startSSIFromList()
        obj.matchLicenseStatus()

        if appears("migrate_setting_dialog",10):
            print("Setting Migration dialog appeared")
            mouseMoveTo("migrate_setting_dialog")
            mouseLeftClickOn("migrate_settings_no")
            pyautogui.moveTo(400, 400)
            pyautogui.moveTo(400, 400)
            pyautogui.moveTo(400, 400)
        sleep(2)
        if appears("hull_and_structure_tab"):
            print("select_shipcon_environment option appeared")
            mouseLeftClickOn("hull_and_structure_tab")
        sleep(10)

    # find the installer and run the batch file
    def runSetup(self):
        if config.Unzip_Installer == "Yes" :
            currentWorkingDirectory = os.getcwd()
            baseLocation= os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.batches/")
                os.system("extract_install.bat")
            else:
                os.chdir("../../test.batches/")
                os.system("extract_install.bat")
            os.chdir(currentWorkingDirectory)
        elif config.Unzip_Installer == "No" :
            currentWorkingDirectory = os.getcwd()
            os.chdir(config.Path_Installer)
            for file in glob.glob("*Setup.exe"):
                commandline = "start " + file
                os.system(commandline)
            os.chdir(currentWorkingDirectory)
        else:
            raise AssertionError("Please insert the value of Unzip_Installer & Path_Installer from Config file.")
        if appears("setup_click"):
            mouseLeftClickOn("setup_click")
        else:
            raise AssertionError("Setup click did not appear")

    # Normal Installation of SC [Currently being used]
    @my_timer
    def SC_Install(self):
        obj = AppUtil()
        folderName = obj.findRegistyFolder()
        if folderName is not None:
            print("Registry has been cleaned.")
            os.system('reg delete "HKEY_CURRENT_USER\Software\SSI\\' + folderName + '"' + ' /f')
        else:
            print("Registry is already cleaned.")
        self.runSetup()
        if appears("sc_installation_logo",10):
            mouseMoveTo("sc_installation_logo")
            self.logger.info("Setup application has launched properly")
            self.logger.info("The Installation menu was displayed")
            mouseLeftClickOn("shipcon_option")
            mouseLeftClickOn("sc_setup_screen_next_button")
            if appears("accept_license"):
                self.logger.info("ShipConstructor End-User License Agreement was displayed")
                mouseLeftClickOn("accept_license")
            mouseLeftClickOn("install_button")
            sleep(1)
            if appears("installing_1"):
                mouseMoveTo("installing_1")
                self.logger.info("Installation started")
                waitForVanish("installing_1")
                waitToAppear("done_button")
                if appears("done_button"):
                    mouseLeftClickOn("done_button")
                    self.logger.info("Application was installed successfully")
                else:
                    raise AssertionError("Something went wrong. Installation did not complete")
            else:
                raise AssertionError("installing dialog did not appear")
        else:
            raise AssertionError("Setup application has not launched properly")
        self.createFileName("SSI")

    # create a text file which contains the installed path of current application
    # the file is created in test.batch folder
    def createFileName(self, installedApp):
        obj = AppUtil()
        if installedApp == 'SSI':
            ssiFolder = obj.getLatestInstalledFolderPathSSI()
        elif installedApp == 'EP':
            ssiFolder = obj.getLatestInstalledFolderPathEP()
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")
        f = open("ssiFolder.txt", "w+")
        f.write(ssiFolder)
        f.close()
        os.chdir(currentWorkingDirectory)

    # Opening newly installed application to test proper installation
    @my_timer
    def load_newly_installed_app(self):
        obj = AppUtil()
        os.system("TASKKILL /F /IM acad.exe")
        os.system("TASKKILL /F /IM LMU.exe")

        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        if baseLocation == "test.images":
            os.chdir("../test.batches/")
            os.system("Run_SC.bat")
        else:
            os.chdir("../../test.batches/")
            os.system("Run_SC.bat")
        os.chdir(currentWorkingDirectory)

        sleep(2)
        pyautogui.moveTo(400, 400)

        if appears("start_shipcon", 30):
            print(config.AutoCAD_version)
            if config.AutoCAD_version == "AutoCAD2019_mechanical":
                print("AutoCAD2019_mechanical")
                if appears("autocad_mechanical", 10):
                    print("AutoCAD Mechanical")
                    mouseLeftClickOn("autocad_mechanical")
            elif config.AutoCAD_version == "AutoCAD2019":
                print("AutoCAD2019")
                if appears("autocad2019_list", 10):
                    mouseLeftClickOn("autocad2019_list")

            mouseLeftClickOn("start_shipcon")
        if appears("migrate_setting_dialog"):
            self.logger.info("Setting Migration dialog appeared")
            mouseMoveTo("migrate_setting_dialog")
            mouseLeftClickOn("migrate_settings_no")
            pyautogui.moveTo(400,400)
            pyautogui.moveTo(400,400)
            pyautogui.moveTo(400,400)
            sleep(2)
        if appears("hull_and_structure_tab"):
            self.logger.info("select_shipcon_environment option appeared")
            mouseLeftClickOn("hull_and_structure_tab")
        sleep(10)

        if appears("show_this_next",30):
            mouseLeftClickOn("show_this_next")
            mouseLeftClickOn("sc_release_close")

        waitForVanish("ssi_startup_logo")

        if appears("show_this_next", 30):
            mouseLeftClickOn("show_this_next")
            mouseLeftClickOn("sc_release_close")

        obj.close_app()
        os.system("TASKKILL /F /IM acad.exe")
        os.system("TASKKILL /F /IM acad.exe")
        os.system("TASKKILL /F /IM acad.exe")

    # # Connecting to your first project [Section from Manual]
    # @my_timer
    # def connecting_to_your_first_project(self):
    #     self.logger.info('Start:\t Checking Module- connecting_to_your_first_project')
    #     obj = AppUtil()
    #     obj.launch_app()
    #     if appears("migrate_settings"):
    #         self.logger.info("Setting Migration dialog appeared")
    #         mouseLeftClickOn("migrate_settings_no")
    #     if appears("select_shipcon_environment"):
    #         self.logger.info("select_shipcon_environment option appeared")
    #         mouseLeftClickOn("hull_and_structure_tab")
    #     sleep(5)
    #     free()
    #     maximize()
    #     sleep(2)
    #     obj.minimize_tabs()
    #     mouseLeftClickOn('shipconstructor_tab_new')
    #     mouseLeftClickOn("project_pane")
    #     if appears("new_project"):
    #         self.logger.info("New project option appeared")
    #         mouseLeftClickOn("new_project")
    #         if appears("new_sc_project_dialog"):
    #             self.logger.info("New SC Project dialog appeared")
    #             mouseLeftClickOn("browse_project_folder")
    #             if appears("browse_for_folder_window"):
    #                 self.logger.info("browse_for_folder_window appeared")
    #                 mouseLeftClickOn("this_pc")
    #                 mouseLeftClickOn("local_drive_c")
    #                 mouseLeftClickOn("make_new_folder")
    #                 sleep(1)
    #                 # a = datetime.datetime.utcnow()
    #                 # pyautogui.typewrite(str(a), .2)
    #                 pyautogui.typewrite('Test_Project',1)
    #                 pressAction('enter',2)
    #                 self.logger.info('New project folder created named Test_Project')
    #                 if appears("sc_sql_14_selected", 5):
    #                     self.logger.info("Project server was found as selected")
    #                 else:
    #                     mouseLeftClickOn("project_server_dropdown")
    #                     if appears("sc_sql_14"):
    #                         mouseLeftClickOn("sc_sql_14")
    #                     else:
    #                         raise AssertionError("Project server could not be selected")
    #                 mouseLeftClickOn("template_browser")
    #                 if appears("open_template_browser_window"):
    #                     self.logger.info("Template browser window appeared")
    #                     mouseLeftClickOn("open_template_browser_window_dropdown")
    #                     pyautogui.typewrite('C:\Program Files\SSI\ShipConstructor 2018\ProjectTemplates', .1)
    #                     pressAction('enter')
    #                     if appears("pipe_catalog_xml"):
    #                         self.logger.info("Pipe catalog was selected as template")
    #                         mouseDoubleClickOn("pipe_catalog_xml")
    #                         sleep(5)
    #                         if appears("use_window_authentication", 5):
    #                             self.logger.info("Window authentication option was used")
    #                             mouseLeftClickOn("use_window_authentication")
    #                         mouseLeftClickOn("new_sc_project_dialog_ok")
    #                         sleep(10)
    #                         while (exists("new_project_created_dialog") == False):
    #                             sleep(1)
    #                             if appears("new_project_created_dialog", 5):
    #                                 self.logger.info("new_project_created_dialog appeared")
    #                                 break
    #                         if appears("new_project_created_dialog", 5):
    #                             mouseLeftClickOn("new_project_created_dialog_ok")
    #                             obj1 = ProjectRegisterDeployDeleteUtil()
    #                             obj1.register_project()
    #                             sleep(2)
    #                         else:
    #                             raise AssertionError("new_project_created_dialog did not appear")
    #                     else:
    #                         raise AssertionError("pipe_catalog_xml did not appear")
    #                 else:
    #                     raise AssertionError("open_template_browser_window did not appear")
    #         else:
    #             raise AssertionError("new_sc_project_dialog did not appear")
    #     else:
    #         raise AssertionError("new_project option did not appear")
    #     self.logger.info('End:\t Checking Module- connecting_to_your_first_project')

    def uninstall_EXE(self):
        if config.Unzip_Installer == "Yes":
            currentWorkingDirectory = os.getcwd()
            baseLocation = os.path.basename(os.getcwd())
            if baseLocation == "test.images":
                os.chdir("../test.batches/")
                os.system("run_uninstaller.bat")
            else:
                os.chdir("../../test.batches/")
                os.system("run_uninstaller.bat")
            os.chdir(currentWorkingDirectory)
        elif config.Unzip_Installer == "No":
            currentWorkingDirectory = os.getcwd()
            os.chdir(config.Path_Installer)
            for file in glob.glob("*Setup.exe"):
                commandline = "start "+file
                os.system(commandline)
            os.chdir(currentWorkingDirectory)
        else:
            raise AssertionError("Please insert the value of Unzip_Installer & Path_Installer from Config file.")

    def uninstallation_Start(self, uninstallationProcess = None):
        obj = AppUtil()
        if uninstallationProcess == None:
            if appears("shipCon_uninstall_dialog"):
                self.logger.info("shipCon_uninstall_dialog appeared")
                mouseMoveTo("shipCon_uninstall_dialog")
            else:
                raise AssertionError("shipCon_uninstall_dialog did not appear")
            pyautogui.moveTo(200,400)
            sleep(1)
            if appears("uninstall_sc",10):
                mouseLeftClickOn("uninstall_sc")
            else:
                raise AssertionError("uninstall_sc did not appear")

        if appears("shipCon_uninstall_dialog_yes_button"):
            mouseLeftClickOn("shipCon_uninstall_dialog_yes_button")
        else:
            raise AssertionError("shipCon_uninstall_dialog_yes_button")
        pyautogui.moveTo(400,500)
        sleep(1)
        pyautogui.moveTo(400,600)
        sleep(1)
        pyautogui.moveTo(400,700)
        sleep(1)
        if uninstallationProcess == None:
            waitToAppear("uninstall_done")
            if appears("uninstall_done"):
                self.logger.info("SC was successfully uninstalled")
                mouseLeftClickOn("uninstall_done")
                sleep(3)
            else:
                raise AssertionError("Uninstallation was not done")
            os.system("TASKKILL /F /IM Setup.exe")
        else:
            waitForVanish("uninstall_logo_window")
            sleep(2)
            hotKeyAction('alt','f4')

        folderName = obj.findRegistyFolder()
        if folderName is not None:
            print("Registry has been cleaned.")
            os.system('reg delete "HKEY_CURRENT_USER\Software\SSI\\' + folderName + '"' + ' /f')
        else:
            print("Registry is already cleaned.")

    def uninstall_ControlPanel(self):
        obj = AppUtil()
        obj.openApplicationFeatureWindow()
        if appears("uninstallation_icon"):
            mouseDoubleClickOn("uninstallation_icon")
        else:
            raise AssertionError("uninstallation_icon didn't appear.")

    def selectUninstallationType(self):
        if config.Uninstallation_Process == "1":
            self.uninstall_EXE()
            self.uninstallation_Start()
        elif config.Uninstallation_Process == "2":
            self.uninstall_ControlPanel()
            self.uninstallation_Start("2")
        elif config.Uninstallation_Process == "3":
            self.uninstall_EXE()
            self.uninstallation_Start()
            self.SC_Install()
            self.uninstall_ControlPanel()
            self.uninstallation_Start("2")
        else:
            raise AssertionError("Wrong Uninstallation Process value in Config: "+config.Uninstallation_Process)

    # SC Uninstall [Currently being used]
    @my_timer
    def SC_uninstall(self):
        self.selectUninstallationType()

    def EP_uninstall(self):
        self.selectUninstallationType()

    # SSI License
    @my_timer
    def removing_ssi_licensing(self):
        self.logger.info('Start:\t Checking Module- removing_ssi_licensing')
        sleep(5)
        pyautogui.hotkey('win','d')
        sleep(2)
        pyautogui.hotkey('win','q')
        sleep(2)
        pyautogui.typewrite("programs and features", .1)
        pressAction('enter')
        if appears("programs_and_features_window"):
            mouseMoveTo("programs_and_features_window")
            self.logger.info("programs_and_features_window appeared")
            pyautogui.hotkey('ctrl','f')
            pyautogui.typewrite("SSI",.1)
            if appears("ssi_license_server"):
                self.logger.info("ssi_license_server appeared on the found items list")
                mouseLeftClickOn("ssi_license_server")
                mouseLeftClickOn("uninstall_option")
                if appears("warning_before_uninstalling"):
                    mouseLeftClickOn("warning_before_uninstalling_yes")
                    if appears("ssi_license_server_removing_dialog"):
                        waitForVanish("ssi_license_server_removing_dialog")
                        self.logger.info("ssi_license_server was removed successfully")
                        pyautogui.hotkey('alt','f4')
                    else:
                        raise AssertionError("ssi_license_server_removing_dialog did not appear")
            else:
                sleep(2)
                pyautogui.hotkey('alt','f4')
                raise AssertionError("ssi_license_server was not found")
        else:
            raise AssertionError("programs_and_features_window did not appear")
        self.logger.info('End:\t Checking Module- removing_ssi_licensing')

    # Open release.html file and verify all the missing component
    def verifyMissingConponent(self):
        obj = AppUtil()
        releaseHTML = obj.readReleaseHtmlFile()
        missingComponent = "MISSING COMPONENT"

        if "id=\"MISSING COMPONENT\"" in releaseHTML:
            if missingComponent in releaseHTML:
                raise AssertionError("Missing Components are Detected.")
            else:
                self.logger.info("No Missing Components are found.")
        else:
            self.logger.info("No Missing Components are found.")

    # Open release.html file and verify all the missing summary details
    def verifyMissingTechSummary_Details(self):
        obj = AppUtil()
        releaseHTML = obj.readReleaseHtmlFile()
        missingTechSum = "MISSING TECHNOTE SUMMARY"
        missingTechDetails = "MISSING TECHNOTE DETAILS"

        if (missingTechSum in releaseHTML) and (missingTechDetails in releaseHTML):
            returnMes = obj.getSCNumber_TechNotesSummary()
            for temp in returnMes:
                self.logger.info(temp)
            raise AssertionError("Missing Technote Summary Detected. And Missing Technote Details Detected.")
        elif (missingTechSum in releaseHTML) or (missingTechDetails in releaseHTML):
            returnMes = obj.getSCNumber_TechNotesSummary()
            for temp in returnMes:
                self.logger.info(temp)
            raise AssertionError("Missing Technote Summary Or Missing Technote Details Detected.")
        else:
            self.logger.info("No Missing TechNote Details or Summary have been found.")

    # Open release.html file and verify all the no missing summary
    def verifyNoMissingSummary(self):
        obj = AppUtil()
        releaseHTML = obj.readReleaseHtmlFile()
        noMissingSum = " -  ("

        if noMissingSum in releaseHTML:
            returnTNS = obj.getSCNumber_MissingSum(noMissingSum)
            if len(returnTNS) == 0:
                self.logger.info("No Unknown TechNote Status are found.")
            else:
                self.logger.error("Missing TechNotes Summary given below: ")
                for temp in returnTNS:
                    self.logger.info(temp)
                raise AssertionError("Missing TechNotes Summary found.")
        else:
            self.logger.info("No Unknown TechNote Status are found.")

    # Open release.html file and verify all the missing technotes
    def verifyMissingTechNotes(self):
        obj = AppUtil()
        releaseHTML = obj.readReleaseHtmlFile()
        technoteStatus = "TechNote Status is Unknown"

        if technoteStatus in releaseHTML:
            obj.getSCNumber_UnknownStatus()
            raise AssertionError("Unknown TechNote Status is Detected.")
        else:
            self.logger.info("No Unknown TechNote Status are found.")

    # open administrator file and verify its open
    def openadministratorapp(self):
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SC_Administrator.bat")
        if appears("administrator_window_icon", 20):
            mouseMoveTo("administrator_window_icon")
            if appears("administrator_window_auth", 20):
                mouseLeftClickOn("administrator_window_auth")
            else:
                print("Authentication has been already selected.")
            if appears("no_administrator_sql_server_region", 30):
                mouseDoubleClickOn("no_administrator_sql_server")
                if appears("sql_instance_window", 20):
                    if appears("select_sql_server"):
                        mouseMoveTo('select_sql_server')
                        mouseLeftClickOn("sql_instance_dropdown")
                        if appears("administrator_sql_server_instance_unselected", 20):
                            mouseLeftClickOn("administrator_sql_server_instance_unselected")
                        else:
                            raise AssertionError("administrator_sql_server_instance_unselected doesn't appeared.")
                        mouseLeftClickOn('ok_sql_instance', 'ok_sql_instance_region')
            else:
                if appears("administrator_sql_server_unselected", 20):
                    mouseLeftClickOn("administrator_sql_server_unselected")
                elif appears("administrator_sql_server_selected", 20):
                    mouseLeftClickOn("administrator_sql_server_selected")
                else:
                    raise AssertionError("Administrator window doesn't appeared.")
            if appears("administrator_ok_button", 20):
                mouseLeftClickOn("administrator_ok_button")
            else:
                raise AssertionError("administrator_ok_button doesn't appeared.")
            if appears("administrator_max_cross_region", 20):
                mouseLeftClickOn("administrator_maximize_button")
            else:
                print("Administrator window is already maximized.")
            hotKeyAction('win', 'up')
        else:
            raise AssertionError("Administrator window doesn't appeared.")

    # restore an old db from a specific directory given in config file
    def restoreDBFromSpecificFile(self, dbName):
        if dbName == None:
            raise AssertionError("No value available in Database Name.")
        while(True):
            mouseMoveTo("administrator_db_name_title")
            if appears("administrator_db_icon", 20):
                mouseLeftClickOn("administrator_db_icon")
                sleep(2)
                mouseLeftClickOn("administrator_delete_icon")
                pressAction('enter')
                mouseLeftClickOn("administrator_db_name_title")
            elif appears("administrator_db_icon_high", 20):
                mouseLeftClickOn("administrator_db_icon_high")
                sleep(2)
                mouseLeftClickOn("administrator_delete_icon")
                pressAction('enter')
                mouseLeftClickOn("administrator_db_name_title")
            else:
                print("No database is available in the Administrator Application.")
                break
        if appears("administrator_tools_button", 20):
            mouseLeftClickOn("administrator_tools_button")
            if appears("administrator_add_db_backup_button", 20):
                mouseLeftClickOn("administrator_add_db_backup_button")
            else:
                print("administrator_add_db_backup_button didn't found.")
            if appears("administrator_restore_db_name", 20):
                mouseMoveTo("administrator_restore_db_name")
                pyautogui.typewrite(dbName,0.2)
                mouseLeftClickOn("administrator_ok_button")
                sleep(3)
                hotKeyAction('ctrl','a')
                pressAction('backspace')
                backup_dbName = config.BackUp_DB
                backup_dbName = backup_dbName.replace("/","\\")
                pyautogui.typewrite(backup_dbName,0.2)
                sleep(1)
                pressAction('enter')
                sleep(10)
                if appears("administrator_database_title"):
                    mouseMoveTo("administrator_database_title")
                    mouseLeftClickOn("administrator_database_ok")
                else:
                    print("administrator_database_title didn't found.")
            else:
                print("administrator_add_db_backup_button didn't found.")
        else:
            raise AssertionError("administrator_tools_button didn't found.")

    # update the old db from administrator application
    def updateOldDB(self):
        if appears("administrator_db_icon", 20):
            mouseLeftClickOn("administrator_db_icon")
            sleep(2)
            mouseLeftClickOn("administrator_update_db")
        elif appears("administrator_db_icon_high", 20):
            mouseLeftClickOn("administrator_update_db")
        else:
            print("Restore DB database not found in Administrator Application")
        if appears("administrator_update_start_button", 20):
            if appears("check_show_all_db"):
                mouseLeftClickOn("check_show_all_db")
            else:
                print("check_show_all_db is already checked.")
            if appears("down_arrow_db"):
                mouseLeftClickOn("down_arrow_db")
                pressAction("down",10)
                pressAction("enter")
            else:
                print("down_arrow_db didn't appear.")
            mouseLeftClickOn("administrator_update_start_button")
        else:
            print("administrator_update_start_button didn't appeared.")

        if appears("administrator_current_activity_window", 30):
            while(1):
                mouseLeftClickOn("refresh_button_CA")
                sleep(1)
                if appears("current_activity_empty_name",5):
                    break
            mouseLeftClickOn("current_activity_close")
        if appears("administrator_warning_message", 20):
            mouseMoveTo("administrator_warning_message")
            mouseLeftClickOn("administrator_warning_yes_button")
        # else:
        #     raise AssertionError("administrator_warning_message didn't appeared.")
        else:
            self.logger.info("administrator_warning_message didn't appeared.")
        if appears("administrator_backingUp_window", 20):
            mouseMoveTo("administrator_backingUp_window")
            waitForVanish("administrator_backingUp_window")
        if appears("administrator_updating_window", 20):
            mouseMoveTo("administrator_updating_window")
            waitForVanish("administrator_updating_window")
        # else:
        #     raise AssertionError("Update didn't occurred.")
        if appears("administrator_update_status_complete", 20):
            mouseMoveTo("administrator_update_status_complete")
            mouseLeftClickOn("administrator_update_close")
        else:
            print("administrator_update_status_complete didn't appeared.")

    # open the old project file from a specific directory given in config file
    def updateDBVerification(self,deploymentDir,dbaseName):
        if deploymentDir == None:
            raise AssertionError("No value available in deployment directory.")
        if dbaseName == None:
            raise AssertionError("No value available in database name which need to be verified.")
        obj = AppUtil()
        obj.minimize_tabs()
        mouseLeftClickOn('shipconstructor_tab_new')
        if appears("main_panel_new", 10):
            mouseLeftClickOn('main_panel_new')
            mouseLeftClickOn('navigator_button_new_18')
            if appears("register_project_dialog"):
                if appears("use_window_authentication", 10):
                    mouseLeftClickOn("use_window_authentication")
                else:
                    self.logger.info("Windows authentication is already selected.")
                mouseLeftClickOn("register_project_browse")
                if appears("register_project_browserWindow"):
                    mouseLeftClickOn("browser_adrs_dropdown")
                    pyautogui.typewrite(deploymentDir, .1)
                    pressAction('enter')
                    mouseLeftClickOn("window_search_field")
                    pyautogui.typewrite(dbaseName, .1)
                    if appears("deploymentFile"):
                        mouseDoubleClickOn("deploymentFile")
                    else:
                        raise AssertionError("deploymentFile didn't found")
                    if appears("project_open"):
                        mouseLeftClickOn("project_open")
                    else:
                        self.logger.error("Deployment Directory: "+deploymentDir +"And Database: "+dbaseName)
                        raise AssertionError("deploymentFile didn't found")
                if appears("administrator_error_message_updateDB"):
                    raise AssertionError("Database is not updated.")
                else:
                    print("Database has been updated and open project file successfully.")
                if appears("agreement_banner", 10):
                    print('Agreement banner is displayed')
                    mouseLeftClickOn("banner_read", "banner_read_region")
                    mouseLeftClickOn("banner_read_ok")
                    if appears("license_new", 5):
                        mouseLeftClickOn("license_done")
                        if appears("shipcon_navi_close"):
                            mouseLeftClickOn("shipcon_navi_close")
                        else:
                            raise AssertionError("Shipconstructor didn't close the agreement banner.")
                    else:
                        raise AssertionError("Server not found during project registration")
                if appears("check_project_files", 10):
                    mouseLeftClickOn("check_project_files")
                    hotKeyAction('alt', 'f4')
                if appears("shipcon_navi_close"):
                    mouseLeftClickOn("shipcon_navi_close")
        obj.systemTaskKill("TASKKILL /T /F /IM notepad.exe")
        obj.close_app()

    # restore and old db in administrator update the db and verify by opening project file
    def verifyUpdatedDatabase(self):
        os.system("TASKKILL /F /IM Administrator.exe")
        currentWorkingDirectory = os.getcwd()
        os.chdir(config.DeployedProject_Directory)
        databasename = ''
        for file in glob.glob("*.pro"):
            databasename = file
        os.chdir(currentWorkingDirectory)
        db_Name = databasename.replace(".pro","")
        deploymentDirectory = config.DeployedProject_Directory
        deploymentDirectory = deploymentDirectory.replace("/","\\")
        self.openadministratorapp()
        self.restoreDBFromSpecificFile(db_Name)
        os.system("TASKKILL /F /IM Administrator.exe")
        self.openadministratorapp()
        self.updateOldDB()
        os.system("TASKKILL /F /IM Administrator.exe")
        obj = AppUtil()
        obj.launch_app()
        self.updateDBVerification(deploymentDirectory,databasename)

    def verifyUninstall(self):
        obj = AppUtil()
        ssiFolder = obj.getLatestInstalledFolderPathSSI()
        self.verifyAfterUninstall(ssiFolder,"ProjectTemplates","ShipConstructor")
        mouseMoveToCoordinates(10, 10)
        pressAction('win')
        sleep(2)
        if appears("ssi_logo_start_menu"):
            raise AssertionError("Uninstallation is not completed successfully.")
        else:
            self.logger.info("Uninstallation is completed successfully and shortcut icon has been removed from Start window.")
        pressAction('win')

    def verifyAfterUninstall(self, folderPath, folderName, appName1, appName2=None):
        currentWorkingDirectory = os.getcwd()
        os.chdir("C:/Users/Public/Desktop/")
        if appName2 is not None:
            for x in os.listdir('.'):
                if appName1 in x:
                    raise AssertionError("Uninstallation is not completed successfully." + x + 'is still installed.')
                if appName2 in x:
                    raise AssertionError("Uninstallation is not completed successfully." + x + 'is still installed.')
        else:
            for x in os.listdir('.'):
                if appName1 in x:
                    raise AssertionError("Uninstallation is not completed successfully." + x + 'is still installed.')
        os.chdir(currentWorkingDirectory)
        self.logger.info("Uninstallation is completed successfully and desktop icon has been removed.")
        obj = AppUtil()
        obj.openApplicationFeatureWindow()

        if appears("no_items_match"):
            self.logger.info("Ship Constructor has been removed from Programs and Features")
        elif appears("no_name_Cpanel"):
            self.logger.info("No such Name as Ship Constructor in Programs and Features. SSI has been removed.")
        else:
            raise AssertionError("Ship Constructor has not been removed from Programs and Features")

        hotKeyAction("Alt", "f4")

        currentWorkingDirectory = os.getcwd()
        try:
            os.chdir(folderPath)
            for x in os.listdir('.'):
                if folderName in x:
                    raise AssertionError(
                        "Installation Directory contains the " + folderName + " folder after uninstall.")
                else:
                    self.logger.info(
                        "Installation Directory doesn't contain the " + folderName + " folder after uninstall.")
        except:
            self.logger.info("Installation Directory doesn't contain the " + folderName + " folder after uninstall.")

        os.chdir(currentWorkingDirectory)

    def verifyEnterpricePlatformSetupWindow(self):
        if appears("sc_installation_logo",10):
            mouseMoveTo("sc_installation_logo")
            self.logger.info("Setup application has launched properly")
            self.logger.info("The Installation menu was displayed")
            mouseLeftClickOn("enterprise_platform_option")
            sleep(2)
            mouseMoveTo("sc_installation_logo")
        else:
            raise AssertionError("Installation popup doesn't appeared.")

    def getEPVersionNumber(self):
        obj = AppUtil()
        if appears("enterprise_version_number", 10):
            mouseMoveTo("enterprise_version_number")
        else:
            raise AssertionError("enterprise_version_number did not appear")
        loca2 = location("enterprise_version_number")
        if loca2 is not None:
            takeScreenShot("InstallerVersion", loca2[0]+50, loca2[1], loca2[2] + 100, loca2[3])
            os.system("magick convert InstallerVersion.png -resize 300% InstallerVersionExt.png")
        else:
            raise AssertionError("sc_2019_on_release_note did not appear SC About")
        text = obj.imageReader(image_name="InstallerVersionExt.png")
        self.logger.info("Installer's version number is : "+text)

    def setupLicenseServer(self):
        if appears("enterprise_localhost"):
            # mouseMoveTo("enterprise_license_title2")
            mouseLeftClickOn("enterprise_localhost")
            hotKeyAction("ctrl","a")
            pressAction("backspace")
            pyautogui.typewrite(config.LicenseServer)
        else:
            raise AssertionError("Couldn't find enterprise_license_title.")

    def ep_Installation(self):
        self.getEPVersionNumber()
        self.setupLicenseServer()
        if appears("sc_setup_screen_next_button"):
            mouseLeftClickOn("sc_setup_screen_next_button")
        else:
            raise AssertionError("Couldn't find sc_setup_screen_next_button.")
        if appears("accept_license"):
            self.logger.info("ShipConstructor End-User License Agreement was displayed")
            mouseLeftClickOn("accept_license")
        mouseLeftClickOn("install_button")
        sleep(1)
        if appears("installing_1"):
            mouseMoveTo("installing_1")
            self.logger.info("Installation started")
            waitForVanish("installing_1")
            if appears("done_button"):
                mouseLeftClickOn("done_button")
                self.logger.info("Application was installed successfully")
            else:
                raise AssertionError("Something went wrong. Installation did not complete")
        else:
            raise AssertionError("Installing dialog did not appear")

    def ep_node_Install(self):
        self.runSetup()
        self.verifyEnterpricePlatformSetupWindow()
        if appears("enterprise_node_option"):
            mouseLeftClickOn("enterprise_node_option")
        else:
            raise AssertionError("Couldn't find enterprise_node_option.")
        self.ep_Installation()
        if appears("window_firewall_window", 30):
            mouseMoveTo("window_firewall_window")
            if appears("allow_access_button", 20):
                mouseLeftClickOn("allow_access_button")
            else:
                print("allow_access_button didn't appear")
        else:
            print("window_firewall_window didn't appear")

        if appears("enterprise_node_warning", 20):
            mouseMoveTo("enterprise_node_warning")
            mouseLeftClickOn("enterprise_node_warning_ok")
            self.logger.info("Enterprise Platform Node failed to start.")
            # raise AssertionError("Enterprise Platform Node failed to start.")
        else:
            self.logger.info("Enterprise Platform Node started successfully.")
        self.createFileName('SSI')

    def desktopVerification(self, icon1, icon2):
        currentWorkingDirectory = os.getcwd()
        os.chdir("C:/Users/Public/Desktop/")
        i=0
        for x in os.listdir('.'):
            if icon1 in x:
                self.logger.info(icon1 + " Installation is completed successfully and desktop icon is visible.")
                i = i +1
            if icon2 in x:
                self.logger.info(icon2 + " Installation is completed successfully and desktop icon is visible.")
                i = i + 1

        os.chdir(currentWorkingDirectory)
        if i == 0:
            self.logger.error(icon1 + " Installation is not completed successfully.")
            self.logger.error(icon2 + " Installation is not completed successfully.")

    def startWindowVerificationNode(self):
        mouseMoveToCoordinates(10, 10)
        pressAction('win')
        sleep(2)
        # if appears("ssi_logo_start_menu", 20):
        #     self.logger.info("Installation is completed successfully and desktop icon is visible.")
        # else:
        #     raise AssertionError("Installation is not completed successfully.")
        sleep(2)
        if appears("start_win_hash"):
            mouseLeftClickOn("start_win_hash")
            sleep(1)
            mouseLeftClickOn("start_win_S")
            sleep(1)
            mouseLeftClickOn("start_win_Enterprise_new")
            sleep(1)
            mouseMoveTo("start_win_Enterprise_manual")
            self.logger.info("EnterprisePlatform Manual is installed and accessible from Start.")
            mouseMoveTo("start_win_Enterprise_node")
            self.logger.info("EnterprisePlatform Node is installed and accessible from Start.")
        else:
            raise AssertionError("start_win_hash not found.")
        pressAction('win')

    def startWindowVerificationInformationHub(self):
        mouseMoveToCoordinates(10, 10)
        pressAction('win')
        sleep(2)
        if appears("start_win_hash"):
            mouseLeftClickOn("start_win_hash")
            sleep(1)
            mouseLeftClickOn("start_win_S")
            sleep(1)
            mouseLeftClickOn("start_win_Enterprise_new")
            sleep(1)
            mouseMoveTo("start_win_Enterprise_manual")
            self.logger.info("EnterprisePlatform Manual is installed and accessible from Start.")
            mouseMoveTo("start_win_Enterprise_informationhub")
            self.logger.info("EnterprisePlatform Information Hub is installed and accessible from Start.")
            mouseMoveTo("start_win_Enterprise_integrationconnector")
            self.logger.info("EnterprisePlatform Integration Connector is installed and accessible from Start.")
        else:
            raise AssertionError("start_win_hash not found.")
        pressAction('win')

    def showAllSysTrayIcon(self):
        hotKeyAction('win','i')
        if appears("settings_window"):
            hotKeyAction('win','up')
            mouseLeftClickOn("search_field_settings")
            typeWriteAction("Select which icons appear on the taskbar")
            if appears("select_icon_settings"):
                mouseLeftClickOn("select_icon_settings")
            else:
                raise AssertionError("select_icon_settings didn't found")
            if appears("showAllOff"):
                mouseLeftClickOn("showAllOff")
                hotKeyAction('alt','f4')
            else:
                self.logger.info("Already all icons are visible in the notification area.")
                hotKeyAction('alt', 'f4')
        os.system("taskkill /f /im SystemSettings.exe")

    # disable transparency of task bar
    def disableTrans(self):
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Turn_Off_Start_Menu_and_Taskbar_Transparency.bat")
        obj.launchExplorer()

    # find the node icon from system tray and double click on the 1st icon
    def sysTrayVerification(self):
        self.disableTrans()
        obj = AppUtil()
        i = 0
        while True:
            if appears("sysTray_icon",10):
                self.showAllSysTrayIcon()
                if appears("node_icon2",10):
                    mouseDoubleClickOn("node_icon2")
                else:
                    obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_NodeService.bat")
                    sleep(2)
                    mouseDoubleClickOn("node_icon2")
            else:
                if appears("node_icon2",10):
                    mouseDoubleClickOn("node_icon2")
                else:
                    obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_NodeService.bat")
                    sleep(2)
                    mouseDoubleClickOn("node_icon2")
            self.verifyNodeVersion()
            i = i + 1
            if i == 2:
                break
        os.system("TASKKILL /F /IM SSI.EnterprisePlatform.NodeService.exe")

    # after opening node application from system tray it verify's whether the opened window is only node or rest node than it verifys the version number
    def verifyNodeVersion(self):
        if appears("node_window",10):
            mouseMoveTo("node_window")
            if appears("ep_node_window",10):
                self.logger.info("Node application is open to verify it's version.")
            elif appears("ep_rest_node_window",10):
                self.logger.info("Rest Node application is open to verify it's version.")
            if appears("node_help_button",10):
                mouseLeftClickOn("node_help_button")
                if appears("node_about_button",10):
                    mouseLeftClickOn("node_about_button")
                else:
                    raise AssertionError("node_about_button didn't appear.")
            else:
                raise AssertionError("node_help_button didn't appear.")
            if appears("about_window_node",10):
                mouseMoveTo("about_window_node")
                if appears("version_node",10):
                    mouseMoveTo("version_node")
                else:
                    raise AssertionError("version_node didn't appear.")
            else:
                raise AssertionError("about_window_node didn't appear.")
        else:
            raise AssertionError("node_window didn't appear.")
        self.getVersionNumber("version_node")

        # close about window for node application
        if appears("about_cross_region",10):
            mouseLeftClickOn("about_cross","about_cross_region")
        if appears("ep_node_file",10):
            mouseLeftClickOn("ep_node_file")
            if appears("ep_node_exit",10):
                mouseLeftClickOn("ep_node_exit")
            else:
                raise AssertionError("ep_node_exit didn't appeared.")
        else:
            raise AssertionError("ep_node_file didn't appeared.")

    def verify_EP_node(self):
        self.desktopVerification("ShipConstructor","EnterprisePlatform Node")
        self.startWindowVerificationNode()
        self.sysTrayVerification()

    def verify_node_uninstall(self):
        obj = AppUtil()
        ssiFolder = obj.getLatestInstalledFolderPathSSI()
        self.verifyAfterUninstall(ssiFolder,"ProjectTemplates","ShipConstructor")
        mouseMoveToCoordinates(10, 10)
        pressAction('win')
        sleep(2)
        if appears("ssi_logo_start_menu"):
            raise AssertionError("Uninstallation is not completed successfully.")
        else:
            self.logger.info("Uninstallation is completed successfully and shortcut icon has been removed from Start window.")
        pressAction('win')
        os.system("taskkill /f /im explorer.exe")
        currentWorkingDirectory = os.getcwd()
        os.chdir("C:/Windows")
        os.system("explorer.exe")
        os.chdir(currentWorkingDirectory)

    def EP_InformationHub_Install(self):
        self.runSetup()
        self.verifyEnterpricePlatformSetupWindow()
        if appears("enterprise_informationhub_option"):
            mouseLeftClickOn("enterprise_informationhub_option")
        else:
            raise AssertionError("Couldn't find enterprise_informationhub_option.")
        self.ep_Installation()
        self.createFileName('EP')

    def verify_EP_InformationHub(self):
        self.desktopVerification("InformationHub", "IntegrationConnector")
        self.startWindowVerificationInformationHub()

    def verify_Version_InformationHub(self):
        os.system("TASKKILL /F /IM SSI.EnterprisePlatform.HubConsole.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_HubConsole.bat")

        if appears("informationhub_connectors_no_button"):
            mouseLeftClickOn("informationhub_connectors_no_button")
        else:
            raise AssertionError("informationhub_connectors_no_button didn't appear.")

        if appears("informationHub_Window", 10):
            mouseMoveToCoordinates(400,400)
            mouseMoveTo("informationHub_Window")
            self.logger.info("informationHub_Window appeared")
            if appears("informationhub_help_button", 10):
                mouseLeftClickOn("informationhub_help_button")
            else:
                raise AssertionError("help button did not appear")
            if appears("informationhub_about_button", 10):
                mouseLeftClickOn("informationhub_about_button")
            else:
                raise AssertionError("informationhub_about_button did not appear")
        else:
            raise AssertionError("informationHub_Window did not appear")
        self.getVersionNumber("version_EP")
        os.system("TASKKILL /F /IM SSI.EnterprisePlatform.HubConsole.exe")

    # it opens the about window and get the version number from that window
    def getVersionNumber(self, version):
        if appears("about_window",10):
            print("about_window appeared")
            if appears(version,10):
                mouseMoveTo(version)
            else:
                raise AssertionError(version+" did not appear")
            loca2 = location(version)
            obj = AppUtil()
            if loca2 is not None:
                takeScreenShot(version, loca2[0]+45, loca2[1], loca2[2] + 80, loca2[3])
                os.system("magick convert "+ version +".png -resize 500% "+ version +"EXT.png")
            else:
                raise AssertionError("Version did not appear in about version window.")
            text = obj.imageReader(image_name=version+"EXT.png")
            self.logger.info("Version Number: " + text)
            pressAction('esc')
        else:
            raise AssertionError("about_window did not appear")

    def verify_Version_IntegrationConnector(self):
        os.system("TASKKILL /F /IM SSI.EnterprisePlatform.IntegrationConnectorConsole.exe")
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_IntegrationConnectorConsole.bat")

        if appears("intergration_connector_window", 10):
            mouseMoveToCoordinates(400, 400)
            mouseMoveTo("intergration_connector_window")
            self.logger.info("intergration_connector_window appeared")
            if appears("informationhub_help_button", 10):
                mouseLeftClickOn("informationhub_help_button")
            else:
                raise AssertionError("help button did not appear")
            if appears("informationhub_about_button", 10):
                mouseLeftClickOn("informationhub_about_button")
            else:
                raise AssertionError("informationhub_about_button did not appear")
        else:
            raise AssertionError("intergration_connector_window did not appear")
        self.getVersionNumber("version_integration")
        os.system("TASKKILL /F /IM SSI.EnterprisePlatform.IntegrationConnectorConsole.exe")

    def verify_InformationHub_IntegrationConnector_uninstall(self):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        print(currentWorkingDirectory)
        if baseLocation == 'test.images':
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")
        f = open("ssiFolder.txt", "r")
        EPfolderPath = f.read()
        os.chdir(currentWorkingDirectory)
        self.verifyAfterUninstall(EPfolderPath,"ProjectTemplates", "InformationHub Console", "IntegrationConnector Console")

    def EP_Client_Install(self):
        self.runSetup()
        self.verifyEnterpricePlatformSetupWindow()
        if appears("enterprise_client_option"):
            mouseLeftClickOn("enterprise_client_option")
        else:
            raise AssertionError("Couldn't find enterprise_client_option.")
        self.ep_Installation()
        self.createFileName('EP')

    def startWindowVerificationClient(self):
        mouseMoveToCoordinates(10, 10)
        pressAction('win')
        sleep(2)
        if appears("start_win_hash"):
            mouseLeftClickOn("start_win_hash")
            sleep(1)
            mouseLeftClickOn("start_win_S")
            sleep(1)
            mouseLeftClickOn("start_win_Enterprise_new")
            sleep(1)
            mouseMoveTo("start_win_Enterprise_manual")
            self.logger.info("EnterprisePlatform Manual is installed and accessible from Start.")
            mouseMoveTo("start_win_Enterprise_ProjectViewer")
            self.logger.info("ProjectViewer is installed and accessible from Start.")
            mouseMoveTo("start_win_Enterprise_Publisher")
            self.logger.info("Publisher is installed and accessible from Start.")
        else:
            raise AssertionError("start_win_hash not found.")
        pressAction('win')

    def verify_EP_Client(self):
        self.desktopVerification("ProjectViewer", "Publisher")
        self.startWindowVerificationClient()

    def verify_publisher_connection(self):
        os.system("TASKKILL /F /IM Publisher.exe")
        # hotKeyAction('win', 'down')
        # hotKeyAction('win', 'down')
        # hotKeyAction('win', 'down')
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_Publisher.bat")
        if appears("ep_loading_window"):
            mouseMoveTo("ep_loading_window")
        else:
            raise AssertionError("ep_loading_window didn't appear.")
        if appears("ep_warning_window"):
            mouseMoveTo("ep_warning_window")
            self.logger.warn("Publisher not started because it was not configured connecting.")
            if appears("ep_warning_ok_button"):
                mouseLeftClickOn("ep_warning_ok_button")
            else:
                raise AssertionError("ep_warning_ok_button didn't appear.")
        else:
            raise AssertionError("ep_warning_window didn't appear.")
        os.system("TASKKILL /F /IM Publisher.exe")

    def verify_projectViewer_connection(self):
        os.system("TASKKILL /F /IM ProjectViewer.exe")
        # hotKeyAction('win', 'down')
        # hotKeyAction('win', 'down')
        # hotKeyAction('win', 'down')
        obj = AppUtil()
        obj.launchAnyApp("test.images", "../test.batches/", "Run_SSI_EnterprisePlatform_ProjectViewer.bat")
        if appears("ep_loading_window"):
            mouseMoveTo("ep_loading_window")
        else:
            raise AssertionError("ep_loading_window didn't appear.")
        if appears("ep_warning_window"):
            mouseMoveTo("ep_warning_window")
            self.logger.warn("Project Viewer not started because it was not configured connecting.")
            if appears("ep_warning_ok_button"):
                mouseLeftClickOn("ep_warning_ok_button")
            else:
                raise AssertionError("ep_warning_ok_button didn't appear.")
        else:
            raise AssertionError("ep_warning_window didn't appear.")
        os.system("TASKKILL /F /IM ProjectViewer.exe")

    def verify_client_uninstall(self):
        currentWorkingDirectory = os.getcwd()
        baseLocation = os.path.basename(os.getcwd())
        print(currentWorkingDirectory)
        if baseLocation == 'test.images':
            os.chdir("../test.batches/")
        else:
            os.chdir("../../test.batches/")
        f = open("ssiFolder.txt", "r")
        EPfolderPath = f.read()
        os.chdir(currentWorkingDirectory)
        self.verifyAfterUninstall(EPfolderPath,"Client", "ProjectViewer", "Publisher")

class TCP7_Installation_GuideError(Exception):
    pass
