from IOUtil import mouseLeftClickOn, mouseMoveTo, mouseDoubleClickOn, mouseMoveToCoordinates, hotKeyAction, pressAction
from time import sleep
from AppUtil import AppUtil
from screenObjUtil import exists, appears, waitForVanish
from pyautogui import typewrite
import os

class ProjectRegisterDeployDeleteUtil(object):

    def __init__(self):
        name = self.__class__.__name__
        pass

    def register_project(self, deploy, regiPath):
        # To complete the registration part
        mouseLeftClickOn('shipconstructor_tab_new')
        if appears("main_panel_new", 10):
            mouseLeftClickOn('main_panel_new')
            mouseLeftClickOn('navigator_button_new_18')
            if appears("register_project_dialog"):
                if appears("use_window_authentication", 10):
                    mouseLeftClickOn("use_window_authentication")
                else:
                    print("Windows authentication is already selected.")
                if appears(deploy + "_listed", 10):
                    mouseDoubleClickOn(deploy + "_listed")
                elif appears(deploy + "_high", 10):
                    mouseDoubleClickOn(deploy + "_high")
                else:
                    mouseLeftClickOn("register_project_browse")
                    if appears("register_project_browserWindow"):
                        mouseLeftClickOn("browser_adrs_dropdown")
                        typewrite("C:\\"+regiPath, .1)
                        pressAction('enter', 1)
                        if appears("window_search_field", 10):
                            self.resizeIcon()
                            mouseLeftClickOn("window_search_field")
                            typewrite(regiPath, .1)
                            if appears("deploymentFile"):
                                mouseDoubleClickOn("deploymentFile")
                            else:
                                mouseDoubleClickOn("deploymentFile2")
                        else:
                            raise AssertionError("Search Field did not appear")
                        mouseDoubleClickOn(deploy)
                if appears("agreement_banner", 10):
                    print('Agreement banner is displayed')
                    mouseLeftClickOn("banner_read", "banner_read_region")
                    mouseLeftClickOn("banner_read_ok")
                    if appears("license_new", 5):
                        mouseLeftClickOn("license_done")
                        if appears("shipcon_navi_close"):
                            mouseLeftClickOn("shipcon_navi_close")
                        else:
                            raise AssertionError("Shipconstructor didn't close the agreement banner.")
                    else:
                        raise AssertionError("Server not found during project registration")
                # else:
                #     raise AssertionError("Agreement Banner not found.")
                if appears("check_project_files", 10):
                    mouseLeftClickOn("check_project_files")
                    hotKeyAction('alt', 'f4')

                if appears("mdi_window",10):
                    mouseMoveTo("mdi_window")
                    mouseLeftClickOn("select_mdi_option")
                # else:
                #     raise AssertionError("Problem Occured on finding Project Files")

                if appears("shipcon_navi_close",10):
                    mouseLeftClickOn("shipcon_navi_close")
        #         else:
        #             raise AssertionError("Problem Occured")
        #     else:
        #         raise AssertionError("register_project_dialog did not appear")
        # else:
        #     raise AssertionError("Main panel did not appear")
        obj = AppUtil()
        obj.systemTaskKill("TASKKILL /T /F /IM notepad.exe")
        obj.hardwareaccelerationOFF()

    def delete_previous_deploy(self, module):
        sleep(2)
        obj = AppUtil()
        var1 = obj.systemTaskKill("TASKKILL /T /F /IM notepad.exe")
        if var1==0:
            print("notepad.exe was closed")
        else:
            print("NOTEPAD.EXE was not opened")

        sleep(2)
        var2 = obj.systemTaskKill("TASKKILL /T /F /IM acwebbrowser.exe")
        if var2==0:
            print("acwebbrowser.exe was closed")
        else:
            print("acwebbrowser.EXE was not opened")

        sleep(2)
        var3 = obj.systemTaskKill("TASKKILL /T /F /IM AdAppMgrSvc.exe")
        if var3==0:
            print("AdAppMgrSvc.exe was closed")
        else:
            print("AdAppMgrSvc.EXE was not opened")

        sleep(2)
        var4 = obj.systemTaskKill("TASKKILL /F /IM acad.exe")
        if var4==0:
            print("acad.exe was closed")
        else:
            print("acad.EXE was not opened")

        sleep(3)
        var = obj.systemTaskKill('rd /s /q C:\\New' + module)
        if var==0:
            print("New"+module+" deploy folder was deleted")
        else:
            print("New"+module+" deploy folder was not found")
        sleep(3)

    def project_deployer(self, module):
        obj = AppUtil()
        obj.delete_database(module=module)
        sleep(2)
        mouseMoveToCoordinates(400, 400)
        sleep(2)
        pressAction('esc', 3)
        sleep(10)
        typewrite("DYNMODE", .2)
        sleep(2)
        pressAction('enter', 1)
        if appears("enter_new_dynmode_value"):
            pressAction('backspace', 1)
            sleep(1)
            pressAction('3', 1)
            pressAction('enter', 2)
        else:
            raise AssertionError("Dynmode Value doesn't entered in correct place.")
        sleep(2)
        pressAction('esc', 3)
        typewrite("SCDEPLOYPROJECT", .2)
        pressAction('enter', 1)
        if appears("deploy_project_dialog"):
            sleep(1)
            name = "NewDeploy_" + module
            typewrite(name, .1)
            pressAction('tab', 1)
            sleep(1)
            pressAction('enter', 1)
            if appears("browser_open"):
                mouseLeftClickOn("browser_adrs_dropdown")
                mycwd = os.getcwd()
                curLoc = os.path.basename(os.getcwd())
                if curLoc == "test.images":
                    os.chdir("../../SCTrainingProjectFile/")
                    typewrite(os.getcwd(), .1)
                    pressAction('enter', 1)
                else:
                    os.chdir("../../../SCTrainingProjectFile/")
                    typewrite(os.getcwd(), .1)
                    pressAction('enter', 1)
                os.chdir(mycwd)

                if appears("window_search_field",10):
                    self.resizeIcon()
                    if appears("deploymentFile"):
                        mouseDoubleClickOn("deploymentFile")
                    else:
                        mouseDoubleClickOn("deploymentFile2")
                else:
                    raise AssertionError("Search Field did not appear")
                mouseLeftClickOn("project_server_dropdown")
                if appears("selected_sc_sql_14", 5):
                    mouseLeftClickOn("selected_sc_sql_14")
                elif appears("sc_sql_14", 5):
                    mouseLeftClickOn("sc_sql_14")
                elif appears("browse_sc_sql", 5):
                    mouseLeftClickOn("browse_sc_sql")
                    if appears("sql_browse_win", 5):
                        waitForVanish("sql_loading")
                        mouseLeftClickOn("project_server_dropdown")
                        if appears("sql_shipcon14", 5):
                            mouseLeftClickOn("sql_shipcon14")
                            mouseLeftClickOn('sql_ok')
                else:
                    pressAction('esc')
                    raise AssertionError("SQL server is not set")
                if appears("use_window_auth", 10):
                    mouseLeftClickOn("use_window_auth")
                else:
                    print("Windows authentication is already selected.")
                mouseLeftClickOn("deploy_project_dialog_ok")
                if appears("folder_exists_warning",10):
                    pressAction('enter')
                    print("Already a Project has been deployed of same name but handled the existing deploy.")
                    # raise AssertionError("Project could not be deployed due to existing deploy.")
                while 1:
                    if appears("project_deployed_message"):
                        break
                    # else:
                    #     raise AssertionError("project_deployed_message did not appear")
                if appears("project_deployed_message_ok_button"):
                    mouseLeftClickOn("project_deployed_message_ok_button")
                # else:
                #     raise AssertionError("project_deployed_message_ok_button did not appear")
            else:
                raise AssertionError("File browser did not appear")
        else:
            raise AssertionError("deploy_project_dialog did not appear")

    def resizeIcon(self):
        if appears("open_window_icon_dropdown_region"):
            mouseLeftClickOn("open_window_icon_dropdown","open_window_icon_dropdown_region")
            if appears("contentSizeIcon"):
                mouseLeftClickOn("contentSizeIcon")
        mouseMoveTo("window_search_field")