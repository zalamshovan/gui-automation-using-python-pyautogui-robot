"""
*** TCP2_HVAC_Catalog.py ***

Automation script for HVAC Catalog and Modeling (Page 1 - 244)

Covers the following sections:
- HVAC Stock Catalog Prerequisite
- The Accessory Packages Library
- User Defined Attributes
- Sheet Stocks Library
- HVAC Stock Catalog, End Treatments
- HVAC Stock Catalog, Create/Edit HVAC Elements
- HVAC Stock Catalog, Connections
- HVAC Model Drawing
- System Manager
- Modelling HVAC system
- Creating Bent HVAC Ducts
- Creating Flange, Tee, Laterals, Cross, Transition and Y HVAC Elements
- Creating Saddles
- Applying Finishes and Insulations to HVAC elements
- Using HVAC – UCS Intersection command
- Using HVAC – UCS Intersection command
- Stock Constraints
- Modifying HVAC routing
- Spooling a System
- HVAC Utilities

"""

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

import os, pyautogui

from time import sleep

from logger_extended import logger_extended

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseRightClickOn, mouseDoubleClickOn, mouseClickAppears

from screenObjUtil import exists, appears, waitForVanish

from SSIUtil import osnap, maximize, free, open_manager, line, hvac_sysman, straight, change_viewpoint_from_aft_stbd_up, change_viewpoint_plan_looking_down, reset_viewport, otf, activate_wcs, free_white


class TCP2_HVAC_Catalog(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP5_HVAC')


    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC02_HVAC_catalog")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_hvac")
        obj.launch_app()
        obj1.project_deployer(module="hvac")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    def register_project(self):
        maximize()
        obj= AppUtil()
        obj.ribbon_load("mechanical_tab")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("hvac_deploy", "NewDeploy_hvac")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_hvac")

    # HVAC Stock Catalog – Prerequisite
    # Materials Library
    def materials_library(self):
        self.logger.info('Start:\t Checking Module- materials_library')
        if appears("shipconstructor_tab_new", 30):
            mouseLeftClickOn('shipconstructor_tab_new')
        else:
            self.logger.info("shipconstructor_tab_new is selected")
        mouseLeftClickOn("manage_pane")
        mouseLeftClickOn('manage_button')
        sleep(3)
        pyautogui.hotkey('win','up')
        if appears("general_button",10):
            mouseLeftClickOn("general_button")
            mouseLeftClickOn("material_button")
            if appears("material_window"):
                mouseLeftClickOn("density_units")
                if appears("density_units_menu"):
                    self.logger.info("Materials Library checked")
                    pyautogui.click()
                    mouseLeftClickOn("material_button_okay")
                    self.logger.info("Materials library was checked")
                else:
                    raise AssertionError("Materials Library could not be checked")
            else:
                raise AssertionError("material_window did not appear")
        else:
            raise AssertionError("General Button did not appear")
        self.logger.info('End:\t Checking Module- materials_library')

    # Manufacturers Library

    def manufacturers_library(self):
        self.logger.info('Start:\t Checking Module- manufacturers_library')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("manufact_button")
        if appears("manufact_window"):
            mouseLeftClickOn("manufact_button_new")
            sleep(1)
            pyautogui.typewrite('Test-Manufacturer', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("manufact_button_ok")
            self.logger.info("Manufacturers Library checked")
        else:
            raise AssertionError("manufacturers window did not appear")
        self.logger.info('End:\t Checking Module- manufacturers_library')

    # Accessory Packages

    def creating_new_accessory_type(self):
        self.logger.info('Start:\t Checking Module- creating_new_accessory_type')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("packages_button")
        if appears("accessory_pkj_window",10):
            sleep(1)
            mouseLeftClickOn("maximize_win")
            self.logger.info('accessory_pkj_window appeared')
        elif appears("accessory_pkj_window2",10):
            self.logger.info('accessory_pkj_window appeared')
        else:
            raise AssertionError("accessory package window did not appear")
        if appears("studs_existing", 30):
            mouseLeftClickOn("studs_existing")
            mouseLeftClickOn("accessory_button_types")
            sleep(1)
            mouseLeftClickOn("hvac_connection")
            if appears("hvac_stud_connection"):
                mouseLeftClickOn("hvac_stud_connection")
                mouseLeftClickOn("hvac_stud_connection_delete","hvac_stud_connection_delete_region")
                mouseLeftClickOn("studs_expand_region_click","studs_expand_region")
                mouseLeftClickOn("studs_m12")
                mouseLeftClickOn("delete_accessory")
                sleep(1)
                mouseLeftClickOn("delete_accessory")

        mouseLeftClickOn("accessory_pkj_newtype_button")
        pyautogui.typewrite('Studs',.1)
        pyautogui.press('enter')
        if appears("accessory_pkj_newstud",10):
            mouseLeftClickOn("accessory_pkj_newstud")
            pyautogui.typewrite('Studs M12x50',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('DIN 835-5.8',.1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("accessory_button_types")

            if appears("hvac_connection_high",3):
                mouseLeftClickOn("hvac_connection_high")
            elif appears("hvac_connection",3):
                mouseLeftClickOn("hvac_connection")

            mouseLeftClickOn("New_package_button")
            pyautogui.typewrite('HVAC Stud Connection',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("studs")
            mouseLeftClickOn("add_button")
            mouseLeftClickOn("nuts_button_expand","nuts_button")
            mouseLeftClickOn("hex_nut")
            mouseLeftClickOn("add_button")
            mouseDoubleClickOn("qty1","qty1_region")
            pyautogui.press('right')
            pyautogui.press('backspace')
            pyautogui.press('6')
            pyautogui.press('down')
            pyautogui.press('enter')
            pyautogui.typewrite('12',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("stud_connection")
            mouseLeftClickOn("assign_spec")
            sleep(2)
            pyautogui.hotkey('win', 'up')
            if appears("assign_to_spec_window"):
                mouseLeftClickOn("assigned")
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                mouseLeftClickOn("assign_ok")
                sleep(1)
                mouseLeftClickOn("hvac_stud_connection")
                mouseLeftClickOn("copy_package")
                sleep(1)
                pyautogui.typewrite('HVAC-1 Stud Connection',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("accessory_pkj_ok")
                self.logger.info("New accessory type was created")
            else:
                raise AssertionError("assign_to_spec_window did not appear")
        else:
            raise AssertionError("New Studs button did not appear")
        self.logger.info('End:\t Checking Module- creating_new_accessory_type')

    def creating_uda(self):
        self.logger.info('Start:\t Checking Module- creating_uda')
        sleep(1)
        mouseLeftClickOn("manager_hvac_button")
        mouseLeftClickOn("uda_hvac_manager")
        sleep(1)
        if appears("maximize_win_region",5):
            mouseLeftClickOn("maximize_win")
        mouseLeftClickOn("new_uda")
        pyautogui.typewrite('Test_Attr')
        pyautogui.press('enter')
        mouseLeftClickOn("parts_expand","parts_region")
        mouseLeftClickOn("Flange")
        sleep(1)
        mouseLeftClickOn("add_button2")
        sleep(1)
        mouseLeftClickOn("not_required")
        sleep(1)
        mouseLeftClickOn("drop_down3")
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('enter')
        pyautogui.press('up')
        pyautogui.press('right')
        pyautogui.typewrite('Value', .1)
        pyautogui.press('enter')
        mouseLeftClickOn("ok4")
        if appears("uda_warning",10):
            pyautogui.press('enter')
            self.logger.info("UDA was created")
        else:
            self.logger.info('uda_warning did not appear')
        self.logger.info('End:\t Checking Module- creating_uda')

    # Naming Conventions - Why You Need Naming Conventions

    def settings_of_autonumber(self):
        self.logger.info('Start:\t Checking Module- settings_of_autonumber')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("naming_convention_button2")
        if appears("naming_window"):
            mouseLeftClickOn("hvac_parts","hvac_parts_region")
            mouseLeftClickOn("hvac_parts_expand")
            mouseLeftClickOn("new_name")
            if appears("new_name2_light",5):
                mouseDoubleClickOn("new_name2_light")
            else:
                mouseDoubleClickOn("new_name2")
            pyautogui.typewrite('Test2',.1)
            pyautogui.press('enter')
            pyautogui.press('up')
            mouseDoubleClickOn("_static")
            pyautogui.click(button='left',clicks=2,interval=.01)
            pyautogui.press('backspace',presses=10)
            pyautogui.typewrite('SSI-H-',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("new_static_drop_down","new_static_region")
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("catalog_region")
            mouseLeftClickOn("drop_down4")
            pyautogui.press('down',presses=2,interval=.2)
            pyautogui.press('enter')
            pyautogui.press('down',presses=10)
            mouseLeftClickOn("drop_down4")
            pyautogui.press('down', presses=2, interval=.2)
            pyautogui.press('enter')
            mouseLeftClickOn("new_static_drop_down","new_db_item")
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseDoubleClickOn("_static")
            pyautogui.click(button='left',clicks=2,interval=.01)
            pyautogui.press('backspace',presses=10)
            pyautogui.typewrite('-',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("auto_number")
            mouseLeftClickOn("auto")
            pyautogui.press('3')
            mouseLeftClickOn("ssi_h")
            mouseMoveTo("up1")
            pyautogui.click(clicks=3)
            mouseLeftClickOn("_'-'")
            mouseMoveTo('down1')
            pyautogui.click(clicks=2, interval=.2)
            if appears("hvac_naming_convention_check"):
                self.logger.info("Setting of auto number is checked")
                mouseLeftClickOn("ok5")
            else:
                raise AssertionError("Sample name was incorrect")
        else:
            raise AssertionError("naming_window did not appear")
        self.logger.info('End:\t Checking Module- settings_of_autonumber')

    # Sheet Stocks Library
    def sheet_stocks(self):
        self.logger.info('Start:\t Checking Module- sheet_stocks')
        mouseLeftClickOn("manager_hvac_button")
        mouseLeftClickOn("sheet_stocks")
        if appears("sheet_window"):
            mouseLeftClickOn("new_sheet")
            pyautogui.typewrite('Stgalv1,5mm',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.press('right')
            pyautogui.typewrite('1.5',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("Sheet stock library was checked")
        else:
            raise AssertionError("sheet_window did not appear")
        self.logger.info('End:\t Checking Module- sheet_stocks')

    # HVAC Stock Catalog, End Treatments
    def creating_new_icon(self):
        self.logger.info('Start:\t Checking Module- creating_new_icon')
        mouseLeftClickOn("manager_hvac_button")
        mouseLeftClickOn("stock_catalog")
        if appears("maximize_win"):
            mouseLeftClickOn("maximize_win")
        if appears("stock_catalog_window"):
            mouseLeftClickOn("end_treat_tab")
            if appears("warning10", 10):
                pyautogui.press('enter')
            if appears("edit_end_button"):
                mouseLeftClickOn("edit_end_button")
                mouseLeftClickOn("linemode")
                mouseLeftClickOn("new","new_linemode")
                pyautogui.typewrite('Test-FL',.1)
                pyautogui.press('enter')
                sleep(.5)
                pyautogui.press('enter')
                pyautogui.press('right')
                pyautogui.press('r')
                pyautogui.press('enter')
                mouseLeftClickOn("scale_to_perspective_click","scale_to_perspective")
                mouseLeftClickOn("ok6","ok_region")
                self.logger.info("New icon was created")
            else:
                raise AssertionError("edit_end_button did not appear")
        else:
            raise AssertionError("stock_catalog_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_icon')

    def creating_new_treatment(self):
        self.logger.info('Start:\t Checking Module- creating_new_treatment')
        mouseLeftClickOn("new3")
        pyautogui.typewrite('Test-Flange',.1)
        pyautogui.press('enter')
        pyautogui.press('right')
        pyautogui.press('f')
        pyautogui.press('right')
        pyautogui.press('b')
        mouseLeftClickOn("ok6")
        self.logger.info("New treatment was created")
        self.logger.info('End:\t Checking Module- creating_new_treatment')

    # Meaningful Names for Treatment Types
    def meaningful_name(self):
        self.logger.info('Start:\t Checking Module- meaningful_name')
        mouseLeftClickOn("fl_30")
        mouseLeftClickOn("new_end_treat")
        sleep(1)
        pyautogui.press('right',presses=2)
        pyautogui.press('t',presses=2)

        if appears("test_flange_3mm_high",10):
            mouseLeftClickOn("test_flange_3mm_high")
            if appears("warning11", 5):
                pyautogui.press('enter')
        elif appears("test_flange_3mm_high2", 5):
            mouseLeftClickOn("test_flange_3mm_high2")
            if appears("warning11", 5):
                pyautogui.press('enter')

        mouseLeftClickOn("test_flange")
        pyautogui.press('right')
        pyautogui.press('4')
        pyautogui.press('enter')
        pyautogui.press('right')
        pyautogui.press('4')
        pyautogui.press('0')
        pyautogui.press('enter')
        mouseLeftClickOn("sheet_stocks_done")
        if appears("warning11",10):
            pyautogui.press('enter')
        self.logger.info("Meaningful names checked")
        self.logger.info('End:\t Checking Module- meaningful_name')

    # HVAC Stock Catalog, Create/Edit HVAC Elements

    def creating_new_specs(self):
        self.logger.info('Start:\t Checking Module- creating_new_specs')
        mouseLeftClickOn("manager_hvac_button")
        mouseLeftClickOn("stock_catalog")
        if appears("stock_catalog_window"):
            if appears("maximize_win", 20):
                mouseLeftClickOn("maximize_win")
            if appears("hvac_stock_tab", 20):
                mouseLeftClickOn("hvac_stock_tab")
            mouseLeftClickOn("edit_specs")
            if appears("edit_specs_window"):
                mouseLeftClickOn("new_specs_button")
                pyautogui.typewrite('Test-HVAC-Spec',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("ok6")
                self.logger.info('New spec is created')
            else:
                raise AssertionError("edit_specs_window did not appear")
        else:
            raise AssertionError("stock_catalog_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_specs')

    def assign_stocks_to_spec(self):
        self.logger.info('Start:\t Checking Module- assign_stocks_to_spec')
        mouseLeftClickOn("expand_duct","expand_duct_region")
        mouseLeftClickOn("duct310x190")
        mouseLeftClickOn("assign_to_spec")
        if appears("assign_to_spec_window2"):
            mouseLeftClickOn("test_hvac_option")
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("Stocks were assigned to spec")
        else:
            raise AssertionError("assign_to_spec_window did not appear")
        self.logger.info('End:\t Checking Module- assign_stocks_to_spec')

    def creating_new_catalog(self):
        self.logger.info('Start:\t Checking Module- creating_new_catalog')
        mouseLeftClickOn("edit_catalog")
        if appears("warning11",4):
            pyautogui.press('enter')
        if appears("edit_catalog_window"):
            mouseLeftClickOn("new_catalog")
            pyautogui.typewrite('Test-HVAC-Cat',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("New catalog was created")
        else:
            raise AssertionError("edit_catalog_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_catalog')

    def assign_stocks_to_catalog(self):
        self.logger.info('Start:\t Checking Module- assign_stocks_to_catalog')
        if appears("duct_region_collapsed_click",4):
            mouseLeftClickOn("duct_region_collapsed_click","duct_region_collapsed")
        if appears("expand_duct_region",4):
            mouseLeftClickOn("expand_duct","expand_duct_region")
        mouseLeftClickOn("duct310x190")
        mouseLeftClickOn("catalog_assign")
        if appears("catalog_assign_window"):
            if appears("test_hvac_cat_selected",5):
                pyautogui.press('space')
            else:
                mouseLeftClickOn("test_hvac_cata")
                pyautogui.press('space')
            # pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("Stocks were assigned to catalog")
        else:
            raise AssertionError("catalog_assign_window did not appear")
        self.logger.info('End:\t Checking Module- assign_stocks_to_catalog')

    # Filtering Lists

    def filtering_by_cloumn(self):
        self.logger.info('Start:\t Checking Module- filtering_by_cloumn')
        mouseLeftClickOn("filter_text")
        pyautogui.typewrite('200',.1)
        pyautogui.press('enter')
        sleep(2)
        mouseLeftClickOn('delete_filter')
        self.logger.info("Filtering by column was done")
        self.logger.info('End:\t Checking Module- filtering_by_cloumn')

    # Creating HVAC elements

    def creating_new_duct(self):
        self.logger.info('Start:\t Checking Module- creating_new_duct')
        mouseLeftClickOn("new_duct")
        if appears("new_duct_window"):
            mouseLeftClickOn("stock_name")
            pyautogui.press('right')
            pyautogui.typewrite('Test-Duct',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            pyautogui.press('s', 6, 0.5)
            pyautogui.press('enter')
            mouseLeftClickOn("profile_shape")
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('r')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('500',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('300', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 2, 0.5)
            pyautogui.typewrite('2000', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 2, 0.5)
            pyautogui.press('t')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')

            pyautogui.press('down')
            pyautogui.press('t')
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info('New Duct is created')
        else:
            raise AssertionError("new_duct_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_duct')

    def creating_elbow(self):
        self.logger.info('Start:\t Checking Module- creating_elbow')
        mouseLeftClickOn("new_stock")
        mouseLeftClickOn("new_elbow")
        mouseLeftClickOn("drop_cheek_elbow")
        if appears("new_duct_window"):
            mouseLeftClickOn("stock_name")
            pyautogui.press('right')
            sleep(1)
            pyautogui.typewrite('Test-Elbow', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            pyautogui.press('s', 6, 0.5)
            pyautogui.press('enter')
            pyautogui.press('down', 7, 0.5)
            pyautogui.typewrite('200',.1)
            pyautogui.press('enter')
            pyautogui.press('down', 3, 0.2)
            pyautogui.typewrite('300', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('100', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('t')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down', 3, 0.2)
            pyautogui.typewrite('300', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('t')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('125', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info('New Elbow is created')
        else:
            raise AssertionError("New Elbow window did not appear")
        self.logger.info('End:\t Checking Module- creating_elbow')

    def creating_flanges(self):
        self.logger.info('Start:\t Checking Module- creating_flanges')
        mouseLeftClickOn("new_stock")
        mouseLeftClickOn("new_flange")
        if appears("new_duct_window"):
            mouseLeftClickOn("stock_name3")
            pyautogui.press('right')
            sleep(1)
            pyautogui.typewrite('Test-Flange', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('s', 6, 0.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 7, 0.1)
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 3, 0.1)
            pyautogui.press('r')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('0')
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('500',.1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('300', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('0')
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('t')
            pyautogui.press('enter')
            pyautogui.press('down', 3, 0.1)
            pyautogui.typewrite('25', .1)
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('p', 3, 0.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info('New Flange is created')
        else:
            raise AssertionError("New stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_flanges')

    def creating_branch(self):
        self.logger.info('Start:\t Checking Module- creating_branch')
        mouseLeftClickOn("new_stock")
        mouseLeftClickOn("new_branch")
        mouseLeftClickOn("new_branch_tees")
        mouseLeftClickOn("new_branch_tees_double")
        if appears("new_duct_window"):
            mouseLeftClickOn("conical_check","conical_region")
            if appears("stock_name5",2):
                mouseLeftClickOn("stock_name5")
            else:
                mouseLeftClickOn("stock_name2")
            pyautogui.press('right')
            pyautogui.typewrite('Test-DRT', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            pyautogui.press('s', presses=6)
            pyautogui.press('enter')

            pyautogui.press('down',presses=10)
            pyautogui.typewrite('500',.1)
            pyautogui.press('enter')
            pyautogui.press('down', presses=3)
            pyautogui.press('c')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('300', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('t')
            pyautogui.press('enter')

            pyautogui.press('down', presses=3)
            pyautogui.press('c')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('200', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('t')
            pyautogui.press('enter')

            pyautogui.press('down', presses=3)
            pyautogui.press('c')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('150', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('200', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('b')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('150', .1)
            pyautogui.press('enter')

            pyautogui.press('down', presses=3)
            pyautogui.press('c')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('150', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('b')
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('370', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info('New Branch is created')
        else:
            raise AssertionError("New stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_branch')

    def excercise_hcm_r003(self):
        self.logger.info('Start:\t Checking Module- excercise_hcm_r003')
        mouseLeftClickOn("new_stock")
        mouseLeftClickOn("new_duct2")
        if appears("new_duct_window"):
            mouseLeftClickOn("stock_name")
            pyautogui.press('right')
            pyautogui.typewrite('Ex_DUCT', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            pyautogui.press('s', 6, 0.2)
            pyautogui.press('enter')
            mouseLeftClickOn("profile_shape")
            pyautogui.press('right')
            pyautogui.press('r')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('400', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('200', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('50', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('2000', .1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('100', .1)
            pyautogui.press('enter')
            pyautogui.press('down', 2)
            pyautogui.press('e')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 2)
            pyautogui.press('e')
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("Exercise R003 done")
        else:
            raise AssertionError("New stock window did not appear")
        self.logger.info('End:\t Checking Module- excercise_hcm_r003')

    def excercise_hcm_a002(self):
        self.logger.info('Start:\t Checking Module- excercise_hcm_a002')
        mouseLeftClickOn("new_stock")
        mouseLeftClickOn("new_offset")
        mouseLeftClickOn("new_offset_curved")
        if appears("new_duct_window"):
            mouseLeftClickOn("stock_name")
            pyautogui.press('right')
            pyautogui.typewrite('Ex_TRANS', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("sheet_stock")
            pyautogui.press('right')
            pyautogui.press('s', 6, 0.2)
            pyautogui.press('enter')
            pyautogui.press('down', 11, 0.2)
            pyautogui.press('r')
            pyautogui.press('enter')
            pyautogui.press('down', 2, 0.2)
            pyautogui.typewrite('500')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('300')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('600')
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('200')
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.typewrite('200')
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            self.logger.info("Exercise A002 done")
        else:
            raise AssertionError("New stock window did not appear")
        self.logger.info('End:\t Checking Module- excercise_hcm_a002')

    # HVAC Stock Catalog, Connections

    def connection_parameters(self):
        self.logger.info('Start:\t Checking Module- connection_parameters')
        mouseLeftClickOn("connections_tab")
        if appears("warning11", 4):
            pyautogui.press('enter')
        if appears("fl40_fl40_delete_region",5):
            mouseLeftClickOn("fl40_fl40_delete","fl40_fl40_delete_region")
            mouseLeftClickOn("fl40_fl40_delete2")
            mouseLeftClickOn("connection_delete")
            sleep(1)
            pyautogui.press('enter')
            pyautogui.click()
            sleep(1)
            pyautogui.press('enter')
        mouseLeftClickOn("new_type")
        pyautogui.typewrite('FL40-FL40',.1)
        pyautogui.press('right')
        mouseDoubleClickOn("new_connection")
        pyautogui.press('t')
        pyautogui.press('enter')
        pyautogui.press('up')
        pyautogui.press('right',presses=2)
        pyautogui.press('2')
        pyautogui.press('right')
        pyautogui.press('1')
        pyautogui.press('enter')
        self.logger.info("Connection parameters checked")
        self.logger.info('End:\t Checking Module- connection_parameters')

    def assigning_accessory_packages_to_connections(self):
        self.logger.info('Start:\t Checking Module- assigning_accessory_packages_to_connections')
        if appears("fl40_fl40",40):
            mouseLeftClickOn("fl40_fl40")
        mouseLeftClickOn("add_button3")
        if appears("select_accessory",40):
            mouseLeftClickOn("610_air_checkbox_region")
            sleep(2)
            mouseLeftClickOn("610_air_checkbox")
            mouseLeftClickOn("select_accessory_ok")
        else:
            raise AssertionError("select_accessory window didn't found.")
        mouseLeftClickOn("sheet_stocks_done")
        if appears("warning10",4):
            pyautogui.press('enter')
        # pyautogui.hotkey('alt','f4')
        self.logger.info("Accessory packages were assigned")
        self.logger.info('End:\t Checking Module- assigning_accessory_packages_to_connections')

    # HVAC Model Drawing

    def creating_hvac_model_drawing(self):
        self.logger.info('Start:\t Checking Module- creating_hvac_model_drawing')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("hvac_node")
            mouseLeftClickOn("hvac_node2")
            mouseLeftClickOn("new_hvac")
            pyautogui.typewrite('Test HVAC',.2)
            if appears("open_new_drawing2",5):
                mouseLeftClickOn("open_new_drawing2")
            if appears("1vp_metric",5):
                mouseLeftClickOn("1vp_metric")
            pyautogui.press('enter')
            sleep(5)
            if appears("test_hvac_tab"):
                # HVAC Model Drawing Options
                mouseLeftClickOn("structure_main_pane")
                mouseMoveTo("drawing_option_selected")
                mouseLeftClickOn("hovered_drop_down")
                mouseLeftClickOn("hvac_drawing")
                if appears("hvac_drawing_window"):
                    sleep(1)
                    pyautogui.hotkey('win', 'up')
                    sleep(1)
                    mouseLeftClickOn("navaid_tab")
                    sleep(1)
                    mouseLeftClickOn("transform_tab")
                    sleep(1)
                    mouseLeftClickOn("routing_tab")
                    mouseLeftClickOn("show_flow_check_click","show_flow_check")
                    pyautogui.press('tab',presses=3)
                    pyautogui.press('space')
                    sleep(1)
                    mouseLeftClickOn("behavior_tab")
                    sleep(3)
                    mouseLeftClickOn("ok4")
                    # Set up Drawing Options
                    if appears("hvac_ribbon", 3):
                        mouseDoubleClickOn("hvac_ribbon")
                    mouseLeftClickOn("structure_main_pane")
                    if appears("display_options_region", 5):
                        mouseLeftClickOn("display_options_region")
                    else:
                        mouseMoveTo("hvac_drawing_option")
                        mouseLeftClickOn("hovered_drop_down")
                        mouseLeftClickOn("drawing_option_on_list")
                    if appears("display_window"):
                        mouseLeftClickOn("expand_hvac", "expand_hvac_region")
                        sleep(2)
                        pyautogui.hotkey('alt', 'f4')
                        self.logger.info("Hvac model drawing is created")
                    else:
                        raise AssertionError("display_window did not appear")
                else:
                    raise AssertionError("hvac_drawing_window did not appear")
            else:
                raise AssertionError("test_hvac_tab did not appear")
        else:
            raise AssertionError("shipcon_navigator did not open")
        self.logger.info('End:\t Checking Module- creating_hvac_model_drawing')

    # ShipConstructor/AutoCAD Object Snap Options

    def object_snap(self):
        self.logger.info('Start:\t Checking Module- object_snap')
        free()
        pyautogui.typewrite('+DSETTING',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('999',.1)
        pyautogui.press('enter')
        if appears("drafting_window"):
            sleep(1)
            mouseMoveTo("object_snap_left")
            pyautogui.click(clicks=5)
            mouseLeftClickOn("object_snap_tab")
            sleep(2)
            pyautogui.press('enter')
            self.logger.info("Object snap was checked")
        else:
            raise AssertionError("drafting_window did not appear")
        self.logger.info('End:\t Checking Module- object_snap')

    # System Manager

    def creating_systems(self):
        self.logger.info('Start:\t Checking Module- creating_systems')
        hvac_sysman()
        pyautogui.hotkey('win','up')
        mouseLeftClickOn("610_expand_click")
        mouseLeftClickOn("new_system")
        pyautogui.typewrite('HVAC-system-test',.1)
        pyautogui.press('enter')
        sleep(1)
        #mouseLeftClickOn('hvac_system_test')
        pyautogui.press('up')
        free_white()
        mouseLeftClickOn("new_branch_sysman")
        pyautogui.typewrite('HVAC-branch-test', .1)
        pyautogui.press('enter')
        sleep(1)
        self.logger.info("System manager created")
        #mouseLeftClickOn("ok6")
        self.logger.info('End:\t Checking Module- creating_systems')

    def exercise_hcm_r005(self):
        self.logger.info('Start:\t Checking Module- exercise_hcm_r005')
        if appears("610_air"):
            mouseLeftClickOn("610_air")
            mouseLeftClickOn("new_system")
            pyautogui.typewrite('Ex_HVAC-system', .1)
            pyautogui.press('enter')
            pyautogui.press('up')
            mouseDoubleClickOn("red_color")
            mouseDoubleClickOn("blue_select")
            mouseLeftClickOn("new_branch_sysman")
            pyautogui.typewrite('Ex_HVAC-branch', .1)
            pyautogui.press('enter')
            pyautogui.press('up')
            mouseLeftClickOn("airflow")
            pyautogui.press('right')
            pyautogui.typewrite('10',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            free()
            self.logger.info("Exercise R005 done")
        else:
            raise AssertionError("610_air region did not found")
        self.logger.info('End:\t Checking Module- exercise_hcm_r005')

    # Modelling HVAC system


    def modeling_hvac(self):
        self.logger.info('Start:\t Checking Module- modeling_hvac')
        hvac_sysman()
        mouseLeftClickOn("610_expand_click","hvac_system_test_region")
        mouseLeftClickOn("hvac_system_branch2")
        mouseLeftClickOn("ok6")
        free()
        if appears("hvac_ribbon", 5):
            mouseDoubleClickOn("hvac_ribbon")
        if appears("hvac_pane",5):
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
        if appears("new_duct_win",10):
            mouseLeftClickOn("new_duct_win_cancel")
            sleep(1)
            free()
            free()
            otf()
            free()
            pyautogui.click()
            pyautogui.press('esc',presses=2)
            sleep(1)
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
        if appears("select_stock",10):
            pyautogui.hotkey('win','up')
            mouseLeftClickOn("delete_filter")
            mouseDoubleClickOn("filter_text2")
            pyautogui.typewrite('250',.1)
            pyautogui.press('enter')
            mouseDoubleClickOn("duct_250x100")
            sleep(4)
            sleep(4)
            free()
            pyautogui.typewrite('0,0',.1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('1000,0',.1)
            sleep(1)
            pyautogui.press('enter')
            change_viewpoint_from_aft_stbd_up()
            sleep(1)
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
            free()
            pyautogui.click(button='right')
            if appears("cOnnects"):
                mouseLeftClickOn("cOnnects")
                pyautogui.typewrite('1049.3814,46.1008', .1)
                pyautogui.press('enter')
                if appears("select_stock"):
                    mouseLeftClickOn("ok6")
                sleep(1)
                pyautogui.typewrite('1000', .1)
                sleep(.5)
                pyautogui.press(',')
                sleep(.5)
                pyautogui.press('0')
                sleep(1)
                pyautogui.press('enter')
                pyautogui.typewrite('500', .1)
                pyautogui.press('enter')
                change_viewpoint_from_aft_stbd_up()
                sleep(1)
                pyautogui.hotkey('ctrl', 's')
                sleep(3)
                self.logger.info("Modeling HVAC done")
            else:
                raise AssertionError("cOnnects option did not appear")
        else:
            raise AssertionError("select_stock did not appear")
        self.logger.info('End:\t Checking Module- modeling_hvac')

    def exercise_hcm_r006(self):
        self.logger.info('Start:\t Checking Module- exercise_hcm_r006')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("new_hvac")
            pyautogui.typewrite('EX-R', .2)
            pyautogui.press('enter')
            sleep(3)
            if appears("ex_r_tab"):
                change_viewpoint_plan_looking_down()
                hvac_sysman()
                mouseLeftClickOn("610_expand_click")
                mouseLeftClickOn("ex_system_expand","ex_system")
                mouseLeftClickOn("ex_hvac_branch")
                mouseLeftClickOn("ok6")

                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("straight")
                free()
                sleep(3)
                pyautogui.typewrite('0,0',.1)
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("forward1")
                pyautogui.typewrite('500',.1)
                pyautogui.press('enter')
                change_viewpoint_plan_looking_down()
                pyautogui.scroll(-10)
                pyautogui.scroll(-10)
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("straight")
                free()
                pyautogui.click(button='right')
                if appears("cOnnects"):
                    mouseLeftClickOn("cOnnects")
                    pyautogui.typewrite('500,0', .1)
                    pyautogui.press('enter')
                    if appears("select_stock"):
                        mouseLeftClickOn("ok6")
                    sleep(2)
                    free()
                    pyautogui.typewrite('500,0', .2)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.typewrite('500', .2)
                    pyautogui.press('enter')
                    change_viewpoint_plan_looking_down()
                    pyautogui.typewrite('select',.1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('517.7973,-16.0411')
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('delete',.1)
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.hotkey('ctrl','z')
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('540,154')
                    pyautogui.press('enter')
                    pyautogui.typewrite('448,87')
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    mouseLeftClickOn("show_flow")
                    pyautogui.hotkey('ctrl','s')
                    self.logger.info("Exercise R006 done")
                else:
                    raise AssertionError("cOnnect option did not appear")
            else:
                raise AssertionError("ex_r_tab did not appear")
        self.logger.info('End:\t Checking Module- exercise_hcm_r006')

    # Creating Bent HVAC Ducts

    def creating_bent(self):
        self.logger.info('Start:\t Checking Module- creating_bent')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseDoubleClickOn("test_hvac_node")
            sleep(3)
            if appears("test_hvac_tab"):
                sleep(5)
                line('2442', '381', '2942', '-315')
                change_viewpoint_from_aft_stbd_up()
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("hvac_bent")
                free()
            else:
                raise AssertionError("Test HVAC did not appear")
        if appears("radius"):
            pyautogui.typewrite('500', .1)
            pyautogui.press('enter')
        free()
        sleep(1)
        pyautogui.typewrite('1500,0', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('500', .1)
        pyautogui.press('enter')
        pyautogui.click(button='right')
        mouseLeftClickOn("starboard1")
        sleep(1)
        pyautogui.typewrite('1000', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        self.logger.info("Bent is created")

        self.logger.info('End:\t Checking Module- creating_bent')

    def polyline_duct(self):
        self.logger.info('Start:\t Checking Module- polyline_duct')
        sleep(2)
        free()
        osnap()

        pyautogui.typewrite('polyline',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('0,1000',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('#1000,1000',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('#1000,2000',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('#2000,2000',.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        change_viewpoint_from_aft_stbd_up()
        sleep(1)
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_bent")
        free()
        sleep(2)
        pyautogui.moveTo(433,566)
        pyautogui.click()
        pyautogui.press('y')
        pyautogui.press('enter')
        pyautogui.typewrite('300',.1)
        pyautogui.press('enter')
        pyautogui.press('n')
        pyautogui.press('enter')
        pyautogui.press('n')
        pyautogui.press('enter')
        self.logger.info("Polyline duct was created")
        self.logger.info('End:\t Checking Module- polyline_duct')

    def offset_routing(self):
        self.logger.info('Start:\t Checking Module- offset_routing')
        # sleep(2)
        # free()
        reset_viewport()
        # change_viewpoint_from_aft_stbd_up()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_bent")
        free()
        pyautogui.click(button='right')
        mouseLeftClickOn("xOffset")
        pyautogui.typewrite('400',.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('0,1000',.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.moveTo(457,571)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(579,495)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(579,436)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(509,396)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(506,332)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(633,258)
        pyautogui.click()
        sleep(5)


        pyautogui.press('enter')
        self.logger.info("Offset routing was done")
        self.logger.info('End:\t Checking Module- offset_routing')

    # On-The-Fly (OTF) Mode

    def otf_mode(self):
        self.logger.info('Start:\t Checking Module- otf_mode')
        otf()
        mouseLeftClickOn("hvac_util")
        mouseLeftClickOn("otf_default")
        #free()
        mouseLeftClickOn("pick_from_part")
        sleep(1)
        free()
        pyautogui.typewrite('2061.1175,2514.7160',.1)
        pyautogui.press('enter')
        mouseLeftClickOn("hvac_util")
        mouseLeftClickOn("otf_default")
        sleep(2)
        pyautogui.press('enter')
        free()
        pyautogui.hotkey('ctrl','s')
        sleep(3)
        self.logger.info("OTF mode checked")
        self.logger.info('End:\t Checking Module- otf_mode')

    def exercise_hcm_r007(self):
        self.logger.info('Start:\t Checking Module- exercise_hcm_r007')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseDoubleClickOn("ex_r_node")
            sleep(3)
        else:
            raise AssertionError("Shipcon Navigation did not appear")
        if appears("ex_r_tab"):
            pyautogui.moveTo(767,731)
            pyautogui.scroll(-10)
            pyautogui.scroll(-10)
            pyautogui.scroll(-10)
            pyautogui.scroll(-10)
            pyautogui.scroll(-10)
            otf()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("hvac_bent")
            free()
            if appears("select_stock",10):
                if appears("duct_250x100_selected",5):
                    mouseLeftClickOn("ok6")
                    free()
                else:
                    mouseLeftClickOn("duct_250x100")
                    mouseLeftClickOn("ok6")
            if appears("radius",5):
                pyautogui.typewrite('200',.1)
                pyautogui.press('enter')
            pyautogui.typewrite('0,1000',.1)
            pyautogui.press('enter')
            pyautogui.click(button='right')
            mouseLeftClickOn("forward1")
            pyautogui.typewrite('500', .1)
            pyautogui.press('enter')
            pyautogui.click(button='right')
            mouseLeftClickOn("portside")
            sleep(1)
            pyautogui.moveTo(771,469)
            sleep(1)
            pyautogui.typewrite('500', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.moveTo(1171,492)
            sleep(1)
            pyautogui.typewrite('500', .1)
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(1)
            reset_viewport()
            pyautogui.hotkey('ctrl','s')
            self.logger.info("Exercise R007 done")
        else:
            raise AssertionError("ex_r_tab did not appear")

        self.logger.info('End:\t Checking Module- exercise_hcm_r007')

    def exercise_hcm_a003(self):
        self.logger.info('Start:\t Checking Module- exercise_hcm_a003')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("new_hvac")
            pyautogui.typewrite('EX-A', .1)
            pyautogui.press('enter')
            sleep(3)
            if appears("ex_a_tab"):
                change_viewpoint_from_aft_stbd_up()
                pyautogui.typewrite('line', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('40000,3700,3900', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('#45000,3700,3900', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                reset_viewport()
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('40000,3700,3900',.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('delete', .1)
                pyautogui.press('enter')
                hvac_sysman()
                mouseLeftClickOn("610_expand_click")
                mouseLeftClickOn("ex_system_expand", "ex_system")
                mouseLeftClickOn("ex_hvac_branch")
                mouseLeftClickOn("ok6")
                free()
                free()
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("hvac_bent")
                free()
                pyautogui.typewrite('40000,3700,3900', .1)
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("forward1")
                pyautogui.typewrite('2000', .1)
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("starboard1")
                pyautogui.typewrite('1000', .1)
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("forward1")
                pyautogui.typewrite('1000', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("warning4", 5):
                    pyautogui.press('right')
                    pyautogui.press('enter')
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("hvac_bent")
                free()
                pyautogui.click(button='right')
                mouseLeftClickOn("xOffset")
                pyautogui.typewrite('400', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('40000,3700,3900', .1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.moveTo(571, 588)
                sleep(1)
                pyautogui.click()
                sleep(1)
                pyautogui.moveTo(799, 587)
                sleep(1)
                pyautogui.click()
                sleep(1)
                pyautogui.moveTo(1019, 593)
                sleep(1)
                pyautogui.click()
                sleep(1)
                pyautogui.moveTo(1127, 529)
                sleep(1)
                pyautogui.click()
                sleep(1)
                pyautogui.press('enter')
                if appears("warning4", 4):
                    pyautogui.press('right')
                    pyautogui.press('enter')
                pyautogui.typewrite('scmlink', .1)
                pyautogui.press('enter')
                if appears("mlink_manager"):
                    if appears("mlink_DWG",10):
                        mouseLeftClickOn("mlink_DWG")
                    else:
                        mouseLeftClickOn("mlink_DWG_dropdown")
                        sleep(2)
                        mouseLeftClickOn("mlink_Attach_DWG")
                    if appears("mlink_DWG_window", 10):
                        mouseLeftClickOn("mlink_structure")
                        pyautogui.press('right')
                        mouseLeftClickOn("mlink_frame")
                        pyautogui.press('right')
                        mouseLeftClickOn("mlink_fr67")
                        pyautogui.press('space')
                        mouseLeftClickOn("mlink_fr71")
                        pyautogui.press('space')
                        pyautogui.hotkey('win','up')
                        mouseLeftClickOn("mlink_ok")
                        sleep(5)
                        waitForVanish("mlink_loading")
                        if appears("mlink_manager"):
                            mouseLeftClickOn("mlink_manager")
                        elif appears("mlink_manager_high"):
                            mouseLeftClickOn("mlink_manager_high")
                        pyautogui.hotkey('alt', 'f4')
                        sleep(10)
                        change_viewpoint_from_aft_stbd_up()
                        pyautogui.hotkey('ctrl', 's')
                        self.logger.info("Exercise A003 done")
                else:
                    raise AssertionError("mlink DWG window did not appear")
            else:
                raise AssertionError("ex_a_tab did not appear")
        else:
            raise AssertionError("shipcon_navi didn't appeared.")
        self.logger.info('End:\t Checking Module- exercise_hcm_a003')

    # Additional functions for support

    def temporary_line(self):
        # this line is drawn to change the viewport so that the automation job can be done easily. But this is not necessary for manual drawing. The line will be deleted later.
        pyautogui.typewrite('line', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('4676,7510', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('#3563,-3075', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')

    def delete_temp_line(self):
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('4676,7510', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete', .1)
        pyautogui.press('enter')

    # Creating Flange, Tee, Laterals, Cross, Transition and Y HVAC Elements

    def creating_flange(self):
        self.logger.info('Start:\t Checking Module- creating_flange')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseDoubleClickOn("test_hvac_node")
            sleep(3)
            if appears("test_hvac_tab"):
                hvac_sysman()
                mouseLeftClickOn("612_expand","612_expand_region")
                mouseLeftClickOn("ac1_101")
                mouseLeftClickOn("ok6")
                free()
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("hvac_flange")
                mouseLeftClickOn("create_flange")
                if appears("new_hvac_stock"):
                    pyautogui.press('space')
                    pyautogui.press('backspace', 40, 0.02)
                    pyautogui.typewrite('Test-Flange-HVAC',.1)
                    sleep(1)
                    pyautogui.press('enter')
                    pyautogui.press('down',3, 0.1)
                    pyautogui.press('s',2, 0.1)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('down',7, 0.1)
                    sleep(1)
                    pyautogui.typewrite('50',.1)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.press('down',8, 0.1)
                    pyautogui.press('t')
                    sleep(1)
                    pyautogui.press('enter')
                    mouseLeftClickOn("ok6")
                    self.logger.info("Flange was created")
                else:
                    raise AssertionError("new_hvac_stock did not appear")
                pyautogui.hotkey('alt','f4')
                if appears('hvac_util',20) == False:
                    pyautogui.hotkey('alt', 'f4')
            else:
                raise AssertionError("test_hvac_tab did not appear")
        self.logger.info('End:\t Checking Module- creating_flange')


    def creating_flange_otf(self):
        self.logger.info('Start:\t Checking Module- creating_flange_otf')
        sleep(5)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_flange")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("end1_profile","end1_profile_region")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2010.2097,1882.9902',.1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("end2_profile","end2_profile_region")
            pyautogui.press('down',presses=2)
            pyautogui.press('right')
            pyautogui.press('b')
            mouseLeftClickOn("ok6")
            pyautogui.typewrite('2000,2000',.1)
            pyautogui.press('enter')
            if appears("warning9",5):
                pyautogui.press('enter')
                self.logger.info("Flange(OTF) was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_flange_otf')

    def creating_tee_elements(self):
        self.logger.info('Start:\t Checking Module- creating_tee_elements')
        sleep(2)
        otf()
        self.temporary_line()
        change_viewpoint_from_aft_stbd_up()
        sleep(2)
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_tee")
        if appears("new_hvac_stock"):
            if appears("tee_400x150",30):
                mouseLeftClickOn("tee_400x150")
                mouseLeftClickOn("ok6")
            else:
                mouseLeftClickOn("tee_400x150_unselected")
                mouseLeftClickOn("ok6")
        free()
        pyautogui.typewrite('2700,2400',0.1)
        pyautogui.press('enter')
        pyautogui.moveTo(992,324)
        pyautogui.click()
        pyautogui.moveTo(586,304)
        pyautogui.click()
        self.logger.info("Tee element was created")
        self.logger.info('End:\t Checking Module- creating_tee_elements')

    def creating_tee_elements_otf(self):
        self.logger.info('Start:\t Checking Module- creating_tee_elements_otf')
        sleep(1)
        self.delete_temp_line()
        change_viewpoint_from_aft_stbd_up()
        sleep(1)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_tee")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("main_body_profile","main_body_profile_region")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2612.4517,2273.9494', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("branch_offset")
            pyautogui.press('right')
            pyautogui.typewrite('200',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            pyautogui.typewrite('3200,2400', .1)
            pyautogui.press('enter')
            pyautogui.moveTo(986,281)
            pyautogui.click()
            self.logger.info("Tee element(OTF) was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_tee_elements_otf')

    def creating_lateral(self):
        self.logger.info('Start:\t Checking Module- creating_lateral')
        sleep(1)
        otf()
        change_viewpoint_from_aft_stbd_up()
        sleep(5)
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_lateral")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("create_lateral")
            mouseLeftClickOn("stock_name4")
            pyautogui.press('right')
            pyautogui.typewrite('Test-Lateral-HVAC')
            pyautogui.press('enter')
            pyautogui.press('down', 3, 0.2)
            pyautogui.press('s', 6, 0.2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 10, 0.2)
            pyautogui.typewrite('400',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 2, 0.2)
            pyautogui.press('r')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 2, 0.2)
            pyautogui.typewrite('250',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('100',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.press('f')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down')
            sleep(1)
            pyautogui.press('f')
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down', 3, 0.2)
            pyautogui.press('r')
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('down', 2, 0.2)
            pyautogui.typewrite('100',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('100',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.press('f')
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('50',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('down')
            pyautogui.typewrite('60',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6","ok_region2")
            #way around code to avoid warning message starts here
            if appears('test_lateral_warning_window', 20):
                mouseLeftClickOn('test_lateral_continue')
                # mouseLeftClickOn('description_empty_field', 'description_empty_field_region')
                # mouseLeftClickOn('connectsTO')
                # sleep(2)
                # pyautogui.press('esc')
                if appears('Test_Lateral_HVAC', 20):
                    mouseDoubleClickOn('Test_Lateral_HVAC')
                    pyautogui.press('enter')
                    mouseLeftClickOn('delete_filter')
                    mouseDoubleClickOn('Test_Lateral_HVAC')
                else:
                    pyautogui.press('esc')
            # way around code to avoid warning message ends here
            free()
            pyautogui.typewrite('2000,-1000',.1)
            pyautogui.press('enter')
            pyautogui.moveTo(1011,647)
            pyautogui.click()
            self.logger.info("Lateral was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_lateral')

    def creating_lateral_otf(self):
        self.logger.info('Start:\t Checking Module- creating_lateral_otf')
        sleep(2)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_lateral")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("main_body_profile","main_body_profile_region")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2043.3335,-1391.3035', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("main_body_profile","side_branch_region")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('1728.9710,-1356.6220', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("branch_offset")
            pyautogui.press('right')
            pyautogui.typewrite('200',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            pyautogui.typewrite('2000,-1400',.1)
            pyautogui.press('enter')
            pyautogui.moveTo(1155,677)
            pyautogui.click()
            self.logger.info("Lateral(OTF) was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_lateral_otf')

    def creating_cross(self):
        self.logger.info('Start:\t Checking Module- creating_cross')
        sleep(2)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_cross")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("ok6")
            free()
            pyautogui.typewrite('1000,4000',.1)
            pyautogui.press('enter')
            pyautogui.moveTo(387,166)
            pyautogui.click()
            pyautogui.moveTo(72,175)
            pyautogui.click()
            self.logger.info("Cross was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_cross')

    def creating_cross_otf(self):
        self.logger.info('Start:\t Checking Module- creating_cross_otf')
        sleep(2)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_cross")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("main_body_profile","main_body_profile_region")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('3474.7960,2457.6122', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("main_body_length","main_body_profile_region")
            pyautogui.press('right')
            pyautogui.typewrite('500',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("main_body_profile","side_branch_region1")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2066.0693,2490.3082', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("branch_offset")
            pyautogui.press('right')
            pyautogui.typewrite('250',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("diameter")
            pyautogui.press('right')
            pyautogui.typewrite('100',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok6")
            pyautogui.typewrite('3400,2400',.1)
            pyautogui.press('enter')
            pyautogui.moveTo(961,273)
            pyautogui.click()
            self.logger.info("Cross(OTF) was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_cross_otf')

    def creating_transition(self):
        self.logger.info('Start:\t Checking Module- creating_transition')
        sleep(2)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_transition")
        if appears("new_hvac_stock"):
            mouseLeftClickOn('connects_to')
            free()
            pyautogui.typewrite('3918.0615,2215.7171',.1)
            pyautogui.press('enter')
            if appears("400_150_high",4):
                mouseLeftClickOn("ok6")
            else:
                mouseLeftClickOn("400_150")
                mouseLeftClickOn("ok6")
            sleep(1)
            free()
            pyautogui.typewrite('3900,2400',.1)
            pyautogui.press('enter')
            pyautogui.moveTo(850,105)
            pyautogui.click()
            self.logger.info("Transition was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_transition')

    def creating_transition_otf(self):
        self.logger.info('Start:\t Checking Module- creating_transition_otf')
        sleep(2)
        otf()
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_transition")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("main_body_profile","end_profile1")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2050.0081,2422.5366', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("main_body_profile","end_profile2")
            mouseLeftClickOn("sel_button")
            pyautogui.typewrite('2787.0133,2390.3522', .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("ok6")
            # pyautogui.typewrite('2000,2400',.1)
            pyautogui.typewrite('2006.3003,2399.9504', .1)
            pyautogui.press('enter')
            pyautogui.press('esc')
            pyautogui.press('esc')
            change_viewpoint_from_aft_stbd_up()
            pyautogui.hotkey('ctrl','s')
            self.logger.info("Transition(OTF) was created")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_transition_otf')

    # Creating Saddles

    def create_saddles(self):
        self.logger.info('Start:\t Checking Module- create_saddles')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("hvac_node")
            mouseDoubleClickOn("test_hvac_node")
            sleep(3)
            if appears("test_hvac_tab"):
                sleep(10)
                mouseLeftClickOn("hvac_ribbon")
                otf()
                free()
                mouseLeftClickOn("hvac_pane")
                mouseLeftClickOn("straight")
                if appears("new_hvac_stock"):

                    if appears("duct_250x100_selected2",4):
                        sleep(1)
                        mouseLeftClickOn("ok8")
                    else:
                        mouseDoubleClickOn("duct_320x250")
                    free()
                    pyautogui.typewrite('0,6000', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('2000,0', .1)
                    pyautogui.press('enter')
                    change_viewpoint_from_aft_stbd_up()
                    mouseLeftClickOn("hvac_pane")
                    mouseLeftClickOn("straight")
                    free()
                    pyautogui.click(button='right')
                    mouseLeftClickOn("select_stock1")
                    if appears("new_hvac_stock"):
                        mouseLeftClickOn("duct_250x100")
                        mouseLeftClickOn("ok6")
                        free()
                        pyautogui.typewrite('1000,6000', .1)
                        pyautogui.press('enter')
                        pyautogui.click(button='right')
                        mouseLeftClickOn("starboard1")
                        pyautogui.typewrite('1000', .1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("hvac_util")
                        mouseLeftClickOn("create_saddle")
                        free()
                        pyautogui.typewrite('820.9463,5226.8614')
                        pyautogui.press('enter')
                        pyautogui.typewrite('534.9730,5713.3952')
                        pyautogui.press('enter')
                        pyautogui.press('y')
                        pyautogui.press('enter')
                        pyautogui.press('0')
                        pyautogui.press('enter')
                        self.logger.info("Saddles were created")
                else:
                    raise AssertionError("new_hvac_stock window did not appear")
            else:
                raise AssertionError("test_hvac_tab did not appear")
        self.logger.info('End:\t Checking Module- create_saddles')


    def creating_duct_as_saddle(self):
        self.logger.info('Start:\t Checking Module- creating_duct_as_saddle')
        sleep(1)
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("straight")
        free()
        pyautogui.click(button='right')
        mouseLeftClickOn("saddle")
        free()
        pyautogui.typewrite('534.9730,5713.3952',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('500,6000',.1)
        pyautogui.press('enter')
        pyautogui.moveTo(147,245)
        pyautogui.click()
        sleep(1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        self.logger.info("Duct was created as saddle")
        self.logger.info('End:\t Checking Module- creating_duct_as_saddle')

    # Applying Finishes and Insulations to HVAC elements

    def managing_finishes_and_insulation(self):
        self.logger.info('Start:\t Checking Module- managing_finishes_and_insulation')
        hvac_sysman()
        if appears("hvac_system_test2",5):
            mouseLeftClickOn("hvac_system_test2")
        else:
            raise AssertionError("hvac_system_test did not appear")
        if appears("hvac_finish"):
            mouseDoubleClickOn("hvac_finish")
            if appears("select_finish_window"):
                pyautogui.hotkey('win','up')
                mouseLeftClickOn("acme_premier_a")
                mouseLeftClickOn("select_finish_window_right")
                mouseLeftClickOn("ok6")
                mouseLeftClickOn("hvac_system_branch")
                mouseDoubleClickOn("hvac_finish")
                mouseLeftClickOn("inherit_from_system_unselect","inherit_from_system")
                mouseLeftClickOn("select_finish_window_left")
                mouseLeftClickOn("ok6")
                mouseLeftClickOn("new_branch_sysman")
                pyautogui.typewrite('HVAC-branch-test1',.1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('up')
                mouseDoubleClickOn("insulation")
                mouseLeftClickOn("wool37")
                mouseLeftClickOn("select_finish_window_right")
                mouseLeftClickOn("ok6")
                pyautogui.moveTo(600,600)
                mouseLeftClickOn("ok6")
                free()
                self.logger.info("Managing of finishes and insulation done")
            else:
                raise AssertionError("select_finish_window did not appear")
        else:
            raise AssertionError("hvac_finish option did not found")
        self.logger.info('End:\t Checking Module- managing_finishes_and_insulation')

    def applying_insulation(self):
        self.logger.info('Start:\t Checking Module- applying_insulation')
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("straight")
        free()
        pyautogui.typewrite('0,9000',.1)
        pyautogui.press('enter')
        pyautogui.click(button='right')
        mouseLeftClickOn("forward1")
        pyautogui.typewrite('2000',.1)
        pyautogui.press('enter')
        mouseLeftClickOn("structure_main_pane")
        mouseMoveTo("drawing_option_selected")
        mouseLeftClickOn("hovered_drop_down")
        mouseLeftClickOn("hvac_drawing")
        if appears("hvac_drawing_window"):
            mouseLeftClickOn("display_tab")
            mouseLeftClickOn("ok6")
            free()
            sleep(1)
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("hvac_insulation")
            free()
            pyautogui.typewrite('1036.6893,8940.2481',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(2)
            mouseLeftClickOn("inherit_from_system_unselect", "inherit_from_system")
            mouseLeftClickOn("select_finish_window_left")
            mouseLeftClickOn("wool50")
            mouseLeftClickOn("select_finish_window_right")
            mouseLeftClickOn("ok6")
            free()
            self.logger.info("Insulation was applied")
        else:
            raise AssertionError("hvac_drawing_window did not appear")
        self.logger.info('End:\t Checking Module- applying_insulation')

    def applying_finishes(self):
        self.logger.info('Start:\t Checking Module- applying_finishes')
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("apply_finishes_drop_down","apply_finishes_region")
        mouseLeftClickOn("apply_finishes")
        free()
        pyautogui.typewrite('1036.6893,8940.2481', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        mouseLeftClickOn("inherit_from_system_unselect", "inherit_from_system")
        mouseLeftClickOn("select_finish_window_left")
        mouseLeftClickOn("ok6")
        self.logger.info("Finishes were applied")
        free()
        change_viewpoint_from_aft_stbd_up()
        self.logger.info('End:\t Checking Module- applying_finishes')

    # Using HVAC – UCS Intersection command

    def creating_ucs_intersection(self):
        self.logger.info('Start:\t Checking Module- creating_ucs_intersection')
        pyautogui.typewrite('scmlink',.1)
        pyautogui.press('enter')
        if appears("mlink_manager"):
            if appears("mlink_DWG", 10):
                mouseLeftClickOn("mlink_DWG")
            else:
                mouseLeftClickOn("mlink_DWG_dropdown")
                sleep(2)
                mouseLeftClickOn("mlink_Attach_DWG")
            if appears("mlink_DWG_window", 10):
                mouseLeftClickOn("mlink_structure")
                pyautogui.press('right')
                mouseLeftClickOn("mlink_frame")
                pyautogui.press('right')
                pyautogui.press('down')
                pyautogui.press('space')
                mouseLeftClickOn("mlink_ok")
                sleep(5)
                waitForVanish("mlink_loading")
                if appears("mlink_manager", 20):
                    mouseLeftClickOn("mlink_manager")
                elif appears("mlink_manager_high", 20):
                    mouseLeftClickOn("mlink_manager_high")
                pyautogui.hotkey('alt', 'f4')
            else:
                raise AssertionError("mlink DWG window did not appear")
            sleep(5)
            pyautogui.typewrite('select',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('7586.4212,2539.3549',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('-4490.3923,3597.6252',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('delete',.1)
            pyautogui.press('enter')
            change_viewpoint_from_aft_stbd_up()
            pyautogui.typewrite('line',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('34000,2500,4100',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('#37000,1900,3500',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('line',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('41000,-4000,5800',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('#46000,-4800,6600',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            change_viewpoint_from_aft_stbd_up()
            sleep(1)
            mouseLeftClickOn("hvac_pane")
            sleep(1)
            mouseLeftClickOn("straight")
            free()
            free()
            sleep(2)
            pyautogui.click(button='right')
            sleep(1)
            mouseLeftClickOn("select_stock1")
            if appears("select_stock"):
                pyautogui.hotkey('win','down')
                sleep(2)
                pyautogui.hotkey('win','up')
                mouseLeftClickOn("delete_filter")
                mouseDoubleClickOn("stock_tab_resizer2")
                # mouseDoubleClickOn("tab_resizer")
                if appears("duct_270",5):
                    mouseLeftClickOn("duct_270")
                else:
                    raise AssertionError("duct_270 did not appear")
                pyautogui.press('down',presses=45)
                if appears("spiro200",15):
                    mouseDoubleClickOn("spiro200")
                else:
                    raise AssertionError("spiro200 did not appear")
                free()
                pyautogui.typewrite('34000,2500,4100',.1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.moveTo(666,466)
                pyautogui.click()
                if appears("warning10",5):
                    sleep(1)
                    mouseLeftClickOn("warning10_no")
                free()
                pyautogui.press('enter')
                pyautogui.typewrite('41000,-4000,5800', .1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.moveTo(1087, 330)
                pyautogui.click()
                if appears("warning10", 5):
                    sleep(1)
                    mouseLeftClickOn("warning10_no")
                sleep(1)
                mouseDoubleClickOn("sc_utilites")
                mouseLeftClickOn("visual_sc")
                if appears("activate_ucs"):
                    mouseLeftClickOn("activate_ucs")
                    mouseLeftClickOn("fr65_ucs")
                    mouseLeftClickOn("move_z_ucs")
                    pyautogui.typewrite('1000', .1)
                    pyautogui.press('enter')
                    mouseDoubleClickOn("hvac_ribbon")
                    mouseLeftClickOn("hvac_util")
                    mouseLeftClickOn("add_ucs_intersection")
                    free()
                    pyautogui.typewrite('-2735.8712,2575.6560', .1)
                    pyautogui.press('enter')
                    if appears("warning10", 10):
                        mouseLeftClickOn("warning10_no")
                        free()
                    if appears("warning10", 10):
                        pyautogui.moveTo(400, 400)
                        sleep(1)
                        mouseLeftClickOn("warning10_no")
                    mouseLeftClickOn("hvac_util")
                    mouseLeftClickOn("add_ucs_intersection")
                    free()
                    pyautogui.typewrite('7086.5782,8750.8429', .1)
                    pyautogui.press('enter')
                    if appears("warning10", 10):
                        mouseLeftClickOn("warning10_no")
                        free()
                    if appears("warning10", 10):
                        pyautogui.moveTo(400, 400)
                        mouseLeftClickOn("warning10_no")
                    self.logger.info("UCS intersection was created")
                else:
                    raise AssertionError("activate_ucs did not appear")
            else:
                raise AssertionError("select_stock window did not appear")
        else:
            raise AssertionError("mlink_manager did not appear")
        self.logger.info('End:\t Checking Module- creating_ucs_intersection')

    def ucs_intersection_routing(self):
        self.logger.info('Start:\t Checking Module- ucs_intersection_routing')
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_bent")
        free()
        if appears("radius",10):
            pyautogui.typewrite('200',.1)
            pyautogui.press('enter')
        pyautogui.typewrite('-1900,3500,1000',.1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.moveTo(696,458)
        sleep(1)
        pyautogui.click()
        sleep(1)
        pyautogui.moveTo(837,476)
        sleep(1)
        pyautogui.click()
        sleep(1)
        pyautogui.moveTo(928,426)
        sleep(1)
        pyautogui.click()
        sleep(1)
        if appears("warning10",10):
            free()
            pyautogui.moveTo(400, 400)
            mouseLeftClickOn("warning10_no")
        if appears("warning10",10):
            free()
            pyautogui.moveTo(400, 400)
            mouseLeftClickOn("warning10_no")
        self.logger.info("UCS Intersection routing was done")
        self.logger.info('End:\t Checking Module- ucs_intersection_routing')

    def erasing_ucs_intersection(self):
        self.logger.info('Start:\t Checking Module- erasing_ucs_intersection')
        mouseLeftClickOn("hvac_util")
        mouseLeftClickOn("add_ucs_intersection_drop","add_ucs_intersection_region")
        sleep(3)
        pyautogui.press('esc')
        free()
        self.logger.info("UCS intersection erased")
        self.logger.info('End:\t Checking Module- erasing_ucs_intersection')

    # Stock Constraints

    def stock_constraints(self):
        self.logger.info('Start:\t Checking Module- stock_constraints')
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("straight")
        free()
        pyautogui.click(button='right')
        mouseLeftClickOn("select_stock1")
        sleep(2)
        pyautogui.hotkey('win','down')
        sleep(2)
        pyautogui.hotkey('win','up')
        if appears("duct_250x100",5):
            mouseDoubleClickOn("duct_250x100")
        else:
            raise AssertionError("duct_250x100 did not appear")
        free()
        pyautogui.typewrite('0,10000',.1)
        pyautogui.press('enter')
        pyautogui.click(button='right')
        mouseLeftClickOn("forward1")
        pyautogui.typewrite('50', .1)
        pyautogui.press('enter')
        mouseLeftClickOn("hvac_util")
        if appears("hvac_constraint_enforcement"):
            mouseLeftClickOn("hvac_constraint_enforcement")
            free()
            self.logger.info("Stock constraints checked")
        else:
            raise AssertionError("hvac_constraint_enforcement did not appear")
        self.logger.info('End:\t Checking Module- stock_constraints')

    def cutting_ducts_to_max_length(self):
        self.logger.info('Start:\t Checking Module- cutting_ducts_to_max_length')
        mouseLeftClickOn("sc_utilites_main")
        if appears("hvac_drawing_option"):
            mouseLeftClickOn("hvac_drawing_option")
        else:
            mouseLeftClickOn("drawing_options_dropdown", "drawing_options")
            mouseLeftClickOn("hvac_drawing")
        if appears("hvac_drawing_window"):
            mouseLeftClickOn("behavior_tab")
            pyautogui.press('tab')
            pyautogui.press('down')
            mouseLeftClickOn("ok6")
            sleep(1)
            activate_wcs()
            mouseDoubleClickOn("hvac_ribbon")
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
            free()
            pyautogui.typewrite('0,10000', .1)
            pyautogui.press('enter')
            pyautogui.click(button='right')
            mouseLeftClickOn("forward1")
            pyautogui.typewrite('5000', .1)
            pyautogui.press('enter')
            change_viewpoint_from_aft_stbd_up()
            mouseLeftClickOn("sc_utilites_main")
            mouseLeftClickOn("hvac_drawing_option")
            if appears("hvac_drawing_window"):
                pyautogui.press('tab')
                pyautogui.press('up',presses=2)
                mouseLeftClickOn("ok6")
                mouseLeftClickOn("hvac_util")
                if appears("cut_to_max_length"):
                    mouseLeftClickOn("cut_to_max_length")
                    free()
                    pyautogui.typewrite('2241.0362,10195.4365',.1)
                    pyautogui.press('enter',presses=2)
                    pyautogui.hotkey('ctrl','s')
                    self.logger.info("Ducts were cut to max length")
                else:
                    raise AssertionError("cut_to_max_length did not appear")
            else:
                raise AssertionError("hvac_drawing_window din't appear(2)")
        else:
            raise AssertionError("activate_ucs did not appear")
        self.logger.info('End:\t Checking Module- cutting_ducts_to_max_length')

    # Modifying HVAC routing

    def delete_all(self):
        self.logger.info('Start:\t Checking Module- add_manual_cutout')
        pyautogui.typewrite('select',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('63115.5814,5664.0089',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('-11501.6683,5243.0563',.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete',.1)
        pyautogui.press('enter')
        self.logger.info("All drawings are deleted")
        self.logger.info('Start:\t Checking Module- add_manual_cutout')

    def geometry_drawing(self):
        self.logger.info('Start:\t Checking Module- geometry_drawing')
        sleep(2)
        mouseLeftClickOn("hvac_ribbon")
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("straight")
        if appears("new_hvac_stock"):
            mouseLeftClickOn("delete_filter")
            if appears("duct_400x150",5):
                mouseDoubleClickOn("duct_400x150")
            elif appears("duct_400x150_high",5):
                mouseDoubleClickOn("duct_400x150_high")
            elif appears("duct_400x150_high3",5):
                mouseDoubleClickOn("duct_400x150_high3")
            else:
                raise AssertionError("duct_400x150 did not appear")
            free()
            sleep(1)
            pyautogui.typewrite('0,12000',.1)
            pyautogui.press('enter')
            pyautogui.click(button='right')
            mouseLeftClickOn("forward1")
            pyautogui.typewrite('1000',.1)
            pyautogui.press('enter')
            change_viewpoint_plan_looking_down()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("hvac_tee")
            if appears("select_stock"):
                mouseLeftClickOn("ok6")
            free()
            pyautogui.typewrite('1000,12000',.1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(1347,142)
            pyautogui.click()
            reset_viewport()
            free()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
            free()
            pyautogui.typewrite('1500,12000', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('1000', .1)
            pyautogui.press('enter')
            reset_viewport()
            free()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
            free()
            pyautogui.typewrite('1250,12300', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('500', .1)
            pyautogui.press('enter')
            reset_viewport()
            free()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("hvac_tee")
            free()
            pyautogui.typewrite('1250,12800', .1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.moveTo(1347, 142)
            pyautogui.click()
            reset_viewport()
            free()
            mouseLeftClickOn("hvac_pane")
            mouseLeftClickOn("straight")
            free()
            pyautogui.typewrite('1550,13050', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('400', .1)
            pyautogui.press('enter')
            reset_viewport()
            free()
            self.logger.info("Geometry drawing completed")
        else:
            raise AssertionError("new_hvac_stock window did not appear")
        self.logger.info('End:\t Checking Module- geometry_drawing')

    def locking_anchoring_elements(self):
        self.logger.info('Start:\t Checking Module- locking_anchoring_elements')
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("hvac_node")
            mouseDoubleClickOn("test_hvac_node")
            sleep(3)
            if appears("test_hvac_tab"):
                mouseLeftClickOn("hvac_ribbon")
                mouseLeftClickOn("sc_utilites_main")
                mouseMoveTo("drawing_option_selected")
                mouseLeftClickOn("hovered_drop_down")
                mouseLeftClickOn("hvac_drawing")
                # mouseLeftClickOn("drawing_options_dropdown","drawing_options")
                # mouseLeftClickOn("hvac_drawing")
                if appears("hvac_drawing_window"):
                    mouseLeftClickOn("display_tab")
                    if appears("show_insulation_region",5):
                        mouseLeftClickOn("show_insulation","show_insulation_region")
                    mouseLeftClickOn("ok6")
                    free()
                    self.delete_all()
                    self.geometry_drawing()

                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('1948.6382,12872.6820', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    sleep(2)
                    # pyautogui.moveTo(1005, 235)
                    pyautogui.moveTo(1012, 237)
                    pyautogui.click()
                    sleep(2)
                    pyautogui.moveTo(1088, 235)
                    sleep(2)
                    pyautogui.typewrite('200', .1)
                    sleep(2)
                    pyautogui.press('enter')
                    free()
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('1251.1742,13280.9925', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    mouseLeftClickOn("anchor1")
                    sleep(1)
                    pyautogui.hotkey('ctrl', 'z')
                    sleep(1)
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('1439.0433,11802.6270', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    mouseLeftClickOn("lock")
                    sleep(1)
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('2146.4902,12941.6783', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    sleep(2)
                    # pyautogui.moveTo(1090, 236)
                    pyautogui.moveTo(1095, 240)
                    pyautogui.click()
                    sleep(2)
                    pyautogui.moveTo(1100, 236)
                    sleep(2)
                    pyautogui.typewrite('200', .1)
                    sleep(2)
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.press('esc')
                    sleep(2)
                    pyautogui.hotkey('ctrl', 'z')
                    pyautogui.hotkey('ctrl', 'z')
                    pyautogui.hotkey('ctrl', 'z')
                    sleep(2)
                    pyautogui.press('esc')
                    free()
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('1439.0433,11802.6270', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    if appears("disconnect",10):
                        mouseLeftClickOn("disconnect")
                        sleep(2)
                        pyautogui.hotkey('ctrl', 'z')
                        self.logger.info("Locking elements done")
                    else:
                        raise AssertionError("disconnect did not appear")
                else:
                    raise AssertionError("hvac_drawing_window did not appear")
            else:
                raise AssertionError("HVAC Test tab did not appear")
        else:
            raise AssertionError("ShipCon Navigator did not appear")
        self.logger.info('End:\t Checking Module- locking_anchoring_elements')

    # Connecting/Disconnecting Parts

    def connecting_and_disconnecting_part(self):
        self.logger.info('Start:\t Checking Module- connecting_and_disconnecting_part')
        sleep(5)
        free()
        free()
        free()
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('1781.9044,13015.8253', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete',.1)
        pyautogui.press('enter')

        mouseLeftClickOn("hvac_util")
        if appears("connect_util_pane",5):
            mouseLeftClickOn("connect_util_pane")
            free()
            sleep(2)
            pyautogui.moveTo(990,254)
            pyautogui.click()
            sleep(2)
            pyautogui.moveTo(1045,255)
            pyautogui.click()
            free()
        else:
            raise AssertionError("connect_util_pane did not appear")
        self.logger.info('End:\t Checking Module- connecting_and_disconnecting_part')

    # Breaking Duct at Point

    def breaking_duct(self):
        self.logger.info('Start:\t Checking Module- breaking_duct')
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_bent")
        free()
        if appears("radius",10):
            pyautogui.typewrite('300',.1)
            pyautogui.press('enter')
        # else:
        #     raise AssertionError("Radius option did not appear")
        sleep(1)
        pyautogui.typewrite('2150,13050',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('800',.1)
        pyautogui.press('enter')
        sleep(2)
        pyautogui.moveTo(1529,157)
        sleep(2)
        pyautogui.click(button='right')
        mouseLeftClickOn("port1")
        sleep(1)
        pyautogui.typewrite('800',.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        reset_viewport()
        mouseDoubleClickOn("sc_utilites")
        mouseLeftClickOn("sc_utilities_tools_button")
        if appears("break_at_point"):
            mouseLeftClickOn("break_at_point")
            free()
            pyautogui.typewrite('end',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('2593.8950,13039.2915', .1)
            pyautogui.press('enter')
        else:
            raise AssertionError("break_at_point did not appear")
        self.logger.info('End:\t Checking Module- breaking_duct')

    def merging_to_bent_duct(self):
        self.logger.info('Start:\t Checking Module- merging_to_bent_duct')
        mouseDoubleClickOn('hvac_ribbon')
        mouseLeftClickOn("hvac_util")
        if appears("merge_bent"):
            mouseLeftClickOn("merge_bent")
            free()
            pyautogui.typewrite('3327,14233',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('1880,12582',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('single',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('300',.1)
            pyautogui.press('enter')
        else:
            raise AssertionError("merge_bent option did not appear")
        self.logger.info('End:\t Checking Module- merging_to_bent_duct')

    def extracting_center_line(self):
        self.logger.info('Start:\t Checking Module- extracting_center_line')
        mouseLeftClickOn("hvac_util")
        if appears("extract_center_line"):
            mouseLeftClickOn("extract_center_line")
            free()
            pyautogui.typewrite('2025.8775,13249.5860',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
        else:
            raise AssertionError("extract_center_line did not appear")
        self.logger.info('End:\t Checking Module- extracting_center_line')

    def pantsifying_duct_ends(self):
        self.logger.info('Start:\t Checking Module- pantsifying_duct_ends')
        change_viewpoint_from_aft_stbd_up()
        mouseLeftClickOn("hvac_util")
        if appears("pantsify_end"):
            mouseLeftClickOn("pantsify_end")
            free()
            pyautogui.typewrite('2804.5970,13912.1587', .1)
            pyautogui.press('enter')
            if appears("hvac_edit_pants",10):
                mouseLeftClickOn('pantsify_region')
                mouseLeftClickOn("pantsify_horizon")
                mouseLeftClickOn("pantsify_horizon_region")
                mouseLeftClickOn("ok6")
            else:
                pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.press('esc')
                mouseLeftClickOn("hvac_util")
                if appears("pantsify_end"):
                    mouseLeftClickOn("pantsify_end")
                    free()
                    pyautogui.typewrite('2987.2513,14224.8938', .1)
                    pyautogui.press('enter')
                    if appears("hvac_edit_pants", 10):
                        mouseLeftClickOn('pantsify_region')
                        mouseLeftClickOn("pantsify_horizon")
                        mouseLeftClickOn("pantsify_horizon_region")
                        mouseLeftClickOn("ok6")
                    else:
                        raise AssertionError("hvac_edit_pants did not appear")
                else:
                    raise AssertionError("pantsify_end did not appear")
        else:
            raise AssertionError("extract_center_line did not appear")
        self.logger.info('End:\t Checking Module- pantsifying_duct_ends')


    # Spooling a System
    def add_remove_spool_breaks(self):
        self.logger.info('Start:\t Checking Module- add_remove_spool_breaks')
        change_viewpoint_from_aft_stbd_up()
        mouseLeftClickOn("hvac_spool")
        if appears("add_spool_break",5):
            mouseLeftClickOn("add_spool_break")
            free()
            pyautogui.typewrite('1504.9222,12838.9425',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("hvac_spool")
            mouseMoveTo("add_spool_break")
            mouseLeftClickOn("hovered_drop_down")
            # mouseLeftClickOn("add_spool_break_dropdown","add_spool_break")
            if appears("remove_spool_break"):
                mouseLeftClickOn("remove_spool_break")
                free()
                pyautogui.typewrite('1504.9222,12838.9425', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                sleep(2)
                self.logger.info('Re-apply')
                pyautogui.hotkey('ctrl','z')
                mouseLeftClickOn("hvac_spool")
                if appears("set_no_spool"):
                    mouseLeftClickOn("set_no_spool")
                    free()
                    pyautogui.typewrite('1470.9427,11878.0116', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    if appears("select_hierarchy",5):
                        mouseLeftClickOn("unassigned_hvac")
                        sleep(1)
                        mouseLeftClickOn("ok8")
                        mouseLeftClickOn("hvac_spool")
                        mouseMoveTo("set_no_spool")
                        mouseLeftClickOn("hovered_drop_down")
                        if appears("remove_no_spool"):
                            mouseLeftClickOn("remove_no_spool")
                            free()
                            pyautogui.typewrite('1373.0060,11728.6767', .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            mouseLeftClickOn("hvac_spool")
                            mouseLeftClickOn("remove_spool_break2")
                            free()
                            pyautogui.typewrite('1890.8892,11785.1851',.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('1653.8557,12506.2390',.1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('1186.3690,11593.3711',.1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("remove_no_spool did not appear")
                    else:
                        pyautogui.press('esc',presses=5)
                        raise AssertionError("select_hierarchy did not appear")
                else:
                    raise AssertionError("set_no_spool did not appear")
            else:
                raise AssertionError("remove_spool_break option did not appear")
        else:
            raise AssertionError("add_spool_break option did not appear")
        self.logger.info('End:\t Checking Module- add_remove_spool_breaks')

    def spool_manager(self):
        self.logger.info('Start:\t Checking Module- spool_manager')
        mouseLeftClickOn("hvac_spool")
        mouseLeftClickOn("spool_manager_button")
        if appears("spool_manager_window"):
            free()
            pyautogui.click()
            pyautogui.typewrite('select',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('2685.9881,13926.7220',.1)
            pyautogui.press('enter', 2)
            sleep(3)
            pyautogui.press('esc')
            pyautogui.typewrite('select',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('325.8506,12286.8600',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(3)
            pyautogui.press('esc')
            if appears("spool_add_button"):
                mouseLeftClickOn("spool_add_button")
                #free()
                pyautogui.typewrite('1172.8697,11942.0007',.1)
                pyautogui.press('enter', 2)
                if appears("spool_manager_window"):
                    pyautogui.press('esc')
                    mouseLeftClickOn("hvac_main")
                    if appears("product_hierarchy_button"):
                        mouseLeftClickOn("product_hierarchy_button")
                        sleep(1)
                        mouseLeftClickOn("u01_product_hierarchy")
                        while True:
                            if appears("u01_product_hierarchy_high"):
                                break
                            else:
                                mouseDoubleClickOn("u01_product_hierarchy")
                        while exists("new_product_hierarchy") == False:
                            pyautogui.click(button='right')
                        mouseLeftClickOn("new_product_hierarchy")
                        if appears("new_assembly"):
                            mouseLeftClickOn('property_name')
                            pyautogui.typewrite('HVAC-system-test',.1)
                            mouseLeftClickOn("levels")
                            pyautogui.press('s')
                            sleep(1)
                            mouseLeftClickOn("ok9")
                            sleep(1)
                            mouseLeftClickOn("product_hierarchy_close")
                            free()
                            free()
                            free()
                            pyautogui.press('esc',presses=3)
                            mouseLeftClickOn("hvac_spool")
                            mouseLeftClickOn("spool_manager_button")
                            if appears("spool_manager_window"):
                                free()
                                if appears("undefined_spool3"):
                                    mouseRightClickOn("undefined_spool3")
                                    if appears("define_spool"):
                                        mouseLeftClickOn("define_spool")
                                        if appears("spool_properties"):
                                            pyautogui.press('down',presses=20)
                                            sleep(1)
                                            mouseLeftClickOn("ok10")
                                            sleep(1)
                                            mouseLeftClickOn("modified_spools")
                                            pyautogui.click(button='right')
                                            if appears("approve_all"):
                                                mouseLeftClickOn("approve_all")
                                                mouseLeftClickOn("spool_hvac_sys_test")
                                                pyautogui.click(button='right')
                                                if appears("lock_spools"):
                                                    mouseLeftClickOn("lock_spools")
                                                    mouseLeftClickOn("override_color_check","override_color")
                                                    mouseLeftClickOn("override_color_select")
                                                    if appears("green_color"):
                                                        mouseDoubleClickOn("green_color")
                                                        sleep(1)
                                                        mouseLeftClickOn("spool_done")
                                                        free()
                                                    else:
                                                        raise AssertionError("green_color could not be choosen")
                                                else:
                                                    raise AssertionError("lock_spools did not appear")
                                            else:
                                                raise AssertionError("approve_all")
                                        else:
                                            raise AssertionError("spool_properties did not appear")
                                    else:
                                        raise AssertionError("define_spool did not appear")
                                else:
                                    raise AssertionError("undefined_spool3 did not appear")
                            else:
                                raise AssertionError("spool_manager_window did not open")
                        else:
                            raise AssertionError("new_assembly did not appear")
                    else:
                        raise AssertionError("product_hierarchy_button did not appear")
                else:
                    raise AssertionError("spool_manager_window did not appear")
            else:
                raise AssertionError("spool_add_button did not appear")
        else:
            raise AssertionError("spool_manager_window did not appear")
        self.logger.info('End:\t Checking Module- spool_manager')

    # HVAC Utilities
    def checking_local_interferences(self):
        self.logger.info('Start:\t Checking Module- checking_local_interferences')
        free()
        free()
        pyautogui.press('esc',presses=4)
        mouseLeftClickOn("hvac_main")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("interference")
            sleep(2)
            pyautogui.hotkey('alt','f4')
            sleep(2)
            mouseLeftClickOn("hvac_pane")
            if appears("straight"):
                mouseLeftClickOn("straight")
                free()
                if appears("select_stock",5):
                    mouseLeftClickOn("ok6")
                    free()
                sleep(1)
                pyautogui.typewrite('0,15000',.1)
                pyautogui.press('enter')
                pyautogui.click(button='right')
                mouseLeftClickOn("forward1")
                pyautogui.typewrite('1000',.1)
                pyautogui.press('enter')
                pyautogui.press('up')
                pyautogui.press('enter')
                pyautogui.typewrite('1000,15000',.1)
                pyautogui.press('enter')
                pyautogui.typewrite('1000', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('delete', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('992.6011,14950.5422', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                change_viewpoint_plan_looking_down()
                pyautogui.typewrite('select',.1)
                pyautogui.press('enter')
                pyautogui.typewrite('854.1318,15200.1577', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                # pyautogui.moveTo(682, 180)
                pyautogui.moveTo(686, 170)
                sleep(.5)
                pyautogui.click()
                pyautogui.moveTo(705,170)
                sleep(.5)
                pyautogui.typewrite('100',.1)
                pyautogui.press('enter')
                pyautogui.press('esc')
                mouseDoubleClickOn("sc_utilites")
                mouseLeftClickOn("sc_utilities_tools_button")
                if appears("interference_button"):
                    mouseLeftClickOn("interference_button")
                    if appears('interference_warning_window'):
                        mouseLeftClickOn('interference_warn_ok')
                    if appears("interference_window"):
                        mouseLeftClickOn("run_check")
                        sleep(2)
                        mouseClickAppears("check_box_checked")
                        if appears("hvac_check_region", 5):
                            mouseLeftClickOn("hvac_check", "hvac_check_region")
                        if appears("hvac_check_region", 5):
                            mouseLeftClickOn("hvac_check", "hvac_check_region")
                        mouseLeftClickOn("ok11")
                        if appears("only_int_region", 2):
                            mouseLeftClickOn("only_int", "only_int_region")
                        if appears("center3", 2):
                            mouseLeftClickOn("center3_check", "center3")
                        mouseLeftClickOn("report_button")
                        sleep(1)
                        mouseLeftClickOn("cancel_button2")
                        free_white()
                        pyautogui.click()
                        mouseRightClickOn("duct_400_report")
                        if appears("solution", 5):
                            mouseLeftClickOn("solution")
                            sleep(1)
                            pyautogui.typewrite('Move the DUCT', .1)
                            mouseLeftClickOn("ok12")
                            mouseLeftClickOn("only_int", "show_all")
                            pyautogui.hotkey('alt', 'f4')
                            pyautogui.press('esc')
                        else:
                            raise AssertionError("solution did not appear")
                    else:
                        raise AssertionError("interference_window did not appear")
                else:
                    raise AssertionError("interference_button did not appear")
            else:
                raise AssertionError("straight did not appear")
        else:
            raise AssertionError("shipcon_navi did not appear")
        self.logger.info('End:\t Checking Module- checking_local_interferences')

    # Transferring Parts to another Model Drawing
    def transferring_parts(self):
        self.logger.info('Start:\t Checking Module- transferring_parts')
        mouseDoubleClickOn("hvac_ribbon")
        mouseLeftClickOn("hvac_main")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("new_hvac")
            sleep(1)
            pyautogui.typewrite('Test HVAC-1',.1)
            mouseLeftClickOn("open_new_drawing_uncheck","open_new_drawing")
            mouseLeftClickOn("ok13")
            sleep(2)
            pyautogui.hotkey('alt','f4')
            sleep(2)
            change_viewpoint_plan_looking_down()
            sleep(1)
            pyautogui.typewrite('select',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('2514.6331,13248.0731',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("hvac_util")
            if appears("transfer_object2"):
                mouseLeftClickOn("transfer_object2")
                free()
                pyautogui.press('enter')
                if appears("select_single"):
                    mouseLeftClickOn("transfer_to_test_hvac")
                    mouseLeftClickOn("ok14")
                    free()
                    sleep(3)
                    free()
                    pyautogui.press('esc',presses=3)
                    pyautogui.typewrite('scmlink',.2)
                    pyautogui.press('enter')
                    if appears("mlink_manager"):
                        if appears("mlink_DWG", 10):
                            mouseLeftClickOn("mlink_DWG")
                        else:
                            mouseLeftClickOn("mlink_DWG_dropdown")
                            sleep(2)
                            mouseLeftClickOn("mlink_Attach_DWG")
                        if appears("mlink_DWG_window", 10):
                            mouseLeftClickOn("mlink_test_hvac")
                            pyautogui.press('space')
                            mouseLeftClickOn("mlink_ok")
                            sleep(5)
                            waitForVanish("mlink_loading")
                            if appears("mlink_manager", 20):
                                mouseLeftClickOn("mlink_manager")
                            elif appears("mlink_manager_high", 20):
                                mouseLeftClickOn("mlink_manager_high")
                            pyautogui.hotkey('alt', 'f4')
                    else:
                        raise AssertionError("mlink_manager did not appear")
                else:
                    raise AssertionError("select_single drawing did not appear")
            else:
                raise AssertionError("transfer_object2 did not appear")
        self.logger.info('End:\t Checking Module- transferring_parts')

    # Connecting Parts from Different Drawings
    def connecting_parts(self):
        self.logger.info('Start:\t Checking Module- connecting_parts')
        mouseLeftClickOn("hvac_main")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseDoubleClickOn("test_hvac_1_node")
            sleep(1)
            pyautogui.press('enter')
            sleep(2)
            if appears("test_hvac_tab"):
                change_viewpoint_plan_looking_down()
                pyautogui.typewrite('scmlink', .1)
                pyautogui.press('enter')
                if appears("mlink_manager"):
                    if appears("mlink_DWG", 10):
                        mouseLeftClickOn("mlink_DWG")
                    else:
                        mouseLeftClickOn("mlink_DWG_dropdown")
                        sleep(2)
                        mouseLeftClickOn("mlink_Attach_DWG")
                    if appears("mlink_DWG_window", 10):
                        mouseLeftClickOn("mlink_test_hvac")
                        pyautogui.press('space')
                        mouseLeftClickOn("mlink_ok")
                        sleep(5)
                        waitForVanish("mlink_loading")
                        if appears("mlink_manager", 20):
                            mouseLeftClickOn("mlink_manager")
                        elif appears("mlink_manager_high", 20):
                            mouseLeftClickOn("mlink_manager_high")
                        pyautogui.hotkey('alt', 'f4')
                    change_viewpoint_plan_looking_down()
                    sleep(1)
                    pyautogui.typewrite('SCHVACSETSYSTEMFROMPART',.1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('3029.4783,14144.4025',.1)
                    pyautogui.press('enter')
                    sleep(1)
                    pyautogui.typewrite('SCHVACSTRAIGHT',.1)
                    pyautogui.press('enter')
                    if appears("select_stock"):
                        mouseLeftClickOn("connects_to")
                        free()
                        pyautogui.typewrite('2339.4796,12240.3655',.1)
                        pyautogui.press('enter')
                    else:
                        pyautogui.click(button='right')
                        mouseLeftClickOn("select_stock1")
                        sleep(1)
                        mouseLeftClickOn("connects_to")
                        free()
                        pyautogui.typewrite('2339.4796,12240.3655', .1)
                        pyautogui.press('enter')
                    mouseLeftClickOn("ok6")
                    free()
                    sleep(1)
                    pyautogui.typewrite('2500,12000', .2)
                    pyautogui.press('enter')
                    pyautogui.typewrite('500', .1)
                    pyautogui.press('enter')
                    pyautogui.hotkey('ctrl','s')
                else:
                    raise AssertionError("mlink_manager did not appear")
            else:
                raise AssertionError("test_hvac_tab did not appear")
        else:
            raise AssertionError(" Ship Con Navigation did not open")
        self.logger.info('Start:\t Checking Module- connecting_parts')

class TCP2_HVAC_CatalogError(Exception):
    pass
