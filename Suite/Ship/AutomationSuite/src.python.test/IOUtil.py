from pyautogui import locateCenterOnScreen, moveTo, click, doubleClick, hotkey, press, typewrite, screenshot
from screenObjUtil import exists, appears, midpoint, location
from Initialize import Initialize
import Logger

# Move mouse to an object recognized from image in parameter
def mouseMoveTo(screenObj=None, region=None):
    try:
        obj = Initialize()
        path = obj.get_image_directory()
        if path is None:
            path = obj.read_path_from_module_file()
        str_path_screen_obj = path + screenObj + ".png"
        if region is None:
            loc_screen_obj = locateCenterOnScreen(str_path_screen_obj, grayscale=True)
        else:
            loc_reg_obj = location(region)
            loc_screen_obj = locateCenterOnScreen(str_path_screen_obj, region=loc_reg_obj, grayscale=True)
    except TypeError:
        raise AssertionError("Item %s could not be located. Type mismatched" % screenObj)
    except IOError:
        raise AssertionError("No such file named %s.png presents in the test_image directory" % screenObj)
    if loc_screen_obj is None:
        Logger.add_missed_image(screenObj)
        AssertionError("%s is not located on the screen" % screenObj)
    else:
        moveTo(loc_screen_obj)

# Move mouse to an object recognized from co-ordinates in parameter
def mouseMoveToCoordinates(x,y):
    moveTo(x,y)
    Logger.info("Mouse has been moved to :" + str(x) +","+ str(y) + " Co-ordinate")

# Left click mouse on an object recognized from image in parameter
def mouseLeftClickOn(screenObj=None, region=None):
    if region is not None:
        if appears(region, 10):
            mouseMoveTo(screenObj, region)
            click(button="left")
            Logger.info(screenObj + " appeared [LEFT CLICK]")
        else:
            Logger.add_missed_image(screenObj)
    else:
        if appears(screenObj, 10):
            mouseMoveTo(screenObj, region)
            click(button="left")
            Logger.info(screenObj + " appeared [LEFT CLICK]")
        else:
            Logger.add_missed_image(screenObj)

# Mouse double click on an object recognized from image in parameter
def mouseDoubleClickOn(screenObj=None,region=None):
    if region is not None:
        if appears(region, 10):
            mouseMoveTo(screenObj, region)
            doubleClick(button="left")
            Logger.info(screenObj + " appeared [DOUBLE CLICK]")
        else:
            Logger.add_missed_image(screenObj)
    else:
        if appears(screenObj, 10):
            mouseMoveTo(screenObj, region)
            doubleClick(button="left")
            Logger.info(screenObj + " appeared [DOUBLE CLICK]")
        else:
            Logger.add_missed_image(screenObj)

# Mouse right click on an object recognized from image in parameter
def mouseRightClickOn(screenObj=None, region=None):
    if region is not None:
        if appears(region, 10):
            mouseMoveTo(screenObj, region)
            click(button="right")
            Logger.info(screenObj + " appeared [RIGHT CLICK]")
        else:
            Logger.add_missed_image(screenObj)
    else:
        if appears(screenObj, 10):
            mouseMoveTo(screenObj, region)
            click(button="right")
            Logger.info(screenObj + " appeared [RIGHT CLICK]")
        else:
            Logger.add_missed_image(screenObj)

# Keep clicking an object as long as it appears on the screen
def mouseClickAppears(screenObj):
    while appears(screenObj, 2):
        mouseLeftClickOn(screenObj)

# Scroll down until an object is found
def goDownUntilAppears(screenObj, slidebarDown):
    mouseMoveTo(slidebarDown)
    while exists(screenObj) == False:
        click()

# perform keyboard hot key actions
def hotKeyAction(specialkey, character):
    hotkey(specialkey, character)
    Logger.info("Hotkey pressed for " + specialkey + " + " + character)

# perform keyboard actions for single key at a time
def pressAction(actionType, count=None):
    if count is None:
        press(actionType)
        Logger.info("Key pressed for " + actionType)
    else:
        press(actionType, count, 1)
        Logger.info("Key pressed for " + actionType)

# perform keyboard type write action
def typeWriteAction(wordToType):
    typewrite(wordToType, 0.2)
    Logger.info("Type wrote the word " + wordToType)

# for taking screenshot. The screenshot will be stored in AutomationSuite\test.images folder
def takeScreenShot(screenshotName, left=None, top=None, width=None, height=None):
    if left is None or top is None or width is None or height is None:
        im = screenshot(screenshotName + '.png')
        print("Full screen screenshot taken")
        print(im)
    else:
        im = screenshot(screenshotName + '.png', region=(left, top, width, height))
        print("Selected portion of screen's screenshot taken")
        print(im)