"""
*** TCP4_SC_Essentials.py ***

Automation script for ShipConstructor Essential Training Curriculum

Covers the following sections:
-  Navigator (p 28-50)
-  ShipConstructor and AutoCAD Design Tools excluding Orbit/Pan/Zoom (p 51 – 66)
-  Product Hierarchy (p 70 – 116)
-  ShipConstructor Utilities (p 117 – 203)

"""

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

import random, os, pyautogui

from time import sleep

from logger_extended import logger_extended

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseClickAppears, mouseRightClickOn, mouseDoubleClickOn

from screenObjUtil import exists, appears, waitForVanish

from SSIUtil import maximize, free, line, visual_style_change, free_white, change_viewpoint_body_looking_fwd, conceptual_visual, \
    wireframe2d_visual, reset_viewport, change_viewpoint_stbd_to_prt, change_viewpoint_from_aft_stbd_up, change_viewpoint_from_fwd_stbd_up, \
    change_viewpoint_from_aft_up, change_viewpoint_plan_looking_down, change_viewpoint_from_aft_prt_up

class TCP4_SC_Essentials(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP7_SC_Essential')


    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC04_ShiCon_Essentials")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_sc_essentials")
        obj.launch_app()
        obj1.project_deployer(module="sc_essentials")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    def register_project(self):
        maximize()
        sleep(3)
        obj= AppUtil()
        obj.ribbon_load("hull_and_structure_tab")
        # obj.ribbon_load("mechanical_tab")
        sleep(5)
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("deploy_sc_essential_list", "NewDeploy_sc_essentials")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_sc_essentials")

    # The Navigator Window

    def navigator_window(self):
        self.logger.info('Start:\t Checking Module- navigator_window')
        sleep(1)
        sleep(1)
        mouseDoubleClickOn("shipconstructor_tab_new")
        if appears("structure_main_pane"):
            self.logger.info("Structure main pane appeared")
            mouseLeftClickOn("structure_main_pane")
            if appears("navigator_button_new"):
                self.logger.info("Navigator button appeared")
                mouseLeftClickOn("navigator_button_new")
                if appears("shipcon_navi"):
                    self.logger.info("ShipCon Navigator appeared")
                else:
                    self.logger.error("shipcon_navigator did not appear")
                    raise AssertionError("shipcon_navigator did not appear")
            else:
                self.logger.error("navigator_button_new did not appear")
                raise AssertionError("navigator_button_new did not appear")
        else:
            self.logger.error("structure_main_pane did not appear")
            raise AssertionError("structure_main_pane did not appear")
        self.logger.info('End:\t Checking Module- navigator_window')

    # Customizing the Component List

    def customizing_component_list(self):
        self.logger.info('Start:\t Checking Module- customizing_component_list')
        sleep(1)
        if appears("ssi_node2",10):
            mouseLeftClickOn("ssi_node2")
        if appears("u01_node"):
            self.logger.info("U01 node appeared")
            mouseLeftClickOn("u01_node")
            if appears("u01_component_list"):
                self.logger.info("Component list appeared")
                mouseRightClickOn("distributed_system")
                if appears("customize_button"):
                    self.logger.info("Customize button appeared")
                    mouseLeftClickOn("customize_button")
                    if appears('customize_navigator_window'):
                        mouseLeftClickOn("customize_navigator_window_pipe")
                        pyautogui.press('space')
                        pyautogui.press('down')
                        pyautogui.press('space')
                        pyautogui.press('down')
                        pyautogui.press('space')
                        mouseLeftClickOn("customize_navigator_window_ok")
                        if appears("u01_component_list_after_change"):
                            mouseRightClickOn("distributed_system")
                            mouseLeftClickOn("customize_button")
                            mouseLeftClickOn("customize_navigator_window_pipe_unchecked")
                            pyautogui.press('space')
                            pyautogui.press('down')
                            pyautogui.press('space')
                            pyautogui.press('down')
                            pyautogui.press('space')
                            mouseLeftClickOn("customize_navigator_window_ok")
                            self.logger.info("Customizing component list was done")
                        else:
                            self.logger.error("Component list did not get update")
                            raise AssertionError("Component list did not get update")
                    else:
                        self.logger.error("customize_navigator_window did not appear")
                        raise AssertionError("customize_navigator_window did not appear")
                else:
                    self.logger.error("customize_button did not appear")
                    raise AssertionError("customize_button did not appear")
            else:
                self.logger.error("u01_component_list did not appear")
                raise AssertionError("u01_component_list did not appear")
        else:
            self.logger.error("u01_node did not appear")
            raise AssertionError("u01_node did not appear")
        self.logger.info('End:\t Checking Module- customizing_component_list')

    # The Project

    def the_project(self):
        self.logger.info('Start:\t Checking Module- the_project')
        sleep(1)
        if appears("sc_navi_change_project"):
            mouseMoveTo("sc_navi_change_project")
            self.logger.info('Change Project button appeared')
        else:
            self.logger.error("sc_navi_change_project did not appear")
            raise AssertionError("sc_navi_change_project did not appear")
        if appears("sc_navi_reload_db"):
            mouseMoveTo("sc_navi_reload_db")
            self.logger.info('sc_navi_reload_db button appeared')
        else:
            self.logger.error("sc_navi_reload_db did not appear")
            raise AssertionError("sc_navi_reload_db did not appear")
        if appears("sc_navi_user_permission"):
            mouseMoveTo("sc_navi_user_permission")
            self.logger.info('sc_navi_user_permission appeared')
        else:
            self.logger.error("sc_navi_user_permission did not appear")
            raise AssertionError("sc_navi_user_permission did not appear")
        if appears("sc_navi_settings"):
            mouseMoveTo("sc_navi_settings")
            self.logger.info('sc_navi_settings appeared')
        else:
            self.logger.error("sc_navi_settings did not appear")
            raise AssertionError("sc_navi_settings did not appear")
        if appears("sc_navi_revision"):
            mouseMoveTo("sc_navi_revision")
            self.logger.info('sc_navi_revision appeared')
        else:
            self.logger.error("sc_navi_revision did not appear")
            raise AssertionError("sc_navi_revision did not appear")
        if appears("sc_navi_open"):
            mouseMoveTo("sc_navi_open")
            self.logger.info('sc_navi_open appeared')
        else:
            self.logger.error("sc_navi_open did not appear")
            raise AssertionError("sc_navi_open did not appear")
        if appears("sc_navi_new_unit"):
            mouseLeftClickOn("sc_navi_new_unit")
            self.logger.info('sc_navi_new_unit appeared')
        else:
            self.logger.error("sc_navi_new_unit did not appear")
            raise AssertionError("sc_navi_new_unit did not appear")
        self.logger.info('End:\t Checking Module- the_project')

    # Working with Navigator

    def working_with_navigator(self):
        self.logger.info('Start:\t Checking Module- working_with_navigator')
        sleep(5)
        if appears("sc_navi_new_unit", 5):
            mouseLeftClickOn("sc_navi_new_unit")
        else:
            mouseLeftClickOn("new_unit")
        sleep(5)
        pyautogui.typewrite(['U', '0', '3'], interval=0.1)
        sleep(1)
        pyautogui.press('enter')
        sleep(5)
        if appears("u03_tab"):
            self.logger.info("U03 drawing was created")
            if appears("structure_main_pane"):
                mouseLeftClickOn("structure_main_pane")
                if appears("navigator_button_new"):
                    mouseLeftClickOn("navigator_button_new")
                else:
                    raise AssertionError("navigator_button_new did not appear")
            else:
                raise AssertionError("structure_main_pane did not appear")
            if appears("shipcon_navi"):
                mouseLeftClickOn("ssi_node")
                mouseLeftClickOn("remove_node_highlight")
                mouseRightClickOn("u03_unit_list")
                if appears("u03_shell_delete"):
                    mouseLeftClickOn("u03_shell_delete")
                    if appears("delete_unit_window"):
                        self.logger.info("Delete option was checked")
                        pyautogui.press('esc')
                    else:
                        raise AssertionError("delete_unit_window did not appear")
                else:
                    raise AssertionError("u03_shell_delete did not appear")
        else:
            raise AssertionError("u03_tab did not appear")
        self.logger.info('End:\t Checking Module- working_with_navigator')


        # Working with Drawings

    def creating_drawings(self):
        self.logger.info('Start:\t Checking Module- creating_drawings')
        sleep(1)
        if appears("pipe"):
            mouseLeftClickOn("pipe")
            mouseLeftClickOn("piping")
            mouseLeftClickOn("new_pipe")
            if appears("new_pipe_window"):
                pyautogui.typewrite("741_U03_Salt water cooling lines", .1)
                if appears("open_new_drawing2", 4):
                    mouseLeftClickOn("open_new_drawing2._check", "open_new_drawing2")
                # if appears("pipe_3vp_metric", 4):
                #     mouseLeftClickOn("pipe_3vp_metric")
                if appears("1vp_metric", 4):
                    mouseLeftClickOn("1vp_metric")
                mouseLeftClickOn("pipe_drawing_options_windows_ok")
                # sleep(5)
                # pyautogui.press('enter')
                # sleep(3)
                if appears("drawing_warning"):
                    mouseLeftClickOn("drawing_warning")
                if appears("741_u03_tab"):
                    free()
                    self.logger.info("741_U03_Salt water cooling lines was created")
                    pyautogui.hotkey('ctrl', 's')
                else:
                    raise AssertionError("741_u03_tab did not appear")
            else:
                raise AssertionError("new pipe window did not appear")
        else:
            raise AssertionError("pipe option did not appear")
        self.logger.info('End:\t Checking Module- creating_drawings')

    def opening_drawings_new(self):
        mouseLeftClickOn("ssi_node1")
        mouseLeftClickOn("navigator_u02_node")
        mouseLeftClickOn("structure_node2")
        mouseMoveTo("navi_slide_up")
        while exists("u02_l_750PS") == False:
            pyautogui.click(clicks=10)
        if appears("u02_l_750PS"):
            mouseDoubleClickOn("u02_l_750PS")
            sleep(10)
        else:
            raise AssertionError("u02_l_750PS did not appear")
        if appears("u02_l_750PS_tab"):
            self.logger.info("u02_l_750PS was opened")
            free()
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.press('esc')
            # visual_style_change("conceptual_visual")
            # sleep(3)
            # if appears("750ps_conceptual", 5):
            #     self.logger.info('Visual Style was changed')
            # else:
            #     raise AssertionError("Visual style was not changed")
            # visual_style_change("2d_wireframe")
            free()
            pyautogui.hotkey('ctrl', 's')
            sleep(3)
        else:
            raise AssertionError("u02_l_750PS_tab did not appear")

    def opening_drawings(self):
        self.logger.info('Start:\t Checking Module- opening_drawings')
        sleep(2)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("structure_node2")
            mouseMoveTo("navi_slide_up")
            while exists("u02_l_750PS") == False:
                pyautogui.click(clicks=10)
            if appears("u02_l_750PS"):
                mouseDoubleClickOn("u02_l_750PS")
                sleep(10)
                if appears("u02_l_750PS_tab"):
                    self.logger.info("u02_l_750PS was opened")
                    visual_style_change("conceptual_visual")
                    sleep(3)
                    if appears("750ps_conceptual", 5):
                        self.logger.info('Visual Style was changed')
                    else:
                        raise AssertionError("Visual style was not changed")
                    visual_style_change("2d_wireframe")
                    free()
                    pyautogui.hotkey('ctrl', 's')
                    sleep(3)
                else:
                    raise AssertionError("u02_l_750PS_tab did not appear")
            else:
                raise AssertionError("u02_l_750PS did not appear")
        else:
            raise AssertionError("shipcon_navi did not appear")
        self.logger.info('End:\t Checking Module- opening_drawings')

    # Open a ShipConstructor Database-only Drawing

    def database_only_drawing(self):
        self.logger.info('Start:\t Checking Module- database_only_drawing')
        sleep(1)
        free()
        pyautogui.hotkey('ctrl', 's')
        sleep(5)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("structure_node2")
            mouseMoveTo("navi_slide_up")
            while exists("u01_fr66") == False:
                pyautogui.click(clicks=10)
            if appears("u01_fr66"):
                self.logger.info("U01 Fr66 database only drawing was found")
                mouseDoubleClickOn("u01_fr66")
                if appears("warning11", 5):
                    pyautogui.press('enter')
                if appears("select_template_window"):
                    mouseLeftClickOn("structure_template_metric")
                    mouseLeftClickOn("select_template_window_ok")
                    if appears("warning11", 5):
                        pyautogui.press('enter')
                    sleep(20)
                    if appears("u01_fr66_tab"):
                        free()
                        self.logger.info("u01_fr66 database only drawing was opened")
                        sleep(5)
                        mouseLeftClickOn("structure_main_pane")
                        mouseLeftClickOn("navigator_button_new")
                        if appears("shipcon_navi"):
                            if appears("u01_fr66_1"):
                                mouseDoubleClickOn("u01_fr66_1")
                        if appears("u01_fr66_tab"):
                            pyautogui.hotkey('ctrl', 's')
                        sleep(5)
                    else:
                        raise AssertionError("u01_fr66_tab did not appear")
                else:
                    raise AssertionError("select_template_window did not appear")
            else:
                raise AssertionError("u01_fr66 did not appear")
        else:
            raise AssertionError("shipcon_navi did not appear")
        self.logger.info('End:\t Checking Module- database_only_drawing')

    def deleting_drawing(self):
        self.logger.info('Start:\t Checking Module- deleting_drawing')
        sleep(1)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u03_node2")
            mouseLeftClickOn("pipe")
            mouseRightClickOn("741_u03_node")
            if appears("u03_shell_delete"):
                mouseLeftClickOn("u03_shell_delete")
                if appears("warning11", 5):
                    pyautogui.press('enter')
                self.logger.info("Drawing was deleted")
            else:
                raise AssertionError("Delete option did not appear")
            self.logger.info('End:\t Checking Module- deleting_drawing')

    def show_out_of_date_new(self):
        self.logger.info('Start:\t Checking Module- show_out_of_date')
        sleep(1)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            sleep(1)
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u03_node2")
            mouseLeftClickOn("pipe")
            if appears("sc_navi_show_out_dated"):
                mouseLeftClickOn("sc_navi_show_out_dated")
                self.logger.info("Show out of the date button was checked")
            else:
                raise AssertionError("sc_navi_show_out_dated did not appear")
        self.logger.info('End:\t Checking Module- show_out_of_date')

    def show_out_of_date(self):
        self.logger.info('Start:\t Checking Module- show_out_of_date')
        sleep(1)
        if appears("sc_navi_show_out_dated"):
            mouseLeftClickOn("sc_navi_show_out_dated")
            self.logger.info("Show out of the date button was checked")
        else:
            raise AssertionError("sc_navi_show_out_dated did not appear")
        self.logger.info('End:\t Checking Module- show_out_of_date')

        # Additional Navigator Tools

    def properties_palette(self):
        self.logger.info('Start:\t Checking Module- properties_palette')
        sleep(1)
        mouseLeftClickOn("ssi_node")
        mouseLeftClickOn("u01_node")
        mouseLeftClickOn("structure_node2")
        if appears("u01_fr65"):
            mouseDoubleClickOn("u01_fr65")
            if appears("warning11", 5):
                pyautogui.press('enter')
            sleep(5)
            if appears("u01_fr66_tab"):
                self.logger.info("u01_fr66 was opened")
                change_viewpoint_from_aft_stbd_up()
                sleep(1)
                free()
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('1245.7580,1233.5579', interval=0.1)
                pyautogui.press('enter')
                pyautogui.click(button='right', clicks=2, interval=0.25)
                mouseLeftClickOn("part_information_button")
                mouseRightClickOn('piecemark')
                mouseLeftClickOn("zoom_to_part")
                sleep(1)
                pyautogui.press('esc')
                free()
                pyautogui.hotkey('ctrl', '1')
                self.logger.info("Properties panel was opened via Hot Key")
                sleep(2)
                if appears("extracted_line_properties"):
                    mouseLeftClickOn("extracted_line_properties")
                    mouseLeftClickOn("properties_close_button")
                    free()
                    self.logger.info("Properties palette was checked")
                else:
                    raise AssertionError("Properties panel did not appear")
            else:
                raise AssertionError("u01_fr66_tab did not appear")
        else:
            raise AssertionError("u01_fr65 did not appear")
        self.logger.info('End:\t Checking Module- properties_palette')

    def layers_option(self):
        self.logger.info('Start:\t Checking Module- layers_option')
        sleep(1)
        free()
        pyautogui.press('esc')
        mouseDoubleClickOn("home_button")
        if appears("layer_button"):
            mouseLeftClickOn("layer_button")
        else:
            if appears("layer_properties_button"):
                mouseLeftClickOn("layer_properties_button")
                free()
                if appears("layer_properties_manager_unhigh"):
                    self.logger.info("Layer properties manager was opened")
                    mouseLeftClickOn("layer_properties_manager_unhigh")
                    mouseLeftClickOn("layer_properties_manager_close")
                    free()
                    self.logger.info("Layer option was checked")
                else:
                    raise AssertionError("layer_properties_manager did not appear")
            else:
                raise AssertionError("layer_properties_button did not appear")
            raise AssertionError("layer_button did not appear")
        self.logger.info('End:\t Checking Module- layers_option')

    def list(self):
        self.logger.info('Start:\t Checking Module- list')
        sleep(1)
        free()
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('1245.7580,1233.5579', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        mouseLeftClickOn("properties_panel")
        if appears("list_option"):
            self.logger.info("List option was found")
            mouseLeftClickOn("list_option")
            if appears("list_text_region",10):
                pyautogui.press('enter', presses=2)
                sleep(2)
                pyautogui.press('esc')
                free()
                self.logger.info("List option was checked")
            else:
                raise AssertionError("list_text_region did not appear")
        else:
            raise AssertionError("list_option did not appear")
        self.logger.info('End:\t Checking Module- list')

    def grips(self):
        self.logger.info('Start:\t Checking Module- grips')
        sleep(1)
        free()
        pyautogui.press('esc')
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('1245.7580,1233.5579', interval=0.1)
        pyautogui.press('enter')
        pyautogui.click(button='right', clicks=2, interval=0.25)
        mouseLeftClickOn("part_information_button")
        mouseRightClickOn('piecemark')
        mouseLeftClickOn("zoom_to_part")
        sleep(1)
        pyautogui.press('esc')
        free()
        pyautogui.press('esc')
        pyautogui.press('esc')
        sleep(5)
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('1149.4141,1354.8790', interval=0.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(3)
        pyautogui.moveTo(780, 214)
        pyautogui.click()
        sleep(4)
        pyautogui.moveTo(722, 197)
        sleep(2)
        pyautogui.click()
        if appears("update_related_objects2"):
            self.logger.info("Update related object window was appeared")
            pyautogui.press('enter')
            pyautogui.hotkey('ctrl', 's')
            sleep(5)
            self.logger.info("Grip was checked")
        else:
            raise AssertionError("update_related_objects2 did not appear")
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("hull_node")
            if appears("u02_shell"):
                mouseDoubleClickOn("u02_shell")
                if appears("u02_shell_tab"):
                    mouseLeftClickOn("layer_button")
                    mouseLeftClickOn("current_layer_dropdown")
                    if appears("lines_layer_region", 5):
                        mouseLeftClickOn("bulb_on", "lines_layer_region")
                    if appears("surface_layer_region", 5):
                        mouseLeftClickOn("surface_layer_region_bulb", "surface_layer_region")
                    free()
                    pyautogui.press('esc')

                    mouseDoubleClickOn("structure_ribbon_button")
                    pyautogui.press('esc')
                    change_viewpoint_from_aft_stbd_up()
                    free()
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('29251.7554,5340.3938', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.moveTo(686, 509)
                    pyautogui.click()
                    pyautogui.moveTo(684, 515)
                    pyautogui.click()
                    free()
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    pyautogui.press('esc')

                    pyautogui.hotkey('ctrl', 's')
                    sleep(3)
                else:
                    raise AssertionError("u02_shell_tab did not appear")
            else:
                raise AssertionError("u02_shell did not appear")
        else:
            raise AssertionError("ShipCon Navi did not appear")
        self.logger.info('End:\t Checking Module- grips')

    def visual_styles(self):
        self.logger.info('Start:\t Checking Module- visual_styles')
        sleep(1)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("structure_node2")
            if appears("u01_fr65_high", 10):
                mouseDoubleClickOn("u01_fr65_high")
            else:
                mouseDoubleClickOn("u01_fr65")
            if appears("warning11", 5):
                pyautogui.press('enter')
            if appears("u01_fr66_tab"):
                self.logger.info("U01 Fr65 drawing was opened")
                sleep(10)
                sleep(10)
                free()
                pyautogui.typewrite('scmlink', .2)
                pyautogui.press('enter')
                if appears("mlink_manager",10):
                    if appears("mlink_DWG",10):
                        mouseLeftClickOn("mlink_DWG")
                    else:
                        mouseLeftClickOn("mlink_DWG_dropdown")
                        sleep(2)
                        mouseLeftClickOn("mlink_Attach_DWG")
                    if appears("mlink_DWG_window",10):
                        sleep(2)
                        mouseLeftClickOn("mlink_u01_fr67")
                        pyautogui.press('space')
                        mouseLeftClickOn("mlink_u01_fr69")
                        pyautogui.press('space')
                        mouseMoveTo("mlink_scroll_down")
                        pyautogui.click(clicks=5)
                        mouseLeftClickOn("mlink_lngbhd")
                        pyautogui.press('right')
                        mouseLeftClickOn("u01_cl")
                        pyautogui.press('down', presses=2)
                        # mouseLeftClickOn("mlink_750ps")
                        pyautogui.press('space')
                        pyautogui.press('down')
                        pyautogui.press('space')
                        pyautogui.press('down')
                        pyautogui.press('space')
                        pyautogui.press('down')
                        pyautogui.press('space')
                        mouseLeftClickOn("mlink_ok")
                        waitForVanish("mlink_loading")
                        sleep(5)
                        if appears("mlink_manager",20):
                            mouseLeftClickOn("mlink_manager")
                        elif appears("mlink_manager_high",20):
                            mouseLeftClickOn("mlink_manager_high")
                        # if appears("mlink_cross_region1",20):
                        #     mouseLeftClickOn("mlink_cross1","mlink_cross_region1")
                        # elif appears("mlink_cross_region",20):
                        #     mouseLeftClickOn("mlink_cross","mlink_cross_region")
                        #     pyautogui.click(clicks=2)
                        # mouseLeftClickOn("mlink_manager")
                        pyautogui.hotkey('alt', 'f4')
                    else:
                        raise AssertionError("mlink DWG window didn't appear.")
                    self.logger.info("Drawings were included using Model link manager")
                    sleep(15)
                    change_viewpoint_from_fwd_stbd_up()
                    visual_style_change("realistic_view", "realistic_view2", "realistic_view_region")
                    sleep(3)
                    visual_style_change("2d_wireframe")
                    pyautogui.hotkey('ctrl', 's')
                    self.logger.info("Visual Style was chacked")
                    sleep(3)
                else:
                    raise AssertionError("mlink_manager did not appear")
            else:
                raise AssertionError("u01_fr65 tab did not appear")
        else:
            raise AssertionError("ShipCOn Navi did not appear")
        self.logger.info('End:\t Checking Module- visual_styles')

    # Product Hierarchy

    def product_hierarchy_tools(self):
        self.logger.info('Start:\t Checking Module- product_hierarchy_tools')
        sleep(1)
        mouseDoubleClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("structure_main_pane")
        if appears("product_hierarchy_button"):
            mouseLeftClickOn("product_hierarchy_button")
            if appears("product_hierarchy_window",20):
                self.logger.info("Product hierarchy panel was opened")
                free()
                if appears("prod_hier_tools",10):
                    mouseMoveTo("prod_hier_tools")
                    while exists("prod_hier_tools_prod_hier") == False:
                        pyautogui.click()
                    mouseLeftClickOn("prod_hier_tools_prod_hier")
                    mouseLeftClickOn('new_prod_hier')
                    if appears("new_prod_hier_window"):
                        pyautogui.typewrite('SWBS', .2)
                        pyautogui.press('enter')
                        sleep(6)
                        if appears("SWBS_prod_hier", 10):
                            self.logger.info("SWBS Product Hierarchy was created")
                        else:
                            raise AssertionError("SWBS_prod_hier did not appear")
                    else:
                        raise AssertionError("new_prod_hier_window did not appear")
                else:
                    raise AssertionError("prod_hier_tools did not appear")

            else:
                raise AssertionError("product_hierarchy_window did not appear")
        else:
            raise AssertionError("product_hierarchy_button did not appear")
        self.logger.info('End:\t Checking Module- product_hierarchy_tools')

    def copy_product_hierarchy(self):
        self.logger.info('Start:\t Checking Module- copy_product_hierarchy')
        sleep(1)
        sleep(1)
        mouseMoveTo("prod_hier_tools")
        while exists("prod_hier_tools_prod_hier") == False:
            pyautogui.click()
        mouseLeftClickOn("prod_hier_tools_prod_hier")
        mouseLeftClickOn("copy_prod_hier")
        if appears("copy_prod_hier_window"):
            pyautogui.typewrite('Build Strategy Ship 2', .1)
            pyautogui.press('enter')
            if appears("copyingProductHierarchy_WarningWindow"):
                pyautogui.press('enter')
            else:
                print("No warning message")
            sleep(15)
            if appears("BuildStrategyShip_2", 5):
                self.logger.info("Build Strategy Ship 2 was created")
        else:
            raise AssertionError("copy_prod_hier_window did not appear")
        self.logger.info('End:\t Checking Module- copy_product_hierarchy')

    def export_to_another_shipCon_project(self):
        self.logger.info('Start:\t Checking Module- export_to_another_shipCon_project')
        sleep(1)
        mouseLeftClickOn("BuildStrategyShip_2")
        # mouseLeftClickOn("SWBS_prod_hier")
        mouseMoveTo("prod_hier_tools")
        while exists("prod_hier_tools_prod_hier") == False:
            pyautogui.click()
        mouseLeftClickOn("prod_hier_tools_prod_hier")
        mouseLeftClickOn("export_prod_hier")
        if appears("save_as_window"):
            self.logger.info("'Save as' window was appeared")
            a = random.randint(1, 1000)
            pyautogui.typewrite(str(a), .1)
            pyautogui.press('enter')
            if appears("same_name_warning", 5):
                pyautogui.press('left')
                pyautogui.press('enter')
            self.logger.info("Export to XML was done")
        else:
            raise AssertionError("save_as_window did not appear")
        self.logger.info('End:\t Checking Module- export_to_another_shipCon_project')

    def import_from_another_shipCon_project(self):
        self.logger.info('Start:\t Checking Module- import_from_another_shipCon_project')

        mouseMoveTo("prod_hier_tools")
        while exists("prod_hier_tools_prod_hier") == False:
            pyautogui.click()
        mouseLeftClickOn("prod_hier_tools_prod_hier")
        if appears("import_prod_hier"):
            mouseMoveTo("import_prod_hier")
            sleep(1)
            free()
            mouseLeftClickOn("product_hierarchy_window")
            mouseLeftClickOn("close_product_hierarchy_button")
            self.logger.info("Import from another shipCon project was checked")
            free()
            sleep(1)
            pyautogui.hotkey('ctrl', 's')
        else:
            raise AssertionError("import_prod_hier option did not appear")
        self.logger.info('End:\t Checking Module- import_from_another_shipCon_project')

    # Set up Product Hierarchy Drawings

    def create_product_hierarchy_drawing(self):
        self.logger.info('Start:\t Checking Module- create_product_hierarchy_drawing')
        sleep(1)
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("navigator_button_new")
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("product_hierarchy_node")
            mouseLeftClickOn("product_hierarchy_u02")
            mouseLeftClickOn("new_drawing")
            if appears("new_product_hierarchu_window"):
                self.logger.info("New product hierarchy window was appeared")
                pyautogui.typewrite('U02_Complete', .1)
                mouseLeftClickOn("new_product_hierarchu_window_ok")
                if appears("select_model_drawing_window"):
                    self.logger.info("select model drawing was appeared")
                    mouseLeftClickOn("u02_in_model_drawing")
                    pyautogui.press('right')
                    mouseLeftClickOn("select_model_structure_click", "select_model_structure_region")
                    mouseLeftClickOn("select_model_drawing_window_ok")
                    waitForVanish("working_dialog")
                    sleep(20)
                    free()
                    self.logger.info("Product Hierarchy drawing was created")
                    pyautogui.hotkey('ctrl', 's')
                    sleep(3)
                else:
                    raise AssertionError("select_model_drawing_window did not appear")
            else:
                raise AssertionError("new_product_hierarchu_window did not appear")
        else:
            raise AssertionError("shipcon_navi did not appear")
        self.logger.info('End:\t Checking Module- create_product_hierarchy_drawing')

    def rename_product_hierarchy_drawing(self):
        self.logger.info('Start:\t Checking Module- rename_product_hierarchy_drawing')
        sleep(2)
        free()
        pyautogui.press('esc',presses=3)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseDoubleClickOn("u0200")
            sleep(10)
        else:
            raise AssertionError("shipcon_navi did not appear")
        if appears("u0200_tab"):
            self.logger.info("U0200 tab was opened")
            sleep(2)
            pyautogui.typewrite('SCNAVIGATE', .1)
            sleep(2)
            pyautogui.press('enter')
            if appears("shipcon_navi"):
                mouseRightClickOn("u02_complete")
                if appears("drawing_rename"):
                    mouseLeftClickOn("drawing_rename")
                    pyautogui.typewrite("U02_Complete_Updated", .1)
                    pyautogui.press('enter')
                    if appears("u02_complete_high"):
                        self.logger.info("Renamed successfully")
                    else:
                        raise AssertionError("u02_complete wasn't renamed")
                else:
                    raise AssertionError("drawing_rename did not appear")
        else:
            raise AssertionError("u0200_tab did not appear")

        self.logger.info('End:\t Checking Module- rename_product_hierarchy_drawing')


    def delete_product_hierarchy_drawing(self):
        self.logger.info('Start:\t Checking Module- delete_product_hierarchy_drawing')
        sleep(2)
        mouseRightClickOn("u02_complete_high")
        if appears("delete_drawing"):
            self.logger.info("Delete option appeared")
            pyautogui.press('esc')
            sleep(1)
            pyautogui.press('esc')
        else:
            raise AssertionError("delete_drawing option did not appear")
        self.logger.info('End:\t Checking Module- delete_product_hierarchy_drawing')

    # Product Hierarchy Drawing Interactions

    def hide_show_find_zoom(self):
        self.logger.info('Start:\t Checking Module- hide_show_find_zoom')
        sleep(1)
        free()
        pyautogui.hotkey('ctrl', 's')
        sleep(3)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseDoubleClickOn("u02_complete")
            sleep(2)
            if appears("u02_complete_tab"):
                self.logger.info("u02_complete_tab opened successfully")
            else:
                raise AssertionError("u02_complete_tab did not appear")
        else:
            raise AssertionError('ShipCon Navigator did not appear')
        self.logger.info('End:\t Checking Module- hide_show_find_zoom')

    def find_part(self):
        self.logger.info('Start:\t Checking Module- find_part')
        sleep(2)
        change_viewpoint_from_fwd_stbd_up()
        mouseLeftClickOn("structure_main_pane")
        if appears("product_hierarchy_button"):
            mouseLeftClickOn("product_hierarchy_button")
            if appears("product_hierarchy_window"):
                if appears("primary_tab",10):
                    mouseMoveTo("primary_tab")
                else:
                    raise AssertionError("primary_tab did not appear")
                while exists("primaty_tab_selected") == False:
                    pyautogui.click()
                mouseMoveTo("prod_hier_tools")
                while exists("prod_hier_tools_prod_hier") == False:
                    pyautogui.click()
                mouseLeftClickOn("prod_hier_find")
                if appears("find_window_prod_hier"):
                    self.logger.info("Find product hierarchy window was appeared")
                    pyautogui.typewrite('SSI-0201-P65-S02', .1)
                    pyautogui.press('enter')
                    if appears("ssi_020_found",10):
                        self.logger.info("Part was found")
                    elif appears("ssi_020_found2",10):
                        self.logger.info("Part was found")
                    else:
                        raise AssertionError("ssi_020_found did not appear. Part was not found")
                else:
                    raise AssertionError("find_window_prod_hier did not appear")
            else:
                raise AssertionError("product_hierarchy_window did not appear")
            self.logger.info('End:\t Checking Module- find_part')

    # Zoom to a Part

    def zoom_to_part(self):
        self.logger.info('Start:\t Checking Module- zoom_to_part')
        sleep(5)
        if appears("ssi_020_found", 5):
            mouseMoveTo("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseMoveTo("ssi_020_found2")
        while exists("zoom_to_part_option") == False:
            pyautogui.click(button='right')
        mouseLeftClickOn("zoom_to_part_option")
        sleep(2)
        mouseMoveTo("part_bulb", "part_bulb_region")
        self.logger.info("Part bulb was clicked")
        pyautogui.click()
        sleep(2)
        pyautogui.click()
        self.logger.info("Zoom to part was done")
        self.logger.info('End:\t Checking Module- zoom_to_part')

    def copy_names_unload_reload(self):
        self.logger.info('Start:\t Checking Module- copy_names_unload_reload')
        sleep(1)
        if appears("ssi_020_found", 5):
            mouseMoveTo("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseMoveTo("ssi_020_found2")
        while exists("copy_names") == False:
            pyautogui.click(button='right')
        if appears("copy_names"):
            mouseLeftClickOn("copy_names")
            self.logger.info('copy_names option appeared')
        else:
            raise AssertionError("copy_names option did not appear")

        if appears("ssi_020_found", 5):
            mouseMoveTo("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseMoveTo("ssi_020_found2")
        while exists("unload_option") == False:
            pyautogui.click(button='right')
        if appears("unload_option"):
            mouseLeftClickOn("unload_option")
            self.logger.info('unload_option option appeared')
            if appears("unload_warning", 5):
                pyautogui.press('enter')
            if appears("part_unloaded", 5):
                self.logger.info("Part was successfully unloaded")
            elif appears("part_unloaded2", 5):
                self.logger.info("Part was successfully unloaded")
            else:
                raise AssertionError("Indicator did not change after unloading")
        else:
            raise AssertionError("unload_option option did not appear")
        self.logger.info('End:\t Checking Module- copy_names_unload_reload')

    # Assign to Assemblies

    def assign_to_assemblies(self):
        self.logger.info('Start:\t Checking Module- assign_to_assemblies')
        sleep(1)
        free()
        mouseLeftClickOn("product_hierarchy_window")
        mouseMoveTo("prod_hier_scroll_down")
        pyautogui.click(clicks=4)
        if appears("ssi_020_found", 5):
            mouseLeftClickOn("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseLeftClickOn("ssi_020_found2")
        else:
            mouseLeftClickOn("ssi_0201_s01")
        pyautogui.keyDown('shift')
        mouseLeftClickOn("ssi_0201_s06")
        pyautogui.keyUp("shift")
        if appears("ssi_020_found", 5):
            mouseRightClickOn("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseRightClickOn("ssi_020_found2")
        if appears("assign_to_assembly"):
            mouseLeftClickOn("assign_to_assembly")
            if appears("select_assembly_window"):
                mouseDoubleClickOn("expand_column", "expand_column_region")
                if appears("0201_p13", 5):
                    mouseLeftClickOn("0201_p13")
                    mouseLeftClickOn("select_assembly_window_ok")
                    sleep(10)
                    if appears("product_hierarchy_warning"):
                        pyautogui.press('enter')
                    elif appears('prod_hier_warning'):
                        pyautogui.press('enter')
                    self.logger.info("Successfully assigned to assemblies")
                    sleep(1)
                    if appears("ssi_020_found", 5):
                        mouseRightClickOn("ssi_020_found")
                    elif appears("ssi_020_found2", 5):
                        mouseRightClickOn("ssi_020_found2")
                    if appears("assign_to_assembly"):
                        mouseLeftClickOn("assign_to_assembly")
                        if appears("select_assembly_window"):
                            sleep(1)
                            mouseLeftClickOn("0201_es12_region_expand", "0201_es12_region")
                            mouseLeftClickOn("p65")
                            mouseLeftClickOn("select_assembly_window_ok")
                            sleep(5)
                            if appears("product_hierarchy_warning"):
                                pyautogui.press('enter')
                            elif appears('prod_hier_warning'):
                                pyautogui.press('enter')
                            self.logger.info('Successfully re-assigned to Assemblies')
                        else:
                            raise AssertionError("Select assembly window did not appear during re-assigning")
                else:
                    raise AssertionError("0201_p13 did not appear")
            else:
                raise AssertionError("Select Assembly window did not appear")
        else:
            raise AssertionError("assign_to_assembly did not appear")
        self.logger.info('End:\t Checking Module- assign_to_assemblies')

    def properties(self):
        self.logger.info('Start:\t Checking Module- properties')
        sleep(1)
        mouseMoveTo("prod_hier_scroll_down")
        while exists("ssi_020_found") == False:
            pyautogui.click(clicks=5)
            if appears("ssi_020_found2", 1):
                break
        if appears("ssi_020_found", 5):
            mouseLeftClickOn("ssi_020_found")
            sleep(2)
            mouseRightClickOn("ssi_020_found")
        elif appears("ssi_020_found2", 5):
            mouseLeftClickOn("ssi_020_found2")
            sleep(2)
            mouseRightClickOn("ssi_020_found2")
        else:
            raise AssertionError("SSI 0201_P65_S02 did not appear")
        if appears("properties_prod_hier"):
            mouseLeftClickOn("properties_prod_hier")
            sleep(1)
            if appears("ssi_0201_s02_properties"):
                self.logger.info("ssi_0201_s02_properties appeared")
                pyautogui.hotkey('alt', 'f4')
                mouseLeftClickOn("product_hierarchy_window")
                mouseLeftClickOn("close_product_hierarchy_button")
                free()
                self.logger.info("properties section was checked")
                sleep(1)
            else:
                raise AssertionError("ssi_0201_s02_properties did not appear")
        else:
            raise AssertionError("properties_prod_hier did not appear")
        self.logger.info('Start:\t Checking Module- properties')

    # To Assign a User-defined Attribute to a Product Hierarchy

    def assign_a_uda_to_a_product_hierarchy(self):
        self.logger.info('Start:\t Checking Module- assign_a_uda_to_a_product_hierarchy')
        sleep(2)
        mouseLeftClickOn("structure_main_pane")
        if appears("product_hierarchy_button"):
            mouseLeftClickOn("product_hierarchy_button")
            if appears("product_hierarchy_window"):
                mouseMoveTo("prod_hier_tools")
                while exists("prod_hier_tools_prod_hier") == False:
                    pyautogui.click()
                mouseLeftClickOn("uda_prod_hier")
                if appears("assembly_uda_window"):
                    mouseLeftClickOn("description1")
                    pyautogui.keyDown('ctrl')
                    mouseLeftClickOn("description2")
                    pyautogui.keyUp('ctrl')
                    mouseLeftClickOn("swbs_uda")
                    mouseLeftClickOn("uda_right")
                    if appears("description1_no", 5):
                        self.logger.info("UDA was assigned")
                    else:
                        raise AssertionError("Uda was not assigned")
                    if appears("description2_remove_region2", 5):
                        mouseLeftClickOn("description2_remove", "description2_remove_region2")
                    elif appears("description2_remove_region", 5):
                        mouseLeftClickOn("description2_remove", "description2_remove_region")
                    sleep(1)
                    mouseLeftClickOn("uda_left")
                    if exists("description2_remove_region") == False:
                        self.logger.info("UDA was removed")
                    else:
                        raise AssertionError("UDA was not removed")
                    mouseLeftClickOn("assembly_uda_window_ok")
                    sleep(1)
                else:
                    raise AssertionError("assembly_uda_window did not appear")
                self.logger.info('Start:\t Checking Module- assign_a_uda_to_a_product_hierarchy')

    # Set up Assembly Levels

    def setting_up_assembly_levels(self):
        self.logger.info('Start:\t Checking Module- setting_up_assembly_levels')
        sleep(2)
        mouseMoveTo("prod_hier_tools")
        while exists("edit_levels_prod_hier") == False:
            pyautogui.click()
        mouseLeftClickOn("edit_levels_prod_hier")
        if appears("edit_levels_window"):
            mouseLeftClickOn("edit_levels_window_ok")
            self.logger.info("Setting up assemblt levels was checked")
        else:
            raise AssertionError("edit_levels_window did not appear")
        self.logger.info('End:\t Checking Module- setting_up_assembly_levels')

    # Set up Assemblies - Add an Assembly

    def adding_assembly(self):
        self.logger.info('Start:\t Checking Module- adding_assembly')
        sleep(1)
        mouseMoveTo("u03_prod_hier")
        while exists("u03_prod_hier_high2") == False:
            pyautogui.click()
            if appears("u03_prod_hier_high", 5):
                break
            elif appears("u03_prod_hier_high3", 5):
                break
        if appears("u03_prod_hier_high2", 5):
            mouseRightClickOn("u03_prod_hier_high2")
        elif appears("u03_prod_hier_high", 5):
            mouseRightClickOn("u03_prod_hier_high")
        elif appears("u03_prod_hier_high3", 5):
            mouseRightClickOn("u03_prod_hier_high3")

        if appears("new_assembly_prod_hier"):
            mouseLeftClickOn("new_assembly_prod_hier")
            if appears("new_assembly_window"):
                sleep(2)
                if appears("assembly_name_field"):
                    mouseDoubleClickOn("assembly_name_field")
                elif appears("assembly_name_field_1"):
                    mouseDoubleClickOn("assembly_name_field_1")
                else:
                    raise AssertionError("assembly_name_field")
                pyautogui.typewrite('0309', .1)
                sleep(1)
                # free()
                pyautogui.press('tab')
                pyautogui.hotkey('ctrl', 'a')
                sleep(1)
                # mouseDoubleClickOn("new_assembly_number")
                pyautogui.press('4')
                mouseLeftClickOn("acme_premier_a_new_assembly_1")
                mouseLeftClickOn("edit_levels_window_ok")
                if appears("new_assemblies", 5):
                    self.logger.info("new_assemblies were created")
                elif appears("new_assemblies2", 5):
                    self.logger.info("new_assemblies were created")
                else:
                    raise AssertionError("new_assemblies was not created")
            else:
                raise AssertionError("new_assembly_window did not appear")
        else:
            raise AssertionError("new_assembly_prod_hier did not appear")
        self.logger.info('End:\t Checking Module- adding_assembly')

    # Set up Assemblies - Rename an Assembly

    def renaming_an_assembly(self):
        self.logger.info('Start:\t Checking Module- renaming_an_assembly')
        sleep(1)
        if appears("0309_high", 5):
            mouseLeftClickOn("0309_high")
        elif appears("0309_high2", 5):
            mouseLeftClickOn("0309_high2")
        pyautogui.click(button='right')
        mouseLeftClickOn("rename_assemb")
        mouseDoubleClickOn("0309_rename")
        pyautogui.typewrite("0308", .1)
        pyautogui.press('enter')
        if appears("0308_renamed"):
            self.logger.info("Renaming was done")
        elif appears("0308_renamed2"):
            self.logger.info("Renaming was done")
        else:
            self.logger.warn("Renaming was not done")
        self.logger.info('End:\t Checking Module- renaming_an_assembly')

    # Edit an Assembly

    def editing_assembly(self):
        self.logger.info('Start:\t Checking Module- editing_assembly')
        sleep(1)
        if appears("0308_renamed", 5):
            mouseRightClickOn("0308_renamed")
        elif appears("0308_renamed2", 5):
            mouseRightClickOn("0308_renamed2")
        else:
            raise AssertionError("0308_renamed node was not matched with the stored image")
        # mouseRightClickOn("0308_renamed")
        if appears("edit1"):
            mouseLeftClickOn("edit1")
            if appears("edit_assembly_window"):
                mouseLeftClickOn("edit_levels_window_ok")
                self.logger.info("Editing Assembly was checked")
            else:
                raise AssertionError("edit_assembly_window did not appear")
        else:
            raise AssertionError("edit option did not appear")
        self.logger.info('End:\t Checking Module- editing_assembly')

    # Change the Level of an Assembly

    def changing_level_of_an_assembly(self):
        self.logger.info('Start:\t Checking Module- changing_level_of_an_assembly')
        sleep(1)
        if appears("0308_renamed", 5):
            mouseRightClickOn("0308_renamed")
        elif appears("0308_renamed2", 5):
            mouseRightClickOn("0308_renamed2")
        else:
            raise AssertionError("0308_renamed node was not matched with the stored image")

        mouseLeftClickOn("new_product_hierarchy")
        if appears("new_assembly_window"):
            pyautogui.typewrite('0308-ES08', .1)
            mouseLeftClickOn("stage_92_1")
            pyautogui.press('down', presses=8)
            sleep(1)
            mouseLeftClickOn("edit_levels_window_ok")
            if appears("0308_es08_2", 5):
                self.logger.info("0308_es08 was created")
                mouseRightClickOn("0308_es08_2")
            elif appears("0308_es08", 5):
                self.logger.info("0308_es08 was created")
                mouseRightClickOn("0308_es08")
            else:
                raise AssertionError("0308_es08 was not created")
            mouseLeftClickOn("edit1")
            if appears("edit_assembly_window"):
                mouseLeftClickOn("stage_92_1")
                pyautogui.press('down', presses=9)
                sleep(1)
                mouseLeftClickOn("edit_levels_window_ok")
                self.logger.info("Changing level of an assembly was checked")
            else:
                raise AssertionError("edit_assembly_window did not appear")
        else:
            raise AssertionError("new_assembly_window did not appear")
        self.logger.info('End:\t Checking Module- changing_level_of_an_assembly')

    def copy_an_assembly(self):
        self.logger.info('Start:\t Checking Module- copy_an_assembly')
        sleep(2)
        if appears("0308_es08_2", 5):
            self.logger.info("0308_es08 was created")
            mouseRightClickOn("0308_es08_2")
        elif appears("0308_es08", 5):
            self.logger.info("0308_es08 was created")
            mouseRightClickOn("0308_es08")
        mouseLeftClickOn("copy_assembly")
        mouseMoveTo("0310")
        while exists("0310_high") == False:
            pyautogui.click()
            if appears("0310_high_2", 5):
                break
        pyautogui.click(button='right')
        mouseLeftClickOn("paste_assembly")
        if appears("0310_high3", 5):
            mouseMoveTo("0310_high2_click", "0310_high3")
        elif appears("0310_high2", 5):
            mouseMoveTo("0310_high2_click", "0310_high2")
        elif appears("0310_high4", 5):
            mouseMoveTo("0310_high2_click", "0310_high4")
        while exists("0308_es08_copied") == False:
            pyautogui.click()
            if appears("0308_es08_copied2", 5):
                break
        if appears("0308_es08_copied", 5):
            self.logger.info("0308_es08 was successfully copied")
        elif appears("0308_es08_copied2", 5):
            self.logger.info("0308_es08 was successfully copied")
        else:
            raise AssertionError("0308_es08 was not copied")
        self.logger.info('End:\t Checking Module- copy_an_assembly')

    # Set up Assemblies - Deleting Assemblies

    def deleting_assemblies(self):
        self.logger.info('Start:\t Checking Module- deleting_assemblies')
        sleep(1)
        mouseLeftClickOn("prod_hier_scroll_down")
        pyautogui.click(clicks=2)
        mouseLeftClickOn("0308_unhigh")
        pyautogui.click(clicks=3)
        pyautogui.keyDown('shift')
        mouseLeftClickOn("0312")
        pyautogui.keyUp('shift')
        mouseRightClickOn("0310_high")
        if appears("delete_assembly"):
            mouseLeftClickOn("delete_assembly")
            if appears("prod_hier_delete"):
                pyautogui.press('right')
                pyautogui.press('enter')
                self.logger.info('Assemblies were deleted')
            else:
                raise AssertionError("prod_hier_delete did not appear")
        else:
            raise AssertionError("delete_assembly option did not appear")
        self.logger.info('End:\t Checking Module- deleting_assemblies')

    # Set up Product Hierarchy for Training

    def create_training_product_hierarchy(self, object):
        mouseMoveTo("prod_hier_scroll_up")
        while exists("u03_prod_hier") == False:
            if appears("u03_prod_hier_high4", 5):
                break
            elif appears("u03_prod_hier_high5", 5):
                break
            pyautogui.click(clicks=3)

        if appears("u03_prod_hier", 5):
            mouseLeftClickOn("u03_prod_hier")
            while exists("u03_prod_hier_high") == False:
                pyautogui.click()
                if appears("u03_prod_hier_high4", 5):
                    break
            pyautogui.click(button='right')
            mouseRightClickOn("u03_prod_hier")
            mouseLeftClickOn("new_prod_hier2")

        elif appears("u03_prod_hier_high4", 5):
            mouseMoveTo("u03_prod_hier_high4")
            mouseLeftClickOn("u03_prod_hier_high4")
            while exists("new_prod_hier2") == False:
                pyautogui.click(button='right')
            mouseLeftClickOn("new_prod_hier2")

        elif appears("u03_prod_hier_high5", 5):
            mouseMoveTo("u03_prod_hier_high5")
            mouseLeftClickOn("u03_prod_hier_high5")
            while exists("new_prod_hier2") == False:
                pyautogui.click(button='right')
            mouseLeftClickOn("new_prod_hier2")

        if appears("new_assembly_window"):
            pyautogui.typewrite(object, .1)
            pyautogui.press('enter')
            mouseLeftClickOn("new_assembly_window_ok")
            self.logger.info(object + " product hierarchy was created")
        else:
            raise AssertionError(object + " was not created")

    def setting_up_product_hierarchy_for_training(self):
        self.logger.info('Start:\t Checking Module- setting_up_product_hierarchy_for_training')
        sleep(1)
        # warn("The icon of U03 node in product hierarchy tree does not get matched with the captured images sometimes. That results in failure.")
        self.create_training_product_hierarchy(object="MDECK")
        sleep(2)
        self.create_training_product_hierarchy(object="TTOP")
        sleep(2)
        self.create_training_product_hierarchy(object="750P")
        sleep(2)
        self.create_training_product_hierarchy(object="2500P")
        sleep(2)
        self.create_training_product_hierarchy(object="F60")
        sleep(2)
        self.create_training_product_hierarchy(object="F61")
        sleep(2)
        self.create_training_product_hierarchy(object="F62")
        sleep(2)
        self.create_training_product_hierarchy(object="F63")
        sleep(2)
        self.create_training_product_hierarchy(object="F64")
        sleep(2)
        self.create_training_product_hierarchy(object="SHELL")
        self.logger.info('End:\t Checking Module- setting_up_product_hierarchy_for_training')

    # Exploded Assembly View

    def displacement(self, object, x, y, z):
        if appears(object):
            mouseMoveTo(object)
            while exists("02_high") == False:
                pyautogui.click()
                if appears("02_high2", 5):
                    break
            pyautogui.click(button='right')
            if appears("select_parts"):
                mouseLeftClickOn("select_parts")
            free()
            sleep(2)
            pyautogui.typewrite('move', .2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('d')
            pyautogui.press('enter')
            if appears("displacement"):
                pyautogui.typewrite(x, .1)
                pyautogui.typewrite(',', .1)
                pyautogui.typewrite(y, .1)
                pyautogui.typewrite(',', .1)
                pyautogui.typewrite(z, .1)
                pyautogui.press('enter')
                self.logger.info(object + " was moved")
            else:
                raise AssertionError(object + " could not be moved")

    def exploded_assembly_view(self):
        self.logger.info('Start:\t Checking Module- exploded_assembly_view')
        sleep(1)
        free()
        mouseLeftClickOn("product_hierarchy_window")
        sleep(.5)
        mouseLeftClickOn("close_product_hierarchy_button")
        free()
        free()
        free()
        pyautogui.moveTo(500,500)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.hotkey('ctrl', 's')
        sleep(5)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("product_hierarchy_u02")
            mouseLeftClickOn("new_drawing")
            sleep(1)
            if appears("new_prod_hier_window"):
                pyautogui.typewrite("PH-Exploded-SCE", .1)
                pyautogui.press('enter')
                if appears("select_model_drawing_window"):
                    mouseLeftClickOn("u02_in_model_drawing")
                    pyautogui.press('right')
                    mouseLeftClickOn("select_model_structure_click", "select_model_structure_region")
                    sleep(.5)
                    pyautogui.press('right')
                    pyautogui.press('down', presses=5)
                    if appears("curved_model_drawing_region2", 5):
                        mouseLeftClickOn("curved_model_drawing_click", "curved_model_drawing_region2")
                    elif appears("curved_model_drawing_region", 5):
                        mouseLeftClickOn("curved_model_drawing_click", "curved_model_drawing_region")
                    sleep(1)
                    mouseLeftClickOn("select_model_drawing_window_ok")
                    waitForVanish("working_dialog")
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    if appears("ph_exploded_sce_tab"):
                        self.logger.info("ph_exploded_sce was created")
                        change_viewpoint_from_aft_stbd_up()
                        sleep(1)
                        mouseLeftClickOn("structure_main_pane")
                        if appears("product_hierarchy_button"):
                            mouseLeftClickOn("product_hierarchy_button")
                            if appears("product_hierarchy_window"):
                                mouseMoveTo("prod_hier_scroll_up")
                                pyautogui.click(clicks=40)
                                if appears("project_in_prod_hierarchy",10):
                                    mouseMoveTo("project_in_prod_hierarchy")
                                    pyautogui.click(clicks=2)
                                    pyautogui.click(clicks=2)
                                if appears("u01_bulb_on", 5):
                                    mouseMoveTo("u01_bulb_on_click", "u01_bulb_on")
                                    while exists("u01_bulb_off") == False:
                                        pyautogui.click()
                                if appears("u03_bulb_on", 5):
                                    mouseMoveTo("u01_bulb_on_click", "u03_bulb_on")
                                    while exists("u03_bulb_off") == False:
                                        pyautogui.click()
                                if appears("u02_bulb_off", 5):
                                    mouseMoveTo("u02_bulb_off_click", "u02_bulb_off")
                                    while exists("u02_bulb_on") == False:
                                        pyautogui.click()
                                        if appears("u02_bulb_on",5):
                                            break
                                if appears("u03_bulb_off_collapse_region", 5):
                                    mouseMoveTo("u03_bulb_off_collapse_click", "u03_bulb_off_collapse_region")
                                    while exists("u03_bulb_off_collapsed") == False:
                                        pyautogui.click()
                                if appears("project_click", 5):
                                    mouseLeftClickOn("project_click")
                                    mouseDoubleClickOn("project_click")
                                if appears("u02_bulb_on_expand_region", 5):
                                    mouseMoveTo("u02_bulb_on_expand_click", "u02_bulb_on_expand_region")
                                    while exists("u02_bulb_on_expanded") == False:
                                        pyautogui.click()
                                if appears("unspooled_pipe",10):
                                    mouseMoveTo("unspooled_pipe")
                                    while exists("unspooled_pipe_high") == False:
                                        pyautogui.click()
                                        if appears("unspooled_pipe_high2"):
                                            break
                                else:
                                    raise AssertionError("unspooled_pipe did not appear")
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down', presses=9)
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                pyautogui.press('down')
                                pyautogui.press('space')
                                free()
                                sleep(1)
                                self.displacement(object='0207', x='0', y='0', z='10000')

                                mouseMoveTo("prod_hier_scroll_up")
                                pyautogui.click(clicks=5)

                                self.displacement(object='0205', x='0', y='-10000', z='0')
                                self.displacement(object='0204', x='0', y='10000', z='0')
                                self.displacement(object='0203', x='0', y='-5000', z='0')
                                self.displacement(object='0202', x='0', y='5000', z='0')

                                mouseMoveTo("prod_hier_scroll_up")
                                pyautogui.click(clicks=3)

                                self.displacement(object='0201', x='0', y='0', z='-10000')
                                self.displacement(object='0200', x='8000', y='0', z='0')
                                free()
                                mouseLeftClickOn("product_hierarchy_window")
                                sleep(.5)
                                mouseLeftClickOn("close_product_hierarchy_button")
                                free()
                                free()
                                reset_viewport()
                                conceptual_visual()
                                sleep(5)
                                wireframe2d_visual()
                                sleep(5)
                                pyautogui.hotkey('ctrl', 's')
                                sleep(3)
                                self.logger.info("Drawing was exploded successfully")
                            else:
                                raise AssertionError("product_hierarchy_window did not appear")
                    else:
                        raise AssertionError("ph_exploded_sce_tab did not appear")
                else:
                    raise AssertionError("select_model_drawing_window did not appear")
            else:
                raise AssertionError("New product hierarchy drawing window did not appear")
        else:
            raise AssertionError("ShipCon Navigator did not appear")
        self.logger.info('End:\t Checking Module- exploded_assembly_view')

    # Product Hierarchy and Naming Conventions

    def naming_convention(self):
        self.logger.info('Start:\t Checking Module- naming_convention')
        sleep(1)
        mouseLeftClickOn("manage_pane")
        if appears("manage_button"):
            mouseLeftClickOn("manage_button")
            if appears("shipcon_manager"):
                mouseLeftClickOn("general_button")
                mouseLeftClickOn("naming_convention_button2")
                if appears("naming_window"):
                    mouseLeftClickOn("naming_convention_window_ok")
                    self.logger.info(
                        "Naming convention was checked and Nothing was changed as mentioned in the curriculum.")
                    sleep(1)
                    pyautogui.hotkey('alt', 'f4')
                else:
                    raise AssertionError("naming_window did not appear")
            else:
                raise AssertionError("shipcon_manager did not appear")
        else:
            raise AssertionError("manage_button did not appear")
        self.logger.info('End:\t Checking Module- naming_convention')

    # Product Hierarchy and Production Drawings

    def production_drawings(self):
        self.logger.info('Start:\t Checking Module- production_drawings')
        sleep(1)
        free()
        pyautogui.hotkey('ctrl', 's')
        sleep(3)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("assembly_drawing")
            mouseLeftClickOn("create_drawing")
            sleep(5)
            if appears("create_assembly_drawing"):
                mouseLeftClickOn("next_button_assembly_window")
                mouseLeftClickOn("drop_down_create_assembly")
                sleep(1)
                pyautogui.click()
                if appears("primary_selected"):
                    self.logger.info("Prodduction drawing was checked")
                mouseLeftClickOn("cancel_create_assembly")
            else:
                raise AssertionError("create_assembly_drawing did not appear")
        else:
            raise AssertionError("ShipCon Navigator did not appear")
        self.logger.info('End:\t Checking Module- production_drawings')

    # Product Hierarchy and Nesting

    def create_nest_test(self):
        self.logger.info('Start:\t Checking Module- create_nest_test')
        sleep(1)
        sleep(1)
        pyautogui.hotkey('ctrl', 's')
        sleep(3)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            if appears("navigator_nest_node"):
                mouseLeftClickOn("navigator_nest_node")
                mouseLeftClickOn("u02_folder_region")
                if appears("new_button"):
                    mouseLeftClickOn("new_button")
                    if appears("new_plate_nest_drawing_window"):
                        self.logger.info("New Plate Window is visible")
                        pyautogui.typewrite('Nest-Test', interval=0.2)
                        mouseLeftClickOn("new_plate_drawing_ok_button")
                        if appears("pl12_region"):
                            mouseLeftClickOn("pl12_region")
                            mouseLeftClickOn("select_plate_stock_ok_button")
                            sleep(5)
                            sleep(5)
                            if appears("save_warning"):
                                pyautogui.press('enter')
                            if appears("nest_test_drawing_tab"):
                                self.logger.info("Nest test created")
                            else:
                                raise AssertionError("Nest Test not created")
                        else:
                            raise AssertionError("Plate stock window not visible")
                else:
                    raise AssertionError("New Plate Window is not visible")
            else:
                raise AssertionError("Nest node not visible")
        else:
            raise AssertionError("Navigator window did not open")
        self.logger.info('End:\t Checking Module- create_nest_test')

    def browse_product_hierarchy(self):
        self.logger.info('Start:\t Checking Module- browse_product_hierarchy')
        sleep(2)
        mouseLeftClickOn("profile_plots_button")
        mouseDoubleClickOn("plate_nest")
        mouseDoubleClickOn("insert_part_button")
        if appears("save_warning"):
            pyautogui.press('enter')
        if appears("product_hierarchy_drop_down_button"):
            mouseLeftClickOn("product_hierarchy_drop_down_button")
            sleep(2)
            mouseDoubleClickOn("cancel_button")
            mouseDoubleClickOn("cancel_button")
            free()
            pyautogui.press('esc')
            self.logger.info("Product Hierarchy and Nesting checked")
        else:
            raise AssertionError("Product Hierarchy drop down not visible")
        self.logger.info('End:\t Checking Module- browse_product_hierarchy')

    # ShipConstructor Utilities

    # 3D Viewpoint

    def viewpoint_3d(self):
        self.logger.info('Start:\t Checking Module- viewpoint_3d')
        sleep(1)
        free()
        mouseDoubleClickOn("shipconstructor_tab_new")
        free()
        pyautogui.press('esc')
        sleep(1)
        self.logger.info("Structure main pane appeared")
        mouseLeftClickOn("structure_main_pane")
        if appears("navigator_button_new"):
            self.logger.info("Navigator button appeared")
            mouseLeftClickOn("navigator_button_new")
            if appears("shipcon_navi"):
                self.logger.info("ShipCon Navigator appeared")
                mouseLeftClickOn("navigator_u02_node")
                mouseLeftClickOn("structure_node2")
                mouseDoubleClickOn("u02_md_6700")
                sleep(15)
                if appears("u02_md_6700_tab"):
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    sleep(5)
                    self.logger.info("u02_md_6700 drawing opened")
                    free()
                    pyautogui.click()
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    mouseLeftClickOn("main_panel_new")
                    mouseLeftClickOn("3d_viewpoint_button")
                    if appears("3d_viewpoint_window"):
                        mouseLeftClickOn("plan_tab_3d_viewpoint")
                        self.logger.info("Plan tab was appeared and clicked")
                        mouseLeftClickOn("looking_up_click", "looking_up_region")
                        mouseLeftClickOn("viewport_window_ok")
                        free()
                        pyautogui.hotkey('ctrl', 's')
                        sleep(7)
                        self.logger.info("3D Viewpoint was checked")
                    else:
                        raise AssertionError("3d_viewpoint_window didn't appear")
                else:
                    raise AssertionError("u02_md_6700_tab did not appear")
            else:
                self.logger.error("shipcon_navigator did not appear")
                raise AssertionError("shipcon_navigator did not appear")
        else:
            self.logger.error("navigator_button_new did not appear")
            raise AssertionError("navigator_button_new did not appear")
        self.logger.info('End:\t Checking Module- viewpoint_3d')

    # Activate UCS

    def activating_ucs(self):
        self.logger.info('Start:\t Checking Module- activating_ucs')
        sleep(1)
        free()
        pyautogui.hotkey('ctrl', 's')
        sleep(20)
        free()
        free()
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseMoveTo("prod_hier_scroll_up")
            pyautogui.click(clicks=50)
            mouseDoubleClickOn("u02_fr47")
            sleep(3)
            if appears("u02_fr47_tab"):
                mouseLeftClickOn("main_panel_new")
                mouseLeftClickOn("3d_viewpoint_button")
                if appears("3d_viewpoint_window"):
                    mouseLeftClickOn("3d_tab")
                    mouseLeftClickOn("viewport_window_ok")
                change_viewpoint_from_aft_stbd_up()
                mouseDoubleClickOn("sc_utilites")
                mouseLeftClickOn("visual_sc")
                if appears("activate_ucs"):
                    mouseLeftClickOn("activate_ucs")
                    if appears("activate_ucs_menu"):
                        mouseLeftClickOn("activate_from_object")
                        free()
                        pyautogui.typewrite('-2876.1676,1157.3920', .1)
                        pyautogui.press('enter')
                        if appears("stiff_UCS_window"):
                            mouseLeftClickOn("stiff_ucs_click")
                            sleep(.5)
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("stiff_UCS_window did not appear")
                        free()
                        mouseLeftClickOn("visual_sc")
                        mouseLeftClickOn("activate_ucs")
                        mouseLeftClickOn("activate_from_object")
                        free()
                        pyautogui.typewrite('34682,-5818', .1)
                        pyautogui.press('enter')
                        if appears("activate_ucs_menu"):
                            mouseLeftClickOn("forward_ucs")
                            sleep(.5)
                            pyautogui.press('enter')
                            self.logger.info("UCS was changed perfectly")
                            if appears("ucs_before_flipping", 5):
                                self.logger.info("UCS was changed perfectly and matched with the screenshot")
                        else:
                            raise AssertionError("activate_ucs_menu did not appear")
                    else:
                        raise AssertionError("activate_ucs_menu did not appear")
                else:
                    raise AssertionError("Activate UCS option did not appear")
                self.logger.info('End:\t Checking Module- activating_ucs')

    def flip_UCS_X(self):
        self.logger.info('Start:\t Checking Module- flip_UCS_X')
        sleep(1)
        mouseLeftClickOn("visual_sc")
        if appears("flip_ucs_x"):
            mouseLeftClickOn("flip_ucs_x")
            free()
            if appears("ucs_flipped_x", 5):
                self.logger.info("UCS was flipped")
            mouseLeftClickOn("visual_sc")
            mouseLeftClickOn("flip_ucs_x")
            free()
            if appears("ucs_before_flipping", 5):
                self.logger.info("Flip UCS-X was checked perfectly")
        else:
            raise AssertionError("flip_ucs_x option did not appear")
        self.logger.info('End:\t Checking Module- flip_UCS_X')

    def flip_UCS_Y(self):
        self.logger.info('Start:\t Checking Module- flip_UCS_Y')
        sleep(2)
        mouseLeftClickOn("visual_sc")
        if appears("flip_ucs_x"):
            mouseMoveTo("flip_ucs_x")
            mouseLeftClickOn("hovered_drop_down")
            # mouseLeftClickOn("flip_ucs_x_dropdown","flip_ucs_x_region")
            mouseLeftClickOn("flip_ucs_y_on_list")
            free()
            mouseLeftClickOn("visual_sc")
            mouseLeftClickOn("flip_ucs_y")
            free()
            self.logger.info("Flip UCS-Y was checked perfectly")
        else:
            raise AssertionError("flip_ucs_y option did not appear")
        self.logger.info('End:\t Checking Module- flip_UCS_Y')

    def swap_UCS(self):
        self.logger.info('Start:\t Checking Module- swap_UCS')
        sleep(2)
        mouseLeftClickOn("visual_sc")
        if appears("flip_ucs_y"):
            mouseMoveTo("flip_ucs_y")
            mouseLeftClickOn("hovered_drop_down")
            # mouseLeftClickOn("flip_ucs_x_dropdown","flip_ucs_x_region")
            mouseLeftClickOn("swap_ucs_xy_on_list")
            free()
            mouseLeftClickOn("visual_sc")
            mouseLeftClickOn("swap_ucs_xy")
            free()
            self.logger.info("Swap UCS was checked")
        else:
            raise AssertionError("flip_ucs_x option did not appear")
        self.logger.info('End:\t Checking Module- swap_UCS')

    def hide_object(self):
        self.logger.info('Start:\t Checking Module- hide_object')
        sleep(1)
        if appears("u02_fr47_tab"):
            change_viewpoint_body_looking_fwd()
            conceptual_visual()
            mouseLeftClickOn("visual_sc")
            mouseLeftClickOn("hide_object")
            free()
            sleep(1)
            # pyautogui.typewrite('-5307.9491,1969.3653', .1)
            pyautogui.typewrite('-5385.8508,1926.1464', .1)
            pyautogui.press('enter')
            sleep(.5)
            pyautogui.press('enter')
            wireframe2d_visual()
            self.logger.info("Hide object was checked")
        else:
            raise AssertionError("u02_fr50_tab did not appear")
        self.logger.info('End:\t Checking Module- hide_object')

    def unhide_object(self):
        self.logger.info('Start:\t Checking Module- unhide_object')
        sleep(1)
        mouseLeftClickOn("visual_sc")
        mouseLeftClickOn("hide_obj_selected")
        # mouseLeftClickOn("hide_object_list")
        if appears("unhide_obj"):
            mouseLeftClickOn("unhide_obj")
            free()
            pyautogui.hotkey('ctrl', 's')
            self.logger.info("Unhide object was checked")
            sleep(2)
        else:
            raise AssertionError("unhide_obj option did not appear")
        self.logger.info('End:\t Checking Module- unhide_object')

    def unhide_all_objects(self):
        self.logger.info('Start:\t Checking Module- unhide_all_objects')
        sleep(1)
        mouseLeftClickOn("visual_sc")
        # mouseMoveTo("unhide_obj_selected_icon")
        mouseLeftClickOn("unhide_obj_selected")
        if appears("unhide_all_obj"):
            mouseMoveTo("unhide_all_obj")
            free()
            pyautogui.press('esc')
            sleep(2)
            pyautogui.hotkey('ctrl', 's')
            sleep(5)
            self.logger.info("Unhide All Object was checked")
        else:
            raise AssertionError("unhide_all_obj did not appear")
        self.logger.info('End:\t Checking Module- unhide_all_objects')

    # Clip Current View

    def clipping_current_view(self):
        self.logger.info('Start:\t Checking Module- clipping_current_view')
        sleep(1)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseLeftClickOn("product_hierarchy_node")
            free_white()
            pyautogui.click()
            mouseDoubleClickOn("ph_exploded_sce")
            sleep(3)
            if appears("ph_exploded_sce_tab"):
                sleep(1)
                mouseLeftClickOn("layout1")
                sleep(2)
                mouseDoubleClickOn("viewport_window")
                pyautogui.press('esc')
                change_viewpoint_from_aft_up()
                free_white()
                sleep(2)
                pyautogui.typewrite("NAVBARDISPLAY",0.2)
                pyautogui.press('enter')
                pyautogui.typewrite("1", 0.2)
                pyautogui.press('enter')
                if appears("navi_toolbar", 5):
                    mouseLeftClickOn("navi_toolbar_dropdown", "navi_toolbar")
                    if appears("zoom_extent",10):
                        self.logger.info("zoom extent was seletced")
                    else:
                        raise AssertionError("zoom_extent option did not appear")
                    free_white()
                    pyautogui.press('esc')
                else:
                    self.logger.warn("navi_toolbar did not appear")
                if appears("navi_toolbar3", 5):
                    mouseLeftClickOn("navi_toolbar_dropdown3", "navi_toolbar3")
                    if appears("zoom_extent",10):
                        self.logger.info("zoom extent was seletced")
                    else:
                        raise AssertionError("zoom_extent option did not appear")
                    free_white()
                    pyautogui.press('esc')
                else:
                    self.logger.info("navi_toolbar3 did not appear")
                if appears("navi_toolbar2", 5):
                    mouseLeftClickOn("navi_toolbar_dropdown2", "navi_toolbar2")
                    if appears("zoom_extent", 10):
                        self.logger.info("zoom extent was seletced")
                    else:
                        raise AssertionError("zoom_extent option did not appear")
                    free_white()
                    pyautogui.press('esc')
                else:
                    self.logger.info("navi_toolbar2 did not appear")
                mouseLeftClickOn("visual_sc")
                mouseLeftClickOn("clip_current_view")
                free_white()
                pyautogui.typewrite('27750,6058.1807,-8584.4592', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('33618.2232,5864.4449,-8505.0017', .1)
                pyautogui.press('enter')
                free_white()
                sleep(1)
                pyautogui.typewrite('3dclip', .2)
                pyautogui.press('enter')
                sleep(5)
                if appears("adjust_clipping_window",10):
                    sleep(3)
                    self.logger.info("Adjust clipping window appeared")
                    pyautogui.hotkey('alt', 'f4')
                else:
                    raise AssertionError("adjust_clipping_window did not appear")
                free_white()
                mouseLeftClickOn("visual_sc")
                mouseMoveTo("clip_current_view")
                mouseLeftClickOn("hovered_drop_down")
                # mouseLeftClickOn("clip_current_view_drop_down", "clip_current_view")
                if appears("remove_clip",10):
                    self.logger.info("remove clip appeared and clicked")
                    mouseLeftClickOn("remove_clip")
                    free_white()
                    sleep(5)
                    pyautogui.hotkey('ctrl', 's')
                    self.logger.info("Clipping current view was checked successfully")
                else:
                    raise AssertionError("remove_clip did not appear")
            else:
                raise AssertionError("ph_exploded_sce_tab did not appear")
            self.logger.info('End:\t Checking Module- clipping_current_view')

    # 3D to 2D

    def test_3d_to_2d(self):
        self.logger.info('Start:\t Checking Module- test_3d_to_2d')
        sleep(1)
        free_white()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("structure_node2")
            mouseMoveTo("prod_hier_scroll_down")
            while exists("u01_tt_1400") == False:
                pyautogui.click(clicks=10)
        if appears("u01_tt_1400"):
            mouseDoubleClickOn("u01_tt_1400")
            sleep(10)
            if appears("u01_tt_1400_tab"):
                self.logger.info("u01_tt_1400 opened")
                mouseLeftClickOn("visual_sc")
                mouseLeftClickOn("activate_ucs")
                if appears("activate_ucs_menu"):
                    self.logger.info("Activate UCS menu appeared")
                    mouseLeftClickOn("world_cs")
                    mouseLeftClickOn("world_coordinate_activate")
                else:
                    raise AssertionError("activate_ucs_menu did not appear")
            else:
                raise AssertionError("u01_tt_1400_tab did not appear")
        else:
            raise AssertionError("u01_tt_1400 did not appear")
        mouseDoubleClickOn("home_button")
        mouseLeftClickOn("layer_button")
        mouseLeftClickOn("layer_properties_button")
        free()
        if appears("draft_mark",10):
            mouseRightClickOn("draft_mark")
            mouseLeftClickOn("set_current_layer")
            self.logger.info("Current Layer was changed")
            mouseLeftClickOn("draft_mark_selected")
            mouseLeftClickOn("layer_properties_manager_close")
            free()
            line('40492', '2047', '42290', '2047')
            mouseDoubleClickOn("structure_ribbon_button")
            mouseLeftClickOn("parts_pane")
            mouseLeftClickOn("add_object")
            free()
            pyautogui.typewrite('41405.4245,2278.4823', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('40492,2047', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("cmd_extend")
            if appears("failed_to_identify", 5):
                self.logger.info("Object could not be added")
            free()
            pyautogui.press('esc')
            mouseDoubleClickOn("sc_utilites")
            mouseLeftClickOn("visual_sc")
            if appears("activate_ucs"):
                mouseLeftClickOn("activate_ucs")
                mouseLeftClickOn("activate_from_object")
                free()
                pyautogui.typewrite('41405.4245,2278.4823', .1)
                pyautogui.press('enter')
                if appears("activate_ucs_menu"):
                    pyautogui.press('enter')
                else:
                    raise AssertionError("activate_ucs_menu did not appear")
                mouseLeftClickOn("sc_utilities_tools_button")
                mouseLeftClickOn("3d_to_2d")
                free()
                pyautogui.typewrite('40492,2047', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                mouseDoubleClickOn("structure_ribbon_button")
                mouseLeftClickOn("parts_pane")
                mouseLeftClickOn("add_object")
                free()
                pyautogui.typewrite('41405.4245,2278.4823', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('40492,2047', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("markline_style"):
                    if appears("continuous_unhigh", 5):
                        mouseLeftClickOn("continuous_unhigh")
                    else:
                        mouseLeftClickOn("continuous")
                    mouseLeftClickOn("select_markline_ok")
                    self.logger.info("3D to 2D operation was done")
                else:
                    raise AssertionError("markline_style did not appear")
        else:
            raise AssertionError("draft mark layer did not appear")
        self.logger.info('End:\t Checking Module- test_3d_to_2d')

    # Orthographic Projection

    def orthographic_projection(self):
        self.logger.info('Start:\t Checking Module- orthographic_projection')
        sleep(2)
        free()
        pyautogui.hotkey('ctrl', 's')
        sleep(5)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseMoveTo("prod_hier_scroll_down")
            while exists("u01_lsec_f74_ps") == False:
                pyautogui.click()
            if appears("u01_lsec_f74_ps"):
                mouseDoubleClickOn("u01_lsec_f74_ps")
                sleep(3)
                if appears("u01_lsec_f74_ps_tab"):
                    sleep(10)
                    sleep(10)
                    self.logger.info("u01_lsec_f74_ps opened")
                    if appears("sc_utilites",5):
                        mouseDoubleClickOn("sc_utilites")
                    else:
                        raise AssertionError("sc_utilites did not appear")
                    mouseLeftClickOn("visual_sc")
                    mouseLeftClickOn("activate_ucs")
                    if appears("activate_ucs_menu"):
                        mouseLeftClickOn("world_cs")
                        mouseLeftClickOn("world_coordinate_activate")
                        self.logger.info("World coordinate was activated")
                        sleep(1)
                    else:
                        raise AssertionError("activate_ucs_menu did not appear")
                    mouseDoubleClickOn("home_button")
                    mouseLeftClickOn("layer_button")
                    mouseLeftClickOn("layer_properties_button")
                    if appears("draft_mark"):
                        if appears("layer_0_unhigh", 5):
                            mouseRightClickOn("layer_0_unhigh")
                        else:
                            mouseRightClickOn("layer_0")
                        mouseLeftClickOn("set_current_layer")
                        self.logger.info("Layer 0 was set to the current layer")
                        mouseLeftClickOn("draft_mark")
                        mouseLeftClickOn("layer_properties_manager_close")
                        free()
                        sleep(1)
                        free()
                        pyautogui.typewrite('line', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite("45000,3700,1000", .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite("#46800,4000,1200", .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        sleep(1)
                        mouseDoubleClickOn("sc_utilites")
                        mouseLeftClickOn("visual_sc")
                        mouseLeftClickOn("activate_ucs")
                        mouseLeftClickOn("activate_from_object")
                        free()
                        pyautogui.typewrite('46734.2107,-63.3544,770.6621', .1)
                        pyautogui.press('enter')
                        if appears("activate_ucs_menu"):
                            sleep(.5)
                            pyautogui.press('enter')
                        else:
                            raise AssertionError("Activate UCS menu window did not appear")
                        mouseLeftClickOn("sc_utilities_tools_button")
                        if appears("orthographic_projection"):
                            mouseLeftClickOn("orthographic_projection")
                            free()
                            pyautogui.typewrite('45824.6235,1114.3945', .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            pyautogui.press('y')
                            pyautogui.press('enter')
                            pyautogui.hotkey('ctrl', 's')
                            sleep(3)
                            self.logger.info("Orthographic Projection Operation was done")
                        else:
                            raise AssertionError("orthographic_projection did not appear")
                    else:
                        raise AssertionError("Layer 0 could not be selected")
                else:
                    raise AssertionError("u01_lsec_f74_ps_tab did not appear")
            else:
                raise AssertionError("u01_lsec_f74_ps did not appear")
            self.logger.info('End:\t Checking Module- orthographic_projection')

    # Remove Vertices below Tolerance

    def removing_vertices_below_tolerance(self):
        self.logger.info('Start:\t Checking Module- removing_vertices_below_tolerance')
        sleep(2)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseLeftClickOn("hull_node")
            mouseMoveTo("prod_hier_scroll_up")
            pyautogui.click(clicks=3)
            mouseDoubleClickOn("hull_u01")
            sleep(2)
            if appears("hull_u01_tab"):
                self.logger.info("Hull drawing U01 opened")
                free()
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('44990.1288,-765.8627', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.typewrite('list', .3)
                pyautogui.press('enter')
                if appears("control_points_85",10):
                    self.logger.info("List was shown")
                    free()
                    pyautogui.click()
                    mouseLeftClickOn("sc_utilities_tools_button")
                    mouseLeftClickOn("remove_vertices")
                    free()
                    pyautogui.typewrite('44990.1288,-765.8627', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    if appears("tolerance_remove_vertices",10):
                        pyautogui.press('1')
                        pyautogui.press('enter')
                        sleep(2)
                        pyautogui.typewrite('select', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('44990.1288,-765.8627', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('list', .3)
                        pyautogui.press('enter')
                        if appears("control_points_40",10):
                            self.logger.info("List was shown. Control points value is now 40")
                            free()
                            pyautogui.click()
                            self.logger.info("Remove vertices was done perfectly")
                        else:
                            raise AssertionError("control_points_40 was not shown")
                    else:
                        raise AssertionError("tolerance level was not asked to be selected")
                else:
                    raise AssertionError("control_points_85 did not appear")
            else:
                raise AssertionError("hull_u01_tab did not appear")
            self.logger.info('End:\t Checking Module- removing_vertices_below_tolerance')

    # Convert Ellipse/Spline to Polyline

    def converting_ellipse_to_polyline(self):
        self.logger.info('Start:\t Checking Module- converting_ellipse_to_polyline')
        sleep(1)
        free()
        pyautogui.typewrite('circle', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('36667.0225,189.4651', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('700', .1)
        pyautogui.press('enter')
        pyautogui.press('Circle was created')
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('37367.0225,189.4651', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('PROPERTIES', .1)
        pyautogui.press('enter')
        if appears("object_type_circle"):
            self.logger.info("Object type: Circle found")
            mouseLeftClickOn("object_type_circle")
            mouseLeftClickOn("layer_properties_manager_close")
            pyautogui.click()
            free()
            mouseLeftClickOn("sc_utilities_tools_button")
            mouseLeftClickOn("convert_circle_to_polyline_option")
            self.logger.info("Convert circle to polyline option was clicked")
            free()
            pyautogui.press('esc')
            pyautogui.typewrite('select', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('37367.0225,189.4651', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('PROPERTIES', .1)
            pyautogui.press('enter')
            if appears("object_type_polyline"):
                self.logger.info("Object was converted to polyline")
                mouseLeftClickOn("object_type_polyline")
                mouseLeftClickOn("layer_properties_manager_close")
                pyautogui.click()
                free()
                pyautogui.press('esc')
                pyautogui.press('esc')
                self.logger.info("Converting ellipse to polyline was done")
            else:
                raise AssertionError("Object was not converted to polyline")
        else:
            raise AssertionError("object_type_circle did not appear")
        self.logger.info('End:\t Checking Module- converting_ellipse_to_polyline')


    def layers(self):
        self.logger.info('Start:\t Checking Module- layers')
        sleep(2)
        mouseDoubleClickOn("home_button")
        mouseLeftClickOn("layer_button")
        mouseLeftClickOn("layer_properties_button")
        free()
        if appears("layer_0_unhigh", 5):
            mouseRightClickOn("layer_0_unhigh")
        elif appears("layer_0", 5):
            mouseRightClickOn("layer_0")
        else:
            raise AssertionError("Layer 0 could not be selected")
        mouseLeftClickOn("set_current_layer")
        pyautogui.click()
        self.logger.info("Layer 0 was set to the current layer")
        mouseLeftClickOn("layer_properties_manager_close")
        free()
        free()
        pyautogui.press('esc')
        self.logger.info("Layer 0 was selected")
        mouseDoubleClickOn("sc_utilites")
        mouseLeftClickOn("visual_sc")
        if appears("deactive_layer"):
            mouseLeftClickOn("deactive_layer")
            if appears("select_layer_to_deactive_window"):
                mouseLeftClickOn("frames_bevel")
                mouseLeftClickOn("select_layer_to_deactive_window_ok")
                self.logger.info("Frames Bevel layer was deactivated")
            else:
                raise AssertionError("select_layer_to_deactive_window did not appear")
        else:
            raise AssertionError("deactive_layer did not appear")
        mouseLeftClickOn("visual_sc")
        if appears("activate_layer"):
            mouseLeftClickOn("activate_layer")
            if appears("select_layer_to_active_window"):
                mouseLeftClickOn("frames_bevel")
                mouseLeftClickOn("select_layer_to_deactive_window_ok")
                self.logger.info("Frames Bevel layer was activated")
            else:
                raise AssertionError("select_layer_to_active_window did not appear")
        else:
            raise AssertionError("activate_layer did not appear")
        mouseLeftClickOn("sc_utilities_tools_button")
        mouseLeftClickOn("move_geometry")
        free()
        pyautogui.typewrite('58419.5304,1658.2244', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('36640.9135,-3929.1803', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("move_geom_window"):
            mouseLeftClickOn("part")
            mouseLeftClickOn("select_layer_to_deactive_window_ok")
            self.logger.info("Move Gemoetry window was appeared")
        else:
            raise AssertionError("Move geometry to layer window did not appear")
        mouseLeftClickOn("visual_sc")
        if appears("activate_layer"):
            mouseLeftClickOn("activate_layer")
            if appears("select_layer_to_active_window"):
                mouseLeftClickOn("part")
                mouseLeftClickOn("select_layer_to_deactive_window_ok")
                self.logger.info("Part layer was activated")
            else:
                raise AssertionError("select_layer_to_active_window did not appear")
        else:
            raise AssertionError("activate_layer did not appear")
        mouseLeftClickOn("sc_utilities_tools_button")
        mouseLeftClickOn("copy_geometry")
        free()
        pyautogui.typewrite('58419.5304,1658.2244', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('36640.9135,-3929.1803', .1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        if appears("copy_geometry_window"):
            self.logger.info("Copy Geometry window was appeared")
            mouseLeftClickOn("frames_bevel")
            mouseLeftClickOn("select_layer_to_deactive_window_ok")
        else:
            raise AssertionError("Copy geometry to layer window did not appear")
        mouseLeftClickOn("visual_sc")
        if appears("activate_layer"):
            mouseLeftClickOn("activate_layer")
            if appears("select_layer_to_active_window"):
                mouseLeftClickOn("frames_bevel")
                mouseLeftClickOn("select_layer_to_deactive_window_ok")
                self.logger.info("frames_bevel layer was activated")
                pyautogui.hotkey('ctrl', 's')
                sleep(3)
            else:
                raise AssertionError("select_layer_to_active_window did not appear")
        else:
            raise AssertionError("activate_layer did not appear")
        self.logger.info('End:\t Checking Module- layers')

    def toolpath(self):
        self.logger.info('Start:\t Checking Module- toolpath')
        sleep(2)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseLeftClickOn("structure_node2")
            mouseMoveTo("navi_slide_up")
            pyautogui.click(clicks=100)
            mouseDoubleClickOn("u01_fr65")
            sleep(15)
        else:
            raise AssertionError("u01_fr65 could not be opened")
        if appears("u01_fr66_tab"):
            self.logger.info("u01_fr65 was opened")
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(5)
            change_viewpoint_body_looking_fwd()
            sleep(2)
            free()
            pyautogui.typewrite('line', .2)
            pyautogui.press('enter')
            pyautogui.typewrite('221,2729.2973', .2)
            pyautogui.press('enter')
            pyautogui.typewrite('#221,1918', .2)
            pyautogui.press('enter')
            pyautogui.typewrite('#-306,1918', .2)
            pyautogui.press('enter')
            pyautogui.typewrite('#-306,2800', .2)
            pyautogui.press('enter')
            pyautogui.typewrite('#167.0619,2800', .2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("sc_utilities_tools_button")
            if appears("tool_path"):
                mouseLeftClickOn("tool_path")
                free()
                pyautogui.typewrite('405.5221,3041.6072', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('-448.2106,1719.3340', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("tool_path_error_window"):
                    mouseLeftClickOn("tool_path_error_window_create_circle")
                    sleep(2)
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('177.0619,2800', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('221.0000,2739.2973', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.typewrite('delete', .1)
                    pyautogui.press('enter')
                    self.logger.info('circle was deleted')
                    sleep(1)
                    pyautogui.typewrite('line', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('221,2729.2973', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('#167.0619,2800', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    mouseLeftClickOn("sc_utilities_tools_button")
                    mouseLeftClickOn("tool_path")
                    free()
                    pyautogui.typewrite('405.5221,3041.6072', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('-448.2106,1719.3340', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    if appears("tool_path_error_window_success", 5):
                        self.logger.info("Toolpath operation was checked")
                        pyautogui.press('enter')
                    else:
                        raise AssertionError("the toolpath was not successfully created.")
                else:
                    raise AssertionError("tool_path_error_window did not appear")
            else:
                raise AssertionError("tool_path did not appear")

            self.logger.info('End:\t Checking Module- toolpath')

    def fillet(self):
        self.logger.info('Start:\t Checking Module- fillet')
        sleep(2)
        pyautogui.typewrite('select', .1)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('405.5221,3041.6072', .1)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('-448.2106,1719.3340', .1)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('delete', .1)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        free()
        sleep(1)
        pyautogui.typewrite('scmlink', .2)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        sleep(1)
        if appears("mlink_manager"):
            mouseLeftClickOn("mlink_u01_fr67_DWG")
            sleep(2)
            pyautogui.hotkey('ctrl','a')
            mouseLeftClickOn("mlink_cross_DWG")
            sleep(5)
            free()
            if appears("mlink_manager", 20):
                mouseLeftClickOn("mlink_manager")
            elif appears("mlink_manager_high", 20):
                mouseLeftClickOn("mlink_manager_high")
            # if appears("mlink_cross_region1", 20):
            #     mouseLeftClickOn("mlink_cross1", "mlink_cross_region1")
            # elif appears("mlink_cross_region", 20):
            #     mouseLeftClickOn("mlink_cross", "mlink_cross_region")
            #     pyautogui.click(clicks=2)
            # mouseLeftClickOn("mlink_manager")
            pyautogui.hotkey('alt', 'f4')
            sleep(10)
            sleep(10)
            reset_viewport()
            sleep(2)
            sleep(1)
            pyautogui.typewrite('select', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('1905.6999,176.4418', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('979.6006,22.0870', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('copy', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.moveTo(898, 728)
            sleep(1)
            pyautogui.click()
            sleep(1)
            sleep(1)
            sleep(1)
            pyautogui.moveTo(1360, 733)
            sleep(1)
            pyautogui.click()
            sleep(1)
            pyautogui.press('esc')
            sleep(1)
            free()
            reset_viewport()
            sleep(1)
            free()
            sleep(1)
            pyautogui.typewrite('explode', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            sleep(1)
            pyautogui.typewrite('7112,58', .1)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('up')
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.typewrite('7112,58', .1)
            sleep(1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            self.logger.info("EXPLODE option was used via CMD")
            mouseLeftClickOn("sc_utilities_tools_button")
            if appears("fillet",10):
                mouseLeftClickOn("fillet")
                free()
                if appears("radius", 5):
                    pyautogui.typewrite('400', .2)
                    pyautogui.press('enter')
                else:
                    pyautogui.press('r')
                    pyautogui.press('enter')
                    pyautogui.typewrite('400', .2)
                    pyautogui.press('enter')
                pyautogui.typewrite('6923.1788,17.8878', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('6085,-50', .1)
                pyautogui.press('enter')
                pyautogui.hotkey('ctrl', 's')
                sleep(3)
                sleep(3)
                self.logger.info("Fillet option was checked")
            else:
                raise AssertionError("fillet option did not appear")
        else:
            raise AssertionError("Imported frames and lngbhds could not be deleted")
        self.logger.info('End:\t Checking Module- fillet')

    # Mirror about Centerline

    def mirror_about_centerline(self):
        self.logger.info('Start:\t Checking Module- mirror_about_centerline')
        sleep(2)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseMoveTo("prod_hier_scroll_down")
            while exists("u01_lsec_f74_ps") == False:
                pyautogui.click(clicks=4)
            if appears("u01_lsec_f74_ps"):
                mouseDoubleClickOn("u01_lsec_f74_ps")
                sleep(3)
                if appears("u01_lsec_f74_ps_tab"):
                    self.logger.info("u01_lsec_f74_ps opened")
                    mouseLeftClickOn("sc_utilities_tools_button")
                    if appears("sc_utilities_mirror_button"):
                        mouseLeftClickOn("sc_utilities_mirror_button")
                        free()
                        pyautogui.typewrite('47457.5501,1145.9385', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        change_viewpoint_plan_looking_down()
                        sleep(2)
                        pyautogui.typewrite('select', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('50240.0328,4256.0274,2224.0138', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('44330.5847,4256.0274,3404.8092', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('delete')
                        pyautogui.press('enter')
                        change_viewpoint_stbd_to_prt()
                        sleep(3)
                        self.logger.info("Mirror about centerline was checked")
                    else:
                        raise AssertionError("sc_utilities_mirror_button did not appear")
                else:
                    raise AssertionError("u01_lsec_f74_ps_tab did not appear")
            else:
                raise AssertionError("u01_lsec_f74_ps did not appear")
            self.logger.info('End:\t Checking Module- mirror_about_centerline')

    def reload_drawing(self):
        self.logger.info('Start:\t Checking Module- reload_drawing')
        sleep(1)
        mouseLeftClickOn("visual_sc")
        if appears("reload_drawing"):
            mouseLeftClickOn("reload_drawing")
            if appears("reload_drawing_warning"):
                pyautogui.press('left')
                pyautogui.press('enter')
                sleep(10)
                free()
                pyautogui.hotkey('ctrl', 's')
                sleep(10)
                self.logger.info("Drawing was reloaded")
            else:
                raise AssertionError("reload_drawing_warning did not appear")
        else:
            raise AssertionError("reload_drawing option did not appear")
        self.logger.info('End:\t Checking Module- reload_drawing')

    # Create a Quality Matrix

    def quality_matrix(self):
        self.logger.info('Start:\t Checking Module- quality_matrix')
        sleep(1)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        if appears("shipcon_navi"):
            mouseLeftClickOn("assembly_node")
            mouseDoubleClickOn("assembly_0101_p01")
            if appears("save_yes",10):
                mouseLeftClickOn("save_yes")
            if appears("assembly_0101_tab"):
                self.logger.info("assembly_0101_p01 opened")
                mouseLeftClickOn("sc_utilities_tools_button")
                if appears("quality_matrix"):
                    mouseLeftClickOn("quality_matrix")
                    if appears("quality_matrix_window"):
                        self.logger.info("Quality matrix was appeared")
                        pyautogui.press('tab', presses=4)
                        pyautogui.press('2')
                        pyautogui.press('tab')
                        pyautogui.press('s')
                        pyautogui.press('tab')
                        pyautogui.press('2')
                        pyautogui.press('tab')
                        pyautogui.press('2')
                        pyautogui.press('tab')
                        pyautogui.press('tab')
                        pyautogui.press('s')
                        pyautogui.press('tab')
                        pyautogui.press('2')
                        pyautogui.press('tab', presses=6)
                        pyautogui.press('0')
                        mouseLeftClickOn("orient_to_view_region")
                        mouseLeftClickOn("quality_matrix_window_ok")
                        pyautogui.typewrite('38850.0062,-4633.4727,1139.1287', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('38850.0062,4633.4727,1139.1287', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('49950,2423.7584,2009.3582', .1)
                        pyautogui.press('enter')
                        pyautogui.typewrite('49950,-2442.4799,2009.3582', .1)
                        pyautogui.press('enter')
                        pyautogui.press('enter')
                        pyautogui.typewrite('94.2861,272.4225', .1)
                        pyautogui.press('enter')
                        sleep(5)
                        pyautogui.hotkey('ctrl', 's')
                        sleep(5)
                        sleep(5)
                        if appears("quality_matrix_table", 5):
                            self.logger.info("table with distances between the points appeared")
                    else:
                        raise AssertionError("quality_matrix_window did not appear")
                else:
                    raise AssertionError("quality_matrix did not appear")
            else:
                raise AssertionError("assembly_0101_p01 tab did not appear")
            self.logger.info('End:\t Checking Module- quality_matrix')

    def property_labels_new(self):
        self.logger.info('Start:\t Checking Module- property_labels')
        sleep(1)
        pyautogui.hotkey('ctrl', 's')
        free_white()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("assembly_node")
        else:
            raise AssertionError("shipcon_navi did not appear")
        if appears("prod_hier_scroll_down",10):
            mouseMoveTo("prod_hier_scroll_down")
        else:
            raise AssertionError("prod_hier_scroll_down did not appear")
        while exists("0202_p09") == False:
            pyautogui.click(clicks=20)
        mouseDoubleClickOn("0202_p09")
        if appears("save_yes", 10):
            mouseLeftClickOn("save_yes")
        if appears("0202_p09_tab"):
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(5)
            free_white()
            pyautogui.click(clicks=2)
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.typewrite('VSCURRENT', .2)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('2')
            sleep(1)
            pyautogui.press('enter')
        else:
            raise AssertionError("0202_p09_tab did not appear")
        mouseLeftClickOn("production_ribbon")
        mouseLeftClickOn("property_label_pane")
        if appears("style_manager",10):
            mouseLeftClickOn("style_manager")
        else:
            raise AssertionError("style_manager button did not appear")
        if appears("property_label_style_manager_window"):
            # SSI – Name and Stock
            if appears("ssi_part_stock",5):
                mouseLeftClickOn("ssi_part_stock")
            elif appears("ssi_part_stock_high",5):
                mouseLeftClickOn("ssi_part_stock_high")
            elif appears("ssi_part_stock_unhigh",5):
                mouseLeftClickOn("ssi_part_stock_unhigh")
            else:
                raise AssertionError("ssi_part_stock did not appear")
            mouseLeftClickOn("set_current")
            mouseLeftClickOn("property_label_style_manager_window_close")
            free_white()
        else:
            raise AssertionError("property_label_style_manager_window did not appear")
        mouseLeftClickOn("property_label_pane")
        if appears("styled_based_label",5):
            mouseLeftClickOn("styled_based_label")
            free_white()
            # pyautogui.click(button='left')
            # mouseLeftClickOn("snap_override")
            # mouseLeftClickOn("onap_nearest")
            pyautogui.typewrite('nearest',.2)
            pyautogui.press('enter')
            free_white()
            pyautogui.moveTo(410,414)
            sleep(1)
            pyautogui.click()
            sleep(2)
            pyautogui.moveTo(488,411)
            sleep(1)
            pyautogui.click()
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.hotkey('ctrl', 's')
            sleep(5)
            sleep(5)
            sleep(5)
        else:
            raise AssertionError("styled_based_label did not appear")
        self.logger.info('End:\t Checking Module- property_labels')

    def property_labels(self):
        self.logger.info('Start:\t Checking Module- property_labels')
        sleep(1)
        pyautogui.hotkey('ctrl', 's')
        free_white()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("assembly_node")
            mouseMoveTo("prod_hier_scroll_down")
            while exists("0202_p09") == False:
                pyautogui.click(clicks=20)
            mouseDoubleClickOn("0202_p09")
            if appears("save_yes",10):
                mouseLeftClickOn("save_yes")
            if appears("0202_p09_tab"):
                sleep(5)
                sleep(5)
                sleep(5)
                free_white()
                pyautogui.click(clicks=2)
                pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.press('esc')
                pyautogui.typewrite('VSCURRENT', .2)
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('c')
                sleep(1)
                pyautogui.press('enter')

                mouseLeftClickOn("production_ribbon")
                mouseLeftClickOn("property_label_pane")
                if appears("property_label_option"):
                    mouseLeftClickOn("property_label_option")
                    free_white()
                    if appears("plate_conceptual", 10):
                        mouseLeftClickOn("plate_conceptual")
                    else:
                        raise AssertionError("plate_conceptual region did not appear")
                    if appears("field_text_window"):
                        self.logger.info("Field text window appeared")
                        mouseLeftClickOn("part_name_field")
                        mouseLeftClickOn("field_text_window_right")
                        mouseMoveTo("end_of_2nd_line")
                        sleep(2)
                        pyautogui.press('enter')
                        mouseMoveTo("prod_hier_scroll_down")
                        pyautogui.click(clicks=30)
                        mouseLeftClickOn("stock_name_field_text")
                        mouseLeftClickOn("field_text_window_right")
                        mouseMoveTo("end_of_2nd_line")
                        sleep(2)
                        pyautogui.press('enter')
                        # mouseLeftClickOn("dimension_style_drop_down")
                        # pyautogui.press('p')
                        # pyautogui.press('enter')
                        mouseLeftClickOn("field_text_window_ok")
                        sleep(2)
                        pyautogui.moveTo(474, 379)
                        sleep(2)
                        pyautogui.click()
                        # pyautogui.typewrite('213.1553,213.325', .1)
                        # pyautogui.press('enter')
                        sleep(1)
                        pyautogui.press('esc')
                        pyautogui.press('esc')
                        free_white()
                        pyautogui.typewrite('VSCURRENT', .2)
                        sleep(1)
                        pyautogui.press('enter')
                        sleep(1)
                        pyautogui.press('2')
                        sleep(1)
                        pyautogui.press('enter')
                        if appears("label_check", 5):
                            self.logger.info("Property Label was set correctly")
                        sleep(1)
                        pyautogui.hotkey('ctrl', 's')
                        sleep(5)
                    else:
                        raise AssertionError("field_text_window did not appear")
                else:
                    raise AssertionError("property_label_option did not appear")
            else:
                raise AssertionError("0202_p09_tab did not appear")
            self.logger.info('End:\t Checking Module- property_labels')

    def partviews(self):
        self.logger.info('Start:\t Checking Module- partviews')
        sleep(1)
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        sleep(.5)
        mouseLeftClickOn("ssi_node")
        mouseLeftClickOn("u01_node")
        mouseLeftClickOn("structure_node2")
        mouseMoveTo("prod_hier_scroll_up")
        while exists("u01_fr65") == False:
            pyautogui.click(clicks=20)
        if appears("u01_fr65"):
            mouseDoubleClickOn("u01_fr65")
            sleep(3)
            if appears("warning11", 3):
                pyautogui.press('enter')
        if appears("u01_fr66_tab"):
            sleep(10)
            free()
            self.logger.info("u01 fr65 opened")
            pyautogui.typewrite('select',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('12876,7101',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('8077,-869',.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('delete',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('select',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('8139,942',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('6382,-394',.2)
            pyautogui.press('enter')
            pyautogui.press('enter')
            pyautogui.typewrite('delete',.2)
            pyautogui.press('enter')
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.press('esc')
            sleep(1)
            change_viewpoint_from_fwd_stbd_up()
            conceptual_visual()
            mouseDoubleClickOn("sc_utilites")
            free()
            reset_viewport()
            mouseLeftClickOn("sc_util_partview")
            mouseLeftClickOn("load_view_partview")
            mouseLeftClickOn("load_associated")
            free()
            pyautogui.typewrite('574.6523,134.5214', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(5)
            if appears("associated_part", 5):
                self.logger.info("Associated part was appeared")
            mouseLeftClickOn("sc_util_partview")
            mouseLeftClickOn("load_associated_selected")
            mouseLeftClickOn("load_extent")
            free()
            pyautogui.typewrite('718,300,-44.5000', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('4700.00,1430.7000,8.4800', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('45,1082', .1)
            pyautogui.press('enter')
            sleep(5)
            if appears("selected_extent", 5):
                self.logger.info("Loaded extents were appeared")
            pyautogui.hotkey('ctrl', 'z')
            mouseLeftClickOn("sc_util_partview")
            mouseLeftClickOn("load_view_partview")
            mouseLeftClickOn("load_selected_extent")
            free()
            pyautogui.typewrite('547.9733,1270.1835', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('-4201.8635,-3401.0098', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            sleep(7)
            if appears("loaded_selected_extent", 3):
                self.logger.info("Selected extents were appeared")
            elif appears("loaded_selected_extent3", 3):
                self.logger.info("Selected extents were appeared")
            else:
                self.logger.warn(
                    "Selected extents were not appeared. Or the screen object was not matched with the stored screenshot.")
            mouseLeftClickOn("sc_util_partview")
            if appears("partview_delete_all"):
                mouseMoveTo("partview_delete_all")
                self.logger.info("partview_delete_all appered")
            else:
                raise AssertionError("partview_delete_all did not appear")
            if appears("partview_refresh"):
                mouseMoveTo("partview_refresh")
                self.logger.info("partview_refresh appered")
            else:
                raise AssertionError("partview_refresh did not appear")
            free()
            free()
        else:
            raise AssertionError("u01_fr65 tab did not appear")
        self.logger.info('End:\t Checking Module- partviews')

    def mlink_manager(self):
        self.logger.info('Start:\t Checking Module- mlink_manager')
        sleep(1)
        mouseLeftClickOn("sc_util_partview")
        mouseLeftClickOn("partview_delete_all")
        free()
        free()
        mouseLeftClickOn("structure_main_pane")
        if appears("model_link"):
            mouseLeftClickOn("model_link")
            if appears("mlink_manager"):
                if appears("mlink_DWG", 10):
                    mouseLeftClickOn("mlink_DWG")
                else:
                    mouseLeftClickOn("mlink_DWG_dropdown")
                    sleep(2)
                    mouseLeftClickOn("mlink_Attach_DWG")
                if appears("mlink_DWG_window", 10):
                    sleep(2)
                    mouseLeftClickOn("mlink_u01_fr67")
                    pyautogui.press('space')
                    mouseLeftClickOn("mlink_ok")
                waitForVanish("mlink_loading")
                sleep(5)
                if appears("mlink_manager",20):
                    mouseLeftClickOn("mlink_manager")
                elif appears("mlink_manager_high",20):
                    mouseLeftClickOn("mlink_manager_high")
                # if appears("mlink_cross_region1",20):
                #     mouseLeftClickOn("mlink_cross1","mlink_cross_region1")
                # elif appears("mlink_cross_region",20):
                #     mouseLeftClickOn("mlink_cross","mlink_cross_region")
                #     pyautogui.click(clicks=2)
                # mouseLeftClickOn("mlink_manager")
                pyautogui.hotkey('alt', 'f4')
                reset_viewport()
                mouseLeftClickOn("visual_sc")
                if appears("list_item_within_xref"):
                    mouseLeftClickOn("list_item_within_xref")
                    free()
                    pyautogui.moveTo(698,651)
                    # mouseMoveTo("fr67_xref")
                    sleep(2)
                    pyautogui.click()
                    sleep(3)
                    pyautogui.press('enter')
                    sleep(5)
                    pyautogui.press('f2')
                    if appears("properties_of_picked_up_parts", 10):
                        mouseMoveTo("properties_of_picked_up_parts")
                        free()
                        pyautogui.press('esc')
                        free()
                        mouseLeftClickOn("structure_main_pane")
                        mouseLeftClickOn("model_link")
                        if appears("mlink_manager"):
                            mouseLeftClickOn("mlink_u01_fr67_DWG")
                            sleep(2)
                            pyautogui.hotkey('ctrl', 'a')
                            mouseLeftClickOn("mlink_cross_DWG")
                            sleep(5)
                            free()
                            if appears("mlink_manager",20):
                                mouseLeftClickOn("mlink_manager")
                            elif appears("mlink_manager_high",20):
                                mouseLeftClickOn("mlink_manager_high")
                            # if appears("mlink_cross_region1",20):
                            #     mouseLeftClickOn("mlink_cross1","mlink_cross_region1")
                            # elif appears("mlink_cross_region",20):
                            #     mouseLeftClickOn("mlink_cross","mlink_cross_region")
                            #     pyautogui.click(clicks=2)
                            # mouseLeftClickOn("mlink_manager")
                            pyautogui.hotkey('alt', 'f4')
                            sleep(5)
                            pyautogui.hotkey('ctrl', 's')
                            sleep(5)
                            self.logger.info("Model link manager was checked")
                        else:
                            raise AssertionError("mlink_manager did not appear")
                    else:
                        raise AssertionError("properties_of_picked_up_parts did not appear")
                else:
                    raise AssertionError("list_item_within_xref did not appear")
            else:
                raise AssertionError("mlink_manager did not appear")
        else:
            raise AssertionError("model_link option did not appear")
        self.logger.info('End:\t Checking Module- mlink_manager')

    # Load PartViews by Selected Extents

    def load_partviews_by_selected_extents(self):
        self.logger.info('Start:\t Checking Module- load_partviews_by_selected_extents')
        sleep(1)
        free()
        pyautogui.typewrite('SCNAVIGATE', .1)
        pyautogui.press('enter')
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("pipe")
            if appears("u02_chilled_water"):
                mouseDoubleClickOn("u02_chilled_water")
                if appears("save_yes",10):
                    mouseLeftClickOn("save_yes")
                if appears("u02_chilled_water_tab"):
                    self.logger.info("u02_chilled_water drawing opened")
                    sleep(10)
                    sleep(10)
                    change_viewpoint_from_aft_prt_up()
                    free()
                    sleep(5)
                    pyautogui.moveTo(563, 524)
                    sleep(2)
                    pyautogui.scroll(clicks=50)
                    sleep(2)
                    pyautogui.scroll(clicks=50)
                    sleep(2)
                    pyautogui.scroll(clicks=50)
                    free()
                    mouseLeftClickOn("sc_util_partview")
                    mouseLeftClickOn("load_selected_extent_selected")
                    free()
                    pyautogui.typewrite('41157.2524,-943.8937', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('40028.0586,-138.7975', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('39117.6568,34.0031', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    sleep(10)
                    if appears("loaded_selected_extent2", 5):
                        self.logger.info("Successfully loaded")
                    free()
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('41016.9016,559.9020', .1)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    mouseLeftClickOn("properties2")
                    if appears("extracted_line_properties",10):
                        mouseLeftClickOn("layer_properties_close_unhigh")
                        free()
                        pyautogui.press('esc')
                        self.logger.info("Load partviews by selected extents was done")
                    else:
                        raise AssertionError("Properties of the loaded extent was not shown")
                else:
                    raise AssertionError("u02_chilled_water_tab did not appear")
            else:
                raise AssertionError("u02_chilled_water did not appear")
            self.logger.info('End:\t Checking Module- load_partviews_by_selected_extents')

    # Viewing PartView in Product Hierarchy

    def viewing_partview_in_product_hierarchy(self):
        self.logger.info('Start:\t Checking Module- viewing_partview_in_product_hierarchy')
        sleep(1)
        mouseLeftClickOn("structure_main_pane")
        mouseLeftClickOn("product_hierarchy_button")
        if appears("product_hierarchy_window"):
            free()
            pyautogui.typewrite('select', .1)
            pyautogui.press('enter')
            pyautogui.typewrite('41016.9016,559.9020', .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("p_3580", 5):
                self.logger.info("Selected part was shown in product hierarchy")
                mouseLeftClickOn("p_3580")
                mouseLeftClickOn("close_product_hierarchy_button")
                free()
                sleep(1)
            elif appears("p_3580_2", 5):
                self.logger.info("Selected part was shown in product hierarchy")
                mouseLeftClickOn("p_3580_2")
                mouseLeftClickOn("close_product_hierarchy_button")
                free()
                sleep(1)
                self.logger.info("Viewing partview in Product Hierarchy was done")
            else:
                raise AssertionError("Selected part (p_3580) was not shown in product hierarchy")
        else:
            raise AssertionError("product_hierarchy_window did not appear")
        self.logger.info('End:\t Checking Module- viewing_partview_in_product_hierarchy')

    # Checking for Interferences with PartViews

    def checking_interferences_with_partviews(self):
        self.logger.info('Start:\t Checking Module- checking_interferences_with_partviews')
        sleep(1)
        change_viewpoint_from_aft_prt_up()
        mouseLeftClickOn("sc_utilities_tools_button")
        mouseLeftClickOn("interference_button")
        if appears('interference_warning_window'):
            pyautogui.press('enter')
        if appears("interference_window"):
            mouseLeftClickOn("run_check2")
            if appears("interference_check_window"):
                mouseClickAppears("check_box_checked")
                mouseLeftClickOn("interference_pipe")
                pyautogui.press('space')
                mouseLeftClickOn("interference_structure")
                mouseLeftClickOn("interference_structure")
                pyautogui.press('space')
                mouseLeftClickOn("interference_check_window_ok")
                if appears("p_6497", 5):
                    mouseRightClickOn("p_6497")
                elif appears("p_6497_2", 5):
                    mouseRightClickOn("p_6497_2")
                else:
                    raise AssertionError("p_6497 was not found")
                mouseLeftClickOn("view_interference")
                sleep(2)
                if appears("ssi_0204", 4):
                    mouseRightClickOn("ssi_0204")
                else:
                    mouseRightClickOn("ssi_0204_dark")
                mouseLeftClickOn("solution")
                if appears("solutions_window"):
                    pyautogui.typewrite('Field cut penetration', .1)
                    pyautogui.press('enter')
                    if appears("solution_field", 3):
                        self.logger.info("Solution was written in the solution_field")
                    elif appears("solution_field_dark", 3):
                        self.logger.info("Solution was written in the solution_field")
                    else:
                        raise AssertionError("Solution was not sritten")
                else:
                    raise AssertionError("solutions_window did not appear")
            else:
                raise AssertionError("interference_check_window did not appear")
        else:
            raise AssertionError("interference_window_ok did not appear")
        self.logger.info('End:\t Checking Module- checking_interferences_with_partviews')

    def creating_interference_report(self):
        self.logger.info('Start:\t Checking Module- creating_interference_report')
        sleep(1)
        mouseLeftClickOn('report_button')
        if appears("interference_report_window"):
            pyautogui.press('tab', presses=4)
            pyautogui.press('space')
            pyautogui.press('tab')
            pyautogui.press('space')
            pyautogui.press('tab')
            pyautogui.press('space')
            mouseLeftClickOn("interference_check_window_ok")
            if appears("report_log", 10):
                self.logger.info('Report was generated successfully')
                pyautogui.hotkey('alt', 'f4')
                sleep(1)
                pyautogui.hotkey('alt', 'f4')
                pyautogui.hotkey('ctrl', 's')
                pyautogui.hotkey('ctrl', 's')
                sleep(10)
                pyautogui.hotkey('ctrl', 's')
            else:
                raise AssertionError("report_log was not appeared")
        else:
            raise AssertionError("interference_report_window did not appear")
        self.logger.info('End:\t Checking Module- creating_interference_report')

    # Load PartViews by SCAdvPartViewLoadManager

    def SCAdvPartViewLoadManager(self):
        self.logger.info('Start:\t Checking Module- SCAdvPartViewLoadManager')
        sleep(1)
        mouseLeftClickOn("visual_sc")
        mouseLeftClickOn("unhide_obj_selected")
        mouseLeftClickOn("unhide_all_obj")
        sleep(1)
        free()
        free()
        pyautogui.typewrite("SCAdvPartViewLoadManager", .1)
        pyautogui.press('enter')
        if appears("subscription_advantage_pack"):
            pyautogui.press('enter')
        if appears("load_manager"):
            mouseLeftClickOn("load_manager_assemblies")
            sleep(1)
            mouseLeftClickOn("load_manager_select_specific")
            if appears("select_assemblies_window"):
                mouseLeftClickOn("select_assemblies_u02_expand", "select_assemblies_u02_region")
                if appears("assembly_204_region"):
                    mouseLeftClickOn("assembly_204_select", "assembly_204_region")
                    mouseLeftClickOn("select_assemblies_window_ok")
                    sleep(15)
                    mouseLeftClickOn("sc_util_partview")
                    mouseLeftClickOn("partview_delete_all")
                    mouseLeftClickOn("partview_refresh")
                    free()
                    sleep(3)
                    pyautogui.hotkey('ctrl', 's')
                    sleep(10)
                    self.logger.info("SCAdvPartViewLoadManager was checked")
                else:
                    raise AssertionError("assembly_204_region did not appear")
            else:
                raise AssertionError("select_assemblies_window did not appear")
        else:
            raise AssertionError("load_manager did not appear")
        self.logger.info('End:\t Checking Module- SCAdvPartViewLoadManager')

class TCP4_SC_EssentialsError(Exception):
    pass
