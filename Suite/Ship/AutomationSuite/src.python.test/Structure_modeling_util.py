# Structure_modeling.py
# It contains the commonly used functions in Structure modeling curriculum

from time import sleep

from IOUtil import mouseLeftClickOn, mouseDoubleClickOn

from screenObjUtil import exists, appears

from pyautogui import typewrite, press, click, hotkey

import SSIUtil


def no_markline(point1, point2, point3=None, point4=None):
    # @desc - Used when NO MARKLINE option is not available
    mouseLeftClickOn("markline")
    mouseLeftClickOn("extract")
    typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
    press("enter")
    typewrite(point1, interval=0.25)
    typewrite(',',interval=0.25)
    typewrite(point2, interval=0.25)
    press("enter")
    if point3 is not None:
        typewrite(point3, interval=0.25)
        typewrite(',', interval=0.25)
        typewrite(point4, interval=0.25)
        press("enter")
    press("enter")
    sleep(1)
    press("enter")

def create_planar_groups(x1, y1, x2= None, y2= None, name= None):
    """
    @desc - To create planar group from hull drawing
    :param - coordinates of planar group
    """
    typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
    press("enter")
    typewrite(x1 ,.2)
    press(',')
    typewrite(y1 ,.2)
    press('enter')
    if x2 is not None:
        typewrite(x2 ,.2)
        press(',')
        typewrite(y2 ,.2)
        press('enter')
    click(button='right', clicks=2, interval=0.25)
    if appears("markline", 30):
        if x2 is not None:
            no_markline(point1=x1, point2=y1, point3=x2, point4=y2)
        else:
            no_markline(point1=x1, point2=y1)
    else:
        press('esc')
    press('esc')
    press('esc')
    sleep(1)
    mouseDoubleClickOn("structure_ribbon_button")
    mouseLeftClickOn("utilities_pane")
    if appears("create_planar_group_button_region1",5):
        mouseLeftClickOn("create_planar_group_drop_down_arrow2","create_planar_group_button_region1")
        mouseLeftClickOn("create_planar_group_button2")
    elif appears("create_planar_group_button_region",5):
        mouseLeftClickOn("create_planar_group_button_region")
        # mouseLeftClickOn("create_planar_group_drop_down_arrow", "create_planar_group_button_region2")
        # mouseLeftClickOn("create_planar_group_button3")
    SSIUtil.free()
    typewrite(['s', 'e', 'l', 'e', 'c', 't'], interval=0.25)
    press("enter")
    typewrite(x1, .2)
    press(',')
    typewrite(y1, .2)
    press('enter')
    sleep(1)
    if x2 is not None:
        typewrite(x2, .2)
        press(',')
        typewrite(y2, .2)
        press('enter')

    press("enter")
    if appears("new_frame_group_window"):
        press("enter")
        if appears("join_window",5):
            mouseLeftClickOn("join_curves")
            press('enter')
    else:
        raise AssertionError("New Frame group window not visible")
    if appears("planar_group_created_message"):
        print(name+" Planar Group Created")
        hotkey('alt', 'f4')
    else:
        raise AssertionError(name + " Planar Group not Created")


def open_U03_drawing(drawing_name, tab_name):
    """
    @desc - To open U03 drawing
    :param - name of the screenshot of drawing and drawing tab
    """
    sleep(2)
    SSIUtil.open_navigator()
    mouseLeftClickOn("u03_node2","u03_node2_region")
    mouseLeftClickOn("structure_node2")
    if appears(drawing_name):
        mouseDoubleClickOn(drawing_name)
        if appears("save_caution",5):
            mouseLeftClickOn("save_caution_no")
        sleep(3)
        if appears(tab_name):
            print(tab_name+ " properly opened")
        else:
            raise AssertionError("Hull Drawing not properly opened")
    else:
        raise AssertionError(drawing_name+" node not visible")


def select_new_plate_option():
    # @desc - To select create new plate option
    if appears("structure_ribbon_button",2):
        mouseDoubleClickOn("structure_ribbon_button")
    if appears("plates_pane"):
        mouseLeftClickOn("plates_pane")
        if appears("new_plate_button"):
            mouseLeftClickOn("new_plate_button")
            SSIUtil.free()
            sleep(1)
        else:
            raise AssertionError("new_plate_button did not appear")


def add_corner_treatment():
    # @desc - To select add corner treatment option
    if appears("structure_ribbon_button",2):
        mouseLeftClickOn("structure_ribbon_button")
    mouseLeftClickOn("plates_pane")
    if appears("corner_treatment"):
        mouseLeftClickOn("corner_treatment")
        SSIUtil.free()
        SSIUtil.free()
    else:
        raise AssertionError("corner_treatment did not appear")


def select_new_stiffener_option():
    # @desc - To select create new stiffener option
    if appears("structure_ribbon_button",2):
        mouseDoubleClickOn("structure_ribbon_button")
    if appears("stiffeners"):
        mouseLeftClickOn("stiffeners")
        sleep(1)
        mouseLeftClickOn("new_stiffeners")
        SSIUtil.free()
    else:
        raise AssertionError("stiffeners option did not appear")


def select_new_faceplate_option():
    # @desc - To select create new faceplate option
    if appears("structure_ribbon_button",2):
        mouseDoubleClickOn("structure_ribbon_button")
    mouseLeftClickOn("faceplates_button")
    if appears("new_faceplate"):
        mouseLeftClickOn("new_faceplate")
        SSIUtil.free()
    else:
        raise AssertionError("new_faceplate did not appear")


def select_new_custom_plate_option():
    # @desc - To select create new custom plate option
    if appears("structure_ribbon_button",2):
        mouseDoubleClickOn("structure_ribbon_button")
    mouseLeftClickOn("custom_plate_pane")
    if appears("new_custom_plate"):
        mouseLeftClickOn("new_custom_plate")
        SSIUtil.free()
    else:
        raise AssertionError("new_custom_plate option did not appear")


