import os
import ctypes
import config

user32 = ctypes.windll.user32

class Initialize(object):

    value = {
        "Image directory": None,
    }

    def set_image_directory(self):
        # Setting up the Module name
        module_name= None
        file_name = "Module.txt"

        # Looking for Module.txt file. If the file is absent it will be created
        if file_name not in os.listdir(os.getcwd()):
            file = open(file_name, "w")

        # Module.txt is opened and the name of the module is written on module_name variable
        file = open(file_name, 'r')
        for word in file:
            if word is not None:
                module_name= word

        self.value["Image directory"] = config.ImageFolder + "/windows/" + str(user32.GetSystemMetrics(0)) + "x" + str(user32.GetSystemMetrics(1)) + "/" + module_name + "/"
        file.close()

    def get_image_directory(self):
        # Returns the Image directory value
        if self.value["Image directory"] is not None:
            # warn("IMAGE-> " + self.value["Image directory"])
            return self.value["Image directory"]
        else:
            return None

    def read_path_from_module_file(self):
        os.chdir("../../test.images/")
        file_name = "Module.txt"
        f = open(file_name, "r")
        for word in f:
            if word is not None:
                # warn(word)
                module_name = word

        self.value["Image directory"] = config.ImageFolder + "/windows/" + str(user32.GetSystemMetrics(0)) + "x" + str(
            user32.GetSystemMetrics(1)) + "/" + module_name + "/"
        f.close()
        return  self.value["Image directory"]