"""
*** TCP5_Pipe_Catalog.py ***

Automation script for Pipe Catalog (Page 1 - 115)

Covers the following sections:
-  Pipe Stock Catalog Prerequisites
-  Manufacturers Library
-  Accessory Packages Library
-  Pipe Stock Catalog, Size Definitions
-  Pipe Stock Catalog, End Treatments
-  Pipe Stock Catalog, Create/Edit Pipe Elements
-  Specs
-  Catalogs
-  Creating Pipe Elements
-  Pipe Stock Catalog and Connections
-  Pipe Benders Catalog

"""

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

from time import sleep

from logger_extended import logger_extended

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseDoubleClickOn

from screenObjUtil import exists, appears, waitForVanish

import pyautogui, os

from SSIUtil import free, maximize

from time_tracker import my_timer


class TCP5_Pipe_Catalog(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP8_Pipe_Catalog')

    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC05_Pipe_Catalog")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_pipeCatalog")
        obj.launch_app()
        obj1.project_deployer(module="pipeCatalog")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    @my_timer
    def register_project(self):
        maximize()
        obj= AppUtil()
        obj.ribbon_load("hull_and_structure_tab")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("deploy_pipe_catalog_list", "NewDeploy_pipeCatalog")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_pipeCatalog")

    # Pipe Stock Catalog – Prerequisites

    # Materials Library
    @my_timer
    def materials_library(self):
        self.logger.info('Start:\t Checking Module- materials_library')
        sleep(2)
        mouseLeftClickOn("shipconstructor_tab_new")
        if appears("manage_pane"):
            mouseLeftClickOn("manage_pane")
            mouseLeftClickOn("manage_button")
            if appears("shipcon_manager"):
                pyautogui.hotkey('win','up')
                self.logger.info("shipcon_manager appeared")
                mouseLeftClickOn("general_button")
                mouseLeftClickOn("material_button")
                if appears("material_window"):
                    self.logger.info("material_window appeared")
                    mouseLeftClickOn("density_units")
                    if appears("density_units_menu"):
                        self.logger.info("density_units_menu appeared")
                        pyautogui.click()
                        mouseLeftClickOn("material_button_okay")
                        sleep(1)
                        self.logger.info("Materials library was checked")
                    else:
                        raise AssertionError("Materials Library could not be checked")
                else:
                    raise AssertionError("material_window did not appear")
            else:
                raise AssertionError("shipcon_manager window did not appear")
        else:
            raise AssertionError("manage_pane did not appear")
        self.logger.info('End:\t Checking Module- materials_library')

    # Manufacturers Library
    @my_timer
    def manufacturers_library(self):
        self.logger.info('Start:\t Checking Module- manufacturers_library')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("manufact_button")
        if appears("manufact_window"):
            self.logger.info("manufact_window appeared")
            mouseLeftClickOn("manufact_button_new")
            sleep(1)
            pyautogui.typewrite('Test-Manufacturer', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("manufact_button_ok")
            self.logger.info("Manufacturers Library checked")
        else:
            raise AssertionError("manufacturers window did not appear")
        self.logger.info('End:\t Checking Module- manufacturers_library')

    @my_timer
    def creating_uda(self):
        self.logger.info('Start:\t Checking Module- creating_uda')
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("uda_hvac_manager")
        sleep(1)
        if appears("maximize_win_region",2):
            mouseLeftClickOn("maximize_win")
        if appears("new_uda"):
            self.logger.info("UDA Manager opened")
        mouseLeftClickOn("new_uda")
        pyautogui.typewrite('Test_Attr')
        pyautogui.press('enter')
        mouseLeftClickOn("parts_expand","parts_region")
        mouseLeftClickOn("Flange")
        sleep(1)
        mouseLeftClickOn("add_button2")
        sleep(1)
        mouseLeftClickOn("not_required")
        sleep(1)
        mouseLeftClickOn("drop_down3")
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('down')
        pyautogui.press('enter')
        pyautogui.press('up')
        pyautogui.press('right')
        pyautogui.typewrite('Value', .1)
        pyautogui.press('enter')
        mouseLeftClickOn("ok4")
        if appears("uda_warning"):
            pyautogui.press('enter')
            self.logger.info("New UDA was created")
        else:
            self.logger.info('uda_warning did not appear')
        sleep(2)
        self.logger.info('End:\t Checking Module- creating_uda')
    # Accessory Packages

    @my_timer
    def creating_new_accessory_type(self):
        self.logger.info('Start:\t Checking Module- creating_new_accessory_type')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("packages_button")
        if appears("accessory_pkj_window",10):
            sleep(1)
            mouseLeftClickOn("maximize_win")
            self.logger.info('accessory_pkj_window appeared')
        elif appears("accessory_pkj_window2",10):
            self.logger.info('accessory_pkj_window appeared')
        else:
            raise AssertionError("accessory package window did not appear")
        mouseLeftClickOn("accessory_pkj_newtype_button")
        pyautogui.typewrite('Studs',.1)
        pyautogui.press('enter')
        if appears("accessory_pkj_newstud",1):
            self.logger.info("Studs package was created")
            mouseLeftClickOn("accessory_pkj_newstud")
            pyautogui.typewrite('Studs M12x50',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('DIN 835-5.8',.1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("New_package_button")
            pyautogui.typewrite('DN80 Stud Connection',.1)
            pyautogui.press('enter')
            if appears("studs2",2):
                mouseLeftClickOn("studs2")
            elif appears("studs",2):
                mouseLeftClickOn("studs")
            mouseLeftClickOn("add_button")
            mouseLeftClickOn("nuts_button_expand","hex_bolt_region")
            if appears("hex_bolt_12x60"):
                mouseLeftClickOn("hex_bolt_12x60")
            else:
                raise AssertionError("hex_bolt_12x60 did not appear")
            mouseLeftClickOn("add_button")
            mouseDoubleClickOn("qty1","qty1_region")
            pyautogui.press('right')
            pyautogui.press('backspace')
            pyautogui.press('6')
            pyautogui.press('down')
            pyautogui.press('enter')
            pyautogui.typewrite('12',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("stud_connection")
            mouseLeftClickOn("assign_spec")
            sleep(2)
            pyautogui.hotkey('win', 'up')
            if appears("assign_to_spec_window4"):
                self.logger.info("assign_to_spec_window appeared")
                mouseLeftClickOn("assigned")
                for num in range(0, 21):
                    pyautogui.press('down')
                    pyautogui.press('enter')
                mouseLeftClickOn("assign_ok")
                sleep(2)
                mouseLeftClickOn("dn80_stud_connection")
                mouseLeftClickOn("copy_package")
                sleep(1)
                pyautogui.typewrite('Copy of dn80 stud connection', .1)
                pyautogui.press('enter')
                mouseLeftClickOn("accessory_pkj_ok")
                self.logger.info("New accessory type was created")
            else:
                raise AssertionError("assign_to_spec_window did not appear")
        else:
            raise AssertionError("New Studs button did not appear")
        self.logger.info('End:\t Checking Module- creating_new_accessory_type')

    # Naming Conventions - Why You Need Naming Conventions
    @my_timer
    def settings_of_autonumber(self):
        self.logger.info('Start:\t Checking Module- settings_of_autonumber')
        if appears('general_button'):
            mouseLeftClickOn("general_button")
        mouseLeftClickOn("naming_convention_button2")
        if appears("naming_window"):
            self.logger.info("naming_window appeared")
            mouseLeftClickOn("hvac_parts_expand","naming_conv_pipe_parts_region")
            mouseLeftClickOn("pipe_parts")
            mouseLeftClickOn("new_name")
            if appears("new_name2_light"):
                mouseDoubleClickOn("new_name2_light")
            else:
                mouseDoubleClickOn("new_name2")
            pyautogui.typewrite('Test',.1)
            pyautogui.press('enter')
            pyautogui.press('up')
            mouseDoubleClickOn("_static")
            pyautogui.click(button='left',clicks=2,interval=.01)
            pyautogui.press('backspace',presses=10)
            pyautogui.typewrite('SSI-P-',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("new_static_drop_down","new_static_region")
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("catalog_region")
            mouseLeftClickOn("drop_down4")
            pyautogui.press('down',presses=2,interval=.2)
            pyautogui.press('enter')
            pyautogui.press('down',presses=10)
            mouseLeftClickOn("drop_down4")
            pyautogui.press('down', presses=2, interval=.2)
            pyautogui.press('enter')
            mouseLeftClickOn("new_static_drop_down","new_db_item")
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseDoubleClickOn("_static")
            pyautogui.click(button='left',clicks=2,interval=.01)
            pyautogui.press('backspace',presses=10)
            pyautogui.typewrite('-',.1)
            pyautogui.press('enter')
            if appears("auto_number"):
                mouseLeftClickOn("auto_number")
                mouseLeftClickOn("auto")
                pyautogui.press('3')
                pyautogui.press('enter')
            else:
                raise AssertionError("AutoNumber field was not found")
            mouseLeftClickOn("ssi_p")
            mouseMoveTo("up1")
            pyautogui.click(clicks=3,interval=.5)
            mouseLeftClickOn("_'-'")
            mouseMoveTo('down1')
            pyautogui.click(clicks=2,interval=.5)
            if appears("sample_name_pipe"):
                mouseLeftClickOn("ok5")
                self.logger.info("Setting of auto number is checked")
            else:
                mouseLeftClickOn("ok5")
                raise AssertionError("sample_name_pipe was not matched.")
        else:
            raise AssertionError("naming_window did not appear")
        self.logger.info('End:\t Checking Module- settings_of_autonumber')

    @my_timer
    def exercise_pc_r001(self):
        self.logger.info('Start:\t Checking Module- exercise_pc_r001')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("packages_button")
        if appears("accessory_pkj_window",10):
            self.logger.info("accessory_pkj_window appeared")
        elif appears("accessory_pkj_window2",10):
            self.logger.info("accessory_pkj_window appeared")
        else:
            raise AssertionError("accessory package window did not appear")
        sleep(1)
        mouseLeftClickOn("studs_created")
        mouseLeftClickOn("accessory_pkj_newstud")
        pyautogui.typewrite('Studs M12x40', .1)
        pyautogui.press('enter')
        pyautogui.press('right')
        pyautogui.typewrite('DIN 835-5.8', .1)
        pyautogui.press('enter')
        sleep(1)
        mouseLeftClickOn("New_package_button")
        pyautogui.typewrite('Ex_Pack', .1)
        pyautogui.press('enter')
        sleep(1)
        mouseLeftClickOn("accessory_pkj_ok")
        sleep(1)
        self.logger.info("New accessory type was created")

        sleep(2)
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("uda_hvac_manager")
        sleep(1)
        if appears("short_desc"):
            mouseLeftClickOn("short_desc")
            sleep(.5)
            mouseLeftClickOn("parts_expand", "parts_region")
            mouseLeftClickOn("parts_expand", "uda_ball_region")
            sleep(.5)
            mouseLeftClickOn("uda_ball")
            sleep(1)
            mouseLeftClickOn("add_button2")
            sleep(1)
            mouseLeftClickOn("not_required")
            sleep(1)
            mouseLeftClickOn("drop_down3")
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('down')
            pyautogui.press('enter')
            pyautogui.press('up')
            pyautogui.press('right')
            pyautogui.typewrite('Value', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("ok4")
            if appears("uda_warning"):
                pyautogui.press('enter')
                self.logger.info("UDA was created")
                self.logger.info("Exercise R001 was done")
            else:
                self.logger.info('uda_warning did not appear')
        else:
            raise AssertionError("short_desc did not appear")
        sleep(2)
        self.logger.info('End:\t Checking Module- exercise_pc_r001')

    # Pipe Stock Catalog, Size Definitions
    @my_timer
    def filtering_by_units(self):
        self.logger.info('Start:\t Checking Module- filtering_by_units')
        sleep(4)
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("stock_catalog")
        sleep(3)
        if appears("maximize_win", 5):
            mouseLeftClickOn("maximize_win")
        if appears("pipe_stock_catalog_window_sign"):
            self.logger.info("pipe_stock_catalog_window appeared")
            if appears("size_definition_tab",5):
                mouseLeftClickOn("size_definition_tab")
            if appears("filter_matric_region"):
                mouseLeftClickOn("filter_matric_radio","filter_matric_region")
            else:
                raise AssertionError("filter_matric_region did not appear")
            mouseLeftClickOn("pressure_rating_filter_click","pressure_rating_filter_region")
            pyautogui.typewrite("DIN",.1)
            pyautogui.press('enter')
            sleep(1)
            if appears("delete_filter18"):
                mouseLeftClickOn("delete_filter18")
                self.logger.info("Filtering by unit was checked")
            else:
                raise AssertionError("delete_filter option did nor appear")
        else:
            raise AssertionError("pipe_stock_catalog_window_sign did not appear")
        self.logger.info('End:\t Checking Module- filtering_by_units')

    @my_timer
    def creating_nominal_size(self):
        self.logger.info('Start:\t Checking Module- creating_nominal_size')
        mouseLeftClickOn("edit_nominal_size")
        if appears("nomial_size_window"):
            self.logger.info("nomial_size_window appeared")
            mouseLeftClickOn("nominal_sizez_new_button","nominal_sizez_new_button_region")
            pyautogui.typewrite("175",.1)
            pyautogui.press('enter')
            sleep(2)
            pyautogui.press('right')
            pyautogui.press('right')
            pyautogui.press('7')
            pyautogui.press('enter')
            mouseLeftClickOn("nominal_size_window_ok")
            self.logger.info("Nominal size was created")
        else:
            raise AssertionError("nomial_size_window did not appear")
        self.logger.info('End:\t Checking Module- creating_nominal_size')

    @my_timer
    def creating_an_international_standard(self):
        self.logger.info('Start:\t Checking Module- creating_an_international_standard')
        mouseLeftClickOn("edit_standards")
        sleep(2)
        pyautogui.hotkey('win', 'up')
        sleep(1)
        if appears("standards_window2"):
            self.logger.info("standards_window appeared")
            mouseLeftClickOn("new_international_button")
            pyautogui.typewrite('Test',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.press('right')
            pyautogui.typewrite('Test International Standard',.1)
            pyautogui.press('enter')
            self.logger.info("Test International Standard was created")
            mouseLeftClickOn("new_geometric_button")
            pyautogui.typewrite('1111',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.press('right')
            pyautogui.typewrite('Test Geometric Standard',.1)
            pyautogui.press('enter')
            self.logger.info("Test Geometric Standard was created")
            mouseLeftClickOn("standards_new_pressure_rating","standards_new_delete_region")
            pyautogui.typewrite("Test1",.1)
            pyautogui.press('enter')
            pyautogui.press('left')
            pyautogui.press('space')
            mouseLeftClickOn("pipe_stock_pipe")
            pyautogui.press('left')
            pyautogui.press('space')
            for x in range(0, 27):
                sleep(.5)
                if x == 10:
                    pyautogui.press('down')
                elif x == 16:
                    pyautogui.press('down')
                else:
                    pyautogui.press('down')
                    pyautogui.press('space')
            mouseLeftClickOn("standards_window_apply")
            sleep(.5)
            mouseLeftClickOn("standards_window_ok")
        else:
            raise AssertionError("standards_window did not appear")
        self.logger.info('End:\t Checking Module- creating_an_international_standard')

    @my_timer
    def creating_a_size_definition(self):
        sleep(1)
        self.logger.info('Start:\t Checking Module- creating_a_size_definition')
        mouseMoveTo("connection_tab_scroll_down")
        while exists("400_DN") == False:
            pyautogui.click(clicks=10)
            if appears("400_DN_selected",1):
                break
            elif appears("400_DN_selected2",1):
                break
        if appears("400_DN",2):
            mouseLeftClickOn("400_DN")
        mouseLeftClickOn("size_definition_tab_new_button")
        pyautogui.press('down',presses=20)
        if appears("175_7_region"):
            mouseLeftClickOn("175_7_region")
            self.logger.info("175 was selected as DN")
        pyautogui.press('right')
        pyautogui.press('enter')
        if appears("pressure_rating_test_expand_region"):
            mouseLeftClickOn("pressure_rating_test_expand","pressure_rating_test_expand_region")
            mouseLeftClickOn("pressure_rating_test_expand","pressure_rating_test_expand_region2")
            mouseLeftClickOn("1111_select")
            pyautogui.press('down')
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('175',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('5',.5)
            pyautogui.press('enter')
            if appears("calculated_inner_diameter",2):
                self.logger.info("Inner Diameter was set to 165")
            elif appears("calculated_inner_diameter2",2):
                self.logger.info("Inner Diameter was set to 165")
            mouseLeftClickOn("new_next_size")
            if appears("new_size_definitio2",2):
                self.logger.info("New Size Definition was created")
                mouseLeftClickOn("next_size_outer_diameter2")
            elif appears("new_size_definition1",2):
                self.logger.info("New Size Definition was created")
                mouseLeftClickOn("next_size_outer_diameter")
            pyautogui.typewrite('200',.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('8',.5)
            pyautogui.press('enter')
        else:
            raise AssertionError("pressure_rating_test_expand_region was not found")
        self.logger.info('End:\t Checking Module- creating_a_size_definition')

    @my_timer
    def exercise_pc_r002(self):
        self.logger.info('Start:\t Checking Module- exercise_pc_r002')
        mouseLeftClickOn("edit_nominal_size")
        sleep(20)
        if appears("nomial_size_window"):
            self.logger.info("nomial_size_window appeared")
            mouseLeftClickOn("nominal_sizez_new_button", "nominal_sizez_new_button_region")
            pyautogui.typewrite("225", .1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            mouseLeftClickOn("nominal_size_window_ok")
            mouseLeftClickOn("edit_standards")
            if appears("standards_window2"):
                pyautogui.hotkey('win', 'up')
                sleep(1)
                mouseLeftClickOn("new_international_button")
                pyautogui.typewrite('EX_Int', .1)
                pyautogui.press('enter')
                mouseLeftClickOn("new_geometric_button")
                pyautogui.typewrite('EX_Geom', .1)
                pyautogui.press('enter')
                mouseLeftClickOn("2th2_0")
                pyautogui.press('left')
                pyautogui.press('space')
                pyautogui.press('down')
                pyautogui.press('space')
                mouseLeftClickOn("pipe_stock_pipe")
                pyautogui.press('left')
                pyautogui.press('space')
                pyautogui.press('down')
                pyautogui.press('space')
                pyautogui.press('down', presses=6, interval=.2)
                pyautogui.press('space')
                mouseLeftClickOn("standards_window_apply")
                sleep(.5)
                mouseLeftClickOn("standards_window_ok")
                sleep(1)
                mouseLeftClickOn("size_definition_tab_new_button")
                pyautogui.press('2')
                pyautogui.press('enter')
                self.logger.info("225 was selected as DN")
                mouseLeftClickOn("std_pressure_rating_selector")
                if appears("ex_int_expand_region"):
                    mouseLeftClickOn("pressure_rating_test_expand", "ex_int_expand_region")
                    mouseLeftClickOn("pressure_rating_test_expand", "ex_geom_expand_region")
                    mouseDoubleClickOn("2th2_0_list")
                    pyautogui.press('right')
                    pyautogui.typewrite('225', .1)
                    pyautogui.press('enter')
                    pyautogui.press('right')
                    pyautogui.typewrite('2', .1)
                    pyautogui.press('enter')
                    mouseLeftClickOn("new_next_size")
                    if appears("int_ex_geom_selected", 2):
                        self.logger.info("New Size Definition was created")
                        mouseLeftClickOn("int_ex_geom_selected")
                        mouseLeftClickOn("std_pressure_rating_selector")
                        mouseDoubleClickOn("2th2_3_list")
                        pyautogui.press('right')
                        pyautogui.typewrite('225', .1)
                        pyautogui.press('enter')
                        pyautogui.press('right')
                        pyautogui.typewrite('2.3', .1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("size_definition_tab_apply")
                else:
                    raise AssertionError("pressure_rating_test_expand_region was not found")
            else:
                raise AssertionError("standards_window2 did not appear")
        else:
            raise AssertionError("nomial_size_window did not appear")
        self.logger.info('End:\t Checking Module- exercise_pc_r002')

    @my_timer
    def creating_new_icon(self):
        self.logger.info('Start:\t Checking Module- creating_new_icon')
        mouseLeftClickOn("end_treat_tab_piping")
        if appears("save_warning_18"):
            mouseLeftClickOn("save_warning_18_yes")
        sleep(2)
        mouseLeftClickOn("line_mode_button")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("line_mode_window"):
            self.logger.info("line_mode_window appeared")
            mouseLeftClickOn("line_mode_new")
            pyautogui.typewrite('Test-FL', .1)
            pyautogui.press('enter')
            pyautogui.press('space')
            pyautogui.press('right')
            pyautogui.press('r')
            pyautogui.press('enter')
            mouseLeftClickOn("scale_to_paperspace_check", "scale_to_paperspace_region")
            mouseLeftClickOn("line_mode_window_ok")
            self.logger.info("Creating new icon done")
        else:
            raise AssertionError("line_mode_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_icon')

    @my_timer
    def creating_new_treatment_type(self):
        self.logger.info('Start:\t Checking Module- creating_new_treatment_type')
        mouseLeftClickOn("treatment_type_button")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("treatment_type_window"):
            self.logger.info("treatment_type_window appeared")
            mouseLeftClickOn("treatment_type_new")
            pyautogui.typewrite("Test-FL",.1)
            pyautogui.press('enter')
            pyautogui.press('right',presses=3,interval=.2)
            pyautogui.press('f',presses=2,interval=.2)
            sleep(1)
            mouseLeftClickOn("treatment_type_window_ok")
            self.logger.info("Creating new treatment type was done")
        else:
            raise AssertionError("treatment_type_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_treatment_type')

    @my_timer
    def properties(self):
        self.logger.info('Start:\t Checking Module- properties')
        mouseLeftClickOn("edit_properties_button")
        sleep(1)
        pyautogui.hotkey('win','up')
        if appears("end_treat_properties_window"):
            self.logger.info("end_treat_properties_window appeared")
            mouseLeftClickOn("general_properties_new","general_properties_new_region")
            pyautogui.typewrite("Test-Gen-Prop",.1)
            pyautogui.press('enter')
            mouseLeftClickOn("general_properties_new","description_prop_new_region")
            pyautogui.typewrite("Test-Descr-Prop",.1)
            pyautogui.press('enter')
            mouseLeftClickOn("general_properties_new", "facing_properties_new_region")
            pyautogui.typewrite("Test-Test-Prop", .1)
            pyautogui.press('enter')
            sleep(1)
            mouseLeftClickOn("end_treat_properties_window_ok")
            self.logger.info("End treatment properties was checked")
        else:
            raise AssertionError("end_treat_properties_window did not appear")
        self.logger.info('End:\t Checking Module- properties')

    @my_timer
    def creating_end_treatment(self):
        self.logger.info('Start:\t Checking Module- creating_end_treatment')
        mouseLeftClickOn("175mm_treatment")
        mouseLeftClickOn("end_treatment_new")
        if appears("edit_end_treatment_window"):
            self.logger.info("edit_end_treatment_window appeared")
            if appears("type_select2",2):
                mouseLeftClickOn("type_select2")
            elif appears("type_select",2):
                mouseLeftClickOn("type_select")
            pyautogui.press('t')
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.press('c')
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('24', .1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.typewrite('310', .1)
            pyautogui.press('enter')
            pyautogui.press('right', presses=2)
            pyautogui.press('t')
            pyautogui.press('right')
            pyautogui.press('t')
            pyautogui.press('right')
            pyautogui.press('t')
            sleep(1)
            mouseLeftClickOn("edit_end_treatment_window_ok")
            if appears("test_fl_created"):
                self.logger.info("New end treatment was created")
            else:
                raise AssertionError("New end treatment was not created")
        else:
            raise AssertionError("edit_end_treatment_window did not appear")
        self.logger.info('End:\t Checking Module- creating_end_treatment')

    @my_timer
    def exercise_pc_r003(self):
        self.logger.info('Start:\t Checking Module- exercise_pc_r003')
        mouseLeftClickOn("treatment_type_button")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("treatment_type_window"):
            self.logger.info("treatment_type_window appeared")
            mouseLeftClickOn("treatment_type_new")
            pyautogui.typewrite("EX-PL",.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('P')
            sleep(1)
            pyautogui.moveTo(100,200)
            mouseLeftClickOn("treatment_type_new")
            pyautogui.typewrite("EX-FL",.1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('right')
            sleep(1)
            pyautogui.press('F')
            sleep(1)
            mouseLeftClickOn("treatment_type_window_ok")
            sleep(2)
            mouseLeftClickOn("225mm_treatment")
            mouseLeftClickOn("end_treatment_new")
            if appears("edit_end_treatment_window"):
                self.logger.info("edit_end_treatment_window appeared")
                if appears("type_select2", 2):
                    mouseLeftClickOn("type_select2")
                elif appears("type_select", 2):
                    mouseLeftClickOn("type_select")
                pyautogui.press('e', presses=2)
                pyautogui.press('enter')
                sleep(1)
                mouseLeftClickOn("edit_end_treatment_window_ok")
                mouseLeftClickOn("end_treatment_new")
                if appears("edit_end_treatment_window"):
                    self.logger.info("edit_end_treatment_window appeared")
                    if appears("type_select2", 2):
                        mouseLeftClickOn("type_select2")
                    elif appears("type_select", 2):
                        mouseLeftClickOn("type_select")
                    pyautogui.press('e', presses=3)
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("edit_end_treatment_window_ok")
                    # if appears("ex_pl_fl_created"):
                    #     self.logger.info("Both EX-pl and EX-fl were created")
                    # else:
                    #     raise AssertionError("Exercise R003 could not be done")
                    mouseLeftClickOn("size_definition_tab_apply")
                    sleep(1)
            else:
                raise AssertionError("edit_end_treatment_window did not appear")
        else:
            raise AssertionError("treatment_type_window did not appear")
        self.logger.info('End:\t Checking Module- exercise_pc_r003')

    # Pipe Stock Catalog, Create/Edit Pipe Elements

    @my_timer
    def creating_new_spec(self):
        sleep(4)
        self.logger.info('Start:\t Checking Module- creating_new_spec')
        mouseLeftClickOn("create_spec_tab")
        sleep(1)
        if appears("edit_specs_button"):
            mouseLeftClickOn("edit_specs_button")
            if appears("pipe_spec_window"):
                self.logger.info("pipe_spec_window appeared")
                if appears("pipe_spec_new_region", 10):
                    mouseLeftClickOn("pipe_spec_new", "pipe_spec_new_region")
                elif appears("pipe_spec_new_region_high", 10):
                    mouseLeftClickOn("pipe_spec_new", "pipe_spec_new_region_high")
                pyautogui.typewrite('Test-Spec', .1)
                pyautogui.press('enter')
                mouseLeftClickOn("pipe_spec_window_ok")
                self.logger.info("Creating new spec was done")
            else:
                raise AssertionError("pipe_spec_window did not appear")
        else:
            raise AssertionError("edit_specs_button did not appear")
        self.logger.info('End:\t Checking Module- creating_new_spec')

    # Assigning Stocks to Spec/Specs

    @my_timer
    def assigning_stocks_to_spec(self):
        self.logger.info('Start:\t Checking Module- assigning_stocks_to_spec')
        if appears("pipe_expand_region",2):
            mouseLeftClickOn("pipe_expand_click","pipe_expand_region")
        mouseLeftClickOn("delete_filter18")
        mouseLeftClickOn("pipe_20")
        sleep(.5)
        mouseLeftClickOn("assign_to_spec_button")
        if appears("assign_to_spec_window3"):
            self.logger.info("Assign to spec window appeared")
            if appears("test_spec"):
                mouseLeftClickOn("test_spec")
                pyautogui.press('space')
                sleep(.5)
                mouseLeftClickOn("assign_to_spec_window_ok")
                self.logger.info("Assigning stocks to spec was done")
            else:
                raise AssertionError("test_spec did not appear")
        else:
            raise AssertionError("assign_to_spec_window3 did not appear")
        self.logger.info('End:\t Checking Module- assigning_stocks_to_spec')

    @my_timer
    def creating_new_catalog(self):
        self.logger.info('Start:\t Checking Module- creating_new_catalog')
        sleep(1)
        mouseLeftClickOn("edit_catalog_button")
        if appears("pipe_catalog_window"):
            self.logger.info("pipe_catalog_window appeared")
            mouseLeftClickOn("edit_catalog_new_button")
            pyautogui.typewrite('Test-Cat', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("pipe_spec_window_ok")
            self.logger.info("Creating new catalog was done")
        else:
            raise AssertionError("pipe_catalog_window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_catalog')

    @my_timer
    def assigning_stocks_to_catalog(self):
        self.logger.info('Start:\t Checking Module- assiging_stocks_to_catalog')
        sleep(1)
        mouseLeftClickOn("assign_to_cataog_button")
        sleep(1)
        pyautogui.hotkey('win','up')
        if appears("assign_to_cataog_window"):
            self.logger.info("assign_to_cataog_window appeared")
            if appears("test_cat"):
                mouseLeftClickOn("test_cat")
                pyautogui.press('left')
                pyautogui.press('space')
                sleep(.5)
                mouseLeftClickOn("assign_to_spec_window_ok")
            else:
                raise AssertionError("test_cat did not appear")
        else:
            raise AssertionError("assign_to_cataog_window did not appear")
        self.logger.info('End:\t Checking Module- assiging_stocks_to_catalog')

    @my_timer
    def filtering_by_any_column(self):
        self.logger.info('Start:\t Checking Module- filtering_by_any_column')
        if appears("stock_name_filter_region"):
            mouseLeftClickOn("stock_name_filter","stock_name_filter_region")
            pyautogui.typewrite('114',.1)
            pyautogui.press('enter')
            if appears("pen_114"):
                self.logger.info("Filtering was checked")
                sleep(1)
            else:
                raise AssertionError("filtering was not checked successfully")
        else:
            raise AssertionError("stock_name_filter_region did not appear")
        self.logger.info('End:\t Checking Module- filtering_by_any_column')

    # Creating Pipe Elements

    @my_timer
    def creating_new_pipe(self):
        self.logger.info('Start:\t Checking Module- creating_new_pipe')
        mouseDoubleClickOn("tab_resizer")
        mouseLeftClickOn("edit_new_stock_check")
        if appears("pipe_w_114_3x4"):
            mouseLeftClickOn("pipe_w_114_3x4")
            mouseLeftClickOn("create_pipe_new")
            sleep(3)
            pyautogui.hotkey('win','up')
            if appears("stock_editor_window"):
                self.logger.info("stock_editor_window appeared")
                mouseLeftClickOn("stock_name_18")
                pyautogui.press('right')
                pyautogui.typewrite('Test_Pipe_114.3x2.6',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("schedule_selector")
                sleep(.5)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('enter')
                sleep(.5)
                mouseLeftClickOn("weight_per_m")
                sleep(.5)
                pyautogui.press('right')
                sleep(.5)
                pyautogui.typewrite('7.12',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("minimum_length")
                sleep(.5)
                pyautogui.press('right')
                sleep(.5)
                pyautogui.typewrite('100',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("maximum_length")
                pyautogui.press('right')
                sleep(.5)
                pyautogui.typewrite('6000',.1)
                pyautogui.press('enter')
                mouseLeftClickOn("minimum_usable")
                sleep(.5)
                pyautogui.press('right')
                sleep(.5)
                pyautogui.typewrite('100',.1)
                pyautogui.press('enter')
                sleep(1)
                mouseLeftClickOn("end_1_treatment")
                pyautogui.press('right')
                mouseLeftClickOn("schedule_selector")
                sleep(.5)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('enter')
                mouseLeftClickOn("end_2_treatment")
                pyautogui.press('right')
                mouseLeftClickOn("schedule_selector")
                sleep(.5)
                pyautogui.press('down')
                pyautogui.press('enter')
                mouseLeftClickOn("stock_editor_ok")
                self.logger.info("Pipe was created")
            else:
                raise AssertionError("stock_editor_window did not appear")
        else:
            raise AssertionError("pipe_w_114_3x4 did not appear")
        self.logger.info('End:\t Checking Module- creating_new_pipe')

    # Creating a new Branch

    @my_timer
    def creating_a_new_branch(self):
        self.logger.info('Start:\t Checking Module- creating_a_new_branch')
        if appears("bramch_expand_region"):
            mouseLeftClickOn("branch_expand","branch_expand_region")
        mouseLeftClickOn("tee_branch")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info("stock_editor_window appeared")
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Tee_114.3x3.6', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=11, interval=.1)
            if appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("manufacturer_field")
            pyautogui.press('right')
            pyautogui.press('m', presses=2, interval=.1)
            pyautogui.press('enter')
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            pyautogui.press('enter')
            mouseLeftClickOn("weight_field")
            pyautogui.press('right')
            pyautogui.typewrite('3300', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("water_weight_field")
            pyautogui.press('right')
            pyautogui.typewrite('5500', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('105', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('105', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('105', .1)
            pyautogui.press('enter')
            if appears("end_1_treatment", 5):
                mouseLeftClickOn("end_1_treatment")
            else:
                mouseLeftClickOn("end_1_treatment_red_highlighted_field")
            pyautogui.press('right')
            mouseLeftClickOn("schedule_selector")
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_2615")
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_trimmable_field")
            pyautogui.press('right')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Branch was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_a_new_branch')

    @my_timer
    def creating_a_cap(self):
        self.logger.info('Start:\t Checking Module- creating_a_cap')
        mouseLeftClickOn("branch_collapse","branch_collapse_region")
        sleep(.5)
        mouseLeftClickOn("cap_expand_region")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info("stock_editor_window appeared")
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Cap_DN100', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=11, interval=.1)
            if appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            mouseLeftClickOn("length_field")
            pyautogui.press('right')
            pyautogui.typewrite('50', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("flat_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('0', .1)
            pyautogui.press('enter')
            if appears("end_treatment_field_red_high", 5):
                mouseLeftClickOn("end_treatment_field_red_high")
            else:
                mouseLeftClickOn("end_treatment_field")
            pyautogui.press('right')
            pyautogui.press('enter')
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            sleep(.5)
            mouseLeftClickOn("100_mm_DIN_EN_1123", "100_mm_DIN_EN_1123_region")
            pyautogui.press('enter')
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Cap was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_a_cap')

    @my_timer
    def creating_a_cross(self):
        self.logger.info('Start:\t Checking Module- creating_a_cross')
        mouseLeftClickOn("cross_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Cross_DN100', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=11, interval=.1)
            if appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            mouseLeftClickOn("end_1_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('110', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('110', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('110', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('110', .1)
            pyautogui.press('enter')
            if appears("end_1_treatment_red_high", 5):
                mouseLeftClickOn("end_1_treatment_red_high")
            else:
                mouseLeftClickOn("end_1_treatment")
            pyautogui.press('right')
            mouseLeftClickOn("schedule_selector")
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_EN_1123","100_mm_DIN_EN_1123_region2")
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_trimmable_field")
            pyautogui.press('right')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Cross was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_a_cross')

    @my_timer
    def creating_an_elbow(self):
        self.logger.info('Start:\t Checking Module- creating_an_elbow')
        mouseLeftClickOn("elbow_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Elbow_DN100', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=11, interval=.1)
            if appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            if appears("end_1_treatment_red_high", 5):
                mouseLeftClickOn("end_1_treatment_red_high")
            else:
                mouseLeftClickOn("end_1_treatment")
            pyautogui.press('right')
            mouseLeftClickOn("schedule_selector")
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_din_2605", "100_mm_din_2605_region")
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_trimmable_field")
            pyautogui.press('right')
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            mouseLeftClickOn("radius_field_yellow_shaded")
            pyautogui.press('right')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_straight_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('0',.1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('0',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Elbow was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_an_elbow')

    @my_timer
    def creating_a_reducer(self):
        self.logger.info('Start:\t Checking Module- creating_a_reducer')
        mouseLeftClickOn("branch_collapse","elbow_stock_expanded_region")
        mouseLeftClickOn("reducer_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Red_DN100-80', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=10, interval=.1)
            if appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            mouseLeftClickOn("end_1_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('24',.1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('24',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("transition_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('50',.1)
            pyautogui.press('enter')
            if appears("end_1_treatment_red_high", 5):
                mouseLeftClickOn("end_1_treatment_red_high")
            else:
                mouseLeftClickOn("end_1_treatment")
            pyautogui.press('right')
            mouseLeftClickOn("schedule_selector")
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_2616", "100_mm_DIN_2616_region")
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("end_2_treatment_scroll_down")
            pyautogui.click(clicks=2)
            mouseLeftClickOn("stock_expand", "80mm_expand_region")
            mouseLeftClickOn("stock_expand", "80mm_pl_expand")
            mouseLeftClickOn("80mm_DIN_2616_th2_3", "80mm_DIN_2616_th2_3_region")
            pyautogui.press('enter')
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Reducer was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_a_reducer')

    @my_timer
    def creating_an_adapter(self):
        self.logger.info('Start:\t Checking Module- creating_an_adapter')
        mouseLeftClickOn("branch_collapse", "reducer_stock_collapse_region")
        mouseLeftClickOn("adapter_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Adapter_DN100', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=11, interval=.1)
            if appears("100_mm_expand_region2", 5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region", 5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")

            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            mouseLeftClickOn("length_field")
            pyautogui.press('right')
            pyautogui.typewrite('100', .1)

            mouseLeftClickOn("end_1_treatment")
            pyautogui.press('right')
            pyautogui.press('enter')
            mouseLeftClickOn("treatment_scroll_up")
            pyautogui.click(clicks=5)
            # mouseLeftClickOn("end_2_treatment_scroll_down")
            # pyautogui.click(clicks=30,interval=.05)
            mouseLeftClickOn("stock_expand", "pl_expand_region")
            mouseLeftClickOn("end_2_treatment_scroll_down")
            pyautogui.click(clicks=7)
            if appears("100_mm_unhigh_expand_region2", 5):
                mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region2")
                mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
                mouseLeftClickOn("100_mm_DIN_EN_1123", "100_mm_DIN_EN_1123_region2")
                pyautogui.press('enter')
                pyautogui.press('down')
                pyautogui.press('enter')
                mouseLeftClickOn("treatment_scroll_up")
                # mouseLeftClickOn("end_2_treatment_scroll_down")
                pyautogui.click(clicks=15, interval=.05)
                mouseLeftClickOn("stock_expand", "fl_expand_region")
                # mouseLeftClickOn("end_2_treatment_scroll_down")
                # pyautogui.click(clicks=4)
                mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region2")
                mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
                mouseLeftClickOn("100_mm_DIN_EN_1123", "100_mm_DIN_EN_1123_region2")
                pyautogui.press('enter')
                mouseLeftClickOn("stock_editor_ok")
                self.logger.info("Adapter was created")
            else:
                raise AssertionError("100_mm_unhigh_expand_region2 did not appear")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_an_adapter')

    @my_timer
    def generic_ends(self):
        self.logger.info('Start:\t Checking Module- generic_ends')
        mouseLeftClickOn("delete_filter18")
        mouseLeftClickOn("stock_name_filter", "stock_name_filter_region")
        pyautogui.typewrite('Pipe-60', .1)
        pyautogui.press('enter')
        if appears("pipe_60_3x2_st37"):
            mouseLeftClickOn("pipe_60_3x2_st37")
        else:
            raise AssertionError("pipe_60_3x2_st37 did not appear")
        mouseLeftClickOn("end_types_button")
        if appears("allowable_end_treat_window",5):
            self.logger.info("allowable_end_treat_window appeared")
            mouseLeftClickOn("fthrd_select")
            pyautogui.press('left')
            pyautogui.press('space')
            pyautogui.press('up',presses=2,interval=.1)
            pyautogui.press('space')
            pyautogui.press('down',presses=4,interval=.1)
            pyautogui.press('space')
            pyautogui.press('down')
            pyautogui.press('space')
            mouseLeftClickOn("allowable_end_treat_window_ok")
            self.logger.info("Generic ends part was checked")
        else:
            raise AssertionError("allowable_end_treat_window did not appear")
        self.logger.info('End:\t Checking Module- generic_ends')

    @my_timer
    def creating_different_types_of_valves(self):
        self.logger.info('Start:\t Checking Module- creating_different_types_of_valves')
        if appears("ball_stock",2):
            mouseLeftClickOn("ball_stock")
        elif appears("valves_stock_expand_region",2):
            mouseLeftClickOn("stock_expand","valves_stock_expand_region")
            mouseLeftClickOn("ball_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('Test_Ball_Valve_DN100', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=9, interval=.1)
            if appears("100_mm_expand_region2",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("100_mm_expand_region",5):
                mouseLeftClickOn("100_mm_expand", "100_mm_expand_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("100_mm_expand_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('b', presses=4, interval=.1)
            mouseLeftClickOn("end_1_treatment")
            pyautogui.press('right')
            pyautogui.press('enter')
            mouseLeftClickOn("treatment_scroll_up")
            pyautogui.click(clicks=15, interval=.1)
            mouseLeftClickOn("stock_expand", "fl_expand_region")
            mouseLeftClickOn("end_2_treatment_scroll_down")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region2")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_3202", "100_mm_DIN_3202_region")
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("treatment_scroll_up")
            pyautogui.click(clicks=15, interval=.1)
            mouseLeftClickOn("stock_expand", "fl_expand_region")
            mouseLeftClickOn("end_2_treatment_scroll_down")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region2")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_3202", "100_mm_DIN_3202_region")
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("treatment_scroll_up")
            pyautogui.click(clicks=15, interval=.1)
            mouseLeftClickOn("stock_expand", "fl_expand_region")
            mouseLeftClickOn("end_2_treatment_scroll_down")
            mouseLeftClickOn("stock_expand", "100_mm_unhigh_expand_region2")
            mouseLeftClickOn("stock_expand", "100_mm_pl_expand_region")
            mouseLeftClickOn("100_mm_DIN_3202", "100_mm_DIN_3202_region")
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_2_length_field")
            pyautogui.press('right')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_3_length")
            pyautogui.press('right')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_3_position_field")
            pyautogui.press('right')
            pyautogui.typewrite('150',.1)
            pyautogui.press('enter')
            sleep(2)
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Valve was created")
        else:
            raise AssertionError("stock_editor_window did not appear")
        self.logger.info('End:\t Checking Module- creating_different_types_of_valves')

    @my_timer
    def assigning_icons_to_valve_types(self):
        self.logger.info('Start:\t Checking Module- assigning_icons_to_valve_types')
        if appears("valve_types_button"):
            mouseLeftClickOn("valve_types_button")
            if appears("valve_types_window"):
                self.logger.info("Valve types window appeared")
                mouseLeftClickOn("butterfly_type")
                pyautogui.press('right')
                pyautogui.press('up')
                pyautogui.press('up')
                pyautogui.press('s')
                sleep(1)
                mouseLeftClickOn("valve_types_window_ok")
                self.logger.info("Assigning icons to valve types part was checked")
            else:
                raise AssertionError("valve_types_window did not appear")
        else:
            raise AssertionError("valve_types_button did not appear")
        self.logger.info('End:\t Checking Module- assigning_icons_to_valve_types')

    @my_timer
    def creating_handles_and_2D_geometry_for_handles(self):
        self.logger.info('Start:\t Checking Module- creating_handles_and_2D_geometry_for_handles')
        if appears("valve_handles"):
            mouseLeftClickOn("valve_handles")
            sleep(1)
            pyautogui.hotkey('win','up')
            if appears("valve_handle_editor_window"):
                self.logger.info("valve_handle_editor_window appeared")
                mouseLeftClickOn("valve_handle_editor_window_scroll_down")
                pyautogui.click(clicks=4)
                if appears("lever_handler_expand_region",5):
                    mouseLeftClickOn("lever_handler_expand","lever_handler_expand_region")
                    mouseMoveTo("valve_handle_editor_window_scroll_down")
                    while exists("h245_l250") == False:
                        pyautogui.click(clicks=3)
                    if appears("h245_l250",1):
                        mouseLeftClickOn("h245_l250")
                        mouseLeftClickOn("valve_handle_editor_window_new")
                        sleep(.5)
                        mouseLeftClickOn("handle_type")
                        pyautogui.press('up')
                        pyautogui.press('right')
                        pyautogui.typewrite('Test_handle',.1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("2d_geometry_field")
                        pyautogui.press('right')
                        sleep(1)
                        pyautogui.press('enter')
                        if appears("select_handle_2d_drawing_file_window"):
                            mouseLeftClickOn("browser_adrs_dropdown")
                            pyautogui.typewrite("C:/NewDeploy_pipeCatalog/Training Files/Pipe Catalog and Modeling/",
                                                .1)
                            pyautogui.press('enter')
                            if appears("2d_handle_DWG",5):
                                mouseDoubleClickOn("2d_handle_DWG")
                            else:
                                pyautogui.press('esc')
                                self.logger.warn("2d_handle_DWG was not found. Check the path.")
                        else:
                            raise AssertionError("select_handle_2d_drawing_file_window did not appear")
                        mouseLeftClickOn("weight_field2")
                        pyautogui.press('right')
                        pyautogui.typewrite('.5',.1)
                        pyautogui.press('enter')
                        pyautogui.press('down')
                        pyautogui.typewrite('250',.1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("stem_length_field")
                        pyautogui.press('right')
                        pyautogui.typewrite('270',.1)
                        pyautogui.press('enter')
                        mouseLeftClickOn("valve_handle_editor_window_ok")
                        sleep(2)
                        mouseLeftClickOn("stock_edit_button")
                        if appears("stock_editor_window"):
                            self.logger.info('stock_editor_window appeared')
                            mouseLeftClickOn("handle_type_field")
                            pyautogui.press('right')
                            pyautogui.press('enter')
                            mouseLeftClickOn("lever_handler_expand", "lever_handle_expand_region")
                            mouseMoveTo("valve_handle_editor_window_scroll_down")
                            while exists("test_handle") == False:
                                pyautogui.click(clicks=2)
                            if appears("test_handle"):
                                self.logger.info("Test handle was created")
                                mouseDoubleClickOn("test_handle")
                                sleep(.5)
                                mouseLeftClickOn("handle_position_field")
                                pyautogui.press('right')
                                pyautogui.typewrite('150', .1)
                                pyautogui.press('enter')
                                mouseLeftClickOn("handle_rotation_field")
                                pyautogui.press('right')
                                pyautogui.typewrite('90', .1)
                                pyautogui.press('enter')
                                mouseLeftClickOn("stock_editor_ok")
                                self.logger.info("Creating handle was done")
                            else:
                                raise AssertionError("test_handle did not appear")
                        else:
                            raise AssertionError("stock_editor_window did not appear")
                    else:
                        raise AssertionError("h245_l250 type did not appear")
                else:
                    raise AssertionError("lever_handler_expand_region did not appear")
            else:
                raise AssertionError("valve_handle_editor_window did not appear")
        else:
            raise AssertionError("valve_handles did not appear")
        self.logger.info('End:\t Checking Module- creating_handles_and_2D_geometry_for_handles')


    @my_timer
    def exercise_pc_r004(self):
        self.logger.info('Start:\t Checking Module- exercise_pc_r004')
        mouseLeftClickOn("pipe_stock")
        mouseLeftClickOn("create_pipe_new")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        if appears("stock_editor_window"):
            self.logger.info('stock_editor_window appeared')
            mouseLeftClickOn("stock_name_18")
            pyautogui.press('right')
            pyautogui.typewrite('EX_Pipe', .1)
            pyautogui.press('enter')
            mouseLeftClickOn("schedule_selector")
            sleep(.5)
            pyautogui.press('down', presses=15, interval=.1)
            if appears("225mm_schedule_region2",10):
                mouseLeftClickOn("100_mm_expand", "225mm_schedule_region2")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            elif appears("225mm_schedule_region",10):
                mouseLeftClickOn("100_mm_expand", "225mm_schedule_region")
                pyautogui.press('down')
                sleep(.6)
                pyautogui.press('enter')
            else:
                raise AssertionError("225mm_schedule_region was not found")
            mouseLeftClickOn("materials_field")
            pyautogui.press('right')
            pyautogui.press('s', presses=6, interval=.1)
            mouseLeftClickOn("manufacturer_field")
            pyautogui.press('right')
            pyautogui.press('m',presses=2,interval=.1)
            pyautogui.press('enter')
            mouseLeftClickOn("minimum_length")
            pyautogui.press('right')
            pyautogui.typewrite('50',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("minimum_usable")
            pyautogui.press('right')
            pyautogui.typewrite('50',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("maximum_length")
            pyautogui.press('right')
            pyautogui.typewrite('6000',.1)
            pyautogui.press('enter')
            mouseLeftClickOn("end_1_treatment_red_high")
            pyautogui.press('right')
            pyautogui.press('enter')
            if appears("ex_pl_expand_region",10):
                mouseLeftClickOn("100_mm_expand", "ex_pl_expand_region")
                mouseLeftClickOn("100_mm_expand", "225_mm_expand_region")
                if appears("225_mm_ex_pl_expand_region",10):
                    mouseLeftClickOn("100_mm_expand", "225_mm_ex_pl_expand_region")
                    mouseDoubleClickOn("225_mm_ex_int_ex_click", "225_mm_ex_int_ex_region")
                else:
                    pyautogui.press('esc')
                    self.logger.error("225_mm_ex_pl_expand_region was not found")
            else:
                raise AssertionError("ex_pl_expand_region did not appear")
            mouseLeftClickOn("stock_editor_ok")
            self.logger.info("Exercise R004 was done")
        else:
            raise AssertionError("Stock editor window did not appear")
        self.logger.info('End:\t Checking Module- exercise_pc_r004')

    # Pipe Stock Catalog and Connections

    @my_timer
    def pipe_stock_catalog_and_connection(self):
        self.logger.info('Start:\t Checking Module- pipe_stock_catalog_and_connection')
        if appears("connections_tab_piping",10):
            mouseLeftClickOn("connections_tab_piping")
        else:
            raise AssertionError("connections_tab_piping did not appear")
        if appears("save_warning_18"):
            mouseLeftClickOn("save_warning_18_yes")
        sleep(5)
        if appears("pipe_pipe_coonnection_collapsed"):
            mouseLeftClickOn("pipe_pipe_coonnection_collapsed")
            pyautogui.press('right')
        if appears("new_type_button"):
            mouseLeftClickOn("new_type_button")
        else:
            raise AssertionError("new_type_button did not appear")
        sleep(2)
        pyautogui.typewrite('Test-FL_FL',.1)
        pyautogui.press('enter')
        self.logger.info("Test-FL_FL was created")
        if appears("test_fl_fl_expand_region2",5):
            mouseLeftClickOn("100_mm_expand", "test_fl_fl_expand_region2")
        else:
            mouseLeftClickOn("100_mm_expand", "test_fl_fl_expand_region")
        if appears("connection_tab_scroll_down",5):
            mouseMoveTo("connection_tab_scroll_down")
            pyautogui.click(clicks=3)
        if appears("new_connection_region4",5):
            mouseDoubleClickOn("new_connection_18", "new_connection_region4")
        else:
            mouseDoubleClickOn("new_connection_18", "new_connection_region")
        sleep(2)
        pyautogui.press('down', presses=17, interval=.1)
        if appears("175_mm_expand_region2",2):
            mouseLeftClickOn("100_mm_expand", "175_mm_expand_region2")
        elif appears("175_mm_expand_region",2):
            mouseLeftClickOn("100_mm_expand", "175_mm_expand_region")
        else:
            raise AssertionError("175_mm_expand_region was not found")
        pyautogui.press('down')
        sleep(.6)
        pyautogui.press('enter')
        pyautogui.press('right', presses=3, interval=.1)
        pyautogui.typewrite('2', .1)
        pyautogui.press('enter')
        pyautogui.press('right')
        pyautogui.typewrite('1', .1)
        pyautogui.press('enter')
        sleep(2)
        mouseLeftClickOn("sheet_stocks_done")
        if appears("save_warning_18",10):
            mouseLeftClickOn("save_warning_18_yes")
        self.logger.info("Pipe stock catalog and connection parts were checked")

        self.logger.info('End:\t Checking Module- pipe_stock_catalog_and_connection')

    @my_timer
    def creating_accessory_packages(self):
        self.logger.info('Start:\t Checking Module- creating_accessory_packages')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("packages_button")
        if appears("accessory_pkj_window",10):
            self.logger.info("accessory_pkj_window appeared")
        elif appears("accessory_pkj_window2",10):
            self.logger.info("accessory_pkj_window appeared")
        else:
            raise AssertionError("accessory package window did not appear")
        sleep(1)
        mouseLeftClickOn("accessory_pkj_newtype_button")
        pyautogui.typewrite('Test_Bolts', .1)
        pyautogui.press('enter')
        if appears("new_test_bolts_button", 5):
            mouseLeftClickOn("new_test_bolts_button")
            pyautogui.typewrite('Test_Bolt1', .1)
            pyautogui.press('enter')
        else:
            raise AssertionError("new_test_bolts_button did not appear")
        mouseLeftClickOn("accessory_pkj_newtype_button")
        pyautogui.typewrite('Test_Nuts', .1)
        pyautogui.press('enter')
        if appears("new_test_nuts_button", 1):
            mouseLeftClickOn("new_test_nuts_button")
            pyautogui.typewrite('Test_Nut1', .1)
            pyautogui.press('enter')
        else:
            raise AssertionError("new_test_nuts_button did not appear")
        mouseLeftClickOn("New_package_button")
        pyautogui.typewrite('Test_Package', .1)
        pyautogui.press('enter')
        self.logger.info("Test_Package was created")
        mouseLeftClickOn("test_bolts1")
        mouseLeftClickOn("add_button")
        mouseLeftClickOn("test_nuts1")
        mouseLeftClickOn("add_button")
        sleep(1)
        mouseLeftClickOn("qty_1")
        pyautogui.press('8')
        pyautogui.press('down')
        pyautogui.press('8')
        mouseLeftClickOn("assign_spec")
        if appears("assign_to_spec_window4"):
            mouseLeftClickOn("122_vent")
            pyautogui.press('space')
            mouseLeftClickOn("assign_ok")
        else:
            raise AssertionError("assign_to_spec_window did not appear")
        mouseLeftClickOn("accessory_pkj_ok")
        self.logger.info("Accessory packages were created")
        self.logger.info('End:\t Checking Module- creating_accessory_packages')

    # Assigning Accessory Packages to Connections

    @my_timer
    def assigning_accessory_packages_to_connections(self):
        self.logger.info('Start:\t Checking Module- assigning_accessory_packages_to_connections')
        if appears("save_warning_18",10):
            mouseLeftClickOn("save_warning_18")

            mouseLeftClickOn("save_warning_18_yes")
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("stock_catalog")
        sleep(2)
        if appears("delete_filter18"):
            mouseLeftClickOn("delete_filter18")
        sleep(1)
        mouseLeftClickOn("stock_name_filter")
        pyautogui.typewrite('Test-FL_FL',.1)
        pyautogui.press('enter')
        if appears("test_fl_fl_collapsed",5):
            mouseLeftClickOn("test_fl_fl_collapsed_expand_click","test_fl_fl_collapsed")
        if appears("test_fl_fl_connection",5):
            mouseLeftClickOn("test_fl_fl_connection")
            sleep(2)
        else:
            mouseMoveTo("connection_tab_scroll_down")
            while exists("test_fl_fl_connection") == False:
                pyautogui.click()
            if appears("test_fl_fl_connection"):
                mouseLeftClickOn("test_fl_fl_connection")
                sleep(2)
        mouseLeftClickOn("stock_add_button")
        sleep(1)
        pyautogui.hotkey('win', 'up')
        ####---------------Old Code--------------####
        # if appears("select_accessory_pkj_window"):
        #     mouseLeftClickOn("100_mm_expand")
        #     if appears("test_package"):
        #         mouseLeftClickOn("test_package")
        #         if appears("select_accessory_pkj_window_ok",5):
        #             mouseLeftClickOn("select_accessory_pkj_window_ok")
        #         else:
        #             raise AssertionError("select_accessory_pkj_window_ok did not appear")
        #         if appears("created_test_package",10):
        #             self.logger.info("Test Package was created")
        #         sleep(3)
        #         pyautogui.moveTo(500,500)
        #         sleep(2)
        #         mouseLeftClickOn("sheet_stocks_done")
        #         if appears("save_warning_18"):
        #             mouseLeftClickOn("save_warning_18_yes")
        #         self.logger.info("Assigning accessory packages to connections was done")
        #     else:
        #         raise AssertionError("test_package did not appear")
        # else:
        #     raise AssertionError("select_accessory_pkj_window did not appear")
        ####---------------New Code--------------####
        if appears("select_accessory_pkj_window2"):
            pyautogui.hotkey('win', 'up')
            if appears("test_package1"):
                mouseLeftClickOn("test_package1")
                mouseLeftClickOn("test_package_checkbox")
                if appears("select_accessory_pkj_window_ok3",5):
                    mouseLeftClickOn("select_accessory_pkj_window_ok3")
                else:
                    raise AssertionError("select_accessory_pkj_window_ok3 did not appear")
                if appears("created_test_package",10):
                    self.logger.info("Test Package was created")
                sleep(3)
                pyautogui.moveTo(500,500)
                sleep(2)
                mouseLeftClickOn("sheet_stocks_done")
                if appears("save_warning_18"):
                    mouseLeftClickOn("save_warning_18_yes")
                self.logger.info("Assigning accessory packages to connections was done")
            else:
                raise AssertionError("test_package did not appear")
        else:
            raise AssertionError("select_accessory_pkj_window did not appear")
        self.logger.info('End:\t Checking Module- assigning_accessory_packages_to_connections')

    # Assigning UDAs to Accessories
    @my_timer
    def assigning_uda_to_accessories(self):
        self.logger.info('Start:\t Checking Module- assigning_uda_to_accessories')
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("user_defined_attribute_option")
        if appears("subscription_advantage_pack"):
            pyautogui.press('enter')
        sleep(2)
        if appears("user_defined_attribute_window",10):
            self.logger.info("user_defined_attribute_window appeared")
            if appears("user_defined_attribute_window_maximize",5):
                mouseLeftClickOn("user_defined_attribute_window_maximize")
        elif appears("user_defined_attribute_window_maximized",5):
            self.logger.info("user_defined_attribute_window appeared")
        else:
            raise AssertionError("user_defined_attribute_window did not appear")
        mouseLeftClickOn("uda_article_number")
        mouseLeftClickOn("edit_assignment_2")
        sleep(3)
        pyautogui.hotkey('win', 'up')
        if appears("edit_uda_assignment_window"):
            self.logger.info("edit_uda_assignment_window appeared")
            if appears("pipe_connection_accessories2", 5):
                mouseLeftClickOn("pipe_connection_accessories2")
            else:
                raise AssertionError("pipe_connection_accessories2 did not appear")
            if appears("pipe_connection_accessories_checkbox1", 5):
                mouseLeftClickOn("pipe_connection_accessories_checkbox1")
            else:
                raise AssertionError("pipe_connection_accessories_checkbox1 did not appear")
            if appears("edit_uda_assignment_window_ok2", 5):
                mouseLeftClickOn("edit_uda_assignment_window_ok2")
            else:
                raise AssertionError("edit_uda_assignment_window_ok2 did not appear")
            pyautogui.moveTo(500, 500)
            sleep(1)
            pyautogui.moveTo(500, 500)
            if appears("user_defined_attribute_window_ok", 5):
                mouseLeftClickOn("user_defined_attribute_window_ok")
            else:
                raise AssertionError("user_defined_attribute_window_ok did not appear")
            self.logger.info("Assigning UDA to accessories was done")
        else:
            raise AssertionError("edit_uda_assignment_window did not appear")
        self.logger.info('End:\t Checking Module- assigning_uda_to_accessories')

    @my_timer
    def exercise_pc_r005(self):
        sleep(5)
        self.logger.info('Start:\t Checking Module- exercise_pc_r005')
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("stock_catalog")
        sleep(1)
        mouseLeftClickOn("new_type_button")
        sleep(2)
        pyautogui.typewrite('Ex_PL_PL', .1)
        pyautogui.press('enter')
        self.logger.info("Ex_PL_PL was created")
        if appears("ex_pl_pl_expand_region2",10):
            mouseLeftClickOn("100_mm_expand","ex_pl_pl_expand_region2")
        elif appears("ex_pl_pl_expand_region3",10):
            mouseLeftClickOn("100_mm_expand","ex_pl_pl_expand_region3")
        else:
            mouseLeftClickOn("100_mm_expand", "ex_pl_pl_expand_region")
        mouseMoveTo("connection_tab_scroll_down")
        pyautogui.click(clicks=3)
        if appears("new_connection_region5",10):
            mouseDoubleClickOn("new_connection_18", "new_connection_region5")
        elif appears("new_connection_region7",10):
            mouseDoubleClickOn("new_connection_18", "new_connection_region7")
        else:
            mouseDoubleClickOn("new_connection_18", "new_connection_region2")
        sleep(2)
        pyautogui.press('down', presses=19, interval=.1)
        if appears("225mm_schedule_region2",10):
            mouseLeftClickOn("100_mm_expand", "225mm_schedule_region2")
            pyautogui.press('down')
            pyautogui.press('down')
            sleep(.6)
            pyautogui.press('enter')
        elif appears("225mm_schedule_region",10):
            mouseLeftClickOn("100_mm_expand", "225mm_schedule_region")
            pyautogui.press('down')
            pyautogui.press('down')
            sleep(.6)
            pyautogui.press('enter')
        else:
            raise AssertionError("225mm_schedule_region was not found")

        mouseLeftClickOn("new_type_button")
        sleep(.5)
        pyautogui.typewrite('EX_FL_FL', .1)
        pyautogui.press('enter')
        self.logger.info("EX_FL_FL was created")
        if appears("ex_fl_fl_region2",10):
            mouseLeftClickOn("100_mm_expand", "ex_fl_fl_region2")
        elif appears("ex_fl_fl_region3",10):
            mouseLeftClickOn("100_mm_expand", "ex_fl_fl_region3")
        else:
            mouseLeftClickOn("100_mm_expand", "ex_fl_fl_region2")
        mouseMoveTo("connection_tab_scroll_down")
        pyautogui.click(clicks=3)
        if appears("new_connection_region6",10):
            mouseDoubleClickOn("new_connection_18","new_connection_region6")
        elif appears("new_connection_region7",10):
            mouseDoubleClickOn("new_connection_18", "new_connection_region7")
        else:
            mouseDoubleClickOn("new_connection_18", "new_connection_region3")
        sleep(.5)
        pyautogui.press('down', presses=19, interval=.1)
        if appears("225mm_schedule_region2",5):
            mouseLeftClickOn("100_mm_expand", "225mm_schedule_region2")
            pyautogui.press('down')
            sleep(.6)
            pyautogui.press('enter')
        elif appears("225mm_schedule_region",5):
            mouseLeftClickOn("100_mm_expand", "225mm_schedule_region")
            pyautogui.press('down')
            sleep(.6)
            pyautogui.press('enter')
        else:
            raise AssertionError("225mm_schedule_region was not found")
        mouseLeftClickOn("sheet_stocks_done")
        if appears("save_warning_18"):
            mouseLeftClickOn("save_warning_18_yes")
        self.logger.info("Exercise R005 was done")

        self.logger.info('End:\t Checking Module- exercise_pc_r005')

    @my_timer
    def parameters_of_bending_machines(self):
        self.logger.info('Start:\t Checking Module- parameters_of_bending_machines')
        mouseLeftClickOn("piping_button_manager")
        mouseLeftClickOn("pipe_benders_option")
        if appears("pipe_bending_window"):
            self.logger.info("pipe_bending_window appeared")
            mouseLeftClickOn("pipe_bending_new_button")
            if appears("edit_pipe_bending_window"):
                pyautogui.hotkey('ctrl','a')
                pyautogui.typewrite('Test_Bender',.1)
                self.logger.info("Test_Bender was created")
                mouseLeftClickOn("edit_pipe_bending_window_ok")
                sleep(1)
                mouseLeftClickOn("test_bender")
                mouseLeftClickOn("pipe_bending_add_button_region")
                if appears("edit_bending_window"):
                    self.logger.info("edit_bending_window appeared")
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('1.5', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab', presses=4, interval=.1)
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('250', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('135', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('400', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('250', .1)
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("edit_pipe_bending_window_ok")
                    self.logger.info("parameters of bending machines were checked")
                else:
                    raise AssertionError("edit_bending_window did not appear")
            else:
                raise AssertionError("edit_pipe_bending_window did not appear")
        else:
            raise AssertionError("pipe_bending_window did not appear")
        self.logger.info('End:\t Checking Module- parameters_of_bending_machines')

    @my_timer
    def exercise_pc_r006(self):
        self.logger.info('Start:\t Checking Module- exercise_pc_r006')
        if appears("pipe_bending_window"):
            self.logger.info("pipe_bending_window appeared")
            mouseLeftClickOn("pipe_bending_new_button")
            if appears("edit_pipe_bending_window"):
                pyautogui.hotkey('ctrl','a')
                pyautogui.typewrite('EX_Bender',.1)
                mouseLeftClickOn("can_cut_pipe")
                mouseLeftClickOn("edit_pipe_bending_window_ok")
                sleep(1)
                mouseLeftClickOn("ex_bender")
                mouseLeftClickOn("pipe_bending_add_button_region")
                if appears("edit_bending_window"):
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('1.5', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab', presses=4, interval=.1)
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('500', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('90', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('500', .1)
                    pyautogui.press('enter')
                    pyautogui.press('tab')
                    pyautogui.hotkey('ctrl', 'a')
                    pyautogui.typewrite('500', .1)
                    pyautogui.press('enter')
                    sleep(1)
                    mouseLeftClickOn("edit_pipe_bending_window_ok")
                    mouseLeftClickOn("pipe_bending_window_ok")
                    sleep(2)
                    self.logger.info("Exercise R006 was done")
                    sleep(2)
                    pyautogui.hotkey('alt','f4')
                else:
                    raise AssertionError("edit_bending_window did not appear")
            else:
                raise AssertionError("edit_pipe_bending_window did not appear")
        else:
            raise AssertionError("pipe_bending_window did not appear")
        self.logger.info('End:\t Checking Module- exercise_pc_r006')

class TCP5_Pipe_CatalogError(Exception):
    pass
