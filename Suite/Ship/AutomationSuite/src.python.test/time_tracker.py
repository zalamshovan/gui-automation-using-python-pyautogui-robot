# time_tracker.py
# Used for tracking the time duration of the functions to get completed.
# The log file is saved in the Log directory
# Useful to measure performance

from functools import wraps

from logger_extended import logger_extended

import time

logger = logger_extended().get_logger('Time_Tracker')


def my_timer(orig_func):

    @wraps(orig_func)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        result = orig_func(*args, **kwargs)
        t2 = time.time() - t1
        print('{} ran in: {} sec'.format(orig_func.__name__, t2))
        logger.info('{} \t ran in: {} sec'.format(orig_func.__name__, t2))
        return result

    return wrapper
