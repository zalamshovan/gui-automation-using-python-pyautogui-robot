"""
*** TCP6_Production_Documentation.py ***

Automation script for Product Documentation Essentials (Page 1 - 90)

Covers the following sections:
-  OUTPUT DRAWING CREATION
-  BOMS
-  LABELING
-  DRAWING DETAILS
-  UPDATING DRAWINGS
-  BOM REVISIONS

"""

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

from time import sleep

from logger_extended import logger_extended

from ProductionDocumentationUtil import ProductionDocumentationUtil as prodDoc

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseClickAppears, mouseDoubleClickOn, mouseMoveToCoordinates

from screenObjUtil import exists, appears, waitForVanish

import pyautogui, os

from SSIUtil import free, maximize, free_white, reset_viewport ,change_viewpoint_from_aft_stbd_up, wireframe2d_visual, hidden_visual


class TCP6_Production_Documentation(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP9_Product_Documentation')


    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC06_Production_Documentation")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_production")
        obj.launch_app()
        obj1.project_deployer(module="production")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()

    def register_project(self):
        maximize()
        obj= AppUtil()
        obj.ribbon_load("hull_and_structure_tab")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("deploy_production_list","NewDeploy_production")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_production")

    # Creation Wizard
    def creation_wizard(self):
        self.logger.info('Start:\t Checking Module- creation_wizard')
        mouseDoubleClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        if appears("output_node"):
            mouseLeftClickOn("output_node")
            if appears("slide_down",5):
                mouseMoveTo("slide_down")
            else:
                raise AssertionError("slide_down did not appear")
            while exists("marine_drafting_node") == False:
                pyautogui.click(clicks=2)
            if appears("marine_drafting_node"):
                mouseLeftClickOn("marine_drafting_node")
                mouseLeftClickOn("new_output_button")
                if appears("create_output_drawing_wizard_dialog"):
                    mouseLeftClickOn("name_click")
                    pyautogui.typewrite("Test-output",.2)
                    mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                    sleep(2)
                    pyautogui.moveTo(500,500)
                    if appears("marine_drafting_template",2):
                        mouseLeftClickOn("marine_drafting_template")
                    mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                    if appears("output_drawing_u02_node"):
                        mouseLeftClickOn("output_drawing_u02_node")
                        pyautogui.press("right")
                        pyautogui.press("down")
                        pyautogui.press("right")
                        pyautogui.press("down",presses=3)
                        pyautogui.press("right")
                        if appears("u02_tt_1400"):
                            mouseLeftClickOn("u02_tt_1400")
                            if appears("check_box_blue_region_darker",2):
                                check_box = "check_box_blue_region_darker"
                            elif appears("check_box_blue_region",2):
                                check_box = "check_box_blue_region"
                            mouseLeftClickOn(check_box)
                            pyautogui.press('down')
                            mouseLeftClickOn(check_box)
                            pyautogui.press('down')
                            mouseLeftClickOn(check_box)
                            mouseLeftClickOn("systems_tab")
                            if appears("122_pipe_project"):
                                mouseLeftClickOn("122_pipe_project")
                                mouseLeftClickOn(check_box)
                                sleep(.5)
                                mouseLeftClickOn("volumes_tab")
                                sleep(.5)
                                mouseLeftClickOn("create_output_drawing_wizard_dialog_finish")
                                waitForVanish("working_dialog")
                                sleep(10)
                                if appears("test_output_tab"):
                                    self.logger.info("Test-output was loaded successfully")
                                    pyautogui.hotkey('ctrl','s')
                                    sleep(5)
                                else:
                                    raise AssertionError("test_output_tab did not appear")
                            else:
                                raise AssertionError("122_pipe_project did not appear")
                        else:
                            raise AssertionError("u02_tt_1400 did not appear")
                    else:
                        raise AssertionError("output_drawing_u02_node did not appear")
                else:
                    raise AssertionError("create_output_drawing_wizard_dialog did not appear")
            else:
                raise AssertionError("marine_drafting_node did not appear")
        else:
            raise AssertionError("output_node did not appear")
        self.logger.info('End:\t Checking Module- creation_wizard')

    # Regular Exercise PDE-R001
    def exercise_pde_r001(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_r001')
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        if appears("shipcon_navi"):
            mouseLeftClickOn("marine_drafting_node")
            mouseLeftClickOn("new_output_button")
            if appears("create_output_drawing_wizard_dialog"):
                pyautogui.typewrite("Exercise1", .2)
                mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                sleep(2)
                pyautogui.moveTo(500,500)
                mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                if appears("output_drawing_u02_node"):
                    mouseLeftClickOn("output_drawing_u02_node")
                    pyautogui.press("right")
                    pyautogui.press("down")
                    pyautogui.press("right")
                    pyautogui.press("down", presses=3)
                    pyautogui.press("right")
                    if appears("u02_tt_1400"):
                        mouseLeftClickOn("u02_tt_1400")
                        pyautogui.press('down',presses=3)
                        if appears("check_box_blue_region_darker", 2):
                            check_box = "check_box_blue_region_darker"
                        elif appears("check_box_blue_region", 2):
                            check_box = "check_box_blue_region"

                        mouseLeftClickOn(check_box)
                        mouseLeftClickOn("assemblies_tab")
                        mouseLeftClickOn("assemblies_tab_filter")
                        pyautogui.typewrite('0207-ES05',.1)
                        pyautogui.press('enter')
                        if appears("0207-ES05"):
                            mouseLeftClickOn("0207-ES05")
                            sleep(1)
                            if appears("expand_blue_region_darker",2):
                                mouseLeftClickOn("expand_blue_region_darker")
                            else:
                                mouseLeftClickOn("expand_blue_region")
                            if appears("p02"):
                                mouseLeftClickOn("p02")
                                mouseLeftClickOn(check_box)
                            else:
                                raise AssertionError("p02 did not appear")
                        else:
                            raise AssertionError("0207-ES05 did not appear")
                        mouseLeftClickOn("systems_tab")
                        if appears("slide_down", 30):
                            mouseMoveTo("slide_down")
                        else:
                            raise AssertionError("slide_down did not appear")
                        while exists("641_system") == False:
                            pyautogui.click(clicks=2)
                        if appears("641_system"):
                            mouseLeftClickOn("641_system")
                            mouseLeftClickOn(check_box)
                            sleep(.5)
                            mouseLeftClickOn("volumes_tab")
                            sleep(.5)
                            mouseLeftClickOn("create_output_drawing_wizard_dialog_finish")
                            waitForVanish("working_dialog")
                            sleep(10)
                            if appears("Exercise1_tab"):
                                self.logger.info("Exercise1 output drawing was loaded successfully")
                                if appears("paperspace_click", 10):
                                    mouseDoubleClickOn("paperspace_click")
                                    pyautogui.press('esc')
                                    sleep(2)
                                    pyautogui.press('esc')
                                    sleep(2)
                                    pyautogui.press('esc')
                                    sleep(1)
                                    change_viewpoint_from_aft_stbd_up()
                                    sleep(1)
                                    free_white()
                                pyautogui.hotkey('ctrl', 's')
                                sleep(5)
                            else:
                                raise AssertionError("Exercise1_tab did not appear")
                        else:
                            raise AssertionError("122_pipe_project did not appear")
                    else:
                        raise AssertionError("u02_tt_1400 did not appear")
                else:
                    raise AssertionError("output_drawing_u02_node did not appear")
            else:
                raise AssertionError("create_output_drawing_wizard_dialog did not appear")
        self.logger.info('End:\t Checking Module- exercise_pde_r001')

    # Creating BOM Definitions
    def creating_bom_definition(self):
        self.logger.info('Start:\t Checking Module- creating_bom_definition')
        obj = prodDoc()
        obj.create_BOM_definition(definition_name="TestBOM", title="Test", table_style="BOM")
        sleep(1)
        if appears("test_BOM"):
            mouseLeftClickOn("test_BOM")
            mouseLeftClickOn("add_remove_button")
            sleep(1)
            pyautogui.hotkey('win', 'up')
            if appears("choose_bom_fields_window"):
                self.logger.info("choose_bom_fields_window appeared")
                sleep(1)
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("item#") == False:
                    pyautogui.click(clicks=8)
                mouseLeftClickOn("item#")
                pyautogui.press('left')
                pyautogui.press('space')
                self.logger.info("Item# was selected")
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("quantity") == False:
                    pyautogui.click(clicks=8)
                mouseLeftClickOn("quantity")
                pyautogui.press('left')
                pyautogui.press('space')
                self.logger.info("quantity was selected")
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("stock_name_bom") == False:
                    pyautogui.click(clicks=4)
                mouseLeftClickOn("stock_name_bom")
                pyautogui.press('left')
                pyautogui.press('space')
                self.logger.info("stock_name_bom was selected")
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("total_length") == False:
                    pyautogui.click(clicks=4)
                mouseLeftClickOn("total_length")
                pyautogui.press('left')
                pyautogui.press('space')
                self.logger.info("total_length was selected")
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("total_weight") == False:
                    pyautogui.click(clicks=4)
                mouseLeftClickOn("total_weight")
                pyautogui.press('left')
                pyautogui.press('space')
                self.logger.info("total_weight was selected")
                mouseMoveTo("choose_bom_fields_window")
                mouseLeftClickOn("choose_bom_fields_window_ok")
                sleep(1)
                mouseLeftClickOn("stock_name_selected")
                mouseLeftClickOn("bom_field_up_direction")
                mouseLeftClickOn("quantity_selected")
                pyautogui.press('right')
                pyautogui.typewrite('Qty', .1)
                pyautogui.press('enter')
                mouseLeftClickOn("stock_name_selected")
                pyautogui.press('right', presses=2)
                pyautogui.press('enter')
                if appears("sort"):
                    mouseLeftClickOn("sort")
                    mouseLeftClickOn("bom_field_sorting_ok")
                    sleep(5)
                    pyautogui.press('right')
                    pyautogui.press('y')
                    pyautogui.press('enter')
                    pyautogui.press('down')
                    pyautogui.press('right')
                    pyautogui.press('enter')
                    if appears("length_format"):
                        mouseLeftClickOn("length_format")
                        mouseLeftClickOn("length_format_drop_down")
                        sleep(2)
                        mouseLeftClickOn("units_mm")
                        pyautogui.press('m')
                        pyautogui.press('enter')
                        mouseLeftClickOn("round_up")
                        sleep(.5)
                        pyautogui.press('esc')
                        mouseMoveToCoordinates(400,400)
                        mouseLeftClickOn("mob_field_prop_window_ok")
                        self.logger.info("Length format was checked")
                    else:
                        raise AssertionError("BOM Field Properties window did not appear")
                else:
                    raise AssertionError("BOM Field Sorting dialog appeared")
            else:
                raise AssertionError("choose_bom_fields_window did not appear")
        else:
            raise AssertionError("test_BOM was not created")
        self.logger.info('End:\t Checking Module- creating_bom_definition')

    # Collector Options
    def collector_options(self):
        self.logger.info('Start:\t Checking Module- collector_options')
        if appears("collector_left_arrow", 30):
            mouseMoveTo("collector_left_arrow")
            pyautogui.click(clicks=30)
        if appears("collector_right_arrow",30):
            mouseMoveTo("collector_right_arrow")
        else:
            raise AssertionError("collector_right_arrow did not appear")
        pyautogui.click(clicks=4)
        mouseLeftClickOn("pipe_part")
        pyautogui.press('right')
        pyautogui.press('a', presses=2, interval=.2)
        mouseLeftClickOn("bom_definition_apply")
        mouseLeftClickOn("bom_definition_ok")
        sleep(1)
        pyautogui.hotkey('alt', 'f4')
        self.logger.info('End:\t Checking Module- collector_options')

    # Inserting a BOM Table
    def inserting_a_bom_table(self):
        self.logger.info('Start:\t Checking Module- inserting_a_bom_table')
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node_unhigh")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("pipe_node")
            mouseLeftClickOn("arrangement_node")
            mouseLeftClickOn("new_arrangement")
            if appears("create_arrangement_window"):
                self.logger.info("Create arrangement window appeared")
                pyautogui.typewrite("Test Arrangement",.1)
                pyautogui.press('enter',presses=2,interval=.2)
                if appears("arrangement_pipe_node"):
                    mouseLeftClickOn("arrangement_pipe_node")
                    pyautogui.press('right',presses=2,interval=.1)
                    mouseLeftClickOn("642_u01_fire_fighting")
                    pyautogui.press('space')
                    mouseLeftClickOn("create_arrangement_window_next")
                    pyautogui.click()
                    waitForVanish("working_dialog")
                    sleep(7)
                    if appears("test_arrangement_tab"):
                        mouseLeftClickOn("detail_tab")
                        sleep(2)
                        free_white()
                        obj = prodDoc()
                        obj.insert_BOM_table(definition_name="test_bom_on_definition_wizard", column_width="45",scr_cord_x="1052", scr_cord_y="244")
                        sleep(1)
                        obj.update_BOM_table()
                        sleep(3)
                        pyautogui.hotkey('ctrl', 's')
                        sleep(5)
                    else:
                        raise AssertionError("test_arrangement_tab did not appear")
                else:
                    raise AssertionError("arrangement_pipe_node did not appear")
            else:
                raise AssertionError("create_arrangement_window did not appear")
        else:
            raise AssertionError("shipcon_navi did not appear")
        self.logger.info('End:\t Checking Module- inserting_a_bom_table')


    def exercise_pde_r002(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_r002')
        obj = prodDoc()
        obj.create_BOM_definition(definition_name="Exercise2", title="Exercise2", table_style="BOM")
        sleep(1)
        if appears("exercise2_bom"):
            mouseLeftClickOn("exercise2_bom")
            mouseLeftClickOn("add_remove_button")
            sleep(1)
            pyautogui.hotkey('win', 'up')
            if appears("choose_bom_fields_window"):
                self.logger.info("choose_bom_fields_window appeared")
                sleep(1)
                mouseLeftClickOn("drawing_bom")
                pyautogui.press('left')
                pyautogui.press('space')
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("part_name_bom") == False:
                    pyautogui.click(clicks=8)
                mouseLeftClickOn("part_name_bom")
                pyautogui.press('left')
                pyautogui.press('space')
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("quantity") == False:
                    pyautogui.click(clicks=8)
                mouseLeftClickOn("quantity")
                pyautogui.press('left')
                pyautogui.press('space')
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("stock_name_bom") == False:
                    pyautogui.click(clicks=4)
                mouseLeftClickOn("stock_name_bom")
                pyautogui.press('left')
                pyautogui.press('space')
                mouseMoveTo("choose_bom_fields_window")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                while exists("weight_bom") == False:
                    pyautogui.click(clicks=4)
                mouseLeftClickOn("weight_bom")
                pyautogui.press('left')
                pyautogui.press('space')
                if appears("choose_bom_fields_window", 30):
                    mouseMoveTo("choose_bom_fields_window")
                else:
                    raise AssertionError("choose_bom_fields_window did not appear")
                mouseLeftClickOn("choose_bom_fields_window_ok")
                sleep(2)
                if appears("collector_left_arrow", 30):
                    mouseMoveTo("collector_left_arrow")
                    pyautogui.click(clicks=30)
                if appears("collector_right_arrow", 30):
                    mouseMoveTo("collector_right_arrow")
                else:
                    raise AssertionError("collector_right_arrow did not appear")
                pyautogui.click(clicks=5)
                mouseLeftClickOn("hvac_part")
                pyautogui.press("down",presses=8,interval=.2)
                if appears("pipe_connection_accessories_bom",10):
                    mouseLeftClickOn("pipe_connection_accessories_bom")
                else:
                    raise AssertionError("pipe_connection_accessories_bom did not appear")
                if appears("collector_right_arrow", 30):
                    mouseMoveTo("collector_right_arrow")
                else:
                    raise AssertionError("collector_right_arrow did not appear")
                pyautogui.click(clicks=3)
                mouseLeftClickOn("bom_definition_apply")
                mouseLeftClickOn("bom_definition_ok")
                sleep(1)
                pyautogui.hotkey('alt', 'f4')
                sleep(1)
                mouseLeftClickOn("main_panel_new")
                mouseLeftClickOn("navigator_button_new_18")
                sleep(2)
                if appears("output_node"):
                    mouseLeftClickOn("output_node")
                    if appears("slide_down", 30):
                        mouseMoveTo("slide_down")
                    else:
                        raise AssertionError("slide_down did not appear")
                    while exists("marine_drafting_node") == False:
                        pyautogui.click(clicks=2)
                    mouseLeftClickOn("marine_drafting_node")
                    mouseLeftClickOn("new_output_button")
                    if appears("create_output_drawing_wizard_dialog"):
                        pyautogui.typewrite("Exercise2", .2)
                        mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                        if appears("exercise_template"):
                            mouseLeftClickOn("exercise_template")
                        else:
                            raise AssertionError("exercise_template did not appear")
                        sleep(2)
                        pyautogui.moveTo(500,500)
                        mouseLeftClickOn("create_output_drawing_wizard_dialog_next")
                        mouseLeftClickOn("assemblies_tab_filter")
                        pyautogui.typewrite('U02_FR47', .1)
                        pyautogui.press('enter')
                        if appears("u02_fr47_bom"):
                            mouseLeftClickOn("u02_fr47_bom")
                            if appears("check_box_blue_region_darker", 5):
                                check_box = "check_box_blue_region_darker"
                            elif appears("check_box_blue_region", 5):
                                check_box = "check_box_blue_region"
                            mouseLeftClickOn(check_box)
                            sleep(2)
                            pyautogui.moveTo(500,500)
                            mouseLeftClickOn("create_output_drawing_wizard_dialog_finish")
                            waitForVanish("working_dialog")
                            sleep(10)
                            if appears("exercise2_tab"):
                                free_white()
                                pyautogui.click(clicks=2)
                                pyautogui.press('esc')
                                sleep(1)
                                pyautogui.press('esc')
                                change_viewpoint_from_aft_stbd_up()
                                free_white()
                                sleep(1)
                                obj = prodDoc()
                                obj.insert_BOM_table(definition_name="exercise2_bom_definition", max_row="60",
                                                     column_width="45", cord_x="400.1496", cord_y="282.8982")
                                sleep(1)
                                obj.update_BOM_table()
                                sleep(2)
                                pyautogui.hotkey('ctrl', 's')
                                sleep(5)
                            else:
                                raise AssertionError("exercise2_tab did not appear")
                        else:
                            raise AssertionError("u02_fr47 did not appear")
                    else:
                        raise AssertionError("create_output_drawing_wizard_dialog did not appear")
                else:
                    raise AssertionError("Output node did not appear")
            else:
                raise AssertionError("choose_bom_fields_window did not appear")
        else:
            raise AssertionError("Exercise2 was not done")
        self.logger.info('End:\t Checking Module- exercise_pde_r002')

    # Labeling

    def labeling(self):
        self.logger.info('Start:\t Checking Module- labeling')
        obj = prodDoc()
        obj.open_label_style_manager()
        sleep(1)
        pyautogui.hotkey('alt','f4')
        sleep(1)
        self.logger.info('End:\t Checking Module- labeling')

    # Automatic and Manual Labeling

    def automatic_and_manual_labeling(self):
        self.logger.info('Start:\t Checking Module- automatic_and_manual_labeling')
        free_white()
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        if appears("shipcon_navi"):
            mouseLeftClickOn("ssi_node_unhigh")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("assembly_node")
            if appears("slide_down", 30):
                mouseMoveTo("slide_down")
            else:
                raise AssertionError("slide_down did not appear")
            while exists("0202_p09") == False:
                pyautogui.click(clicks=10)
            mouseDoubleClickOn("0202_p09")
            sleep(2)
            if appears("save_caution",5):
                pyautogui.press('enter')
            if appears("0202_p09_tab"):
                self.logger.info("0202_p09_tab appeared")
                free_white()
                obj = prodDoc()
                obj.delete_all_labels()
            else:
                raise AssertionError("0202_p09_tab did not appear")
        else:
            raise AssertionError("ShipCon Navigator did not appear")
        self.logger.info('End:\t Checking Module- automatic_and_manual_labeling')


    # Option 1: Auto Label All

    def opt1_auto_label_all(self):
        self.logger.info('Start:\t Checking Module- opt1_auto_label_all')
        obj = prodDoc()
        obj.insert_BOM_table(definition_name="weld_identification", column_width="30", max_row="25", cord_x="284.8014",cord_y="252.6773")
        obj.update_BOM_table()
        sleep(3)
        free_white()
        free_white()
        pyautogui.click(clicks=2)
        pyautogui.press('esc')
        wireframe2d_visual()
        sleep(1)
        reset_viewport()
        sleep(1)
        obj.auto_labeling(option_to_be_selected="auto_label_all_selected")
        sleep(1)
        if appears("p09_007_label",10):
            self.logger.info("Auto label added")
        obj.delete_all_labels()
        self.logger.info('End:\t Checking Module- opt1_auto_label_all')

    # Option 2: Label Viewports

    def opt2_label_viewports(self):
        self.logger.info('Start:\t Checking Module- opt2_label_viewports')
        obj = prodDoc()
        obj.auto_labeling(option_to_be_selected="label_viewports_on_list", previously_selected_option="auto_label_all_selected")
        sleep(1)
        if appears("p09_007_label",5):
            self.logger.info("Label viewports added")
        obj.delete_all_labels()
        self.logger.info('End:\t Checking Module- opt2_label_viewports')

    # Option 3: Viewport Options

    def opt3_viewport_options(self):
        self.logger.info('Start:\t Checking Module- opt3_viewport_options')
        obj = prodDoc()
        obj.viewport_options(option_to_be_selected="viewport_options_on_list",previously_selected_option="viewport_display_option_selected2", cord_x = '14.3674', cord_y = '281.7798'  )
        sleep(1)
        if appears("viewport_options_window"):
            self.logger.info("viewport_options_window appeared")
            mouseLeftClickOn("label_items_from_all_boms")
            mouseClickAppears("check_box_checked")
            mouseLeftClickOn("viewport_options_window_ok")
            sleep(1)
            obj.auto_labeling(option_to_be_selected="label_viewport_from_bom_on_list",
                              previously_selected_option="label_viewports_selected2")
            if appears("select_bom_table_option"):
                pyautogui.typewrite('333.1862,274.4991', .1)
                pyautogui.press('enter', presses=2)
            if appears("select_viewports_option", 6):
                free_white()
                pyautogui.typewrite('14.3674,281.7798',.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
        else:
            raise AssertionError("viewport_options_window did not appear")
        sleep(1)
        obj.delete_all_labels()
        self.logger.info('End:\t Checking Module- opt3_viewport_options')

    # Option 4: Auto Label Parts

    def opt4_auto_label_parts(self):
        self.logger.info('Start:\t Checking Module- opt4_auto_label_parts')
        free_white()
        sleep(1)
        pyautogui.click(clicks=2)
        pyautogui.press('esc')
        sleep(2)
        hidden_visual()
        obj = prodDoc()
        obj.vp_freeze_layer(layer_name="weld_layer")
        sleep(1)
        obj.auto_labeling(option_to_be_selected="auto_label_parts_on_list",
                          previously_selected_option="label_viewport_from_bom_selected")
        if appears("select_parts_option"):
            pyautogui.typewrite('34381.5863,9654.5129',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('34167.6559,9099.6625',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
        else:
            raise AssertionError("select_parts_option did not appear")
        sleep(1)
        obj.delete_all_labels()
        self.logger.info('End:\t Checking Module- opt4_auto_label_parts')

    # Option 5: Manual Label

    def opt5_manual_label(self):
        self.logger.info('Start:\t Checking Module- opt5_manual_label')
        obj = prodDoc()
        free_white()
        sleep(1)
        pyautogui.click(clicks=2)
        pyautogui.press('esc')
        sleep(1)
        wireframe2d_visual()
        reset_viewport()
        obj.manual_labeling()
        if appears("select_bom_table",10):
            pyautogui.typewrite('315.2372,273.6279',.1)
            pyautogui.press('enter')
        if appears("select_part_to_label",10):
            pyautogui.moveTo(372, 311)
            sleep(1)
            pyautogui.click()
            sleep(1)
            pyautogui.moveTo(441,207)
            pyautogui.click()
            free_white()
            pyautogui.press('enter')
        free_white()
        pyautogui.moveTo(1214, 429)
        pyautogui.click(clicks=2)
        pyautogui.press('esc')
        obj.copy_label_from_BOM()
        free_white()
        pyautogui.moveTo(441,207)
        sleep(1)
        pyautogui.click()
        free_white()
        if appears("select_a_part"):
            pyautogui.moveTo(320, 281)
            sleep(2)
            pyautogui.click()
        free_white()
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')
        self.logger.info('End:\t Checking Module- opt5_manual_label')

    # Creating Leader Distribution Lines

    def leader_distribution_line(self):
        self.logger.info('Start:\t Checking Module- leader_distribution_line')
        sleep(3)
        obj = prodDoc()
        obj.leader_distribution_line(cord_x1='168.6759', cord_y1='271.6541', cord_x2='214.5010', cord_y2='242.4422', vp_cord_x = '14.3674', vp_cord_y = '281.7798')
        # obj.transfer_leaders_to_other_line(cord_x1='239', cord_y1='273', cord_x2='171', cord_y2='250',leader_x1='189.2098', leader_y1='258.6320')
        obj.transfer_leaders_to_other_line(cord_x1='196.1472', cord_y1='248.966', cord_x2='147.089', cord_y2='250.9917',leader_x1='189.2098', leader_y1='258.6320')
        self.logger.info('End:\t Checking Module- leader_distribution_line')

    # Redistributing Leaders on Distribution Lines

    def redistributing_leaders(self):
        self.logger.info('Start:\t Checking Module- redistributing_leaders')
        obj = prodDoc()
        obj.redistributing_leaders_on_distribution_lines(leader_x1='189.2098', leader_y1='258.6320', mode="center")
        obj.redistributing_leaders_on_distribution_lines(leader_x1='189.2098', leader_y1='258.6320', mode="equidistant")
        obj.redistributing_leaders_on_distribution_lines(leader_x1='189.2098', leader_y1='258.6320', mode="nearest")
        self.logger.info('End:\t Checking Module- redistributing_leaders')

    # Inserting New Leaders

    def inseting_new_leaders(self):
        self.logger.info('Start:\t Checking Module- inseting_new_leaders')
        obj = prodDoc()
        obj.set_leader_insertion_mode(leader_x1='189.2098', leader_y1='258.6320', mode="left",
                                      previously_selected_option="redistribute_leaders")
        obj.set_leader_insertion_mode(leader_x1='189.2098', leader_y1='258.6320', mode="right")
        obj.set_leader_insertion_mode(leader_x1='189.2098', leader_y1='258.6320', mode="center")
        obj.set_leader_insertion_mode(leader_x1='189.2098', leader_y1='258.6320', mode="horizontal")
        self.logger.info('End:\t Checking Module- inseting_new_leaders')

    # Property Labels

    def property_labels_new(self):
        self.logger.info('Start:\t Checking Module- property_labels')
        if appears("0202_p09_tab"):
            sleep(5)
            sleep(5)
            sleep(5)
            sleep(5)
            free_white()
            pyautogui.click(clicks=2)
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.typewrite('VSCURRENT', .2)
            sleep(1)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.press('2')
            sleep(1)
            pyautogui.press('enter')
        else:
            raise AssertionError("0202_p09_tab did not appear")
        mouseLeftClickOn("production_ribbon")
        mouseLeftClickOn("property_label_pane")
        if appears("style_manager",10):
            mouseLeftClickOn("style_manager")
        else:
            raise AssertionError("style_manager button did not appear")
        if appears("property_label_style_manager_window"):
            # SSI – Name and Stock
            if appears("ssi_part_stock",5):
                mouseLeftClickOn("ssi_part_stock")
            elif appears("ssi_part_stock_high",5):
                mouseLeftClickOn("ssi_part_stock_high")
            elif appears("ssi_part_stock_unhigh",5):
                mouseLeftClickOn("ssi_part_stock_unhigh")
            else:
                raise AssertionError("ssi_part_stock did not appear")
            mouseLeftClickOn("set_current")
            mouseLeftClickOn("property_label_style_manager_window_close")
            sleep(1)
            free_white()
        else:
            raise AssertionError("property_label_style_manager_window did not appear")
        free_white()
        sleep(5)
        mouseLeftClickOn("property_label_pane")
        if appears("styled_based_label",5):
            mouseLeftClickOn("styled_based_label")
            free_white()
            # pyautogui.click(button='left')
            # mouseLeftClickOn("snap_override")
            # mouseLeftClickOn("onap_nearest")
            pyautogui.typewrite('nearest',.2)
            pyautogui.press('enter')
            free_white()
            pyautogui.moveTo(375,342)
            sleep(1)
            pyautogui.click()
            sleep(2)
            pyautogui.moveTo(488,411)
            sleep(1)
            pyautogui.click()
            pyautogui.press('esc')
            pyautogui.press('esc')
            pyautogui.hotkey('ctrl', 's')
            sleep(5)
            sleep(5)
            sleep(5)
        else:
            raise AssertionError("styled_based_label did not appear")

    def property_label(self):
        self.logger.info('Start:\t Checking Module- property_label')
        free_white()
        pyautogui.click(clicks=2)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.typewrite('VSCURRENT', .2)
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('c')
        sleep(1)
        pyautogui.press('enter')

        mouseLeftClickOn("production_ribbon")
        mouseLeftClickOn("property_label_pane")
        if appears("property_label_option"):
            mouseLeftClickOn("property_label_option")
            free_white()
            if appears("plate_conceptual", 10):
                mouseLeftClickOn("plate_conceptual")
            else:
                raise AssertionError("plate_conceptual region did not appear")
            if appears("field_text_window"):
                self.logger.info("Field text window appeared")
                mouseLeftClickOn("part_name_field")
                mouseLeftClickOn("field_text_window_right")
                if appears("end_of_2nd_line",20):
                    mouseMoveTo("end_of_2nd_line")

                sleep(2)
                pyautogui.press('enter')
                if appears("prod_hier_scroll_down",20):
                    mouseMoveTo("prod_hier_scroll_down")
                else:
                    raise AssertionError("prod_hier_scroll_down did not appear")
                pyautogui.click(clicks=30)
                mouseLeftClickOn("stock_name_field_text")
                mouseLeftClickOn("field_text_window_right")
                if appears("end_of_2nd_line",20):
                    mouseMoveTo("end_of_2nd_line")

                sleep(2)
                pyautogui.press('enter')
                # mouseLeftClickOn("dimension_style_drop_down")
                # pyautogui.press('p')
                # pyautogui.press('enter')
                mouseLeftClickOn("field_text_window_ok")
                sleep(2)
                pyautogui.moveTo(474, 379)
                sleep(2)
                pyautogui.click()
                # pyautogui.typewrite('213.1553,213.325', .1)
                # pyautogui.press('enter')
                sleep(1)
                pyautogui.press('esc')
                pyautogui.press('esc')
                free_white()
                pyautogui.typewrite('VSCURRENT', .2)
                sleep(1)
                pyautogui.press('enter')
                sleep(1)
                pyautogui.press('2')
                sleep(1)
                pyautogui.press('enter')
                if appears("label_check", 5):
                    self.logger.info("Property Label was set correctly")
                sleep(1)
                pyautogui.hotkey('ctrl', 's')
                sleep(5)
            else:
                raise AssertionError("field_text_window did not appear")
        else:
            raise AssertionError("property_label_option did not appear")

        self.logger.info('End:\t Checking Module- property_label')

    def exercise_pde_r003(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_r003')
        obj = prodDoc()
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        sleep(2)
        mouseLeftClickOn("output_node")
        if appears("slide_down",30):
            mouseMoveTo("slide_down")
        else:
            raise AssertionError("slide_down did not appear")
        while exists("exercise2_high") == False:
            pyautogui.click(clicks=2)
        mouseDoubleClickOn("exercise2_high")
        sleep(5)
        if appears("exercise2_tab"):
            self.logger.info("Exercise2 tab opened")
            obj.update_BOM_table()
            sleep(7)
            free_white()
            obj.auto_labeling(option_to_be_selected="auto_label_all_on_list",
                              previously_selected_option="auto_label_parts_selected")
            sleep(2)
            obj.delete_all_labels()
            obj.auto_labeling(option_to_be_selected="label_viewports_on_list",
                              previously_selected_option="auto_label_all_selected")
            if appears("select_viewports_option",2):
                pyautogui.typewrite('28.1405,275.4544',.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
            sleep(2)
            obj.delete_all_labels()
            mouseDoubleClickOn("out_of_paperspace")
            pyautogui.press('esc')
            obj.auto_labeling(option_to_be_selected="label_viewport_from_bom_on_list",
                              previously_selected_option="label_viewports_selected")
            if appears("select_bom_table_option"):
                pyautogui.typewrite('406.3415,267.7797', .1)
                pyautogui.press('enter', presses=2)

            if appears("select_viewports_option", 2):
                free_white()
                pyautogui.typewrite('28.1405,275.4544', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
            sleep(2)
            obj.delete_all_labels()
            sleep(1)
            pyautogui.hotkey('ctrl', 's')
            sleep(5)
        else:
            raise AssertionError("exercise2_tab did not appear")
        self.logger.info('End:\t Checking Module- exercise_pde_r003')

    def exercise_pde_a001(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_a001')
        obj = prodDoc()
        obj.property_label()
        if appears("select_object2", 5):
            print("select_object appeared")
        elif appears("select_shipcon_obj",5):
            print("select_object appeared")
        else:
            raise AssertionError("select_shipcon_obj option did not appear")
        pyautogui.typewrite('28613.4344,3686.0317', .1)
        pyautogui.press('enter')
        if appears("field_text_window"):
            mouseLeftClickOn("part_name_field_text")
            mouseLeftClickOn("field_text_window_right")
            if appears("slide_down", 30):
                mouseMoveTo("slide_down")
            else:
                raise AssertionError("slide_down did not appear")
            pyautogui.click(clicks=30)
            mouseLeftClickOn("stock_name_field_text")
            mouseLeftClickOn("field_text_window_ok")
            if appears("specify_labeL_position"):
                pyautogui.typewrite('69.2111,101.2285', .1)
                pyautogui.press('enter')
                free_white()
            sleep(1)
            pyautogui.press('up')
            pyautogui.press('enter')
            if appears("select_object2", 5):
                print("select_object appeared")
            elif appears("select_shipcon_obj", 5):
                print("select_object appeared")
            else:
                raise AssertionError("select_shipcon_obj option did not appear")
            pyautogui.typewrite('28911.1980,2601.3463', .1)
            pyautogui.press('enter')
            if appears("field_text_window"):
                mouseLeftClickOn("part_name_field_text")
                mouseLeftClickOn("field_text_window_right")
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                pyautogui.click(clicks=30)
                mouseLeftClickOn("stock_name_field_text")
                mouseLeftClickOn("field_text_window_ok")
                if appears("specify_labeL_position"):
                    pyautogui.typewrite('139.7999,71.9031', .1)
                    pyautogui.press('enter')
                    free_white()
                sleep(1)
                pyautogui.hotkey('ctrl','s')
                sleep(5)
            else:
                raise AssertionError("field_text_window did no tappear [2nd]")
        else:
            raise AssertionError("field_text_window did no tappear")
        self.logger.info('End:\t Checking Module- exercise_pde_a001')

    # Drawing Details

    def keywords(self):
        self.logger.info('Start:\t Checking Module- keywords')
        # mouseLeftClickOn("main_panel_new")
        # mouseLeftClickOn("navigator_button_new_18")
        # if appears("shipcon_navi"):
        #     mouseLeftClickOn("assembly_node")
        #     if appears("slide_down", 30):
        #         mouseMoveTo("slide_down")
        #     else:
        #         raise AssertionError("slide_down did not appear")
        #     while exists("0202_p09_high") == False:
        #         pyautogui.click(clicks=10)
        #     mouseDoubleClickOn("0202_p09_high")
        #     sleep(2)
        #     if appears("save_caution",2):
        #         pyautogui.press('enter')
        # else:
        #     raise AssertionError("ShipCon Navigator did not appear")
        if appears("0202_p09_tab"):
            free_white()
            obj = prodDoc()
            obj.delete_all_labels()
            sleep(1)
            obj.insert_keyword()
            if appears("insert_keyword_window"):
                self.logger.info("insert_keyword_window appeared")
                mouseLeftClickOn("assembly_name")
                pyautogui.keyDown('ctrl')
                mouseLeftClickOn("assembly_properties")
                mouseLeftClickOn("file_name")
                pyautogui.keyUp('ctrl')
                pyautogui.press('tab',presses=5)
                pyautogui.press('3')
                mouseLeftClickOn("insert_keyword_window_ok")
                if appears("please_select_a_point"):
                    pyautogui.typewrite('290.3385,148.4948',.1)
                    pyautogui.press('enter')
                if appears("please_select_a_point"):
                    pyautogui.typewrite('289.5484,96.3814',.1)
                    pyautogui.press('enter')
                if appears("please_select_a_point"):
                    pyautogui.typewrite('289.5484,57.6911',.1)
                    pyautogui.press('enter')
                obj.update_keyword()
                if appears("file_name_updated_keyword",5):
                    self.logger.info("Keywords were updated")
            else:
                raise AssertionError("insert_keyword_window did not appear")
        else:
            raise AssertionError("0202_p09_tab did not appear")

        self.logger.info('End:\t Checking Module- keywords')

    def check_viewport_options(self):
        self.logger.info('Start:\t Checking Module- viewport_options')
        obj = prodDoc()
        obj.viewport_options(option_to_be_selected="viewport_option_selected", cord_x='14.9633', cord_y='281.5418')
        if appears("viewport_options_window"):
            self.logger.info("viewport_options_window appeared")
            mouseLeftClickOn("viewport_options_window_ok")
        else:
            raise AssertionError("viewport_options_window did not appear")
        self.logger.info('End:\t Checking Module- insert_BOM_table')

    def set_global_dimension_to_point(self):
        self.logger.info('Start:\t Checking Module- insert_BOM_table')
        sleep(4)
        obj = prodDoc()
        obj.global_dimension_to_point(viewport_x='14.9633',viewport_y='281.5418')
        if appears("select_point_to_dimension"):
            pyautogui.typewrite('31200,5640,3743.0726',.1)
            pyautogui.press('enter')
            sleep(3)
            pyautogui.hotkey('win','up')
        if appears("global_dimension_to_point_window"):
            self.logger.info("global_dimension_to_point_window appeared")
            mouseLeftClickOn("select_closest_plane")
            mouseLeftClickOn("global_dimension_to_point_window_ok")
            sleep(.5)
            free_white()
            if appears("select_position",5):
                pyautogui.typewrite('27,-43',.1)
                pyautogui.press('enter')
                pyautogui.press('enter')
        else:
            raise AssertionError("global_dimension_to_point_window did not appear")
        self.logger.info('End:\t Checking Module- viewport_options')

    def set_quality_matrix(self):
        self.logger.info('Start:\t Checking Module- set_quality_matrix')
        obj = prodDoc()
        obj.quality_matrix()
        if appears("specify_a_point"):
            pyautogui.typewrite('31188,5604,3743.0726',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('31188,6608.8245,3814.4541',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('31188,6023.4596,1444.6752',.1)
            pyautogui.press('enter')
            pyautogui.typewrite('31200,5604,1796.6436',.1)
            pyautogui.press('enter')
            pyautogui.press('enter')
            if appears("specify_top_right_corner"):
                pyautogui.typewrite('129,102',.1)
                pyautogui.press('enter')
                if appears("quality_matrix_table2",5):
                    self.logger.info("Quality matrix was set up")
            else:
                raise AssertionError("specify_top_right_corner did not appear")
        else:
            raise AssertionError("specify_a_point did not appear")
        self.logger.info('End:\t Checking Module- set_quality_matrix')

    def exercise_pde_r004(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_r004')
        obj = prodDoc()
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        sleep(2)
        mouseLeftClickOn("output_node")
        if appears("slide_down",30):
            mouseMoveTo("slide_down")
        else:
            raise AssertionError("slide_down did not appear")
        while exists("exercise2_high") == False:
            pyautogui.click(clicks=2)
        mouseDoubleClickOn("exercise2_high")
        if appears("save_caution"):
            pyautogui.press('enter')
        sleep(5)
        if appears("exercise2_tab"):
            self.logger.info("Exercise2 tab opened")
            obj.delete_all_labels()
            sleep(1)
            obj.insert_keyword()
            if appears("insert_keyword_window"):
                self.logger.info("insert_keyword_window appeared")
                mouseLeftClickOn("last_update_date_and_time")
                mouseLeftClickOn("insert_keyword_window_ok")
                free_white()
                if appears("please_select_a_point"):
                    pyautogui.typewrite('285,85', .1)
                    pyautogui.press('enter')
            else:
                raise AssertionError("insert_keyword_window did not appear")
            obj.quality_matrix()
            if appears("specify_a_point"):
                pyautogui.typewrite('28208,6643.2077,6580', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('28200,750,1800', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('28211,-2471,725', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('28208,-6643.2077,6700', .1)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("specify_top_right_corner"):
                    pyautogui.typewrite('249,255', .1)
                    pyautogui.press('enter')
                    if appears("quality_matrix_table2",5):
                        self.logger.info("Quality matrix was set up")
                else:
                    raise AssertionError("specify_top_right_corner did not appear")
            else:
                raise AssertionError("specify_a_point did not appear")
        else:
            raise AssertionError("exercise2_tab did not appear")
        self.logger.info('End:\t Checking Module- exercise_pde_r004')

    # BOM Revisions

    def check_BOM_revisions(self):
        self.logger.info('Start:\t Checking Module- check_BOM_revisions')
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        sleep(2)
        mouseLeftClickOn("assembly_node")
        if appears("slide_down",30):
            mouseMoveTo("slide_down")
        else:
            raise AssertionError("slide_down did not appear")
        while exists("0202_p09_high") == False:
            pyautogui.click(clicks=10)
        mouseDoubleClickOn("0202_p09_high")
        sleep(2)
        if appears("save_caution",2):
            pyautogui.press('enter')
        if appears("0202_p09_tab"):
            free_white()
            obj = prodDoc()
            obj.bom_revision(option_to_be_selected="revision_layout_selected")
            sleep(15)
            if appears("up_to_date_warning",10):
                pyautogui.press('enter')
            if appears("enter_name_for_revision"):
                pyautogui.typewrite('Rev.0', .1)
                pyautogui.press('enter')
                sleep(.5)
                pyautogui.press('enter')
                free_white()
                pyautogui.click(clicks=2)
                pyautogui.press('esc')
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                # pyautogui.typewrite('34440.1094,9629.4139', .1)
                pyautogui.typewrite('34479.2084,9760.6996', .1)
                pyautogui.press('enter', presses=2)
                pyautogui.click(button='right')
                if appears("go_to_model_drawing"):
                    mouseLeftClickOn("go_to_model_drawing")
                    if appears("save_caution"):
                        pyautogui.press('enter')
                    sleep(20)
                    if appears("u02_fr52_tab"):
                        self.logger.info("u02_fr52_tab appeared")
                        sleep(10)
                        sleep(10)
                        free()
                        pyautogui.moveTo(400,400)
                        sleep(2)
                        pyautogui.click()
                        sleep(2)
                        pyautogui.press('esc',presses=3)
                        sleep(2)
                        pyautogui.typewrite('select', .1)
                        sleep(2)
                        pyautogui.press('enter')
                        sleep(2)
                        pyautogui.typewrite('-6272.3050,2758.9051', .1)
                        sleep(2)
                        pyautogui.press('enter', presses=2)
                        sleep(2)
                        pyautogui.click(button='right')
                        sleep(2)
                        sleep(2)
                        mouseLeftClickOn("edit_properties_option")
                        if appears("stiff_properties"):
                            # mouseLeftClickOn("hp200x10_selected")
                            mouseLeftClickOn("stock_drop_down")
                            sleep(.5)
                            mouseLeftClickOn("hp240x10")
                            mouseLeftClickOn("stiffeners_ok")
                            sleep(5)
                            free()
                            free()
                            free()
                            pyautogui.typewrite('delete', .1)
                            pyautogui.press('enter')
                            pyautogui.typewrite('-5917.5479,2751.9541', .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            pyautogui.hotkey('ctrl', 's')
                            sleep(8)
                            mouseLeftClickOn("main_panel_new")
                            mouseLeftClickOn("navigator_button_new_18")
                            sleep(2)
                            mouseLeftClickOn("assembly_node")
                            if appears("slide_down", 30):
                                mouseMoveTo("slide_down")
                            else:
                                raise AssertionError("slide_down did not appear")
                            while exists("0202_p09_high") == False:
                                pyautogui.click(clicks=10)
                            mouseDoubleClickOn("0202_p09_high")
                            sleep(2)
                            if appears("0202_p09_tab"):
                                free_white()
                                obj.update_drawing()
                                waitForVanish("working_dialog")
                                sleep(10)
                                if appears("note_window",20):
                                    mouseLeftClickOn("note_window_ok")
                                    waitForVanish("working_dialog")
                                    if appears("discarded_part_on_bom_table",10):
                                        self.logger.info("BOM Table was updated after the changes")
                                    obj.bom_revision(option_to_be_selected="revision_layout_selected")
                                    sleep(15)
                                    if appears("up_to_date_warning",15):
                                        pyautogui.press('enter')
                                    if appears("enter_name_for_revision", 10):
                                        pyautogui.typewrite('Rev.1', .1)
                                        pyautogui.press('enter')
                                        sleep(.5)
                                        pyautogui.press('enter')
                                        if appears("rev_1",5):
                                            self.logger.info("rev_1 was updated")
                                else:
                                    raise AssertionError("note_window did not appear")
                            else:
                                raise AssertionError("0202_p09_tab did not appear")
                        else:
                            raise AssertionError("stiff_properties did not appear")
                    else:
                        raise AssertionError("u02_fr52_tab did not appear")
                else:
                    raise AssertionError("Go_to_model_drawing option did not appear")
            else:
                raise AssertionError("enter_name_for_revision option did not appear")
        else:
            raise AssertionError("0202_p09_high did not appear")
        self.logger.info('End:\t Checking Module- check_BOM_revisions')


    def check_keywords_for_revision(self):
        self.logger.info('Start:\t Checking Module- check_keywords_for_revision')
        obj = prodDoc()
        obj.insert_keyword()
        if appears("insert_keyword_window"):
            self.logger.info("insert_keyword_window appeared")
            if appears("insert_keyword_slide_down",30):
                mouseMoveTo("insert_keyword_slide_down")
            else:
                raise AssertionError("insert_keyword_slide_down did not appear")
            pyautogui.click(clicks=50)
            sleep(1)
            mouseLeftClickOn("revision_date")
            pyautogui.keyDown('ctrl')
            mouseLeftClickOn("revision_name")
            mouseLeftClickOn("revision_user")
            pyautogui.keyUp('ctrl')
            pyautogui.press('tab', presses=5)
            pyautogui.press('3')
            mouseLeftClickOn("insert_keyword_window_ok")
            free_white()
            if appears("please_select_a_point"):
                pyautogui.typewrite('29,58', .1)
                pyautogui.press('enter')
            if appears("please_select_a_point"):
                pyautogui.typewrite('111,58', .1)
                pyautogui.press('enter')
            if appears("please_select_a_point"):
                pyautogui.typewrite('218,58', .1)
                pyautogui.press('enter')
            obj.update_keyword()
        else:
            raise AssertionError("insert_keyword_window did not appear")
        self.logger.info('End:\t Checking Module- check_keywords_for_revision')

    # Viewing Revisions

    def viewing_revisions(self):
        self.logger.info('Start:\t Checking Module- viewing_revisions')
        obj = prodDoc()
        # obj.revision()
        mouseLeftClickOn("production_drawing_revisions_window")
        mouseLeftClickOn("close_button_high")
        free_white()
        sleep(1)
        obj.list_all_revision(option='A', list_part_option='Y')
        mouseLeftClickOn("command_line_expand")
        free_white()
        sleep(2)
        pyautogui.press('esc')
        self.logger.info('End:\t Checking Module- viewing_revisions')

    # Deleting Revisions

    def deleting_revisions(self):
        self.logger.info('Start:\t Checking Module- deleting_revisions')
        obj = prodDoc()
        obj.delete_bom_revision(option_to_be_selected="delete_bom_revision")
        obj.delete_all_revision(option_to_be_selected="delete_all_revision")
        self.logger.info('End:\t Checking Module- deleting_revisions')

    def exercise_pde_r005(self):
        self.logger.info('Start:\t Checking Module- exercise_pde_r005')
        obj = prodDoc()
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        sleep(2)
        mouseLeftClickOn("output_node")
        if appears("slide_down",30):
            mouseMoveTo("slide_down")
        else:
            raise AssertionError("slide_down did not appear")
        while exists("exercise2_node") == False:
            pyautogui.click(clicks=2)
        mouseDoubleClickOn("exercise2_node")
        if appears("save_caution"):
            pyautogui.press('enter')
        sleep(5)
        if appears("exercise2_tab"):
            self.logger.info("Exercise2 tab opened")
            obj.open_BOM_manager()
            if appears("expand_dark_blue_region", 5):
                mouseLeftClickOn("expand_dark_blue_region")
            elif appears("expand_dark_blue_region2", 5):
                mouseLeftClickOn("expand_dark_blue_region2")
            else:
                mouseLeftClickOn("expand_blue_region")
            sleep(3)
            mouseLeftClickOn("exercise2_bom_definition")
            mouseLeftClickOn("add_remove_button")
            if appears("choose_bom_fields_window"):
                if appears("slide_down", 30):
                    mouseMoveTo("slide_down")
                else:
                    raise AssertionError("slide_down did not appear")
                pyautogui.click(clicks=60)
                if appears("revision_bom_field"):
                    mouseLeftClickOn("revision_bom_field")
                    pyautogui.press('left')
                    pyautogui.press('space')
                    mouseLeftClickOn("choose_bom_fields_window_ok")
                    mouseLeftClickOn("bom_definition_apply")
                    mouseLeftClickOn("bom_definition_ok")
                    sleep(1)
                    pyautogui.hotkey('alt', 'f4')
                    sleep(3)
                    obj.update_BOM_table()
                    sleep(1)
                    obj.bom_revision(option_to_be_selected="revision_layout_selected")
                    if appears("enter_name_for_revision"):
                        pyautogui.typewrite('A', .1)
                        pyautogui.press('enter')
                        sleep(.5)
                        pyautogui.press('enter')
                    sleep(1)
                    free_white()
                    pyautogui.click(clicks=2)
                    sleep(1)
                    pyautogui.press('esc',presses=3)
                    sleep(1)
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    sleep(1)
                    # pyautogui.typewrite('28236.2998,-1057.6579', .1)
                    pyautogui.typewrite('28368.1555,-976.1277', .1)

                    pyautogui.press('enter', presses=2)
                    pyautogui.click(button='right')
                    if appears("go_to_model_drawing"):
                        mouseLeftClickOn("go_to_model_drawing")
                        if appears("save_caution"):
                            pyautogui.press('enter')
                        sleep(10)
                    if appears("u02_fr47_tab"):
                        sleep(10)
                        sleep(10)
                        self.logger.info("u02_fr47_tab appeared")
                        free()
                        # reset_viewport()
                        pyautogui.moveTo(400,400)
                        sleep(1)
                        pyautogui.click()
                        sleep(1)
                        pyautogui.press('esc',presses=3)
                        sleep(2)
                        pyautogui.typewrite('select', .1)
                        sleep(1)
                        pyautogui.press('enter')
                        sleep(1)
                        pyautogui.typewrite('1113.1999,399.5359', .2)
                        sleep(1)
                        pyautogui.press('enter', presses=2)
                        pyautogui.click(button='right')
                        sleep(2)
                        mouseLeftClickOn("edit_properties_option")
                        if appears("plate_properties"):
                            mouseLeftClickOn("pl08")
                            pyautogui.press('down', presses=4)
                            mouseLeftClickOn("plate_properties_ok")
                            sleep(5)
                            free()
                            pyautogui.hotkey('ctrl', 's')
                            sleep(8)
                            mouseLeftClickOn("main_panel_new")
                            mouseLeftClickOn("navigator_button_new_18")
                            sleep(2)
                            mouseLeftClickOn("output_node")
                            if appears("slide_down", 30):
                                mouseMoveTo("slide_down")
                            else:
                                raise AssertionError("slide_down did not appear")
                            while exists("exercise2_high") == False:
                                pyautogui.click(clicks=2)
                            mouseDoubleClickOn("exercise2_high")
                            sleep(5)
                            if appears("exercise2_tab"):
                                free_white()
                                obj.update_drawing()
                                if appears("update_output_drawing_window"):
                                    pyautogui.press('enter')
                                waitForVanish("working_dialog")
                                sleep(5)
                                if appears("note_window"):
                                    mouseLeftClickOn("note_window_ok")
                                    waitForVanish("working_dialog")
                                    sleep(5)
                                    obj.bom_revision(option_to_be_selected="revision_layout_selected")
                                    if appears("enter_name_for_revision", 10):
                                        pyautogui.typewrite('B', .1)
                                        pyautogui.press('enter')
                                        sleep(.5)
                                        pyautogui.press('enter')
                                        sleep(5)
                                        pyautogui.hotkey('ctrl','s')
                                else:
                                    raise AssertionError("note_window did not appear")
                            else:
                                raise AssertionError("exercise2_tab did not appear")
                        else:
                            raise AssertionError("plate_properties did not appear")
                    else:
                        raise AssertionError("u02_fr47_tab did not appear")
                else:
                    raise AssertionError("revision_bom_field did not appear")
            else:
                raise AssertionError("choose_bom_fields_window did not appear")
        else:
            raise AssertionError("exercise2_tab did not appear")
        self.logger.info('End:\t Checking Module- exercise_pde_r005')

    def showing_out_of_date(self):
        self.logger.info('Start:\t Checking Module- showing_out_of_date')
        obj = prodDoc()
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new_18")
        sleep(2)
        mouseLeftClickOn("ssi_node_unhigh")
        mouseLeftClickOn("u01_node")
        mouseLeftClickOn("assembly_node")
        if appears("sc_navi_show_out_dated"):
            mouseLeftClickOn("sc_navi_show_out_dated")
            waitForVanish("working_dialog")
            waitForVanish("working_dialog_unhigh")
            sleep(30)
            if appears("0101_es01"):
                self.logger.infoint("Out of date drawings are shown")
            else:
                raise AssertionError("Could not perform the operation")
        else:
            raise AssertionError("sc_navi_show_out_dated did not appear")
        self.logger.info('End:\t Checking Module- showing_out_of_date')

    def check_updating_drawings(self):
        self.logger.info('Start:\t Checking Module- check_updating_drawings')
        mouseDoubleClickOn("0101_es01")
        sleep(4)
        if appears("0101_es01_tab"):
            print("0101_es01_tab appeared")
            obj = prodDoc()
            obj.update_drawing()
            waitForVanish("working_dialog")
            sleep(10)
            if appears("note_window"):
                mouseLeftClickOn("note_window_ok")
                waitForVanish("working_dialog")
                sleep(5)
                pyautogui.hotkey('ctrl','s')
                sleep(5)
                mouseLeftClickOn("main_panel_new")
                mouseLeftClickOn("navigator_button_new_18")
                sleep(2)
                mouseLeftClickOn("sc_navi_show_out_dated")
                waitForVanish("working_dialog")
                waitForVanish("working_dialog_unhigh")
                sleep(30)
            else:
                raise AssertionError("note_window did not appear")
        else:
            raise AssertionError("0101_es01_tab did not appear")
        self.logger.info('End:\t Checking Module- check_updating_drawings')


class TCP6_Production_DocumentationError(Exception):
    pass
