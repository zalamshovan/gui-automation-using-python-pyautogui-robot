#Combine Report Path DONOT CHANGE THIS VARIABLE
CombineReportPath = "C:\AutomationLogs\Combined"

# UNCOMMENT THE DESIRED IMAGE FOLDER FROM BELLOW ["AutoCAD19_Mechanical" recommended]
# ImageFolder = "AutoCAD19"
ImageFolder = "AutoCAD19_Mechanical"

# UNCOMMENT THE DESIRED AUTOCAD VERSION TO START WITH.
# AutoCAD_version = "AutoCAD2018"
AutoCAD_version = "AutoCAD2019"
# AutoCAD_version = "AutoCAD2019_mechanical"

# UNCOMMENT THE AUTOCAD LICENSE STATUS.
# If the "trial" is uncommented, the automation will wait for the trial license message to appear. Otherwise not.
# AutoCAD_license_status = "trial"
AutoCAD_license_status = "licensed"

# To unpack the setup file please set below variable as "Yes"
# To skip unpacking the setup file set below variable as "No".
Unzip_Installer = "No"

# Installer Path [Please don't use blank space after equal( =) sign]
# Provide the full directory path which contians the Setup.exe
# Please provide the diretory path in quotations as Example : "C:/Users/VM/Desktop/SC_Tests/Refactor_SSI_Dev_Branch_ReleaseVerifier_2.0/SCRelease/Latest_installer/Latest_Setup"
# Use only one path at a time. Please delete unnecessary file path. DO NOT comment out. /Latest_Setup
Path_Installer ="C:/Users/VM/Desktop/SC_Tests/SSI_Dev_Branch_AutoCAD2020/SCRelease/Latest_installer"

# backup database directory path with full name of the database
BackUp_DB = "C:/SSI/test2018_18.100.16.0_2018-07-04 04.07.30.bak2014"

# old deployed project file directory
DeployedProject_Directory = "C:/TestingProject/"

# set License Server for Enterprise Platform setup
LicenseServer = "localhost"

# Choose a number from below to uninstallation SSI application.
# 1)from setup.exe
# 2)from programs and features
# 3)from both [in this case automation script will uninstall from exe first then install SSI from exe then uninstall from programs and features]
Uninstallation_Process = "1"

# If anyone wants to save the project file in a specific path set the below varibale as "Yes"
# If anyone doesn't want to save the project file set the below varibale as "No"
ToSaveProjectFile = "No"

# Project Package Path
# Provide the full directory path upto SCProjectPackage Folder
ProjectPath = "C:/Users/VM/Desktop/SC_Tests/SSI_Dev_Branch_AutoCAD2020/SCProjectPackage"