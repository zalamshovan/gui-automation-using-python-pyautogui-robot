# AutoRunner.py

from time import sleep
import os, config
from AppUtil import AppUtil

def create_combined_report_1stTimerun():
    obj = AppUtil()
    firstTimeRun = obj.findTestCasesName("singlerun")
    obj.create_combined_report(firstTimeRun)
    obj.updateProjectFilePath('FirstRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def create_combined_report_2ndTimerun_1():
    obj = AppUtil()
    firstTimeRun, secondTimeRun = obj.findTestCasesName("2timesrun")
    obj.create_combined_report(firstTimeRun)
    obj.updateProjectFilePath('SecondRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def create_combined_report_2ndTimerun_2():
    obj = AppUtil()
    firstTimeRun, secondTimeRun = obj.findTestCasesName("2timesrun")
    obj.create_combined_report(secondTimeRun)
    obj.updateProjectFilePath('FirstRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def create_combined_report_3rdTimerun_1():
    obj = AppUtil()
    firstTimeRun, secondTimeRun, thirdTimeRun = obj.findTestCasesName("3timesrun")
    obj.create_combined_report(firstTimeRun)
    obj.updateProjectFilePath('SecondRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def create_combined_report_3rdTimerun_2():
    obj = AppUtil()
    firstTimeRun, secondTimeRun, thirdTimeRun = obj.findTestCasesName("3timesrun")
    obj.create_combined_report(secondTimeRun)
    obj.updateProjectFilePath('ThirdRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def create_combined_report_3rdTimerun_3():
    obj = AppUtil()
    firstTimeRun, secondTimeRun, thirdTimeRun = obj.findTestCasesName("3timesrun")
    obj.create_combined_report(thirdTimeRun)
    obj.updateProjectFilePath('FirstRun')
    obj.runBuildNumberBatchFile(config.CombineReportPath)

def installApplication():
    obj = AppUtil()
    mycwd = os.getcwd()
    obj.setPathFinder(mycwd)
    os.chdir("../" + "TC07_Installation_Uninstallation")
    os.system("Run_Installation.bat")
    os.chdir(mycwd)

def uninstallApplication():
    mycwd = os.getcwd()
    os.chdir("../" + "TC07_Installation_Uninstallation")
    os.system("Run_Removing.bat")
    os.chdir(mycwd)

def tc01_structure_modeling_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC01_Structure_Modeling")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc01_structure_modeling_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC01_Structure_Modeling")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc01_structure_modeling_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC01_Structure_Modeling")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc02_HVAC_catalog_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC02_HVAC_catalog")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc02_HVAC_catalog_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC02_HVAC_catalog")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc02_HVAC_catalog_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC02_HVAC_catalog")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc03_Pipe_Modeling_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC03_Pipe_Modeling")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc03_Pipe_Modeling_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC03_Pipe_Modeling")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc03_Pipe_Modeling_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC03_Pipe_Modeling")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc04_sc_essentials_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC04_ShiCon_Essentials")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc04_sc_essentials_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC04_ShiCon_Essentials")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc04_sc_essentials_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC04_ShiCon_Essentials")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc05_pipe_catalog_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC05_Pipe_Catalog")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc05_pipe_catalog_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC05_Pipe_Catalog")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc05_pipe_catalog_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC05_Pipe_Catalog")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc06_prod_docu_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC06_Production_Documentation")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc06_prod_docu_second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC06_Production_Documentation")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc06_prod_docu_third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC06_Production_Documentation")
    os.system("Run_Third.bat")
    os.chdir(mycwd)

def tc07_equipment_first():
    mycwd = os.getcwd()
    os.chdir("../" + "TC08_Equipment_Catalog")
    os.system("Run_First.bat")
    os.chdir(mycwd)

def tc07_equipment_Second():
    mycwd = os.getcwd()
    os.chdir("../" + "TC08_Equipment_Catalog")
    os.system("Run_Second.bat")
    os.chdir(mycwd)

def tc07_equipment_Third():
    mycwd = os.getcwd()
    os.chdir("../" + "TC08_Equipment_Catalog")
    os.system("Run_Third.bat")
    os.chdir(mycwd)