"""
*** TCP8_Equipment_Catalog.py ***

Automation script for Equipment Catalog
"""

from AppUtil import AppUtil

from ProjectRegisterDeployDeleteUtil import ProjectRegisterDeployDeleteUtil

from SSILearningVerifierUtil import SSILearningVerifierUtil

from time import sleep

from logger_extended import logger_extended

from ProductionDocumentationUtil import ProductionDocumentationUtil as prodDoc

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseRightClickOn, mouseDoubleClickOn, goDownUntilAppears

from screenObjUtil import exists, appears, waitForVanish

import pyautogui,os

from SSIUtil import zoom, free, maximize, line, free_white, reset_viewport , conceptual_visual, \
    change_viewpoint_from_aft_stbd_up, wireframe2d_visual, hidden_visual, \
    click_dropdown_on_hover, open_manager, enable_3d_tool, change_viewpoint_from_fwd_stbd_up, change_viewpoint_from_fwd_prt_up, \
    reload_drawing

import Logger

class TCP8_Equipment_Catalog(object):
    pyautogui.FAILSAFE = False
    pyautogui.PAUSE = .5

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('TCP8_Equipment_Catalog')

    def __init__(self):
        try:
            self._expression = ''
        except:
            sleep(600)
            os.system("taskkill /f /im explorer.exe")
            sleep(10)
            os.system("start explorer.exe")
            obj = AppUtil()
            obj.launchExplorer()
            self._expression = ''

    def testSetup(self):
        obj = AppUtil()
        obj.minimizeRunnerPrompt()

    def testTearDown(self):
        obj = AppUtil()
        obj.maximizeRunnerPrompt()
        obj.runBuildNumberBatchFile("TC08_Equipment_Catalog")

    def deploy_new_project(self):
        obj = AppUtil()
        obj.set_path_for_image("Common")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.delete_previous_deploy(module = "deploy_equipment")

        obj.launch_app()
        obj1.project_deployer(module="equipment")
        obj.close_app()

    def launch_application(self):
        obj = AppUtil()
        obj.launch_app()

    def launch_app_fresh(self):
        obj = AppUtil()
        obj.launch_app()

    def close_application(self):
        obj = AppUtil()
        obj.close_app()

    def forced_close_application(self):
        obj = AppUtil()
        obj.forced_close()
        sleep(2)
        os.system("TASKKILL /F /IM Report.exe")
        sleep(2)
        os.system("TASKKILL /F /IM Report.exe")
        sleep(2)

    def register_project(self):
        maximize()
        obj= AppUtil()
        obj.ribbon_load("mechanical_tab")
        obj1 = ProjectRegisterDeployDeleteUtil()
        obj1.register_project("newDeploy_equipment", "NewDeploy_equipment")

    def restart_Applicaion(self):
        self.close_application()
        self.launch_application()
        self.register_project()
        free()
        self.store_projectFile()

    def store_projectFile(self):
        obj = AppUtil()
        obj.storeProjectPackage("NewDeploy_equipment")

    def open_equipment_library(self):
        self.logger.info('Start:\t Checking Module- open_equipment_library')
        if appears("euipment_on_manager",10):
            print("euipment_on_manager1 appeared")
            mouseLeftClickOn("euipment_on_manager1")
            if appears("equipment_library_button",5):
                mouseLeftClickOn("equipment_library_button")
                if appears("equip_lib_manager_maximized",10):
                    print("Equipment library manager appeared")
                elif appears("equip_lib_manager",10):
                    print("Equipment library manager appeared")
                    mouseDoubleClickOn("equip_lib_manager")
            else:
                raise AssertionError("equipment_library_button did not appear")
        else:
            raise AssertionError("equipment button did not appear")
        self.logger.info('End:\t Checking Module- open_equipment_library')

    def library_interface(self):
        self.logger.info('Start:\t Checking Module- library_interface')
        maximize()
        open_manager()
        if appears("equip_lib_button",10):
            print("Equipment Library button appeared in the manager toolbar")
            self.open_equipment_library()
            if appears("collapse_all",10):
                mouseLeftClickOn("collapse_all")
                print("collapse_all button was clicked")
            else:
                raise AssertionError("collapse_all button did not appear")
            if appears("expand_all",10):
                mouseLeftClickOn("expand_all")
                print("Expand all button was clicked")
            else:
                raise AssertionError("expand_all button did not appear")
            if appears("equip_lib_filter",10):
                mouseLeftClickOn("equip_lib_filter")
                pyautogui.typewrite("LKSR",.2)
                pyautogui.press('enter')
                if appears("lksr_210x150",10):
                    print("lksr_210x150 appeared")
                    mouseLeftClickOn("lksr_210x150")
                    if appears("equip_lib_find_in_drawings",10):
                        mouseLeftClickOn("equip_lib_find_in_drawings")
                        if appears("610_u02_air_condition_and_ventilation_td_dwg",20):
                            mouseLeftClickOn("610_u02_air_condition_and_ventilation_td_dwg")
                            mouseLeftClickOn("open_drawing")
                            sleep(30)
                            if appears("lksr_210x150_on_stock_usage_pallete"):
                                print("Drawing containing LKSR 210x150 stock was appeared")
                                mouseLeftClickOn("lksr_210x150_on_stock_usage")
                                mouseLeftClickOn('highlight')
                                mouseLeftClickOn("zoom")
                                sleep(1)
                                pyautogui.press('right',presses=6,interval=.2)
                                mouseLeftClickOn("close_drawing")
                                if appears("stock_usage_warning"):
                                    mouseLeftClickOn("discard_changes")
                            else:
                                raise AssertionError("Drawing containing LKSR 210x150 stock was not appeared")
                            if appears("create_log"):
                                mouseLeftClickOn("create_log")
                                if appears("equipment_stock_usage_log"):
                                    print("equipment_stock_usage_log was appeared")
                                    pyautogui.hotkey('alt','f4')
                                    mouseLeftClickOn("ok_button_drawing_using")
                                else:
                                    raise AssertionError("equipment_stock_usage_log did not appear")
                            else:
                                raise AssertionError("create_log did not Appear")
                        else:
                            raise AssertionError("610_u02_air_condition_and_ventilation_td_dwg did not appear. "
                                                 "Find in drawing option checking was failed")
                    else:
                        raise AssertionError("equip_lib_find_in_drawings did not appear")
                else:
                    raise AssertionError("lksr_210x150 did not appear")
                if appears("equip_lib_delete_filter",5):
                    mouseLeftClickOn("equip_lib_delete_filter")
                else:
                    raise AssertionError("equip_lib_delete_filter did not appear")
                mouseLeftClickOn("equip_lib_filter")
                pyautogui.typewrite("el_box", .2)
                pyautogui.press('enter')
                mouseLeftClickOn("equip_lib_delete_filter")
            else:
                raise AssertionError("equip_lib_filter did not appear")
        else:
            raise AssertionError("Equipment Library button was not appeared in the manager toolbar")
        self.logger.info('End:\t Checking Module- library_interface')

    # Creating Equipment Stock
    def creating_equipment_library(self):
        self.logger.info('Start:\t Checking Module- creating_equipment_library')
        mouseMoveTo("equip_lib_scroll_up")
        pyautogui.click(clicks=120)
        mouseLeftClickOn("equipment_stock")
        if appears("new_stock_equipment",10):
            mouseLeftClickOn("new_stock_equipment")
            pyautogui.typewrite("Test Category",.1)
            pyautogui.press("enter")
            sleep(2)
            pyautogui.moveTo(500,500)
            mouseLeftClickOn("new_stock_equipment")
            pyautogui.typewrite("Test Sub-Category", .1)
            pyautogui.press("enter")
            sleep(2)
            pyautogui.moveTo(500, 500)
            mouseLeftClickOn("new_stock_equipment")
            pyautogui.typewrite("Test Equipment", .1)
            pyautogui.press("enter")
            sleep(2)
            pyautogui.moveTo(500, 500)
            mouseLeftClickOn("description_field")
            pyautogui.press('right')
            pyautogui.typewrite('Test equipment',.1)
            pyautogui.press('enter')
            pyautogui.press('D')
            pyautogui.press('enter')
            mouseLeftClickOn("weight_field_equip")
            pyautogui.press('right')
            pyautogui.typewrite('10',.1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('1000',.1)
            pyautogui.press('enter')
            pyautogui.press('down',presses=2)
            pyautogui.press('m')
            pyautogui.press('enter')
        else:
            raise AssertionError("new_stock button did not appear")
        self.logger.info('End:\t Checking Module- creating_equipment_library')

    # Editing Equipment Stock
    def editing_equipment_stock(self):
        self.logger.info('Start:\t Checking Module- editing_equipment_stock')
        sleep(2)
        pyautogui.moveTo(400,400)
        sleep(2)
        if appears("edit_stock"):
            mouseLeftClickOn("edit_stock")
            sleep(5)
            pyautogui.hotkey("win","up")
            sleep(2)
            if appears("edit_equipment_stock_window"):
                print("edit_equipment_stock_window appeared")
                if appears("attribute_tab",10):
                    mouseLeftClickOn("attribute_tab")
                mouseLeftClickOn("cold_feature")
                pyautogui.press('up')
                pyautogui.press('left')
                pyautogui.press('left')
                pyautogui.press('space')
                if appears("article_number"):
                    mouseLeftClickOn("article_number")
                    print("article_number was selected as user defined stock attribute")
                    pyautogui.press('right')
                    pyautogui.typewrite('111111',.1)
                    pyautogui.press('enter')

                else:
                    raise AssertionError("article_number was not selected as user defined stock attribute")
                mouseLeftClickOn("tag_number")
                pyautogui.press('left')
                pyautogui.press('space')
                mouseLeftClickOn("required")
                pyautogui.press('right')
                pyautogui.press('right')
                sleep(1)
                pyautogui.press('r',presses=3)
                pyautogui.press('enter')
                if appears("pipe_ends"):
                    mouseLeftClickOn("pipe_ends")
                    mouseLeftClickOn("new_pipe_end")
                    if appears("select_pipe_end_treatment_window"):
                        print("select_pipe_end_treatment_window appeared")
                        mouseLeftClickOn("pipe_lf_expand","pipe_lf_region")
                        mouseLeftClickOn("pipe_lf_expand","100mm_expand")
                        if appears("fl_100_mm"):
                            mouseDoubleClickOn("fl_100_mm")
                        else:
                            raise AssertionError("fl_100_mm did not appear")
                        mouseLeftClickOn("draw_end_treatment")
                        pyautogui.press('right',presses=5)
                        pyautogui.press('space')
                    else:
                        raise AssertionError("select_pipe_end_treatment_window did not appear")
                else:
                    raise AssertionError("pipe_ends tab did not appear")
                if appears("hvac_ends"):
                    mouseLeftClickOn("hvac_ends")
                    mouseLeftClickOn("new_hvac_end")
                    if appears("select_hvac_end_treatment_window"):
                        mouseLeftClickOn("pipe_lf_expand","fl_30_expand_region")
                        if appears("fl_30_galv"):
                            mouseDoubleClickOn("fl_30_galv")
                        else:
                            raise AssertionError("fl_30_galv did not appear")
                        mouseLeftClickOn("draw_end_treatment")
                        pyautogui.press('right', presses=5)
                        pyautogui.press('space')
                    else:
                        raise AssertionError("select_hvac_end_treatment_window did not appear")
                else:
                    raise AssertionError("hvac_ends did not appear")
                if appears("electrical_tab"):
                    mouseLeftClickOn("electrical_tab")
                    mouseLeftClickOn("add_ep")
                    pyautogui.typewrite('EP1',.1)
                    pyautogui.press('enter')
                    pyautogui.press('right',presses=2)
                    pyautogui.typewrite("500",.1)
                    pyautogui.press('enter')
                else:
                    raise AssertionError("electrical_tab did not appear")
                if appears("accessory_packages"):
                    mouseLeftClickOn("accessory_packages")
                    mouseLeftClickOn("edit_equipment_stock_window_done")
                else:
                    raise AssertionError("accessory_packages tab did not appear")
                if appears("general_button"):
                    mouseLeftClickOn("general_button")
                    mouseLeftClickOn("accessory_button")
                    mouseLeftClickOn("packages_button")
                    if appears("accessory_pkj_window",10):
                        print("accessory_packages window appeared")
                    elif appears("accessory_packages_maximized",10):
                        print("accessory_packages window appeared")
                    else:
                        raise AssertionError("accessory_pkj_window did not appear")
                    mouseLeftClickOn("accessory_pkj_type_dropdown")
                    if appears("accessory_pkj_type_equipment",10):
                        mouseLeftClickOn("accessory_pkj_type_equipment")
                    else:
                        pyautogui.moveTo(500,500)
                        pyautogui.press('esc')
                    mouseLeftClickOn("New_package_button")
                    pyautogui.typewrite("E-pack1",.1)
                    pyautogui.press('enter')
                    mouseLeftClickOn("nuts_button_expand","nuts_button")
                    mouseLeftClickOn("hex_nut")
                    mouseLeftClickOn("add_button")
                    mouseLeftClickOn("nuts_button_expand","hex_bolt_region")
                    mouseLeftClickOn("hex_bolt")
                    pyautogui.press('down')
                    mouseLeftClickOn("add_button")
                    mouseLeftClickOn("qty_1")
                    pyautogui.press('down')
                    pyautogui.press('6')
                    pyautogui.press('up')
                    pyautogui.press('6')
                    pyautogui.press('enter')
                    mouseLeftClickOn("accessory_pkj_ok")
                    sleep(10)
                else:
                    raise AssertionError("general_button did not appear")
            else:
                raise AssertionError("edit_equipment_stock_window did not appear")
        else:
            raise AssertionError("edit_stock did not appear")
        self.logger.info('End:\t Checking Module- editing_equipment_stock')

    # Creating and Editing Standard Drawings
    def creating_editing_standard_drawing(self):
        self.logger.info('Start:\t Checking Module- creating_editing_standard_drawing')
        if appears("edit_std_drawing",10):
            mouseMoveTo("edit_std_drawing")
        if appears("edit_stock"):
            mouseLeftClickOn("edit_stock")
            if appears("std_drawing_tab",10):
                mouseLeftClickOn("std_drawing_tab")
            if appears("import_st_drawing"):
                mouseLeftClickOn("import_st_drawing")
            else:
                raise AssertionError("import_st_drawing did not appear")
            if appears("import_equipment_std_drawing_window"):
                mouseLeftClickOn("browser_adrs_dropdown")
                pyautogui.typewrite("C:",.1)
                pyautogui.press("enter")
                if appears("new_deploy_2",10):
                    mouseDoubleClickOn("new_deploy_2")
                else:
                    raise AssertionError("new_deploy folder did not appear")
                sleep(1)
                pyautogui.press('t')
                pyautogui.press('t')
                pyautogui.press('enter')
                sleep(2)
                pyautogui.press('e')
                pyautogui.press('enter')
                if appears("open_window_icon_dropdown_region"):
                    mouseLeftClickOn("open_window_icon_dropdown", "open_window_icon_dropdown_region")
                    if appears("contentSizeIcon"):
                        mouseLeftClickOn("contentSizeIcon")
                mouseMoveTo("window_search_field")
                if appears("test_model"):
                    mouseDoubleClickOn("test_model")
                    if appears("test_model_loaded"):
                        print("Test model was successfully loaded")
                    else:
                        raise AssertionError("test_model was not loaded")
                    mouseLeftClickOn("edit")

                    if appears("equipment_std_palette",7):
                        print("equipment_std_palette appeared")
                        mouseRightClickOn("equipment_std_palette")
                        mouseLeftClickOn("anchor_left")
                    elif appears("equipment_std_palette_high",7):
                        print("equipment_std_palette appeared")
                        mouseRightClickOn("equipment_std_palette_high")
                        mouseLeftClickOn("anchor_left")
                    else:
                        raise AssertionError("equipment_std_palette did not appear")
                    sleep(2)
                    mouseMoveTo("equipment_std_palette_docked_hidden")
                    mouseRightClickOn("equipment_std_palette_high")
                    mouseLeftClickOn("auto_hide")
                    sleep(3)
                    free()
                    reset_viewport()
                    sleep(1)
                    pyautogui.hotkey("ctrl","a")
                    sleep(1)
                    pyautogui.press('delete')
                    sleep(4)
                    sleep(1)
                    pyautogui.hotkey("ctrl","a")
                    sleep(1)
                    pyautogui.press('delete')
                    sleep(4)
                    enable_3d_tool()
                    mouseLeftClickOn("3d_tool_panel")
                    if appears("modeling_pane"):
                        mouseLeftClickOn("modeling_pane")
                    else:
                        raise AssertionError("modeling_pane did not appear")
                    if appears("box"):
                        mouseLeftClickOn("box")
                        free()
                        if appears("specify_first_point"):
                            pyautogui.typewrite("0,0,0", .2)
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.press('c')
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.moveTo(1392, 505)
                            sleep(1)
                            pyautogui.typewrite('500', .2)
                            pyautogui.press('enter')
                            reset_viewport()
                            if appears("close_panel_unhigh",5):
                                mouseLeftClickOn("close_panel_unhigh")
                            elif appears("close_button_high",5):
                                mouseLeftClickOn("close_button_high")
                            free()
                            mouseLeftClickOn("cg_point")
                            mouseLeftClickOn("edit_cg_point")
                            free()
                            if appears("select_location_cg_point"):
                                pyautogui.typewrite("250,250,250", .2)
                                pyautogui.press('enter')
                            else:
                                raise AssertionError("select_location_cg_point option did not appear")
                            mouseLeftClickOn("insertion_point_add")
                            free()
                            if appears("select_location_of_insertion_point"):
                                pyautogui.typewrite("0,500,0", .2)
                                pyautogui.press('enter')
                            else:
                                raise AssertionError("select_location_of_insertion_point did not appear")
                            pyautogui.typewrite('line', .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("0,0,0", .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("#0,500,500", .2)
                            pyautogui.press('enter')
                            pyautogui.press('enter')

                            pyautogui.typewrite('line', .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("-3.7237,499.9861,503", .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("#499.9861,3.7237,503", .2)
                            pyautogui.press('enter')
                            pyautogui.press('enter')

                            pyautogui.typewrite('line', .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("500,0,500", .2)
                            pyautogui.press('enter')
                            pyautogui.typewrite("#500,500,0", .2)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.typewrite('select', .1)
                            pyautogui.press('enter')
                            pyautogui.typewrite("250,500,0", .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            pyautogui.typewrite("move", .1)
                            pyautogui.press('enter')
                            pyautogui.typewrite("250,500,0", .1)
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.moveTo(941, 523)
                            pyautogui.typewrite("20", .2)
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.typewrite('select', .1)
                            pyautogui.press('enter')
                            pyautogui.typewrite("248.1312,251.8549,503", .1)
                            pyautogui.press('enter')
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.typewrite("move", .1)
                            pyautogui.press('enter')
                            pyautogui.typewrite("248.1312,251.8549,503", .1)
                            pyautogui.press('enter')
                            sleep(2)
                            pyautogui.moveTo(1098, 221)
                            pyautogui.typewrite("30", .2)
                            pyautogui.press('enter')
                            mouseLeftClickOn("new_pipe_end2")
                            mouseLeftClickOn("edit_connection_end")
                            free()
                            sleep(2)
                            if appears("please_pick_point"):
                                sleep(2)
                                pyautogui.moveTo(961, 506)
                                pyautogui.typewrite("-17.9217,258.8776,250", .1)
                                pyautogui.press('enter')
                                sleep(3)
                                pyautogui.moveTo(1410, 255)
                                pyautogui.click()
                            else:
                                raise AssertionError("please_pick_point did not appear")
                            mouseLeftClickOn("new_hvac_end2")
                            mouseLeftClickOn("edit_connection_end")
                            free()
                            sleep(2)
                            if appears("please_pick_point"):
                                sleep(3)
                                pyautogui.moveTo(1108, 282)
                                # pyautogui.typewrite("250.2366,253.9919,502.9745", .1)
                                pyautogui.typewrite("249.8434,253.5928,526.2542", .1)
                                pyautogui.press('enter')
                                pyautogui.click(button="right")
                                if appears("plaNE", 5):
                                    mouseLeftClickOn("plaNE")
                                else:
                                    raise AssertionError("plaNE option did not appear")
                                sleep(3)
                                # pyautogui.moveTo(1102, 764)
                                pyautogui.moveTo(1125, 592)
                                pyautogui.click()
                                sleep(2)
                                pyautogui.moveTo(1292, 380)
                                pyautogui.click()
                            else:
                                raise AssertionError("please_pick_point did not appear")
                            mouseLeftClickOn("ep1")
                            mouseLeftClickOn("edit_entry_point")
                            free()
                            if appears("select_location_for_cp"):
                                pyautogui.typewrite("500,250,250", .1)
                                pyautogui.press('enter')
                            else:
                                raise AssertionError("please_pick_point did not appear")
                            if appears("insertion_point_add"):
                                mouseMoveTo("insertion_point_add")
                                mouseLeftClickOn("insertion_point_add")
                            else:
                                raise AssertionError("insertion_point_add did not appear")
                            if appears("layer_name_window"):
                                pyautogui.typewrite("Maintenance", .2)
                                pyautogui.press('enter')
                                if appears("color_5"):
                                    mouseDoubleClickOn("color_5")
                                else:
                                    raise AssertionError("color_5 did not appear")
                            else:
                                raise AssertionError("layer_name_window did not appear")
                            mouseLeftClickOn("home_button")
                            mouseLeftClickOn("layer_button")
                            if appears("current_layer_dropdown"):
                                mouseLeftClickOn("current_layer_dropdown")
                                if appears("maintenance_layer"):
                                    mouseLeftClickOn("maintenance_layer")
                                    free()
                                    pyautogui.press('esc')
                                    pyautogui.press('esc')
                                else:
                                    raise AssertionError("maintenance_layer did not appear")
                            else:
                                raise AssertionError("current_layer_dropdown did not appear")
                            mouseLeftClickOn("3d_tool_panel")
                            if appears("modeling_pane"):
                                mouseLeftClickOn("modeling_pane")
                            else:
                                raise AssertionError("modeling_pane did not appear")
                            if appears("box"):
                                mouseLeftClickOn("box")
                                free()
                            if appears("specify_first_point"):
                                pyautogui.typewrite("0,0,0", .2)
                                pyautogui.press('enter')
                                pyautogui.typewrite("500,-150,0", .2)
                                pyautogui.press('enter')
                                pyautogui.typewrite("650", .2)
                                pyautogui.press('enter')
                                sleep(2)
                                reset_viewport()
                            else:
                                raise AssertionError("specify_first_point did not appear during creating 2nd box")
                            mouseLeftClickOn("close_drawing")
                            if appears("stock_usage_warning",10):
                                mouseLeftClickOn("save_changes")
                                sleep(10)
                            else:
                                raise AssertionError("stock_usage_warning did not appear")
                            if appears("edit_equipment_stock_window_done",20):
                                mouseLeftClickOn("edit_equipment_stock_window_done")
                            else:
                                pyautogui.moveTo(500,500)
                                sleep(2)
                                pyautogui.click()
                                pyautogui.press('esc')
                                pyautogui.press('esc')
                                pyautogui.press('esc')
                                pyautogui.typewrite("SCCATALOG",.1)
                                pyautogui.press("enter")
                                if appears("edit_equipment_stock_window_done", 20):
                                    mouseLeftClickOn("edit_equipment_stock_window_done")
                                else:
                                    pyautogui.typewrite("SCCATALOG", .1)
                                    pyautogui.press("enter")
                                    if appears("edit_equipment_stock_window_done", 20):
                                        mouseLeftClickOn("edit_equipment_stock_window_done")

                            if appears("is_insertable"):
                                mouseLeftClickOn("is_insertable")
                                pyautogui.press('right')
                                pyautogui.press('space')
                            else:
                                raise AssertionError("is_insertable option did not appear")
                            mouseLeftClickOn("equipment_stock_ok")
                            sleep(10)
                            pyautogui.hotkey('alt','f4')
                        else:
                            raise AssertionError("specify_first_point did not appear")
                    else:
                        raise AssertionError("box did not appear")
                else:
                    raise AssertionError("test_model did not appear")
            else:
                raise AssertionError("import_equipment_std_drawing_window did not appear")
        else:
            raise AssertionError("edit_stock did not appear")
        self.logger.info('End:\t Checking Module- creating_editing_standard_drawing')

    # Equipment Model Drawing
    def creating_equipment_model_drawing(self):
        self.logger.info('Start:\t Checking Module- creating_equipment_model_drawing')
        mouseLeftClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new")
        if appears("sc_navigator_window"):
            mouseLeftClickOn("ssi_node_shadowed")
            mouseLeftClickOn("u01_node")
            mouseLeftClickOn("equipment_node")
            mouseLeftClickOn("equipment_node_drawing")
            mouseLeftClickOn("new_equipment")
            if appears("new_equipment_dialog"):
                pyautogui.typewrite("Test Equipment")
                if appears("open_new_drawing2", 5):
                    mouseLeftClickOn("open_new_drawing2")
                pyautogui.press('enter')
                if appears("test_equipment_tab"):
                    print("test_equipment_tab appeared")
                else:
                    raise AssertionError("test_equipment_tab did not appear")
            else:
                raise AssertionError("new_equipment_dialog did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")
        self.logger.info('End:\t Checking Module- creating_equipment_model_drawing')

    def equipment_model_drawing_option(self):
        self.logger.info('Start:\t Checking Module- equipment_model_drawing_option')
        mouseLeftClickOn("main_panel_new")
        click_dropdown_on_hover("drawing_options", "equipment_drawing_option")
        if appears("equipment_drawing_option_dialog"):
            mouseLeftClickOn("equipment_drawing_option_dialog_ok")
        else:
            raise AssertionError("equipment_drawing_option_dialog did not appear")
        pyautogui.hotkey('ctrl','s')
        sleep(5)
        self.logger.info('End:\t Checking Module- equipment_model_drawing_option')

    def insert_from_library(self):
        if appears("multi_discipline",7):
            mouseLeftClickOn("multi_discipline")
        mouseLeftClickOn("equipment_panel")
        if appears("insert_from_library"):
            mouseLeftClickOn("insert_from_library")
        else:
            raise AssertionError("insert_from_library did not appear")

    # Inserting Equipment into the Model
    def inserting_equipment_into_model(self):
        self.logger.info('Start:\t Checking Module- inserting_equipment_into_model')
        free()
        sleep(2)
        pyautogui.moveTo(400,400)
        sleep(2)
        pyautogui.typewrite('scmlink', .2)
        pyautogui.press('enter')
        if appears("mlink_manager"):
            if appears("mlink_DWG", 10):
                mouseLeftClickOn("mlink_DWG")
            else:
                mouseLeftClickOn("mlink_DWG_dropdown")
                sleep(2)
                mouseLeftClickOn("mlink_Attach_DWG")
            if appears("mlink_DWG_window", 10):
                mouseLeftClickOn("mlink_structure")
                pyautogui.press('right')
                mouseLeftClickOn("mlink_deck")
                pyautogui.press('right')
                if appears("u01_tt_1400_mlink"):
                    mouseLeftClickOn("u01_tt_1400_mlink")
                    pyautogui.press('space')
                else:
                    raise AssertionError("u01_tt_1400_mlink did not appear")
                pyautogui.hotkey('win','up')
                mouseLeftClickOn("mlink_ok")
                sleep(10)
                if appears("mlink_manager",20):
                    mouseLeftClickOn("mlink_manager")
                elif appears("mlink_manager_high",20):
                    mouseLeftClickOn("mlink_manager_high")
                # if appears("mlink_cross_region1",20):
                #     mouseLeftClickOn("mlink_cross1","mlink_cross_region1")
                # elif appears("mlink_cross_region",20):
                #     mouseLeftClickOn("mlink_cross","mlink_cross_region")
                #     pyautogui.click(clicks=2)
                sleep(4)
                pyautogui.hotkey('alt','f4')
            else:
                raise AssertionError("mlink DWG window didn't appear.")
        sleep(25)
        change_viewpoint_from_fwd_stbd_up()
        free()
        pyautogui.moveTo(400,400)
        conceptual_visual()
        pyautogui.moveTo(400,400)
        pyautogui.click()
        pyautogui.press('esc',presses=3)
        self.insert_from_library()
        if appears("select_equipment_window"):
            mouseLeftClickOn("enter_filter_text")
            pyautogui.typewrite("test", .2)
            pyautogui.press('enter')
        else:
            raise AssertionError("select_equipment_window did not appear")
        if appears("test_equipment_on_list"):
            mouseDoubleClickOn("test_equipment_on_list")
        else:
            raise AssertionError("test_equipment_on_list did not appear")
        sleep(10)
        if appears("select_hierarchy2"):
            mouseLeftClickOn("select_hier_dropdown")
            pyautogui.click(clicks=10)
        else:
            raise AssertionError("select_hierarchy dialog did not appear")
        if appears("piping_hierarchy"):
            mouseLeftClickOn("piping_hierarchy")
            pyautogui.press('enter')
            sleep(30)
            pyautogui.click(button='right')
            if appears("custom_insertion", 5):
                mouseLeftClickOn("custom_insertion")
            else:
                pyautogui.press('esc')
            sleep(1)
            pyautogui.click(button='right')
            if appears("next_point", 5):
                mouseLeftClickOn("next_point")
            else:
                raise AssertionError("next_point did not appear")
            sleep(1)
            pyautogui.click(button='right')
            if appears("end_mode", 5):
                mouseLeftClickOn("end_mode")
            else:
                raise AssertionError("end_mode did not appear")
            sleep(1)
            pyautogui.click(button='right')
            if appears("next_end", 5):
                mouseLeftClickOn("next_end")
            else:
                raise AssertionError("next_end did not appear")
            sleep(1)
            pyautogui.click(button='right')
            if appears("custom_insertion", 5):
                mouseLeftClickOn("custom_insertion")
            else:
                raise AssertionError("custom_insertion point did not appear")
            pyautogui.moveTo(400,200)
            if appears("please_pick_the_first_point"):
                pyautogui.typewrite("39619,0,1400", .2)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.moveTo(995, 566)
                pyautogui.click()
                sleep(2)
                pyautogui.moveTo(264, 516)
                pyautogui.click()
                sleep(4)
                pyautogui.typewrite('select', .1)
                pyautogui.press('enter')
                pyautogui.typewrite('39616,500,1400', .2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                pyautogui.click(button='right')
                if appears("change_insulation"):
                    mouseLeftClickOn("change_insulation")
                else:
                    raise AssertionError("change_insulation did not appear")
                sleep(3)
                pyautogui.hotkey('win', 'up')
                if appears("50mm_mineral"):
                    mouseLeftClickOn("50mm_mineral")
                    mouseLeftClickOn("select_finish_window_right")
                    mouseLeftClickOn("insulation_window_ok")
                    sleep(4)
                    pyautogui.moveTo(400, 400)
                    sleep(2)
                    pyautogui.typewrite('select', .1)
                    pyautogui.press('enter')
                    pyautogui.typewrite('39616,500,1400', .2)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    pyautogui.click(button='right')
                    if appears("properties_option"):
                        mouseLeftClickOn("properties_option")
                        sleep(5)
                        pyautogui.moveTo(31, 433)
                        pyautogui.click()
                        pyautogui.click()
                        # mouseDoubleClickOn("data_layer")
                        # mouseDoubleClickOn("stock_attributes_layer")
                        # mouseDoubleClickOn("spool_information_layer")
                        if appears("tag_number_layer", 30):
                            mouseLeftClickOn("tag_number_layer")
                            pyautogui.typewrite('123', .2)
                            pyautogui.press('enter')
                            mouseLeftClickOn("properties_close_button")
                        else:
                            raise AssertionError("tag_number on properties did not appear")
                    else:
                        raise AssertionError("properties_option did not appear")
                    pyautogui.moveTo(500, 500)
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    pyautogui.press('esc')
                    pyautogui.typewrite('scmlink', .2)
                    pyautogui.press('enter')
                    if appears("mlink_manager"):
                        if appears("mlink_DWG", 10):
                            mouseLeftClickOn("mlink_DWG")
                        else:
                            mouseLeftClickOn("mlink_DWG_dropdown")
                            sleep(2)
                            mouseLeftClickOn("mlink_Attach_DWG")
                        if appears("mlink_DWG_window", 10):
                            mouseLeftClickOn("pipe_mlink1")
                            pyautogui.press('right')
                            pyautogui.press('down', presses=11)
                            pyautogui.press('space')
                            mouseLeftClickOn("mlink_ok")
                            sleep(5)
                            if appears("mlink_manager", 20):
                                mouseLeftClickOn("mlink_manager")
                            elif appears("mlink_manager_high", 20):
                                mouseLeftClickOn("mlink_manager_high")
                            # if appears("mlink_cross_region1", 20):
                            #     mouseLeftClickOn("mlink_cross1", "mlink_cross_region1")
                            # elif appears("mlink_cross_region", 20):
                            #     mouseLeftClickOn("mlink_cross", "mlink_cross_region")
                            #     pyautogui.click(clicks=2)
                            sleep(4)
                            pyautogui.hotkey('alt', 'f4')
                        else:
                            raise AssertionError("mlink DWG window didn't appear.")
                        sleep(30)
                        reset_viewport()
                    else:
                        raise AssertionError("mlink_manager did not appear")
                    self.insert_from_library()
                    if appears("select_equipment_window"):
                        mouseLeftClickOn("enter_filter_text")
                        pyautogui.typewrite("scupper", .2)
                        pyautogui.press('enter')
                        if appears("scupper_b_si_002"):
                            mouseDoubleClickOn("scupper_b_si_002")
                        else:
                            raise AssertionError("test_equipment_on_list did not appear")
                    else:
                        raise AssertionError("Select equipment window did not appear")
                    sleep(10)
                    if appears("select_hierarchy2"):
                        mouseLeftClickOn("select_hier_dropdown")
                        pyautogui.click(clicks=10)
                        if appears("piping_hierarchy"):
                            mouseLeftClickOn("piping_hierarchy")
                            pyautogui.press('enter')
                            sleep(15)
                            pyautogui.click(button='right')
                        if appears("custom_insertion", 5):
                            mouseLeftClickOn("custom_insertion")
                        else:
                            pyautogui.press('esc')
                        sleep(3)
                        pyautogui.click(button='right')
                        if appears("end_mode", 5):
                            mouseLeftClickOn("end_mode")
                        if appears("please_pick_the_first_point"):
                            pyautogui.typewrite("41844,-4500,6617.4679", .2)
                            pyautogui.press('enter')
                            sleep(1)
                            pyautogui.moveTo(566, 151)
                            pyautogui.click()
                            sleep(2)
                            pyautogui.click()
                        else:
                            raise AssertionError(
                                "please_pick_the_first_point did not appear during SCUPPER creation")
                    else:
                        raise AssertionError("Select hierarchy did not appear during SCUPPER creation")
                else:
                    raise AssertionError("50mm_mineral did not appear")
            else:
                raise AssertionError("please_pick_the_first_point did not appear")
        else:
            raise AssertionError("piping_hierarchy did not appear")
        self.logger.info('End:\t Checking Module- inserting_equipment_into_model')

    # Modifying Equipment
    def modifying_equipment(self):
        self.logger.info('Start:\t Checking Module- modifying_equipment')
        open_manager()
        if appears("equip_lib_button", 10):
            print("Equipment Library button appeared in the manager toolbar")
            self.open_equipment_library()
            if appears("edit_std_drawing"):
                mouseLeftClickOn("edit_std_drawing")
                if appears("save_yes_1", 15):
                    mouseLeftClickOn("save_yes_1")
                sleep(30)
                free()
                reset_viewport()
            else:
                raise AssertionError("edit_std_drawing did not appear")
        else:
            raise AssertionError("equip_lib_button did not appear")
        mouseLeftClickOn("home_button")
        mouseLeftClickOn("layer_button")
        if appears("current_layer_dropdown"):
            mouseLeftClickOn("current_layer_dropdown")
            if appears("0_layer"):
                mouseLeftClickOn("0_layer")
                free()
                pyautogui.press('esc')
                pyautogui.press('esc')
            else:
                raise AssertionError("maintenance_layer did not appear")
        else:
            raise AssertionError("current_layer_dropdown did not appear")
        change_viewpoint_from_fwd_prt_up()
        sleep(4)
        pyautogui.typewrite("line", .2)
        pyautogui.press('enter')
        pyautogui.typewrite("496.2624,503.7099,0", .2)
        pyautogui.press('enter')
        pyautogui.typewrite("#-503.7237,0.9861,0", .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        reset_viewport()
        pyautogui.hotkey('ctrl','9')
        pyautogui.press('enter')
        mouseLeftClickOn("3d_tool_panel")
        if appears("coordinates"):
            mouseLeftClickOn("coordinates")
            if appears("3_point"):
                mouseLeftClickOn("3_point")
                free()
                if appears("specify_new_origin_point"):
                    sleep(4)
                    pyautogui.moveTo(1038, 787)
                    pyautogui.click()
                    sleep(1)
                    pyautogui.moveTo(1223, 680)
                    pyautogui.click()
                    sleep(1)
                    pyautogui.moveTo(1036, 569)
                    pyautogui.click()
                    sleep(4)
                    pyautogui.hotkey('ctrl','9')
                    pyautogui.press('enter')
                    free()
                else:
                    raise AssertionError("specify_new_origin_point did not appear")
            else:
                raise AssertionError("3_point did not appear")
        else:
            raise AssertionError("coordinates pane did not appear")
        sleep(4)
        pyautogui.typewrite('circle', .2)
        pyautogui.press('enter')
        pyautogui.typewrite('501.8512,0,-247.6312', .2)
        pyautogui.press('enter')
        if appears("specify_radius"):
            pyautogui.typewrite("150", .2)
            pyautogui.press('enter')
        else:
            raise AssertionError("specify_radius did not appear")
        pyautogui.typewrite("EXTRUDE", .2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite("650,0,-250", .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(1)
        pyautogui.moveTo(1217, 588)
        pyautogui.typewrite('300', .2)
        pyautogui.press('enter')
        mouseLeftClickOn("close_drawing")
        if appears("stock_usage_warning"):
            mouseLeftClickOn("save_changes")
            sleep(10)
        else:
            raise AssertionError("stock_usage_warning did not appear")
        if appears("shipcon_manager"):
            print("SC Manager was opened.")
        else:
            open_manager()
        if appears("sc_manager_maximized", 10):
            print("Equipment library appeared")
        else:
            pyautogui.moveTo(500,500)
            sleep(2)
            pyautogui.click()
            pyautogui.press("esc")
            pyautogui.press("esc")

            pyautogui.typewrite("SCCATALOG",.1)
            pyautogui.press("enter")
        if appears("equipment_stock_ok",10):
            mouseLeftClickOn("equipment_stock_ok")
        else:
            raise AssertionError("equipment_stock_ok button did not appear")
        sleep(2)
        if appears("save_yes", 15):
            mouseLeftClickOn("save_yes")
        pyautogui.hotkey('alt', 'f4')
        if appears("save_yes", 15):
            mouseLeftClickOn("save_yes")
        if appears("test_equipment_tab"):
            print("test_equipment_tab appeared")
            reset_viewport()
            reload_drawing()
            sleep(40)
            mouseLeftClickOn("pipe_ribbon")
        else:
            raise AssertionError("test_equipment_tab did not appear")
        mouseLeftClickOn("pipe_modeling_util")
        if appears("pipe_transfer_parts"):
            mouseLeftClickOn("pipe_transfer_parts")
            free()
            pyautogui.moveTo(400, 400)
            if appears("select_parts_to_move"):
                pyautogui.typewrite("41844,-4500,6617.4679", .2)
                pyautogui.press('enter')
                pyautogui.press('enter')
                if appears("reload_yes"):
                    mouseLeftClickOn("reload_yes")
                else:
                    raise AssertionError("SCUPPER could not be selected to transfer")
                if appears("select_single"):
                    mouseLeftClickOn("select_single_pipe")
                    pyautogui.press('right')
                    pyautogui.press('down', presses=11)
                    mouseLeftClickOn("select_single_ok")
                    sleep(30)
                else:
                    raise AssertionError("select single drawing dialog did not appear")
            else:
                raise AssertionError("select_parts_to_move did not appear")
        else:
            raise AssertionError("pipe_transfer_parts did not appear")
        pyautogui.typewrite("SCNAVIGATE", .2)
        pyautogui.press("enter")
        if appears("sc_navigator_window"):
            mouseLeftClickOn("pipe_node")
            if appears("672_U01_Black"):
                mouseDoubleClickOn("672_U01_Black")
                if appears("save_yes", 15):
                    mouseLeftClickOn("save_yes")
                sleep(10)
                if appears("672_U01_Black_tab"):
                    print("672_U01_Black_tab appeared")
                    free()
                    reset_viewport()
                    pyautogui.hotkey('ctrl', 's')
                    sleep(10)
                else:
                    raise AssertionError("672_U01_Black_tab did not appear")
            else:
                raise AssertionError("672_U01_Black did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")
        if appears("3d_tool_panel", 10):
            if appears("pane_for_right_click", 10):
                mouseRightClickOn("pane_for_right_click")
            else:
                raise AssertionError("pane_for_right_click did not appear")
            if appears("show_tabs", 10):
                mouseLeftClickOn("show_tabs")
                if appears("3d_tool_selected", 10):
                    mouseLeftClickOn("3d_tool_selected")
                else:
                    raise AssertionError("3d_tool option did not appear")
            else:
                raise AssertionError("show_tabs did not appear")
        pyautogui.press('esc',presses=4)
        self.logger.info('End:\t Checking Module- moSCRIBBdifying_equipment')

    # Equipment Accessory User Defined Attributes
    def equipment_accessory_uda(self):
        self.logger.info('Start:\t Checking Module- equipment_accessory_uda')
        open_manager()
        self.open_equipment_library()
        mouseLeftClickOn("edit_stock")
        sleep(3)
        if appears("accessory_packages"):
            mouseLeftClickOn("accessory_packages")
            mouseLeftClickOn("accessory_pkj_add")
            if appears("select_accessory_pkj_window2"):
                pyautogui.press('left')
                pyautogui.press('space')
                sleep(2)
                # mouseLeftClickOn("select_accessory_pkj_window_ok")
                # mouseLeftClickOn("select_accessory_pkj_window_ok2")
            else:
                raise AssertionError("select_accessory_pkj_window2 did not appear")
        else:
            raise AssertionError("accessory_packages tab did not appear")

        if appears("select_accessory_pkj_window_ok",20):
            mouseLeftClickOn("select_accessory_pkj_window_ok")
        else:
            mouseLeftClickOn("select_accessory_pkj_window_ok2")

        mouseLeftClickOn("edit_equipment_stock_window_done")
        sleep(2)
        pyautogui.moveTo(500, 500)
        sleep(2)
        mouseLeftClickOn("equipment_stock_ok")
        mouseLeftClickOn("general_button")
        mouseLeftClickOn("accessory_button")
        mouseLeftClickOn("user_defined_attribute_option")
        if appears("subscription_advantage_pack"):
            pyautogui.press('enter')
        sleep(2)
        if appears("user_defined_attribute_window", 10):
            self.logger.info("user_defined_attribute_window appeared")
            if appears("user_defined_attribute_window_maximize", 10):
                mouseLeftClickOn("user_defined_attribute_window_maximize")
        elif appears("user_defined_attribute_window_maximized", 10):
            self.logger.info("user_defined_attribute_window appeared")
        elif appears("user_defined_attribute_window_maximized", 10):
            self.logger.info("user_defined_attribute_window appeared")
        else:
            raise AssertionError("user_defined_attribute_window did not appear")
        if appears("uda_drop_down"):
            mouseLeftClickOn("uda_drop_down")
            if appears("new_string_list_attribute"):
                mouseLeftClickOn("new_string_list_attribute")
                sleep(2)
                pyautogui.typewrite("Detail Level",.1)
                pyautogui.press('enter')
                mouseLeftClickOn("plus_button_blue_shaded_2")
                mouseDoubleClickOn("value_item")
                pyautogui.typewrite("High",.1)
                pyautogui.press('enter')
                mouseLeftClickOn("add_list_button")
                mouseDoubleClickOn("high_1")
                pyautogui.hotkey('ctrl','a')
                pyautogui.typewrite("Medium",.1)
                pyautogui.press('enter')
                mouseLeftClickOn("add_list_button")
                mouseDoubleClickOn("medium_1")
                pyautogui.hotkey('ctrl','a')
                pyautogui.typewrite("Low",.1)
                pyautogui.press('enter')
            else:
                raise AssertionError("new_string_list_attribute did not appear")
        else:
            raise AssertionError("uda_drop_down did not appear")
        if appears("uda_drop_down"):
            mouseLeftClickOn("uda_drop_down")
            if appears("new_numeric_attribute"):
                mouseLeftClickOn("new_numeric_attribute")
                sleep(2)
                pyautogui.typewrite("Cost",.1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.keyDown('ctrl')
                mouseLeftClickOn("detail_level")
                pyautogui.keyUp('ctrl')
                if appears("edit_assignment_2"):
                    mouseLeftClickOn("edit_assignment_2")
                else:
                    raise AssertionError("edit_assignment did not appear")
            else:
                raise AssertionError("new_numeric_attribute did not appear")
        else:
            raise AssertionError("uda_drop_down did not appear")
        sleep(3)
        pyautogui.hotkey("win","up")

        if appears("edit_user_attribute",15):
            if appears("equipment_part_accessories2",5):
                mouseLeftClickOn("equipment_part_accessories2")
            else:
                raise AssertionError("equipment_part_accessories did not appear")
            if appears("equipment_part_accessories_checkbox",10):
                mouseLeftClickOn("equipment_part_accessories_checkbox")
                mouseLeftClickOn("edit_user_attribute_ok1")
            else:
                raise AssertionError("pipe_connection_accessories_checkbox2 did not appear")
        else:
            raise AssertionError("edit_user_attribute window did not appear")

        if appears("equipment_part_accessories_on_list_1"):
            mouseLeftClickOn("equipment_part_accessories_on_list_1")
            mouseDoubleClickOn("not_required_blue_shaded_1")
            pyautogui.press('down')
            pyautogui.press('enter')
            mouseLeftClickOn("equipment_part_accessories_on_list_1")
            mouseDoubleClickOn("not_required_blue_shaded_2")
            pyautogui.press('down')
            pyautogui.press('enter')
        else:
            raise AssertionError("equipment_part_accessories_on_list did not appear")
        mouseLeftClickOn("uda_ok")
        sleep(2)
        pyautogui.moveTo(400,400)
        sleep(2)
        mouseLeftClickOn("equipment_stock_ok")
        sleep(10)
        pyautogui.hotkey('alt', 'f4')
        sleep(2)
        mouseLeftClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new")
        if appears("sc_navigator_window"):
            mouseLeftClickOn("equipment_node_new")
            if appears("test_equipment_on_list_highlighted"):
                mouseDoubleClickOn("test_equipment_on_list_highlighted")
                if appears("save_yes", 5):
                    mouseLeftClickOn("save_yes")
                if appears("test_equipment_tab"):
                    print("test_equipment_tab appeared")
                else:
                    raise AssertionError("test_equipment_tab did not appear")
            else:
                raise AssertionError("test_equipment_on_list_highlighted did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")
        sleep(4)
        sleep(4)
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        pyautogui.typewrite('39616,500,1400', .2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.click(button='right')
        if appears("properties_option"):
            mouseLeftClickOn("properties_option")
            sleep(5)
            pyautogui.moveTo(31, 433)
            pyautogui.click()
            pyautogui.click()
            if appears("accessory_package_prod",10):
                mouseLeftClickOn("accessory_package_prod")
                sleep(1)
                pyautogui.press('enter')
                if appears("select_accessory_pkj_window"):
                    pyautogui.press('left')
                    pyautogui.press('space')
                    mouseLeftClickOn("select_accessory_pkj_window_ok")
                    mouseLeftClickOn("select_accessory_pkj_window_ok2")
                    if appears("properties_close_button", 5):
                        mouseLeftClickOn("properties_close_button")
                        pyautogui.moveTo(500, 500)
                    elif appears("layer_properties_manager_close_unhighlighted_button", 5):
                        mouseLeftClickOn("layer_properties_manager_close_unhighlighted_button")
                        pyautogui.moveTo(500, 500)
                    sleep(2)
                    pyautogui.press('esc')
                    reload_drawing()
                    sleep(20)
                    pyautogui.moveTo(500,500)
                else:
                    raise AssertionError("select_accessory_pkj_window did not appear")
            else:
                raise AssertionError("accessory_package_prod did not appear")
        else:
            raise AssertionError("properties_option did not appear")
        mouseLeftClickOn("advantage_pack_ribbon")
        if appears("uda_editor_pane"):
            mouseLeftClickOn("uda_editor_pane")
            mouseLeftClickOn("uda_edit_drawing_accessories_button")
            pyautogui.moveTo(500, 500)
            sleep(2)
            if appears("subscription_advantage_pack"):
                pyautogui.press('enter')
                if appears("uda_editor_unhigh", 10):
                    print("uda_editor palette appeared")
                elif appears("uda_editor_high", 10):
                    print("uda_editor palette appeared")
                else:
                    raise AssertionError("uda_editor did not appear")
            else:
                raise AssertionError("subscription_advantage_pack dialog did not appear")
        else:
            raise AssertionError("uda_editor_pane did not appear")
        pyautogui.moveTo(500,500)
        sleep(1)
        if appears("test_equipment_001_on_palette",10):
            mouseLeftClickOn("test_equipment_001_on_palette")
        else:
            raise AssertionError("test_equipment_001_on_palette did not appear")

        pyautogui.press('right', presses=6, interval=.1)
        pyautogui.press('1')
        sleep(1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('2')
        pyautogui.press('enter')
        sleep(1)
        pyautogui.press('right')
        pyautogui.press('up')
        sleep(1)

        pyautogui.press('space')
        pyautogui.press('up')
        sleep(1)
        pyautogui.press('space')
        pyautogui.press('up')
        sleep(1)
        pyautogui.press('space')
        pyautogui.press('up')
        sleep(1)
        pyautogui.press('down')
        pyautogui.press('space')
        pyautogui.press('up')
        sleep(1)
        mouseLeftClickOn("save_uda_attribute")
        sleep(5)
        mouseLeftClickOn("properties_close_button")

        pyautogui.moveTo(500, 500)
        sleep(1)
        reload_drawing()
        sleep(20)
        pyautogui.moveTo(500, 500)
        sleep(1)

        pyautogui.moveTo(400, 400)
        sleep(2)
        pyautogui.moveTo(500, 400)
        sleep(2)
        pyautogui.typewrite('select', .1)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('39616,500,1400', .2)
        pyautogui.press('enter',2)
        sleep(1)
        pyautogui.click(button='right')
        if appears("properties_option"):
            mouseLeftClickOn("properties_option")
            sleep(3)
            pyautogui.moveTo(33, 477)
            sleep(1)
            pyautogui.click()
            if appears("updated_value"):
                print("Attributes are updated")
            else:
                raise AssertionError("updated_value did not appear")
            if appears("properties_close_button", 5):
                mouseLeftClickOn("properties_close_button")
                pyautogui.moveTo(500, 500)
            elif appears("layer_properties_manager_close_unhighlighted_button", 5):
                mouseLeftClickOn("layer_properties_manager_close_unhighlighted_button")
                pyautogui.moveTo(500, 500)
            sleep(5)
            pyautogui.hotkey('ctrl','s')
            sleep(10)
        else:
            raise AssertionError("properties_option did not appear")
        self.logger.info('End:\t Checking Module- equipment_accessory_uda')

    # Reporting values of Accessory User Defined Attributes
    def reporting_values_of_accessory_uda(self):
        self.logger.info('Start:\t Checking Module- reporting_values_of_accessory_uda')
        pyautogui.moveTo(700,400)
        sleep(2)
        pyautogui.typewrite("SCNAVIGATE", .2)
        pyautogui.press("enter")
        if appears("sc_navigator_window"):
            mouseLeftClickOn("arrangement_node")
            mouseLeftClickOn("new_arrangement")
            if appears("create_arrangement_window"):
                sleep(2)
                pyautogui.typewrite("UDA_Test",.1)
                pyautogui.press('enter')
                sleep(2)
                pyautogui.press('enter')
                sleep(1)
                mouseLeftClickOn("create_arrangement_window_structure")
                pyautogui.press('right')
                pyautogui.press('right')
                pyautogui.press('down',presses=3)
                pyautogui.press('right')
                pyautogui.press('down',presses=5)
                pyautogui.press('left')
                pyautogui.press('space')
                pyautogui.press('down',presses=6)
                pyautogui.press('right')
                pyautogui.press('right')
                pyautogui.press('down',presses=6)
                pyautogui.press('left')
                pyautogui.press('space')
                mouseLeftClickOn("create_arrangement_window_next")
                sleep(1)
                pyautogui.click()
                waitForVanish("working_dialog")
                sleep(5)
                if appears("uda_test_tab"):
                    print("uda_test_tab appeared")
                    obj = prodDoc()
                    obj.open_BOM_manager()
                    if appears("expand_dark_blue_region", 5):
                        mouseLeftClickOn("expand_dark_blue_region")
                    elif appears("expand_dark_blue_region2", 5):
                        mouseLeftClickOn("expand_dark_blue_region2")
                    else:
                        mouseLeftClickOn("expand_blue_region")
                    sleep(4)
                    if appears("equipment_arrangement_bom"):
                        mouseLeftClickOn("equipment_arrangement_bom")
                        mouseLeftClickOn("add_remove_button")
                        if appears("choose_bom_fields_window"):
                            if appears("user_attribute_tab", 10):
                                mouseLeftClickOn("user_attribute_tab")
                            mouseLeftClickOn("cost_field")
                            pyautogui.press('left')
                            pyautogui.press('space')
                            mouseLeftClickOn("detail_level_attribute")
                            pyautogui.press('left')
                            pyautogui.press('space')
                            mouseLeftClickOn("choose_bom_fields_window_ok")
                            pyautogui.moveTo(700, 400)
                            sleep(1)
                            mouseLeftClickOn("bom_definition_apply")
                            pyautogui.moveTo(700, 400)
                            sleep(1)
                            mouseLeftClickOn("bom_definition_ok")
                            sleep(5)
                            pyautogui.hotkey('alt', 'f4')
                            sleep(1)
                            obj.update_BOM_table()
                            free_white()
                        else:
                            raise AssertionError("choose_bom_fields_window did not appear")
                    else:
                        raise AssertionError("equipment_arrangement_bom did not appear")
                else:
                    raise AssertionError("uda_test_tab did not appear")
            else:
                raise AssertionError("create_arrangement_window did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")
        mouseLeftClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("manage_pane")
        if appears("report_option"):
            mouseLeftClickOn("report_option")
            sleep(20)
            pyautogui.hotkey('win', 'up')
            sleep(1)
            if appears("sc_report_window"):
                print("sc_report_window appeared")
                mouseLeftClickOn("reports_option")
                mouseLeftClickOn("report_definition")
            else:
                raise AssertionError("sc_report_window did not appear")
            if appears("reports_definition_window"):
                mouseMoveTo("reports_definition_window_scroll_down")
                while exists("model_drawing_report") == False:
                    pyautogui.click(clicks=2)
            else:
                raise AssertionError("reports_definition_window did not appear")
            if appears("model_drawing_report"):
                if appears("model_drawing_collapsed", 5):
                    mouseLeftClickOn("model_drawing_collapsed_region", "model_drawing_collapsed")
                mouseLeftClickOn("part_list_report_window")
                mouseLeftClickOn("new_report_button")
                pyautogui.typewrite("Equipment Accessories", .2)
                pyautogui.press('enter')
                pyautogui.press('up')
                pyautogui.press('right', presses=2)
                pyautogui.press('a', presses=2)
                pyautogui.press('right')
                pyautogui.press('p')
                mouseLeftClickOn("edit_report_button")
                if appears("edit_report_window"):
                    mouseMoveTo("reports_definition_window_scroll_down")
                    while exists("equipment_accessories") == False:
                        pyautogui.click(clicks=2)
                    if appears("equipment_accessories"):
                        mouseLeftClickOn("equipment_accessories")
                        sleep(1)
                        pyautogui.press('left')
                        pyautogui.press('space')
                        sleep(1)
                        mouseLeftClickOn("add_field_button")
                        sleep(3)
                        pyautogui.hotkey('win', 'up')
                        if appears("select_field_window"):
                            mouseLeftClickOn("production_fields")
                            mouseLeftClickOn("model_dwg")
                            pyautogui.press('left')
                            pyautogui.press('space')
                            pyautogui.press('down', presses=5)
                            pyautogui.press('space')
                            pyautogui.press('down', presses=4)
                            pyautogui.press('space')
                            mouseLeftClickOn("attribute_fields")
                            mouseLeftClickOn("cost_attribute")
                            pyautogui.press('left')
                            pyautogui.press('space')
                            pyautogui.press('down', presses=3)
                            pyautogui.press('space')
                            mouseLeftClickOn("select_field_window_ok")
                        else:
                            raise AssertionError("select_field_window did not appear")
                        if appears("model_dwg_field"):
                            mouseLeftClickOn("model_dwg_field")
                            pyautogui.press('right')
                            pyautogui.typewrite('60', .1)
                            pyautogui.press('enter', presses=3)
                            pyautogui.typewrite('50', .1)
                            pyautogui.press('enter')
                            mouseLeftClickOn("edit_report_window_ok")
                            sleep(5)
                            mouseLeftClickOn("reports_definition_window_ok")
                            sleep(5)
                        else:
                            raise AssertionError("model_dwg_field did not appear on Edit Report window")
                        if appears("model_drawing_tab"):
                            mouseLeftClickOn("model_drawing_tab")
                            mouseLeftClickOn("model_drawings")
                            pyautogui.press('right')
                            mouseLeftClickOn("units")
                            pyautogui.press('right')
                            if appears("md_u01", 5):
                                mouseLeftClickOn("md_u01")
                            pyautogui.press('right')
                            pyautogui.press('right')
                            if appears("md_equipment"):
                                mouseLeftClickOn("md_equipment")
                            else:
                                raise AssertionError("md_equipment did not appear")
                            pyautogui.press('right')
                            if appears("md_test_equipment"):
                                mouseLeftClickOn("md_test_equipment")
                                pyautogui.press('space')
                            else:
                                raise AssertionError("md_test_equipment did not appear")
                        else:
                            raise AssertionError("model_drawing_tab did not appear")
                    else:
                        raise AssertionError("equipment_accessories did not appear")
                else:
                    raise AssertionError("edit_report_window did not appear")
            else:
                raise AssertionError("model_drawing_report did not appear")
        else:
            raise AssertionError("report_option did not appear")
        if appears("sc_report_window_pull_down"):
            mouseLeftClickOn("sc_report_window_pull_down")
            if appears("model_drawing_equipment_accessories"):
                mouseLeftClickOn("model_drawing_equipment_accessories")
                if appears("generate_report_button"):
                    mouseLeftClickOn("generate_report_button")
                    if appears("report_text1"):
                        mouseMoveTo("report_text1")
                        sleep(2)
                        pyautogui.hotkey('alt','f4')
                        sleep(5)
                    else:
                        raise AssertionError("Report was not generated successfully")
                else:
                    raise AssertionError("generate_report_button did not appear")
            else:
                raise AssertionError("model_drawing_equipment_accessories did not appear")
        else:
            raise AssertionError("sc_report_window_pull_down did not appear")
        self.logger.info('End:\t Checking Module- reporting_values_of_accessory_uda')

    # Project Equipment List (PEL)
    # Managing PEL
    def managing_pel(self):
        self.logger.info('Start:\t Checking Module- managing_pel')
        free_white()
        pyautogui.hotkey('ctrl','s')
        sleep(7)
        pyautogui.typewrite("SCNAVIGATE", .2)
        pyautogui.press("enter")
        if appears("sc_navigator_window"):
            if appears("test_equipment_node", 5):
                mouseDoubleClickOn("test_equipment_node")
            elif appears("test_equipment_on_list_highlighted", 5):
                mouseDoubleClickOn("test_equipment_on_list_highlighted")
            if appears("test_equipment_tab"):
                sleep(10)
                sleep(10)
                sleep(10)
                pyautogui.moveTo(700, 600)
                pyautogui.typewrite('scmlink', .2)
                pyautogui.press('enter')
                if appears("mlink_manager"):
                    if appears("mlink_DWG", 10):
                        mouseLeftClickOn("mlink_DWG")
                    else:
                        mouseLeftClickOn("mlink_DWG_dropdown")
                        sleep(2)
                        mouseLeftClickOn("mlink_Attach_DWG")
                    if appears("mlink_DWG_window", 10):
                        # if appears("mlink_up_arrow_region2"):
                        #     goDownUntilAppears("mlink_units", "mlink_up_arrow_region2")
                        mouseLeftClickOn("mlink_units")
                        pyautogui.press('space')
                        sleep(2)
                        pyautogui.press('space')
                        mouseLeftClickOn("mlink_ok")
                        sleep(5)
                        if appears("mlink_manager", 20):
                            mouseLeftClickOn("mlink_manager")
                        elif appears("mlink_manager_high", 20):
                            mouseLeftClickOn("mlink_manager_high")
                        # if appears("mlink_cross_region1", 20):
                        #     mouseLeftClickOn("mlink_cross1", "mlink_cross_region1")
                        # elif appears("mlink_cross_region", 20):
                        #     mouseLeftClickOn("mlink_cross", "mlink_cross_region")
                        #     pyautogui.click(clicks=2)
                        sleep(5)
                        pyautogui.hotkey('alt', 'f4')
                    wireframe2d_visual()
                else:
                    raise AssertionError("Mlink manager did not appear")
                mouseLeftClickOn("multi_discipline")
                mouseLeftClickOn("equipment_panel")
                if appears("equipment_list_button"):
                    mouseLeftClickOn("equipment_list_button")
                    if appears("project_equipment_list_window"):
                        print("project_equipment_list_window appeared")
                    else:
                        raise AssertionError("project_equipment_list_window did not appear")
                else:
                    raise AssertionError("equipment_list_button did not appear")
            else:
                raise AssertionError("test_equipment_tab did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")
        self.logger.info('End:\t Checking Module- managing_pel')

    # Creating a New PEL Part
    def creating_new_pel_part(self):
        self.logger.info('Start:\t Checking Module- creating_new_pel_part')
        mouseLeftClickOn("new_part_button")
        pyautogui.typewrite("Test equipment", .1)
        pyautogui.press('enter')
        pyautogui.press('right')
        mouseLeftClickOn("stock_browser")
        sleep(4)
        pyautogui.hotkey('win', 'up')
        if appears("select_an_equipment_stock_window"):
            print("select_an_equipment_stock window appeared")
            mouseDoubleClickOn("tab_resizer2")
            mouseDoubleClickOn("tab_resizer3")
            mouseLeftClickOn("enter_filter_text")
            pyautogui.typewrite("test", .1)
            pyautogui.press('enter')
            if appears("test_equip_list"):
                mouseLeftClickOn("test_equip_list")
                mouseLeftClickOn("select_an_equipment_stock_window_ok")
                sleep(2)
                mouseLeftClickOn("project_equipment_list_window_ok")
                sleep(5)
            else:
                raise AssertionError("test_equip_list did not appear")
        else:
            raise AssertionError("select_an_equipment_stock window did not appear")
        self.logger.info('End:\t Checking Module- creating_new_pel_part')

    # Associating/ Disassociating Equipment to/from PEL Items
    def associating_equipment_to_pel(self):
        self.logger.info('Start:\t Checking Module- associating_equipment_to_pel')
        mouseLeftClickOn("equipment_panel")
        mouseMoveTo("diassociate_from_pel_selected")
        mouseLeftClickOn("hovered_drop_down")
        mouseLeftClickOn("associate_equipment_with_pel_onlist")
        pyautogui.moveTo(400,500)
        if appears("select_an_equipment_part"):
            pyautogui.typewrite('39616,500,1400', .2)
            pyautogui.press('enter')
            if appears("project_equipment_association_window"):
                mouseLeftClickOn("project_equipment_association_window_ok")
                if appears("Test_Equipment_001_associated",10):
                    print("Test Equipment-001 part was associated with the Test Equipment PEL item")
            else:
                raise AssertionError("project_equipment_association_window did not appear")
        else:
            raise AssertionError("select_an_equipment_part did not appear")
        self.logger.info('End:\t Checking Module- associating_equipment_to_pel')

    def diassociating_equipment_from_pel(self):
        self.logger.info('Start:\t Checking Module- diassociating_equipment_from_pel')
        mouseLeftClickOn("equipment_panel")
        mouseMoveTo("associate_from_pel_selected")
        mouseLeftClickOn("hovered_drop_down")
        mouseLeftClickOn("diassociate_equipment_with_pel_onlist")
        pyautogui.moveTo(400, 500)
        if appears("select_equipment_parts"):
            pyautogui.typewrite('39616,500,1400', .2)
            pyautogui.press('enter')
            pyautogui.press('enter')
        else:
            raise AssertionError("select_equipment_parts did not appear")
        self.logger.info('End:\t Checking Module- diassociating_equipment_from_pel')

    # Creating PEL Items from Inserted Equipment
    def creating_PEL_Items_from_Inserted_Equipment(self):
        self.logger.info('Start:\t Checking Module- creating_PEL_Items_from_Inserted_Equipment')
        sleep(2)
        mouseLeftClickOn("equipment_panel")
        if appears("create_pel_item"):
            mouseLeftClickOn("create_pel_item")
            pyautogui.moveTo(400,400)
            if appears("select_equipment_parts"):
                pyautogui.typewrite('39616,500,1400', .2)
                pyautogui.press('enter')
                sleep(5)
                pyautogui.press('enter')
                sleep(2)
                if appears("generate_complete_dialog"):
                    print("PEL item was created")
                    mouseLeftClickOn("generate_complete_dialog_close")
                else:
                    raise AssertionError("generate_complete_dialog did not appear. PEL was not created")
                pyautogui.moveTo(400,400)
                pyautogui.hotkey('ctrl','c')
                if appears("select_object"):
                    pyautogui.typewrite('39616,500,1400', .2)
                    pyautogui.press('enter')
                    pyautogui.press('enter')
                    sleep(3)
                    pyautogui.moveTo(400, 500)
                    pyautogui.hotkey('ctrl','v')
                    if appears("specify_insertion_point"):
                        pyautogui.typewrite("39363,-3163",.1)
                        pyautogui.press('enter')
                        sleep(1)
                    pyautogui.moveTo(400,400)
                    pyautogui.hotkey('ctrl','v')
                    if appears("specify_insertion_point"):
                        pyautogui.typewrite("40840,-3228",.1)
                        pyautogui.press('enter')
                        sleep(1)
                    pyautogui.moveTo(400,400)
                    pyautogui.hotkey('ctrl','v')
                    if appears("specify_insertion_point"):
                        pyautogui.typewrite("42490,-3265",.1)
                        pyautogui.press('enter')
                        sleep(1)
                    mouseLeftClickOn("equipment_panel")
                    mouseMoveTo("diassociate_from_pel_selected")
                    mouseLeftClickOn("hovered_drop_down")
                    mouseLeftClickOn("create_pel_all_equipment")
                    pyautogui.moveTo(400, 500)
                    if appears("create_pel_warning",10):
                        pyautogui.press('enter')
                    if appears("save_sign",10):
                        pyautogui.press('enter')
                    sleep(2)
                    if appears("generate_complete_dialog"):
                        print("PEL item was created")
                        mouseLeftClickOn("generate_complete_dialog_close")
                    else:
                        raise AssertionError("generate_complete_dialog did not appear. PEL was not created")
            else:
                raise AssertionError("select_equipment_parts did not appear")
        else:
            raise AssertionError("create_pel_item did not appear")
        self.logger.info('End:\t Checking Module- creating_PEL_Items_from_Inserted_Equipment')

    # Inserting Equipment from PEL
    def inserting_Equipment_from_PEL(self):
        self.logger.info('Start:\t Checking Module- inserting_Equipment_from_PEL')
        sleep(5)
        pyautogui.typewrite("select",.1)
        pyautogui.press('enter')
        pyautogui.typewrite('41149,4295',.1)
        pyautogui.press('enter')
        pyautogui.typewrite('41581,-741',.1)
        pyautogui.press('enter')
        pyautogui.press('enter')
        pyautogui.typewrite('delete',.1)
        pyautogui.press('enter')
        sleep(1)
        mouseLeftClickOn("equipment_panel")
        if appears("insert_from_pel"):
            mouseLeftClickOn("insert_from_pel")
            if appears("project_equipment_insertion_window"):
                print("project_equipment_insertion_window appeared")
                mouseLeftClickOn("project_equipment_insertion_window_ok")
                sleep(2)
                pyautogui.moveTo(400,400)
                # if appears("use_equipment_warning_yes"):
                #     mouseLeftClickOn("use_equipment_warning_yes")
                #     sleep(2)
                #     pyautogui.moveTo(400,400)
                if appears("save_yes_1"):
                    mouseLeftClickOn("save_yes_1")
                    sleep(2)
                    pyautogui.moveTo(400,400)
                if appears("select_hierarchy2"):
                    mouseLeftClickOn("unassigned_equipment")
                    pyautogui.press('enter')
                else:
                    raise AssertionError("select_hierarchy dialog did not appear")
                if appears("please_pick_the_first_point"):
                    pyautogui.typewrite("38200,4082",.1)
                    pyautogui.press('enter')
                    sleep(2)
                    pyautogui.moveTo(734,268)
                    pyautogui.click()
                    sleep(2)
                    pyautogui.moveTo(529,352)
                    pyautogui.click()
                    sleep(2)
                    pyautogui.hotkey('ctrl','s')
                    sleep(10)
                else:
                    raise AssertionError("please_pick_the_first_point did not appear")
            else:
                raise AssertionError()
        else:
            raise AssertionError("insert_from_pel did not appear")
        self.logger.info('End:\t Checking Module- inserting_Equipment_from_PEL')

    def exercise_r001(self):
        maximize()
        open_manager()
        if appears("equip_lib_button",10):
            print("Equipment Library button appeared in the manager toolbar")
            self.open_equipment_library()
        else:
            raise AssertionError("equip_lib_button did not appear")
        if appears("equip_lib_filter", 10):
            mouseLeftClickOn("equip_lib_filter")
            pyautogui.typewrite("TEST", .2)
            pyautogui.press('enter')
        else:
            raise AssertionError("equip_lib_filter did not appear")
        if appears("test_sub_category",10):
            mouseLeftClickOn("test_sub_category")
            mouseLeftClickOn("new_stock_equipment")
            pyautogui.typewrite("Test1", .1)
            pyautogui.press("enter")
            sleep(2)
            pyautogui.moveTo(500, 500)
            mouseLeftClickOn("description_field")
            pyautogui.press('down')
            pyautogui.press('right')
            pyautogui.press('D')
            pyautogui.press('enter')
            mouseLeftClickOn("weight_field_equip")
            pyautogui.press('right')
            pyautogui.typewrite('20', .1)
            pyautogui.press('enter')
            pyautogui.press('down')
            pyautogui.typewrite('10000', .1)
            pyautogui.press('enter')
        else:
            raise AssertionError("test_sub_category did not appear")
        if appears("edit_stock"):
            mouseLeftClickOn("edit_stock")
            sleep(5)
            pyautogui.hotkey("win","up")
            sleep(2)
        else:
            raise AssertionError("edit_stock did not appear")
        if appears("edit_equipment_stock_window"):
            print("edit_equipment_stock_window appeared")
        else:
            raise AssertionError("edit_equipment_stock_window did not appear")
        if appears("attribute_tab", 5):
            mouseLeftClickOn("attribute_tab")
        mouseLeftClickOn("cold_feature")
        pyautogui.press('up')
        pyautogui.press('left')
        pyautogui.press('left')
        pyautogui.press('space')
        pyautogui.press('down',presses=9,interval=.1)
        pyautogui.press('space')
        mouseLeftClickOn("tag_number")
        pyautogui.press('left')
        pyautogui.press('space')
        sleep(1)
        if appears("pipe_ends"):
            mouseLeftClickOn("pipe_ends")
            mouseLeftClickOn("new_pipe_end")
            if appears("select_pipe_end_treatment_window"):
                print("select_pipe_end_treatment_window appeared")
                mouseLeftClickOn("pipe_lf_expand", "pipe_lf_region")
                mouseLeftClickOn("pipe_lf_expand", "65mm_expand")
                if appears("fl_65mm"):
                    mouseDoubleClickOn("fl_65mm")
                else:
                    raise AssertionError("fl_65mm did not appear")
                mouseLeftClickOn("draw_end_treatment")
                pyautogui.press('right', presses=5)
                pyautogui.press('space')
            else:
                raise AssertionError("select_pipe_end_treatment_window did not appear")
            mouseLeftClickOn("new_pipe_end")
        else:
            raise AssertionError("pipe_ends tab did not appear")
        sleep(5)
        if appears("select_pipe_end_treatment_window"):
            print("select_pipe_end_treatment_window appeared")
            mouseLeftClickOn("pipe_lf_expand", "pipe_pl_region")
            mouseMoveTo("pipe_end_scroll_down")
            pyautogui.click(clicks=15)
            mouseLeftClickOn("pipe_lf_expand", "125mm_expand")
            if appears("pl_125_mm"):
                mouseDoubleClickOn("pl_125_mm")
            else:
                raise AssertionError("pl_125_mm did not appear")
            mouseLeftClickOn("draw_end_treatment")
            pyautogui.press('down')
            pyautogui.press('space')
        else:
            raise AssertionError("select_pipe_end_treatment_window did not appear")

        if appears("edit_std_drawing_button",10):
            mouseLeftClickOn("edit_std_drawing_button")
        else:
            raise AssertionError("edit_std_drawing_button did not appear")
        if appears("equipment_std_palette", 20):
            print("equipment_std_palette appeared")
            mouseRightClickOn("equipment_std_palette")
            mouseLeftClickOn("anchor_left")
        elif appears("equipment_std_palette_high", 20):
            print("equipment_std_palette appeared")
            mouseRightClickOn("equipment_std_palette_high")
            mouseLeftClickOn("anchor_left")
        else:
            raise AssertionError("equipment_std_palette did not appear")
        sleep(2)
        mouseMoveTo("equipment_std_palette_docked_hidden")
        mouseRightClickOn("equipment_std_palette_high")
        mouseLeftClickOn("auto_hide")
        sleep(3)
        free()
        free()
        pyautogui.click()
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.typewrite('NAVCUBE',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('ON',.2)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('BOX',.3)
        pyautogui.press('enter')
        if appears("specify_first_corner",10):
            pyautogui.typewrite('0,0,0',.2)
            pyautogui.press('enter')
            pyautogui.typewrite('#600,300,200',.2)
            pyautogui.press('enter')
        else:
            raise AssertionError("specify_first_corner did not appear")
        sleep(2)
        pyautogui.moveTo(1497,220)
        sleep(2)
        pyautogui.click()
        free()
        pyautogui.typewrite('OSNAP',.2)
        pyautogui.press('enter')
        if appears("drafting_window",10):
            mouseLeftClickOn("object_snap_tab")
            mouseLeftClickOn("osnap_midpoint")
            mouseLeftClickOn("drafting_window_ok")
        else:
            raise AssertionError("drafting_window did not appear")
        free()
        pyautogui.typewrite('CYL',.3)
        pyautogui.press('enter')
        free()
        pyautogui.typewrite('0,150,200',.2)
        pyautogui.press('enter')
        if appears("base_radi",10):
            pyautogui.typewrite('100',.2)
            pyautogui.press('enter')
        else:
            raise AssertionError("base_radi did not appear")
        free()
        pyautogui.click(button='right')
        if appears("axis_endpoint",10):
            mouseLeftClickOn("axis_endpoint")
            free()
            pyautogui.typewrite('#600,150,200',.2)
            pyautogui.press('enter')
        else:
            raise AssertionError("axis_endpoint did not appear")
        mouseLeftClickOn("insertion_point_add")
        free()
        if appears("select_location_of_insertion_point"):
            pyautogui.typewrite("0,300,0", .2)
            pyautogui.press('enter')
        else:
            raise AssertionError("select_location_of_insertion_point did not appear")
        if appears("pipe_end_fl",10):
            mouseLeftClickOn("pipe_end_fl")
            mouseLeftClickOn("edit_connection_end")
            free()
            pyautogui.typewrite('600,150,200',.2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.moveTo(1138,351)
            pyautogui.click()
        else:
            raise AssertionError("pipe_end_fl did not appear")
        if appears("pipe_end_pl",10):
            mouseLeftClickOn("pipe_end_pl")
            mouseLeftClickOn("edit_connection_end")
            free()
            pyautogui.typewrite('0,150,200',.2)
            pyautogui.press('enter')
            sleep(1)
            pyautogui.moveTo(742,582)
            pyautogui.click()
        else:
            raise AssertionError("pipe_end_pl did not appear")
        free()

        mouseLeftClickOn("close_drawing")
        if appears("stock_usage_warning"):
            mouseLeftClickOn("save_changes")
            sleep(10)
        else:
            raise AssertionError("stock_usage_warning did not appear")
        if appears("edit_equipment_stock_window_maximized", 10):
            mouseLeftClickOn("edit_equipment_stock_window_done")
        else:
            pyautogui.moveTo(500, 500)
            pyautogui.moveTo(500, 500)
            sleep(2)
            pyautogui.click()
            pyautogui.press("esc")

            pyautogui.press("esc")
            pyautogui.press("esc")

            pyautogui.typewrite("SCCATALOG", .2)
            pyautogui.press("enter")
            if appears("edit_equipment_stock_window_maximized1", 10):
                mouseLeftClickOn("edit_equipment_stock_window_done")
            else:
                raise AssertionError("edit_equipment_stock_window_maximized did not appear")
        if appears("is_insertable",10):
            mouseLeftClickOn("is_insertable")
            pyautogui.press('right')
            pyautogui.press('space')
            mouseLeftClickOn("equipment_stock_ok1")
            sleep(10)
            pyautogui.hotkey('alt', 'f4')
        else:
            raise AssertionError("is_insertable option did not appear")

    def exercise_r002(self):
        mouseLeftClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("main_panel_new")
        mouseLeftClickOn("navigator_button_new")
        if appears("sc_navigator_window"):
            mouseLeftClickOn("ssi_node_shadowed")
            mouseLeftClickOn("navigator_u02_node")
            mouseLeftClickOn("equipment_node")
            mouseLeftClickOn("equipment_node_drawing")
            mouseLeftClickOn("new_equipment")
            if appears("new_equipment_dialog"):
                pyautogui.typewrite("Exercise1")
                if appears("open_new_drawing2", 5):
                    mouseLeftClickOn("open_new_drawing2")
                pyautogui.press('enter')
                if appears("Exercise1_tab"):
                    print("Exercise1_tab appeared")
                    sleep(10)
                    sleep(10)
                    sleep(10)
                    sleep(10)
                    pyautogui.moveTo(500,500)
                    sleep(1)
                else:
                    raise AssertionError("Exercise1_tab did not appear")
            else:
                raise AssertionError("new_equipment_dialog did not appear")
        else:
            raise AssertionError("sc_navigator_window did not appear")


        sleep(10)
        free()
        pyautogui.typewrite('SCDWGOPTIONSEQUIP',.2)
        pyautogui.press('enter')
        if appears("equipment_drawing_option_dialog",10):
            mouseLeftClickOn("show_equipment_envelope")
            mouseLeftClickOn("override_color_for_splitted_part")
            mouseLeftClickOn("equipment_drawing_option_dialog_ok")
            pyautogui.moveTo(500,500)
            pyautogui.hotkey('ctrl','s')
            sleep(5)
            sleep(5)
        else:
            free()
            pyautogui.press('esc',presses=5)
            pyautogui.typewrite('SCDWGOPTIONSEQUIP', .2)
            pyautogui.press('enter')
            if appears("equipment_drawing_option_dialog", 10):
                mouseLeftClickOn("show_equipment_envelope")
                mouseLeftClickOn("override_color_for_splitted_part")
                mouseLeftClickOn("equipment_drawing_option_dialog_ok")
                pyautogui.moveTo(500, 500)
                pyautogui.hotkey('ctrl', 's')
                sleep(5)
                sleep(5)
            else:
                raise AssertionError("equipment_drawing_option_dialog did not appear")

    def exercise_r003(self):
        free()
        sleep(2)
        pyautogui.moveTo(400, 400)
        sleep(2)
        pyautogui.typewrite('scmlink', .2)
        pyautogui.press('enter')
        if appears("mlink_manager"):
            if appears("mlink_DWG", 10):
                mouseLeftClickOn("mlink_DWG")
            else:
                mouseLeftClickOn("mlink_DWG_dropdown")
                sleep(2)
                mouseLeftClickOn("mlink_Attach_DWG")
            if appears("mlink_DWG_window", 10):
                mouseLeftClickOn("mlink_structure1")
                pyautogui.press('right')
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('down')
                sleep(1)
                pyautogui.press('right')
                pyautogui.press('down',presses=11)
                pyautogui.press('space')
                mouseLeftClickOn("mlink_ok")
                sleep(5)
                if appears("mlink_manager",20):
                    mouseLeftClickOn("mlink_manager")
                elif appears("mlink_manager_high",20):
                    mouseLeftClickOn("mlink_manager_high")
                # if appears("mlink_cross_region1",20):
                #     mouseLeftClickOn("mlink_cross1","mlink_cross_region1")
                # elif appears("mlink_cross_region",20):
                #     mouseLeftClickOn("mlink_cross","mlink_cross_region")
                #     pyautogui.click(clicks=2)
                sleep(5)
                pyautogui.hotkey('alt', 'f4')
            else:
                raise AssertionError("mlink DWG window didn't appear.")
            sleep(25)
        else:
            raise AssertionError("mlink_manager did not appear")
        reset_viewport()
        zoom('33342','3855','35814','3476')
        sleep(5)
        pyautogui.typewrite('UCSLIST',.2)
        pyautogui.press('enter')
        sleep(5)
        if appears("activate_ucs_menu",10):
            mouseLeftClickOn("u02_twd_3600_ucs")
            mouseLeftClickOn("world_coordinate_activate")
            pyautogui.moveTo(500,500)
            sleep(2)
        else:
            raise AssertionError("activate_ucs_menu did not appear")
        self.insert_from_library()
        if appears("select_equipment_window"):
            mouseLeftClickOn("enter_filter_text")
            pyautogui.typewrite("test1", .2)
            pyautogui.press('enter')
        else:
            raise AssertionError("select_equipment_window did not appear")
        if appears("test1_equipment_on_list"):
            mouseDoubleClickOn("test1_equipment_on_list")
        else:
            raise AssertionError("test1_equipment_on_list did not appear")
        sleep(10)
        if appears("select_hierarchy2"):
            mouseLeftClickOn("unassigned_equipment_prod")
            pyautogui.press('enter')
            sleep(30)
        pyautogui.press('C')
        pyautogui.press('enter')
        sleep(1)
        pyautogui.typewrite('30898.8572,315.8741',.2)
        pyautogui.press('enter')
        sleep(1)
        pyautogui.moveTo(882,245)
        sleep(2)
        pyautogui.click()
        sleep(2)
        pyautogui.moveTo(612,172)
        sleep(2)
        pyautogui.click()
        sleep(2)
        pyautogui.typewrite('move',.2)
        pyautogui.press('enter')
        pyautogui.typewrite('30898.8572,615.8741',.2)
        pyautogui.press('enter')
        pyautogui.press('enter')
        sleep(2)
        pyautogui.typewrite('30898.8572,615.8741',.2)
        pyautogui.press('enter')
        sleep(4)
        pyautogui.moveTo(444,234)
        sleep(2)
        pyautogui.typewrite('100')
        pyautogui.press('enter')
        conceptual_visual()

    def exercise_r004(self):
        pyautogui.typewrite('SCPROJECTEQUIPMENTLISTSHOW',.2)
        pyautogui.press('enter')
        if appears("project_equipment_list_window"):
            print("project_equipment_list_window appeared")
            mouseLeftClickOn("new_part_button")
            pyautogui.typewrite("PEL_test1", .1)
            pyautogui.press('enter')
            pyautogui.press('right')
            mouseLeftClickOn("stock_browser")
            sleep(4)
            pyautogui.hotkey('win', 'up')
        else:
            raise AssertionError("project_equipment_list_window did not appear")
        if appears("select_an_equipment_stock_window"):
            print("select_an_equipment_stock window appeared")
            mouseDoubleClickOn("tab_resizer2")
            mouseDoubleClickOn("tab_resizer3")
            mouseLeftClickOn("enter_filter_text")
            pyautogui.typewrite("test1", .1)
            pyautogui.press('enter')
            if appears("test1_equip_list"):
                mouseLeftClickOn("test1_equip_list")
                mouseLeftClickOn("select_an_equipment_stock_window_ok")
                sleep(2)
                mouseLeftClickOn("project_equipment_list_window_apply")
                sleep(2)
                mouseLeftClickOn("project_equipment_list_window_ok")
                sleep(5)
                pyautogui.moveTo(500,500)
                sleep(2)
                pyautogui.typewrite('SCEQUIPPELASSOCIATE',.2)
                pyautogui.press('enter')
                if appears("select_an_equipment_part_prompt",10):
                    pyautogui.moveTo(542,214)
                    sleep(2)
                    pyautogui.click()
                    if appears("project_equipment_association_window",10):
                        mouseLeftClickOn("name_col")
                        pyautogui.press('right')
                        pyautogui.press('enter')
                        mouseLeftClickOn("project_equipment_association_window_ok")
                        if appears("associated_msg",5):
                            print("associated_message was displated")
                    else:
                        raise AssertionError("project_equipment_association_window did not appear")
                else:
                    raise AssertionError("select_an_equipment_part_prompt did not appear")
            else:
                raise AssertionError("test_equip_list did not appear")
        else:
            raise AssertionError("select_an_equipment_stock window did not appear")
        sleep(3)
        reload_drawing()
        pyautogui.moveTo(500,500)
        pyautogui.press('esc')
        pyautogui.press('esc')
        pyautogui.press('esc')

        pyautogui.typewrite('SCPROJECTEQUIPMENTLISTSHOW', .2)
        pyautogui.press('enter')
        if appears("project_equipment_list_window"):
            mouseLeftClickOn("enter_filter_pel")
            pyautogui.typewrite('PEL_test1')
            pyautogui.press('right',presses=3)
            if appears("test1_002",10):
                print("test1 was assigned")
            mouseLeftClickOn("project_equipment_list_window_ok")
        else:
            raise AssertionError("project_equipment_list_window did not appear on the 2nd time")
        sleep(3)
        pyautogui.moveTo(500,500)
        sleep(3)
        pyautogui.hotkey('ctrl','s')
        sleep(5)

    def verifyingEquipmentCatalog(self):
        obj = SSILearningVerifierUtil()
        obj.ssi_learning_verifier(projectFilePath="C:/NewDeploy_equipment/", list_image="equipment_verify",
                                  xmlExtractor1="EquipmentCatalogXMLextractor", xmlName1="EquipmentCatalogandModeling")


class TCP8_Equipment_CatalogError(Exception):
    pass
