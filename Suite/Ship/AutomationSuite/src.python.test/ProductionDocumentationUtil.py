# ProductionDocumentationUtil.py
# ProductionDocumentationUtil Class contains the commonly useful functions for Production Documentation Manual

from logger_extended import logger_extended

from time import sleep

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseDoubleClickOn

from screenObjUtil import exists, appears, waitForVanish

from pyautogui import hotkey, typewrite, press, click, moveTo

from SSIUtil import free, free_white


class ProductionDocumentationUtil(object):

    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'

    logger = logger_extended().get_logger('ProductionDocumentationUtil')

    def __init__(self):
        self._expression = ''

    def open_BOM_manager(self):
        # @desc - For opening BOM Manager
        self.logger.info('Start:\t Checking Module- open_BOM_manager')
        if appears("shipconstructor_tab_new",2):
            mouseDoubleClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("manage_pane")
        mouseLeftClickOn("manage_button")
        if appears("shipcon_manager"):
            hotkey('win', 'up')
            mouseLeftClickOn("general_button")
            mouseLeftClickOn("production_output_manager")
            mouseLeftClickOn("bill_of_material_manager")
            if appears("maximize_win",2):
                mouseLeftClickOn("maximize_win")
            if appears("bom_definition"):
                self.logger.info("BOM Manager was opened")
            else:
                raise AssertionError("bom_definition did not appear")
        else:
            raise AssertionError("shipcon_manager did not appear")
        self.logger.info('End:\t Checking Module- open_BOM_manager')

    def open_label_style_manager(self):
        # @desc - For opening Label Style Manager
        self.logger.info('Start:\t Checking Module- open_label_style_manager')
        if appears("shipconstructor_tab_new",2):
            mouseDoubleClickOn("shipconstructor_tab_new")
        mouseLeftClickOn("manage_pane")
        mouseLeftClickOn("manage_button")
        if appears("shipcon_manager"):
            mouseLeftClickOn("general_button")
            mouseLeftClickOn("production_output_manager")
            mouseLeftClickOn("label_style")
            if appears("maximize_win",2):
                mouseLeftClickOn("maximize_win")
            if appears("label_style_window"):
                self.logger.info("label_style_window appeared")
                self.logger.info("Label style manager was opened")
            else:
                raise AssertionError("bom_definition did not appear")
        else:
            raise AssertionError("shipcon_manager did not appear")
        self.logger.info('End:\t Checking Module- open_label_style_manager')


    def insert_BOM_table(self, definition_name, column_width , max_row=None , scr_cord_x=None, scr_cord_y=None, cord_x = None, cord_y = None):
        """
        @desc - For inserting new BOM Table
        :param - definition_name: Name of the BOM Definition
        :param - column_width: Table column width
        :param - max_row: Maximum no. of rows
        :param - cord_x: Table position coordinate (Screen Coordinate)
        :param - cord_y: Table position coordinate (Screen Coordinate)
        """
        self.logger.info('Start:\t Checking Module- insert_BOM_table')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if appears("insert_bom_table"):
            mouseLeftClickOn("insert_bom_table")
            if appears("bom_definition_wizard"):
                self.logger.info("bom_definition_wizard appeared")
                if appears("general_mode_bom",2):
                    mouseLeftClickOn("general_mode_bom")
                mouseMoveTo("slide_down")
                while exists(definition_name) == False:
                    click(clicks=6)
                if appears(definition_name):
                    mouseLeftClickOn(definition_name)
                    sleep(.5)
                    free_white()
                    mouseLeftClickOn("bom_definition_wizard_next")
                    hotkey('ctrl','a')
                    sleep(.5)
                    typewrite(column_width,.1)
                    sleep(.5)
                    if max_row != None:
                        press('tab')
                        press('tab')
                        hotkey('ctrl', 'a')
                        sleep(.5)
                        typewrite(max_row, .1)
                        sleep(.5)
                    free_white()
                    mouseLeftClickOn("bom_definition_wizard_finish")
                    if appears("collector_option_window",2):
                        self.logger.info("collector_option_window appeared")
                        mouseLeftClickOn("collector_option_window_ok")
                    sleep(5)
                    free_white()
                    if appears("select_position_of_table"):
                        if scr_cord_x is not None:
                            moveTo(int(scr_cord_x),int(scr_cord_y))
                            sleep(2)
                            click()
                        elif cord_x is not None:
                            press('#')
                            typewrite(cord_x,.1)
                            press(',')
                            typewrite(cord_y,.1)
                            press('enter')
                    else:
                        free_white()
                        click()
                        free_white()
                        if appears("select_position_of_table"):
                            if scr_cord_x is not None:
                                moveTo(int(scr_cord_x),int(scr_cord_y))
                                sleep(2)
                                click()
                            elif cord_x is not None:
                                press('#')
                                typewrite(cord_x,.1)
                                press(',')
                                typewrite(cord_y,.1)
                                press('enter')
                        else:
                            raise AssertionError("select_position_of_table did not appear")
                else:
                    raise AssertionError(definition_name + "Did not appear")
            else:
                raise AssertionError("bom_definition_wizard did not appear")
        else:
            raise AssertionError("insert_bom_table did not appear")
        self.logger.info('End:\t Checking Module- insert_BOM_table')

    def update_BOM_table(self):
        # @desc - For Updating BOM table
        self.logger.info('Start:\t Checking Module- update_BOM_table')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if appears("update_bom"):
            mouseLeftClickOn("update_bom")
            free_white()
        else:
            raise AssertionError("update_bom did not appear")
        self.logger.info('End:\t Checking Module- update_BOM_table')

    def create_BOM_definition(self , definition_name, title, table_style ):
        """
        @desc - For creating new BOM Definition
        :param - definition_name: Name of the BOM Definition
        :param - title: Title of the BOM Definition
        :param - table_style: Table style selector
        """
        self.logger.info('Start:\t Checking Module- create_BOM_definition')
        self.open_BOM_manager()
        sleep(1)
        mouseLeftClickOn("bom_new")
        mouseLeftClickOn("expand_click", "definition_name_general")
        typewrite(definition_name, .1)
        press('right')
        typewrite(title, .1)
        press('right')
        typewrite(table_style, .1)
        press('right')
        press('a')
        press('enter')
        sleep(1)
        self.logger.info('End:\t Checking Module- create_BOM_definition')

    def delete_all_labels(self):
        # @desc - For deleting all existing labels
        self.logger.info('Start:\t Checking Module- delete_all_labels')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("delete_all_labels"):
            mouseLeftClickOn("delete_all_labels")
            free_white()
            free()
            sleep(1)
        else:
            raise AssertionError("delete_all_labels did not appear")
        self.logger.info('End:\t Checking Module- delete_all_labels')

    def manual_labeling(self):
        # @desc - For selecting the Manual Labeling option
        self.logger.info('Start:\t Checking Module- manual_labeling')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("manual_labeling"):
            mouseLeftClickOn("manual_labeling")
            free_white()
            free()
            sleep(1)
        else:
            raise AssertionError("manual_labeling did not appear")
        self.logger.info('End:\t Checking Module- manual_labeling')

    def auto_labeling(self, option_to_be_selected, previously_selected_option = None ):
        """
        @desc - To execute the Auto Labeling Operation
        :param - option_to_be_selected: Name of Screenshot of the option to be selected
        :param - previously_selected_option: Name of Screenshot of the option selected previously
        """
        self.logger.info('Start:\t Checking Module- auto_labeling')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if previously_selected_option is not None:
            if previously_selected_option == "label_viewports_selected2":
                mouseLeftClickOn(previously_selected_option)
            else:
                mouseMoveTo(previously_selected_option)
                mouseLeftClickOn("hovered_drop_down_region")
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        else:
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        free_white()
        sleep(1)
        self.logger.info('End:\t Checking Module- auto_labeling')

    def copy_label_from_BOM(self):
        # @desc - To select the Copy Label from BOM option
        self.logger.info('Start:\t Checking Module- copy_label_from_BOM')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("copy_label_from_bom"):
            mouseLeftClickOn("copy_label_from_bom")
            free_white()
            free()
            sleep(1)
        else:
            raise AssertionError("copy_label_from_bom did not appear")
        self.logger.info('End:\t Checking Module- copy_label_from_BOM')

    def leader_distribution_line(self, cord_x1, cord_y1, cord_x2, cord_y2, vp_cord_x, vp_cord_y):
        """
        @desc - For creating Leader Distribution line
        :param - cord_x1: Coordinate for the distribution line
        :param - cord_x2: Coordinate for the distribution line
        :param - cord_y1: Coordinate for the distribution line
        :param - cord_y2: Coordinate for the distribution line
        """
        self.logger.info('Start:\t Checking Module- leader_distribution_line')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("leader_distribution_line"):
            mouseLeftClickOn("leader_distribution_line")
            free_white()
            free()
            sleep(1)
            typewrite(cord_x1,.1)
            press(',')
            typewrite(cord_y1,.1)
            press('enter')
            typewrite('#'+cord_x2,.1)
            press(',')
            typewrite(cord_y2,.1)
            press('enter')
            if appears("select_viewport_to_track", 2):
                free_white()
                typewrite(vp_cord_x,.1)
                press(',')
                typewrite(vp_cord_y,.1)
                press('enter')
            else:
                raise AssertionError("select_viewport_to_track option did not appear")
        else:
            raise AssertionError("leader_distribution_line did not appear")
        self.logger.info('End:\t Checking Module- leader_distribution_line')

    def transfer_leaders_to_other_line(self, cord_x1, cord_y1, cord_x2, cord_y2, leader_x1, leader_y1):
        """
        @desc - For transferring leaders to other line
        :param - cord_x1, cord_y1, cord_x2, cord_y2 : Coordinates to select the labels
        :param - leader_x1, leader_y1 : Coordinates to select the leaders
        """
        self.logger.info('Start:\t Checking Module- transfer_leaders_to_other_line')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("transfer_leaders_to_other_lines"):
            mouseLeftClickOn("transfer_leaders_to_other_lines")
            free_white()
            free()
            sleep(1)
            typewrite(cord_x1,.1)
            press(',')
            typewrite(cord_y1,.1)
            press('enter')
            typewrite(cord_x2,.1)
            press(',')
            typewrite(cord_y2,.1)
            press('enter')
            if appears("select_leaders_to_transfer", 2):
                free_white()
                typewrite(leader_x1, .1)
                press(',')
                typewrite(leader_y1, .1)
                press('enter')
                sleep(2)
                press('enter')
            else:
                raise AssertionError("select_leaders_to_transfer option did not appear")
            if appears("select_line_to_transfer", 2):
                free_white()
                typewrite(leader_x1, .1)
                press(',')
                typewrite(leader_y1, .1)
                press('enter')
                press('enter')
                press('enter')
            else:
                raise AssertionError("select_line_to_transfer option did not appear")
        else:
            raise AssertionError("transfer_leaders_to_other_lines did not appear")
        self.logger.info('End:\t Checking Module- transfer_leaders_to_other_line')

    def redistributing_leaders_on_distribution_lines(self, leader_x1, leader_y1, mode):
        """
        @desc - For redistributing the leaders
        :param - leader_x1, leader_y1 : Coordinates to select the leaders
        :param - mode: to select the distribution mode
        """
        self.logger.info('Start:\t Checking Module- redistributing_leaders_on_distribution_lines')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("redistribute_leaders"):
            mouseLeftClickOn("redistribute_leaders")
            free_white()
            free()
            if appears("select_distribution_line", 2):
                free_white()
                typewrite(leader_x1, .1)
                press(',')
                typewrite(leader_y1, .1)
                press('enter')
                press('enter')
            else:
                raise AssertionError("select_distribution_line option did not appear")
            if appears("distribution_mode",2):
                if mode=="center":
                    press('c')
                    press('enter')
                    self.logger.info("Center mode was selected")
                elif mode=="nearest":
                    press('n')
                    press('enter')
                    self.logger.info("Nearest mode was selected")
                elif mode=="equidistant":
                    press('e')
                    press('enter')
                    self.logger.info("Equidistant mode was selected")
            else:
                raise AssertionError("distribution_mode option did not appear")
        else:
            raise AssertionError("redistribute_leaders did not appear")
        self.logger.info('End:\t Checking Module- redistributing_leaders_on_distribution_lines')

    def set_leader_insertion_mode(self, leader_x1, leader_y1, mode, previously_selected_option=None):
        """
        @desc - For setting up leader insertion mode
        :param - leader_x1, leader_y1 : Coordinates to select the leaders
        :param - mode: to select the distribution mode
        :param - previously_selected_option: name of the Screenshot of the option that was previously selected
        """
        self.logger.info('Start:\t Checking Module- set_leader_insertion_mode')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if previously_selected_option is not None:
            mouseMoveTo(previously_selected_option)
            mouseLeftClickOn("hovered_drop_down_region")
            if appears("set_leader_insertion_mode_on_list"):
                mouseLeftClickOn("set_leader_insertion_mode_on_list")
                free_white()
            else:
                raise AssertionError("set_leader_insertion_mode_on_list did not appear")
        else:
            if appears("set_leader_insertion_mode_selected"):
                mouseLeftClickOn("set_leader_insertion_mode_selected")
                free_white()
            else:
                raise AssertionError("set_leader_insertion_mode_selected did not appear")
        free_white()
        free()
        if appears("select_distribution_line", 2):
            free_white()
            typewrite(leader_x1, .1)
            press(',')
            typewrite(leader_y1, .1)
            press('enter')
            press('enter')
        else:
            raise AssertionError("select_distribution_line option did not appear")
        if appears("enter_an_insertion_mode",2):
            if mode=="left":
                press('l')
                press('enter')
                self.logger.info("Left mode was selected")
            elif mode=="right":
                press('r')
                press('enter')
                self.logger.info("Right mode was selected")
            elif mode=="center":
                press('c')
                press('enter')
                self.logger.info("Center mode was selected")
            elif mode=="horizontal":
                press('h')
                press('enter')
                self.logger.info("Closest horizontal mode was selected")
        else:
            raise AssertionError("distribution_mode option did not appear")
        self.logger.info('End:\t Checking Module- set_leader_insertion_mode')

    def viewport_options(self, option_to_be_selected, previously_selected_option=None , cord_x= None, cord_y= None):
        """
        @desc - For changing viewport option
        :param - option_to_be_selected: Name of Screenshot of the option to be selected
        :param - previously_selected_option: Name of Screenshot of the option selected previously
        """
        self.logger.info('Start:\t Checking Module- viewport_options')
        if appears("production_ribbon", 2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("production_utilities_pane")
        if previously_selected_option is not None:
            mouseLeftClickOn(previously_selected_option)
            # mouseLeftClickOn("hovered_drop_down_region2")
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected + "Did not appear")
        else:
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected + "Did not appear")
        free_white()
        if appears("select_viewports_option",2):
            free_white()
            if cord_x is None:
                mouseLeftClickOn("viewport_select_region")
                press('enter')
            else:
                typewrite(cord_x,.1)
                press(',')
                typewrite(cord_y,.1)
                press('enter')
                press('enter')
        free_white()
        sleep(1)
        self.logger.info('End:\t Checking Module- viewport_options')

    def vp_freeze_layer(self, layer_name):
        """
        @desc - For freezing viewport layer
        :param - layer_name: Name of the Screenshot of the layer to freeze
        """
        self.logger.info('Start:\t Checking Module- vp_freeze_layer')
        if appears("home_button",2):
            mouseDoubleClickOn("home_button")
        mouseLeftClickOn("layer_button")
        if appears("layer_properties_button"):
            mouseLeftClickOn("layer_properties_button")
            sleep(1)
            mouseLeftClickOn(layer_name)
            # press('right',presses=10,interval=.2)
            # press('space')
            if appears("vp_freeze_off",10):
                mouseLeftClickOn("vp_freeze_off")

            if appears("vp_freeze_activated",10):
                self.logger.info("VP Freeze was activated")
            mouseLeftClickOn("layer_properties_manager_close")
            free_white()
            free()
        else:
            raise AssertionError("layer_properties_button did not appear")
        self.logger.info('End:\t Checking Module- vp_freeze_layer')

    def property_label(self):
        # @desc - For selecting property label option
        self.logger.info('Start:\t Checking Module- property_label')
        if appears("production_ribbon", 2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("property_label_pane")
        if appears("property_label_option"):
            mouseLeftClickOn("property_label_option")
            free_white()
            free()
        else:
            raise AssertionError("property_label_option did not appear")
        self.logger.info('End:\t Checking Module- property_label')

    def insert_keyword(self):
        # @desc - For selecting insert keyword option
        self.logger.info('Start:\t Checking Module- insert_keyword')
        if appears("production_ribbon", 2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("production_utilities_pane")
        if appears("insert_keywords"):
            mouseLeftClickOn("insert_keywords")
            free_white()
            free()
        else:
            raise AssertionError("insert_keywords did not appear")
        self.logger.info('End:\t Checking Module- insert_keyword')

    def update_keyword(self):
        # @desc - For selecting update keyword option
        self.logger.info('Start:\t Checking Module- update_keyword')
        if appears("production_ribbon", 2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("production_utilities_pane")
        if appears("update_keywords"):
            mouseLeftClickOn("update_keywords")
            free_white()
            free()
        else:
            raise AssertionError("insert_keywords did not appear")
        self.logger.info('End:\t Checking Module- update_keyword')

    def global_dimension_to_point(self, viewport_x = None, viewport_y = None):
        """
        @desc - For applying global dimension to point
        :param - viewport_x: Coordinate value
        :param - viewport_y: Coordinate value
        """
        self.logger.info('Start:\t Checking Module- global_dimension_to_point')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("labeling_pane")
        if appears("global_dimension_to_point"):
            mouseLeftClickOn("global_dimension_to_point")
            free_white()
            free()
            if appears("select_viewport_to_make_active",2):
                typewrite(viewport_x, .1)
                press(',')
                typewrite(viewport_y, .1)
                press('enter')
        else:
            raise AssertionError("global_dimension_to_point option did not appear")
        self.logger.info('End:\t Checking Module- global_dimension_to_point')

    def quality_matrix(self):
        # @desc - For selecting quality matrix option
        self.logger.info('Start:\t Checking Module- quality_matrix')
        if appears("sc_utilites",2):
            mouseDoubleClickOn("sc_utilites")
        mouseLeftClickOn("sc_utilities_tools_button")
        if appears("quality_matrix"):
            mouseLeftClickOn("quality_matrix")
            free_white()
            free()
            if appears("quality_matrix_window"):
                mouseLeftClickOn("quality_matrix_window_ok")
                free_white()
                free_white()
            else:
                raise AssertionError("quality_matrix_window did not appear")
        else:
            raise AssertionError("quality_matrix did not appear")
        self.logger.info('End:\t Checking Module- quality_matrix')

    def bom_revision(self, option_to_be_selected, previously_selected_option = None ):
        """
        @desc - For applying BOM revision
        :param - option_to_be_selected: Name of Screenshot of the option to be selected
        :param - previously_selected_option: Name of Screenshot of the option selected previously
        """
        self.logger.info('Start:\t Checking Module- bom_revision')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if previously_selected_option is not None:
            mouseMoveTo(previously_selected_option)
            mouseLeftClickOn("hovered_drop_down_region")
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        else:
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        free_white()
        sleep(1)
        self.logger.info('End:\t Checking Module- bom_revision')

    def revision(self):
        # @desc - For selecting revision option
        self.logger.info('Start:\t Checking Module- revision')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if appears("revision_option"):
            mouseLeftClickOn("revision_option")
            if appears("production_drawing_revisions_window"):
                self.logger.info("production_drawing_revisions_window appeared")
            else:
                raise AssertionError("production_drawing_revisions_window did not appear")
        else:
            raise AssertionError("revision_option did not appear"+ "Did not appear")
        free_white()
        self.logger.info('End:\t Checking Module- revision')

    def list_all_revision(self,option, list_part_option):
        """
        @desc - For viewing list all revision
        :param - option: What to be listed (Revisions, Operations or All)
        :param - list_part_option: For listing modified parts (yes or no)
        """
        self.logger.info('Start:\t Checking Module- list_all_revision')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if appears("list_revision"):
            mouseLeftClickOn("list_revision")
            free_white()
            if appears("select_what_to_list"):
                press(option)
                press('enter')
                if appears("list_parts"):
                    press(list_part_option)
                    press('enter')
            else:
                raise AssertionError("production_drawing_revisions_window did not appear")
        else:
            raise AssertionError("list_revision did not appear"+ "Did not appear")
        free_white()
        self.logger.info('End:\t Checking Module- list_all_revision')

    def delete_bom_revision(self, option_to_be_selected, previously_selected_option = None ):
        """
        @desc - For deleting BOM revision
        :param - option_to_be_selected: Name of Screenshot of the option to be selected
        :param - previously_selected_option: Name of Screenshot of the option selected previously
        """
        self.logger.info('Start:\t Checking Module- delete_bom_revision')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if previously_selected_option is not None:
            mouseMoveTo(previously_selected_option)
            mouseLeftClickOn("hovered_drop_down")
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        else:
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        free_white()
        sleep(1)
        self.logger.info('End:\t Checking Module- delete_bom_revision')

    def delete_all_revision(self, option_to_be_selected, previously_selected_option = None ):
        """
        @desc - For deleting all revision
        :param - option_to_be_selected: Name of Screenshot of the option to be selected
        :param - previously_selected_option: Name of Screenshot of the option selected previously
        """
        self.logger.info('Start:\t Checking Module- delete_all_revision')
        if appears("production_ribbon",2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("bom_pane")
        if previously_selected_option is not None:
            mouseMoveTo(previously_selected_option)
            mouseLeftClickOn("hovered_drop_down")
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        else:
            if appears(option_to_be_selected):
                mouseLeftClickOn(option_to_be_selected)
            else:
                raise AssertionError(option_to_be_selected+ "Did not appear")
        free_white()
        sleep(1)
        self.logger.info('End:\t Checking Module- delete_all_revision')

    def update_drawing(self):
        # @desc - For selecting the update drawing option
        self.logger.info('Start:\t Checking Module- update_drawing')
        if appears("production_ribbon", 2):
            mouseDoubleClickOn("production_ribbon")
        mouseLeftClickOn("production_utilities_pane")
        if appears("update_drawing"):
            mouseLeftClickOn("update_drawing")
            free_white()
            free()
        else:
            raise AssertionError("update_drawing did not appear")
        self.logger.info('End:\t Checking Module- update_drawing')

class ProductionDocumentationUtilError(Exception):
    pass
