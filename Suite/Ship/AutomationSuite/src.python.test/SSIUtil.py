# SSIUtil.py
#This module contains the useful functions which are used very often during the automation of every Manual/Catalog.

from time import sleep

from IOUtil import mouseMoveTo, mouseLeftClickOn, mouseRightClickOn, mouseDoubleClickOn

from screenObjUtil import exists, appears

from pyautogui import hotkey, typewrite, press, scroll, click, moveTo


def remove_node_highlight():
    # @desc - This is a common Navigator functionality to remove node highlight so that node recognition is more effective. Useful during the Navigation menu operation.
    mouseDoubleClickOn("highlight_remover_region", "highlight_remover_region")


def close_navigator():
    # @desc - Close the Navigator window
    mouseLeftClickOn("navigator_close_button", "navigator_close_button")

def select_pipe_element(stockType):
    if appears("pipe_ribbon",5):
        mouseDoubleClickOn("pipe_ribbon")
    mouseLeftClickOn("pipe_pane")
    if appears(stockType):
        mouseLeftClickOn(stockType)
        free()
    else:
        raise AssertionError(stockType+" did not appear")

def click_pipe_stock(stock, stockhigh = None):
    if appears(stock, 15):
        mouseDoubleClickOn(stock)
    elif appears (stockhigh, 10):
        mouseDoubleClickOn(stockhigh)
    else:
        raise AssertionError(stock+" did not appear")

def select_pipe_stock(stockName):
    free()
    # mouseLeftClickOn("pipe_pane")
    # if stockType is not None:
    #     mouseLeftClickOn(stockType)
    sleep(4)
    hotkey('win', 'up')
    if appears("pipe_stock_window", 10):
        mouseLeftClickOn("delete_filter")
        stock_name_tab_resize()
    else:
        free()
        click(button='right')
        mouseLeftClickOn("select_stock1")
        if appears("pipe_stock_window", 10):
            mouseLeftClickOn("delete_filter")
            stock_name_tab_resize()
        else:
            raise AssertionError("pipe_stock_window did nto appear")
    if appears(stockName, 10):
        mouseDoubleClickOn(stockName)
        sleep(3)
        free()
    else:
        raise AssertionError(stockName + " Did not appear")

def stock_name_tab_resize():
    mouseDoubleClickOn("stock_tab_resizer")
    mouseDoubleClickOn("tab_resizer")

def osnap():
    free()
    typewrite('OSNAP', .2)
    press('enter')
    if appears("drafting_window", 5):
        mouseLeftClickOn("object_snap_tab")
        mouseLeftClickOn("osnap_midpoint")
        mouseLeftClickOn("osnap_endpoint")
        mouseLeftClickOn("drafting_window_ok")
    else:
        raise AssertionError("drafting_window did not appear")
    free()

def open_drawing():
    # @desc - open drawing u03_f60 after a fresh start
    mouseLeftClickOn('shipconstructor_tab_new')
    if appears("main_panel_new"):
        mouseLeftClickOn('main_panel_new', 'main_panel_new')
        mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
        mouseLeftClickOn("u03_node2", "u03_node2_region")
        mouseLeftClickOn("structure_node2")
    if appears("u03_f60_node"):
        mouseDoubleClickOn("u03_f60_node")
        if appears("save_caution"):
            mouseLeftClickOn("save_caution_no")
        sleep(3)
        if appears("u03_f60_drawing_tab"):
            print("Hull drawing F60 properly opened")
        else:
            raise AssertionError("Hull Drawing not properly opened")
    else:
        raise AssertionError("U03_F60 node not visible")


def open_drawing_from_u03(str):
    # @desc - open drawing str when previous drawing is from U03
    mouseLeftClickOn('shipconstructor_tab_new')
    if appears("main_panel_new"):
        mouseLeftClickOn('main_panel_new', 'main_panel_new')
        mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
        mouseLeftClickOn("u03_node2", "u03_node2_region")
        mouseLeftClickOn("structure_node2")
        if appears(str):
            mouseDoubleClickOn(str)
            if appears("save_caution",10):
                mouseLeftClickOn("save_yes_3")
        else:
            msg = str + 'did not appear'
            raise AssertionError(msg)
    else:
        raise AssertionError("main_panel did not appear")


def open_drawing_u03():
    # @desc - open any drawing from U03
    mouseLeftClickOn('shipconstructor_tab_new')
    if appears("main_panel_new"):
        mouseLeftClickOn('main_panel_new', 'main_panel_new')
        mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
        mouseLeftClickOn("u03_node2", "u03_node2_region")
        mouseLeftClickOn("structure_node2")
    else:
        raise AssertionError("main_panel did not appear")


def open_drawing_from_hull(str):
    # @desc - open drawing str from hull
    mouseLeftClickOn('shipconstructor_tab_new')
    if appears("main_panel_new"):
        mouseLeftClickOn('main_panel_new', 'main_panel_new')
        mouseLeftClickOn('navigator_button_new', 'navigator_button_new')
        if appears("hull3",10):
            mouseLeftClickOn("hull3")
            mouseDoubleClickOn(str)
        if appears("u03_node2_region",10):
            mouseLeftClickOn("u03_node2", "u03_node2_region")
            mouseLeftClickOn("hull3")
            mouseDoubleClickOn(str)
        if appears("save_yes",10):
            mouseLeftClickOn("save_yes")
        if appears("save_yes_button",10):
            mouseLeftClickOn("save_yes_button")
    else:
        raise AssertionError("main_panel did not appear")


def mirror_line():
    # @desc - to mirror anything about center line
    if appears("sc_utilites",2):
        mouseDoubleClickOn("sc_utilites")
    if appears("sc_utilities_tools_button"):
        mouseLeftClickOn("sc_utilities_tools_button")
        if appears("sc_utilities_mirror_button"):
            mouseLeftClickOn("sc_utilities_mirror_button")
            sleep(1)
            mouseMoveTo("empty_space2")
        else:
            raise AssertionError("Mirror button didn't found")
    else:
        raise AssertionError("Tool button did not appear")


def check_conventions():
    # @desc - for checking naming conventions
    for i in range(0, 6):
        press("enter")
    press('right')
    sleep(1)
    mouseLeftClickOn("pulldown")
    sleep(2)
    if appears('naming_convention_list',5):
        print('Naming Convention List is appeared')
    press('esc')
    if appears('conventions_ok'):
        mouseLeftClickOn("conventions_ok")
        mouseLeftClickOn("conventions_close")
    else:
       print('OK Button of Conventions is not appeared')

def change_view(str):
    """
    @desc - change visual style
    :param - screenshot name of the visual style to be selected:
    """
    mouseDoubleClickOn("view_pane")
    if appears("palettes"):
        mouseLeftClickOn("palettes")
        if appears("visual_style_button", 30):
            mouseLeftClickOn("visual_style_button")
        elif appears("visual_style_button2", 30):
            mouseLeftClickOn("visual_style_button2")
        else:
            raise AssertionError("visual_style_button did not appear")
        if appears(str):
            mouseDoubleClickOn(str)
            if appears("visual_close_unhigh", 5):
                mouseLeftClickOn("visual_close_unhigh")
            if appears("visual_close", 5):
                mouseLeftClickOn("visual_close")
            mouseMoveTo("empty_space2")
        else:
            raise AssertionError("conceptual_visual did not appear")
    else:
        raise AssertionError("palettes did not appear")


def change_viewpoint_angle():
    # @desc - Change 3d Viewpoint to FROM FWD STBD DWN
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('1')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('-')
        press('1')
        press('enter')
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_aft_prt_dwn():
    # @desc - Change 3d Viewpoint to FROM AFT PRT DWN
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button_18")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('1')
        press('tab')
        press('-')
        press('1')
        press('enter')
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_aft_prt_up():
    # @desc - Change 3d Viewpoint to FROM AFT PRT UP
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button_18")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('1')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_stbd_to_prt():
    # @desc - Change 3d Viewpoint to FROM STBD TO PRT
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button_18")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('0')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('0')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_aft_stbd_up():
    # @desc - Change 3d Viewpoint to FROM AFT STBD UP
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button_18")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_aft_up():
    # @desc - Change 3d Viewpoint to FROM AFT UP
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button_18")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('0')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_fwd_stbd_up():
    # @desc - Change 3d Viewpoint to FROM FWD STBD UP
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('1')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_body_looking_fwd():
    # @desc - Change 3d Viewpoint to FROM BODY LOOKING FWD
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    mouseLeftClickOn("3d_viewpoint_button2")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('0')
        press('tab')
        press('0')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_fwd_prt_up():
    # @desc - Change 3d Viewpoint to FROM DWD PRT UP
    free()
    typewrite("SCVPOINT",.2)
    press('enter')
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('1')
        press('tab')
        press('1')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_from_stbd_up():
    # @desc - Change 3d Viewpoint to AFT STBD UP
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('0')
        press('tab')
        press('-')
        press('1')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def change_viewpoint_plan_looking_down():
    # @desc - Change 3d Viewpoint to PLANAR LOOKING DOWN
    if appears("main_panel_new"):
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    else:
        mouseDoubleClickOn("structure_ribbon_button")
        mouseLeftClickOn("main_panel_new", "main_panel_new")
    mouseLeftClickOn("3d_viewpoint_button")
    if appears("3d_viewpoint_window"):
        press('tab')
        press('tab')
        press('0')
        press('tab')
        press('0')
        press('tab')
        press('1')
        press('enter')
        free()
    else:
        raise AssertionError("3d_viewpoint_window didn't appear")


def set_layer(str):
    """
    @desc - Change layer
    :param - screenshot name of the layer to be selected:
    """
    if appears("home_button"):
        mouseDoubleClickOn("home_button")
        if appears("layer_button"):
            mouseLeftClickOn("layer_button")
            mouseLeftClickOn("layer_properties_button")
            free()
            if appears(str):
                mouseRightClickOn(str)
                mouseLeftClickOn("set_layer")
                if appears("layer_properties_close_unhigh"):
                    mouseLeftClickOn("layer_properties_close_unhigh")
                else:
                    mouseLeftClickOn("layer_properties_manager_close")
            else:
                raise AssertionError("Preffered layed did not found")
        else:
            raise AssertionError("layer_button did not appear")
    else:
        raise AssertionError("home_button did not appear")


def zoom_to_ssi_u03_p01():
    # @desc - For zooming SSI_U03_P01 part
    typewrite('select', interval=.2)
    press('enter')
    typewrite('-306.5433,1332.1759', interval=0.1)
    press('enter')
    click(button='right', clicks=2, interval=0.25)
    mouseLeftClickOn("part_information_button")
    sleep(1)
    mouseRightClickOn('piecemark')
    mouseLeftClickOn("zoom_to_part")
    sleep(1)
    press('esc')


def zoom_to_ssi_u03_p07():
    # @desc - For zooming SSI_U03_P07 part
    typewrite('select', interval=.2)
    press('enter')
    typewrite('-3325.2534,1391.5924', interval=0.2)
    press('enter')
    click(button='right', clicks=2, interval=0.25)
    mouseLeftClickOn("part_information_button")
    mouseRightClickOn('piecemark')
    mouseLeftClickOn("zoom_to_part")
    sleep(1)
    press('esc')


def free():
    # @desc - to move the cursor to the drawing
    mouseMoveTo("empty_space2")
    sleep(2)


def free_white():
    # @desc - to move cursor to the white space
    mouseMoveTo("empty_space_white")
    sleep(2)


def line(x1, y1, x2, y2):
    """
    @desc - To draw lines
    :param - Co-ordinates
    """
    sleep(1)
    typewrite('line', interval=.2)
    sleep(1)
    press('enter')
    sleep(1)
    typewrite('#',interval=.2)
    typewrite(x1,interval=.2)
    typewrite(',', interval=.2)
    typewrite(y1,interval=.2)
    press('enter')
    sleep(2)
    typewrite('#',interval=.2)
    typewrite(x2,interval=.2)
    typewrite(',', interval=.2)
    typewrite(y2,interval=.2)
    sleep(1)
    press('enter')
    sleep(1)
    press('enter')
    sleep(1)


def rectang(x1, y1, x2, y2):
    """
    @desc - To draw rectangles
    :param - Co-ordinates
    """
    typewrite('RECTANG', interval=.2)
    press('enter')
    typewrite('#',interval=.1)
    typewrite(x1,interval=.1)
    typewrite(',', interval=.1)
    typewrite(y1,interval=.1)
    press('enter')
    typewrite('#',interval=.1)
    typewrite(x2,interval=.1)
    typewrite(',', interval=.1)
    typewrite(y2,interval=.1)
    press('enter')
    press('enter')
    press('esc')


def hvac_sysman():
    # @desc - To open HVAC system manager
    if appears("hvac_ribbon",2):
        mouseDoubleClickOn("hvac_ribbon")
    if appears("hvac_pane"):
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("hvac_system_manager")
        sleep(2)
        press('enter')
        if appears("system_manager_window"):
            print("System manager opened")
        else:
            raise AssertionError("system_manager_window did not appear")
    else:
        raise AssertionError("hvac_pane did not appear")


def straight():
    # @desc - To select HVAC straight
    if appears("hvac_ribbon",2):
        mouseDoubleClickOn("hvac_ribbon")
    if appears("hvac_pane"):
        mouseLeftClickOn("hvac_pane")
        mouseLeftClickOn("straight")
        if appears("select_stock"):
            hotkey('win','up')
            print("select_stock opened")
        else:
            raise AssertionError("select_stock did not appear")
    else:
        raise AssertionError("hvac_pane did not appear")


def otf():
    # @desc - To turn the OTF mode on/off
    mouseLeftClickOn("hvac_util")
    if appears("otf",10):
        mouseLeftClickOn("otf")
        free()
    else:
        raise AssertionError("OTF option did not appear")

def reset_viewport():
    # @desc - To reset the viewport.
    sleep(2)
    press('esc')
    press('esc')
    press('esc')
    typewrite('SCVPOINT',.2)
    press('enter')
    if appears("3d_viewpoint_window"):
        press('enter')
    free()
    sleep(1)


def pipe_sysman():
    # @desc - To open PIPE system manager
    if appears("pipe_ribbon",2):
        mouseDoubleClickOn("pipe_ribbon")
    if appears("pipe_pane",2):
        mouseLeftClickOn("pipe_pane")
        mouseLeftClickOn("hvac_system_manager")
        sleep(1)
        press('enter')

        if appears("system_manager_window"):
            hotkey('win','up')
            print("System manager opened")
        else:
            raise AssertionError("system_manager_window did not appear")
    else:
        raise AssertionError("pipe_pane did not appear")


def visual_style_change(screenobject, screenobject2 = None, screenobjectRegion = None):
    """
    @desc - Change visual style via CMD
    :param - name of the screenshot of the visual style
    """
    typewrite("VISUALSTYLES",.1)
    press('enter')
    if appears(screenobject, 20):
        mouseDoubleClickOn(screenobject)
        click()
        sleep(.5)
        mouseLeftClickOn("visual_close")
        free()
    elif appears(screenobject, 20):
        mouseDoubleClickOn(screenobject2, screenobjectRegion)
        click()
        sleep(.5)
        mouseLeftClickOn("visual_close")
        free()
    else:
        raise AssertionError(screenobject+" did not appear")


def maximize():
    # @desc - Maximize the application
    moveTo(500, 500)
    moveTo(500, 500)
    sleep(2)
    moveTo(500, 500)
    moveTo(500, 500)

    if appears("shipcon_win_region",10):
        mouseLeftClickOn("shipcon_win_restore", "shipcon_win_region")
        sleep(3)
        moveTo(500,500)
        mouseLeftClickOn("shipcon_win_maximize", "shipcon_win_region2")
        sleep(1)
    elif appears("shipcon_win_region2",10):
        moveTo(500, 500)
        mouseLeftClickOn("shipcon_win_maximize", "shipcon_win_region2")
        sleep(1)


def view_alter():
    # @desc - Alter the visual style
    press('esc', presses=3)
    free()
    typewrite("VSCURRENT", .1)
    press('enter')
    press('R')
    press('enter')
    sleep(2)
    typewrite("VSCURRENT", .1)
    press('enter')
    press('2')
    press('enter')
    sleep(1)


def conceptual_visual():
    # @desc - Change the Visual style to Conceptual
    press('esc',presses=3)
    free()
    typewrite("VSCURRENT", .2)
    press('enter')
    sleep(1)
    press('C')
    sleep(1)
    press('enter')
    sleep(1)

def off_coordinates():
    # Switching off coordinates option of autoCAD
    if appears("autocad_menu",10):
        mouseLeftClickOn("autocad_menu")
        if appears("coordinates_selected",5):
            mouseLeftClickOn("coordinates_selected")
            sleep(2)
            moveTo(500, 500)
            sleep(2)
            click()
            press('esc', presses=3)
        else:
            sleep(2)
            moveTo(500,500)
            sleep(2)
            click()
            press('esc',presses=3)
    else:
        raise AssertionError("autocad_menu did not appear")

def maximize_layer_name_column():
    # maximizing "layer name" tab in layer manager
    if appears("layer_name",5):
        mouseRightClickOn("layer_name")
    else:
        raise AssertionError("layer_name did not appear")
    if appears("maximize_column", 10):
        mouseLeftClickOn("maximize_column")


def wireframe2d_visual():
    # @desc - Change the Visual style to 2dWireframe
    press('esc', presses=3)
    free()
    typewrite("VSCURRENT", .2)
    press('enter')
    sleep(1)
    press('2')
    sleep(1)
    press('enter')
    sleep(1)


def snap_override_endpoint():
    # @desc - To select endpoint from snap override
    click(button='right')
    mouseLeftClickOn("snap_override")
    mouseLeftClickOn("end_point")


def hidden_visual():
    # @desc - Change the Visual style to 2dWireframe
    press('esc', presses=3)
    free()
    typewrite("VSCURRENT", .1)
    press('enter')
    press('h')
    press('enter')


def open_manager():
    # @desc - For Opening SC Manager
    if appears("shipconstructor_tab_new",5):
        mouseLeftClickOn('shipconstructor_tab_new')
    mouseLeftClickOn("manage_pane")
    mouseLeftClickOn('manage_button')
    sleep(5)
    hotkey('win','up')
    if appears("shipcon_manager"):
        print("SC Manager was opened.")
    else:
        raise AssertionError("Manager could not be opened")


def open_navigator():
    # @desc - To open SC Navigator
    if appears("shipconstructor_tab_new",5):
        mouseLeftClickOn('shipconstructor_tab_new')
    if appears("main_panel_new",10):
        mouseLeftClickOn('main_panel_new')
        mouseLeftClickOn('navigator_button_new')
    else:
        mouseLeftClickOn('shipconstructor_tab_new')
        mouseLeftClickOn('main_panel_new')
        mouseLeftClickOn('navigator_button_new')
    if appears("shipcon_navi"):
        print("Navigator window properly opened")
    else:
        raise AssertionError("Navigator window was not properly opened")


def open_product_hierarchy():
    # @desc - To open product hierarchy window
    if appears("main_panel_new", 5):
        mouseLeftClickOn("main_panel_new")
    else:
        mouseLeftClickOn('shipconstructor_tab_new')
        mouseLeftClickOn('main_panel_new')
    if appears("product_hierarchy_button"):
        mouseLeftClickOn("product_hierarchy_button")
        if appears("product_hierarchy_window"):
            print("Product Hierarchy window appeared")
        else:
            raise AssertionError("Product Hierarchy window did not appear")
    else:
        raise AssertionError("product_hierarchy_button did not appear")


def open_layer_properties():
    # @desc - To open layer properties
    if appears("home_button",5):
        mouseLeftClickOn("home_button")
    mouseLeftClickOn("layer_button")
    mouseLeftClickOn("layer_properties_button")
    free()


def activate_wcs():
    # @desc - to activate World Coordinate System
    if appears("sc_utilites",5):
        mouseDoubleClickOn("sc_utilites")
    mouseLeftClickOn("visual_button")
    mouseLeftClickOn("activate_ucs")
    if appears("activate_ucs_menu"):
        mouseLeftClickOn("world_coordinate")
        mouseLeftClickOn("world_coordinate_activate")
    else:
        raise AssertionError("activate_ucs_menu did not appear")


def open_mgi_window():
    # @desc - to open mark group intersection window
    if appears("structure_ribbon_button",5):
        mouseLeftClickOn("structure_ribbon_button")
    mouseLeftClickOn("utilities_pane")
    mouseLeftClickOn("mark_group_intersection_button")
    free()
    if appears("mark_group_intersection_dialog"):
        print("MGI Dialog appeared")
    else:
        raise AssertionError("MGI Dialog did not appear")

def click_dropdown_on_hover(selected_option, to_be_selected_option):
    mouseMoveTo(selected_option)
    mouseLeftClickOn("hovered_drop_down")
    mouseLeftClickOn(to_be_selected_option)

def enable_3d_tool():
    if appears("3d_tool_panel",10):
        print("3d tool ribbon is already selected")
    else:
        if appears("pane_for_right_click",10):
            mouseRightClickOn("pane_for_right_click")
        else:
            raise AssertionError("pane_for_right_click did not appear")
        if appears("show_tabs",10):
            mouseLeftClickOn("show_tabs")
            if appears("3d_tool_selected",10):
                print("3D tool option is selected")
                free()
                press('esc',presses=4)
            elif appears("3d_tool",10):
                mouseLeftClickOn("3d_tool")
                free()
            else:
                raise AssertionError("3d_tool option did not appear")
        else:
            raise AssertionError("show_tabs did not appear")

def reload_drawing():
    free()
    press('esc',presses=3)
    typewrite("SCRELOAD",.2)
    press('enter')
    if appears("reload_yes"):
        mouseLeftClickOn("reload_yes")

def createTempExercise():
    free()
    typewrite('SCNAVIGATE', .2)
    press('enter')
    if appears("shipcon_navi"):
        mouseLeftClickOn("new_pipe")
    else:
        raise AssertionError("Navigator did not appear")
    if appears("new_pipe_window"):
        typewrite('Tempexercise', .1)
        mouseLeftClickOn("open_new_drawing2._check", "open_new_drawing2")
        press('enter')
        sleep(5)
    if appears("tempexercise_tab"):
        free()
        change_viewpoint_plan_looking_down()
        free()
        hotkey('ctrl','s')
    else:
        raise AssertionError("tempexercise_tab tab did not appear")

def copyToExercise(basePoint, exerciseDWG = "tempexercise", exerciseTab= "tempexercise_tab"):
    free()
    hotkey('ctrl','s')
    sleep(2)
    hotkey('ctrl', 'a')
    press('enter')
    sleep(1)
    typewrite('COPYBASE',.2)
    press('enter')
    if appears("specify_base_point"):
        typewrite(basePoint,.2)
        press('enter')
        sleep(1)
        press('esc')
        free()
        hotkey('ctrl','s')
        sleep(4)
    else:
        raise AssertionError("specify_base_point did not appear")
    typewrite('SCNAVIGATE',.2)
    press('enter')
    if appears(exerciseDWG):
        mouseDoubleClickOn(exerciseDWG)
        sleep(5)
    else:
        raise AssertionError(exerciseDWG+" did not appear")
    if appears(exerciseTab):
        free()
        sleep(10)
        typewrite('PASTECLIP',.2)
        press('enter')
        if appears("specify_insertion_point", 10):
            typewrite(basePoint,.2)
            press('enter')
            sleep(5)
            reset_viewport()
            hotkey('ctrl','s')
            sleep(5)
        else:
            raise AssertionError("specify_insertion_point did not appear")
    else:
        raise AssertionError("Exercise tab did not appear")

def tempDelete(exerciseDWG = "tempexercise"):
    typewrite('SCNAVIGATE', .2)
    press('enter')
    if appears(exerciseDWG):
        mouseRightClickOn(exerciseDWG)
        if appears("delete_drawing",10):
            mouseLeftClickOn("delete_drawing")
            sleep(4)
            press("enter")
            sleep(5)
            sleep(5)
            sleep(5)
            press("esc")
            press("esc")
            press("esc")

def zoom(x1 , y1, x2, y2):
    press('esc',presses=4)
    free()
    typewrite('zoom', .2)
    press('enter')
    typewrite(x1,.2)
    typewrite(',',.2)
    typewrite(y1,.2)
    press('enter')
    typewrite(x2,.2)
    typewrite(',',.2)
    typewrite(y2,.2)
    press('enter')
    press('enter')
    sleep(5)
    free()
    press('esc',presses=3)
