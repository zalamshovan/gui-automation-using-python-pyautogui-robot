from pyautogui import locateOnScreen, center
from time import sleep
from Initialize import Initialize

def location(screenObj):
    # returns the location of particular screen object
    sleep(.5)  # To allow the screen displaying properly
    try:
        obj = Initialize()
        path = obj.get_image_directory()

        if path is None:
            path = obj.read_path_from_module_file()

        str_path_screen_obj = path + screenObj + ".png"
        loc_screen_obj = locateOnScreen(str_path_screen_obj, grayscale=True)
    except TypeError:
        raise AssertionError("Item %s could not be located. Type mismatched" % screenObj)
    except IOError:
        raise AssertionError("No such file named %s.png presents in the test_image directory" % screenObj)

    if loc_screen_obj is None:
        # Logger.add_missed_image(screenObj)
        return None
    else:
        # Logger.info(screenObj + " appeared.")
        return loc_screen_obj  # return the location of the object on the current screen

def midpoint(screenObj):
    # returns midpoint of the screen object passed via the parameter
    return center(location(screenObj))

def appears(screenObj, timeWait=120):
    # Check the screenObj appears or not
    for x in range(0, timeWait):
        if location(screenObj) != None:
            sleep(2)
            # info(screenObj+" was appeared")
            return True
    # if screenObj could not located in time, return False
    return False

def exists(screenObj):
    # Check if a screenObj is on screen or not
    if location(screenObj) == None:
        return False
    else:
        return True

def waitForAppear(screenObj):
    while 1:
        if appears(screenObj):
            break

def waitForVanish(screenObj):
    # Wait till the screenObj is not vanished
    i = 0;
    if location(screenObj) == None:
        print(screenObj+" did not appear")
        return False
    else:
        while(exists(screenObj)):
            sleep(1)
            i = i +1
        print(screenObj + " was on screen for "+str(i*2)+" seconds")
        return True

def waitToAppear(screenObj):
    i = 0

    while 1:
        if location(screenObj) == None:
            sleep(1)
            i = i + 1
            if i > 300:
                print(screenObj + " did not appear after " + str(i*3)+ " seconds")
                return False
        else:
            break

    if location(screenObj) == None:
        print(screenObj+" did not appear")
        return False
    else:
        print(screenObj+" appeared after "+str(i*3)+" seconds")
        return True