# GUI Automation Using Python-PyAutoGUI-Robot

This is an automation framework designed for desktop applications. Using Python as the programming language, PyAutoGUI to detect images and perform I/O tasks, and finally Robot-Framework as the Test Case Management tool